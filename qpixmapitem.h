/******************************************************************************
 * Project    : Smart Grid Table GUI
 * Author     : Dingzhu Chen
 *
 * Description: header file for the base class of all model icon classes
******************************************************************************/
#ifndef QPIXMAPITEM_H
#define QPIXMAPITEM_H
#include <QGraphicsItem>
#include <QRectF>
#include <QPixmap>
#include <QPainter>
#include <QGraphicsScene>

class QPixmapItem:public QGraphicsItem
{
public:
    QPixmapItem(const QString &fileName,QGraphicsScene *scene);
    virtual QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void RemoveQitem(QGraphicsScene *scene);
private:
    QPixmap pixmap;
};

#endif // QPIXMAPITEM_H
