/******************************************************************************
 * Project    : Smart Grid Table GUI
 * Author     : Dingzhu Chen
 *
 * Description: header file for global variables
******************************************************************************/
#ifndef GLOBAL_ITEMS_H
#define GLOBAL_ITEMS_H
#include <QByteArray>

#include <qglobal.h>

#include <QString>
#include <QDebug>
#include <QList>
#include <QString>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonValue>
#include <iostream>
#include <QJsonObject>
#include <QtCharts>

class Global_Items
{
public:

    QMap< qint64, QList<QPointF> > Dy_Unknown_MAP;
    QMap< qint64, QList<QPointF> > Dy_Gen_Model;
    QMap< qint64, QList<QPointF> > Dy_Load_Model;
    QMap< QString, qint64 > Lable_to_RFID;

    QSplineSeries *Dynamic_Gen_Line;
    QSplineSeries *Dynamic_Load_Line;

    QSplineSeries *Coal_Gen_Line;
    QSplineSeries *Wind_Solar_Line;
    QSplineSeries *In_Export_Line;
    QSplineSeries *Gas_Gen_Line;
    QSplineSeries *Volcano_Line;
    QSplineSeries *Nuclear_Line;
    QSplineSeries *Solar_Line;

    QSplineSeries *Proc_Fac_Line;
    QSplineSeries *Car_Char1_Line;
    QSplineSeries *Chemi_Fac_Line;
    QSplineSeries *Hydro_Sta_Line;
    QSplineSeries *HAN_Line;
    QSplineSeries *Car_Char2_Line;
    QSplineSeries *T_House1_Line;
    QSplineSeries *T_House2_Line;
    QSplineSeries *T_House3_Line;
    QSplineSeries *T_House4_Line;
    QSplineSeries *T_House5_Line;
    QSplineSeries *T_House6_Line;
    QSplineSeries *House1_Line;
    QSplineSeries *House2_Line;
    QSplineSeries *House3_Line;
    QSplineSeries *House4_Line;
    QSplineSeries *House5_Line;
    QSplineSeries *Stor_House1_Line;
    QSplineSeries *Stor_House2_Line;
    QSplineSeries *Appartment1_Line;
    QSplineSeries *Appartment2_Line;
    QSplineSeries *Appartment3_Line;
    QSplineSeries *Appartment4_Line;
    QSplineSeries *Appartment5_Line;
    QSplineSeries *Appartment6_Line;
    QSplineSeries *Shop_Mall_Line;
    QSplineSeries *Hospital_Line;
    QSplineSeries *line_3;
    QChart *chart;
    QChartView *chartView;
    QLegendMarker *marker;

    QList<QPointF> Unknown_Model_List;
    QList<QPointF> Unknown_Gen_List;
    QList<QPointF> Unknown_Load_List;

    QList<QPointF> Dynamic_Gen_List;
    QList<QPointF> Dynamic_Load_List;

    QList<QPointF> Coal_Gen_List;
    QList<QPointF> Wind_Solar_List;
    QList<QPointF> In_Export_List;
    QList<QPointF> Gas_Gen_List;
    QList<QPointF> Volcano_List;
    QList<QPointF> Nuclear_List;
    QList<QPointF> Solar_List;

    QList<QPointF> Proc_Fac_List;
    QList<QPointF> Car_Char1_List;
    QList<QPointF> Chemi_Fac_List;
    QList<QPointF> Hydro_Sta_List;
    QList<QPointF> HAN_List;
    QList<QPointF> Car_Char2_List;
    QList<QPointF> T_House1_List;
    QList<QPointF> T_House2_List;
    QList<QPointF> T_House3_List;
    QList<QPointF> T_House4_List;
    QList<QPointF> T_House5_List;
    QList<QPointF> T_House6_List;
    QList<QPointF> House1_List;
    QList<QPointF> House2_List;
    QList<QPointF> House3_List;
    QList<QPointF> House4_List;
    QList<QPointF> House5_List;
    QList<QPointF> Stor_House1_List;
    QList<QPointF> Stor_House2_List;
    QList<QPointF> Appartment1_List;
    QList<QPointF> Appartment2_List;
    QList<QPointF> Appartment3_List;
    QList<QPointF> Appartment4_List;
    QList<QPointF> Appartment5_List;
    QList<QPointF> Appartment6_List;
    QList<QPointF> Shop_Mall_List;;
    QList<QPointF> Hospital_List;
    QString TABLE_Num;
    QString RFID_Pos;

int Table_No;
int RFID_No;
int if_new_received=0;
long long int RFID_Tag_No;

long long int Dynamic_RFID_No;
long long int Dynamic_RFID_No_1;
long long int Dynamic_RFID_No_2;
long long int Dynamic_RFID_No_3;
long long int Dynamic_RFID_No_4;
long long int Dynamic_RFID_No_5;
long long int Dynamic_RFID_No_6;
long long int Dynamic_RFID_No_7;
long long int Dynamic_RFID_No_8;
long long int Dynamic_RFID_No_9;
long long int Dynamic_RFID_No_10;
long long int Dynamic_RFID_No_11;
long long int Dynamic_RFID_No_12;
long long int Dynamic_RFID_No_13;
long long int Dynamic_RFID_No_14;
long long int Dynamic_RFID_No_15;
long long int Dynamic_RFID_No_16;
long long int Dynamic_RFID_No_17;
long long int Dynamic_RFID_No_18;
long long int Dynamic_RFID_No_19;
long long int Dynamic_RFID_No_20;
long long int Dynamic_RFID_No_21;
long long int Dynamic_RFID_No_22;
long long int Dynamic_RFID_No_23;
long long int Dynamic_RFID_No_24;
long long int Dynamic_RFID_No_25;
long long int Dynamic_RFID_No_26;
long long int Dynamic_RFID_No_27;
long long int Dynamic_RFID_No_28;
long long int Dynamic_RFID_No_29;

    Global_Items();
    QString ttt;
    double T1_R5_VALUE=NULL;
    double T1_R1_VALUE=NULL;
    double T3_R0_VALUE=NULL;
    double T3_R1_VALUE=NULL;
    double T3_R2_VALUE=NULL;
    double T3_R3_VALUE=NULL;
    double T3_R4_VALUE=NULL;
    double T3_R5_VALUE=NULL;
    double T3_R6_VALUE=NULL;
    double T3_R7_VALUE=NULL;
    double T6_R0_VALUE=NULL;
    double T6_R1_VALUE=NULL;
    double T6_R2_VALUE=NULL;
    double T6_R3_VALUE=NULL;
    double T6_R4_VALUE=NULL;
    double T6_R5_VALUE=NULL;
    double T6_R6_VALUE=NULL;
    double T6_R7_VALUE=NULL;
    double T5_R0_VALUE=NULL;
    double T5_R1_VALUE=NULL;
    double T5_R2_VALUE=NULL;
    double T5_R3_VALUE=NULL;
    double T5_R4_VALUE=NULL;
    double T5_R5_VALUE=NULL;
    double T5_R6_VALUE=NULL;
    double T5_R7_VALUE=NULL;
    double T4_R0_VALUE=NULL;
    double T4_R1_VALUE=NULL;
    double T4_R2_VALUE=NULL;
    double T4_R3_VALUE=NULL;
    double T4_R4_VALUE=NULL;
    double T4_R5_VALUE=NULL;
    double T4_R6_VALUE=NULL;
    double T4_R7_VALUE=NULL;

    double Max_L_Sum;
    double Max_G_Sum;
    double Max_1G;
    double Max_2G;
    double Max_3G;
    double Max_4G;
    double Max_1L;
    double Max_2L;
    double Max_3L;
    double Max_4L;
    double Max_5L;
    double Max_6L;
    double Max_7L;
    double Max_8L;
    double Max_9L;
    double Max_10L;
    double Max_11L;

    QString T1_R5_RFID=nullptr;
    QString T1_R1_RFID=nullptr;
    QString T3_R1_RFID=nullptr;
    QString T3_R0_RFID=nullptr;
    QString T3_R2_RFID=nullptr;
    QString T3_R3_RFID=nullptr;
    QString T3_R4_RFID=nullptr;
    QString T3_R5_RFID=nullptr;
    QString T3_R6_RFID=nullptr;
    QString T3_R7_RFID=nullptr;

    QString T6_R1_RFID=nullptr;
    QString T6_R0_RFID=nullptr;
    QString T6_R2_RFID=nullptr;
    QString T6_R3_RFID=nullptr;
    QString T6_R4_RFID=nullptr;
    QString T6_R5_RFID=nullptr;
    QString T6_R6_RFID=nullptr;
    QString T6_R7_RFID=nullptr;
    QString T5_R1_RFID=nullptr;
    QString T5_R0_RFID=nullptr;
    QString T5_R2_RFID=nullptr;
    QString T5_R3_RFID=nullptr;
    QString T5_R4_RFID=nullptr;
    QString T5_R5_RFID=nullptr;
    QString T5_R6_RFID=nullptr;
    QString T5_R7_RFID=nullptr;
    QString T4_R1_RFID=nullptr;
    QString T4_R0_RFID=nullptr;
    QString T4_R2_RFID=nullptr;
    QString T4_R3_RFID=nullptr;
    QString T4_R4_RFID=nullptr;
    QString T4_R5_RFID=nullptr;
    QString T4_R6_RFID=nullptr;
    QString T4_R7_RFID=nullptr;

    bool show_all_gens=false;
    bool show_all_loads=false;
    //2
    bool T1_R5;
    bool T1_R1;
//29
    bool T3_R0;
    bool T3_R1;
    bool T3_R2;
    bool T3_R3;
    bool T3_R4;
    bool T3_R5;
    bool T3_R6;
    bool T3_R7;
    bool T4_R0;
    bool T4_R1;
    bool T4_R2;
    bool T4_R3;
    bool T4_R4;
    bool T4_R5;
    bool T4_R6;
    bool T4_R7;
    bool T5_R0;
    bool T5_R1;
    bool T5_R2;
    bool T5_R3;
    bool T5_R4;
    bool T5_R5;
    bool T5_R6;
    bool T5_R7;
    bool T6_R0;
    bool T6_R1;
    bool T6_R2;
    bool T6_R3;
    bool T6_R4;
    bool T6_R5;
    bool T6_R6;
    bool T6_R7;

    bool Dynamic_Gen_Received;
    bool Dynamic_Load_Received;

    bool Coal_Gen_List_Received;
    bool Wind_Solar_List_Received;
    bool In_Export_List_Received;
    bool Gas_Gen_List_Received;
    bool Volcano_List_Received;
    bool Nuclear_List_Received;
    bool Solar_List_Received;

    bool Proc_Fac_List_Received;
    bool Car_Char1_List_Received;
    bool Chemi_Fac_List_Received;
    bool Hydro_Sta_List_Received;
    bool HAN_List_Received;
    bool Car_Char2_List_Received;
    bool T_House1_List_Received;
    bool T_House2_List_Received;
    bool T_House3_List_Received;
    bool T_House4_List_Received;
    bool T_House5_List_Received;
    bool T_House6_List_Received;
    bool House1_List_Received;
    bool House2_List_Received;
    bool House3_List_Received;
    bool House4_List_Received;
    bool House5_List_Received;
    bool Stor_House1_List_Received;
    bool Stor_House2_List_Received;
    bool Appartment1_List_Received;
    bool Appartment2_List_Received;
    bool Appartment3_List_Received;
    bool Appartment4_List_Received;
    bool Appartment5_List_Received;
    bool Appartment6_List_Received;
    bool Shop_Mall_List_Received;;
    bool Hospital_List_Received;
    QString MQTT_RFID_VALUE;

//7loads
    QString Coal_PW_1071771887=nullptr;
    QString In_Ex_PW_1842809804=nullptr;
    QString Gas_PW_1071966141=nullptr;
    QString Nuke_PW_1857348684=nullptr;
    QString Volcano_PW_2041806607=nullptr;
    QString Solar_farm_PW_1071766506=nullptr;
    QString Wind_solar_PW_1071741103=nullptr;
//27 loads
    QString Process_fac_LD_1072066624=nullptr;
    QString Car_char1_LD_1071780135=nullptr;
    QString Chem_fac_LD_1072051546=nullptr;
    QString Hydro_sta_LD_1071991074=nullptr;
    QString HAN_LD_1071729488=nullptr;
    QString Car_char2_LD_1071769060=nullptr;
    QString Ter_house1_LD_1071985354=nullptr;
    QString Ter_house2_LD_1072063229=nullptr;
    QString Ter_house3_LD_1072056604=nullptr;
    QString Ter_house4_LD_699168295=nullptr;
    QString Ter_house5_LD_2305939010=nullptr;
    QString Ter_house6_LD_2836102489=nullptr;
    QString House1_LD_1071986763=nullptr;
    QString House2_LD_1072063101=nullptr;
    QString House3_LD_2298616020=nullptr;
    QString House4_LD_1071733508=nullptr;
    QString House5_LD_3914547211=nullptr;
    QString Stor_house1_LD_1071773816=nullptr;
    QString Stor_house2_LD_1071780377=nullptr;
    QString Appartment1_LD_1071770985=nullptr;
    QString Appartment2_LD_7304921=nullptr;
    QString Appartment3_LD_593072732=nullptr;
    QString Appartment4_LD_1071753108=nullptr;
    QString Appartment5_LD_2836099420=nullptr;
    QString Appartment6_LD_1840834860=nullptr;
    QString Shop_mall_LD_597999943=nullptr;
    QString Hospital1_LD_8538681=nullptr;

    QMap< QString, std::double_t > MQTT_JSON_MAP_GEN;
    QMap< QString, std::double_t > MQTT_JSON_MAP_LOAD;

    double ooo;
    int aa;
    bool p_nom_max=false;
    bool T3R0_p_nom_max=false;
    bool T3R1_p_nom_max=false;
    bool T3R2_p_nom_max=false;
    bool T3R3_p_nom_max=false;
    bool T3R4_p_nom_max=false;
    bool T3R5_p_nom_max=false;
    bool T3R6_p_nom_max=false;
    bool T3R7_p_nom_max=false;

    bool x=false;


    static Global_Items& Instance()
    {
        static Global_Items sta_json_obj;
        return sta_json_obj;
    }


};//GLOBAL_ITEM;

namespace {
//doubleToString保留两位小数
    std::string doubleToString(const double &dbNum)
    {
        char *chCode;
        chCode = new(std::nothrow)char[20];
        sprintf(chCode, "%.3lf", dbNum);  // .2 是控制输出精度的，两位小数
        std::string strCode(chCode);
        delete []chCode;
        return strCode;
    }
    Global_Items& global_instance = Global_Items::Instance();

    QString readonly_string(QString file_path)
    {
        QFile this_file_ptr(file_path);
        if(false == this_file_ptr.exists())
        {
            return "None";
        }
        if(false == this_file_ptr.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            return "None";
        }
        QString string_value = this_file_ptr.readAll();
        this_file_ptr.close();
        return string_value;
    }
}

#endif // GLOBAL_ITEMS_H
