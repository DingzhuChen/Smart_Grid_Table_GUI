/******************************************************************************
 * Project    : Smart Grid Table GUI
 * Author     : Dingzhu Chen
 *
 * Description: function implementations for all model icon classes of all RFID
                position on all table
******************************************************************************/
#include "Global_Models.h"

//T1_R5_Model=========================================================================================================
T1_R5_Model::T1_R5_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
    if(global_instance.TABLE_Num=="Table1"&&global_instance.RFID_Pos=="RFID 1")
    {setPos(537,0);
    }

    if(global_instance.TABLE_Num=="Table1"&&global_instance.RFID_Pos=="RFID 5")
    {setPos(100,0);}

}
void T1_R5_Model::advance(int phase){
    setPos(100,103);

}
void T1_R5_Model::clear(){
    this->clear();
}


//T1_R1_Model=========================================================================================================

T1_R1_Model::T1_R1_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
    if(global_instance.TABLE_Num=="Table1"&&global_instance.RFID_Pos=="RFID 1")
    {setPos(537,0);
    }

    if(global_instance.TABLE_Num=="Table1"&&global_instance.RFID_Pos=="RFID 5")
    {setPos(100,0);}

}
void T1_R1_Model::advance(int phase){

    setPos(537,103);

}
void T1_R1_Model::clear(){
    this->clear();

}

//T3_R0_MODULE==============================================================================================
T3_R0_Model::T3_R0_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
    setPos(576,0);
}
void T3_R0_Model::advance(int phase){

    setPos(576,183);//T3_R0
}
void T3_R0_Model::clear(){
    this->clear();
}
//T3_R1_MODULE==============================================================================================
T3_R1_Model::T3_R1_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
    setPos(576,0);
}
void T3_R1_Model::advance(int phase){

    setPos(576,268);//T3_R1
}
void T3_R1_Model::clear(){
    this->clear();
}
//T3_R2_MODULE==============================================================================================
T3_R2_Model::T3_R2_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
    setPos(576,0);
}
void T3_R2_Model::advance(int phase){

    setPos(576,353);//T3_R2
}
void T3_R2_Model::clear(){
    this->clear();
}
//T3_R3_MODULE==============================================================================================
T3_R3_Model::T3_R3_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
    setPos(576,0);
}
void T3_R3_Model::advance(int phase){

    setPos(576,438);//T3_R3
}
void T3_R3_Model::clear(){
    this->clear();
}

//T3_R4_MODULE==============================================================================================
T3_R4_Model::T3_R4_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
    setPos(576,0);
}
void T3_R4_Model::advance(int phase){

    setPos(61,438);//T3_R4
}
void T3_R4_Model::clear(){
    this->clear();
}

//T3_R5_MODULE==============================================================================================
T3_R5_Model::T3_R5_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
    setPos(576,0);
}
void T3_R5_Model::advance(int phase){

    setPos(61,353);//T3_R5
}
void T3_R5_Model::clear(){
    this->clear();
}
//T3_R6_MODULE==============================================================================================
T3_R6_Model::T3_R6_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
    setPos(576,0);
}
void T3_R6_Model::advance(int phase){
    setPos(61,268);//T3_R6
}
void T3_R6_Model::clear(){
    this->clear();
}

//T3_R7_MODULE==============================================================================================
T3_R7_Model::T3_R7_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
    setPos(576,0);
}
void T3_R7_Model::advance(int phase){
    setPos(61,183);//T3_R7
}
void T3_R7_Model::clear(){
    this->clear();
}
//T6_R0_MODULE==============================================================================================


T6_R0_Model::T6_R0_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
    setPos(50,0);
}
void T6_R0_Model::advance(int phase){
    setPos(50,276);//T3_R7
}
void T6_R0_Model::clear(){
    this->clear();
}



T6_R7_Model::T6_R7_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
setPos(100,0);
}
void T6_R7_Model::advance(int phase){
    setPos(100,436);//T3_R7
}
void T6_R7_Model::clear(){
    this->clear();
}


T6_R6_Model::T6_R6_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
setPos(320,0);
}
void T6_R6_Model::advance(int phase){

    setPos(320,476);//T3_R7
}
void T6_R6_Model::clear(){
    this->clear();
}

T6_R5_Model::T6_R5_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T6_R5_Model::advance(int phase){

    setPos(536,436);//T3_R7
}
void T6_R5_Model::clear(){
    this->clear();
}


T6_R4_Model::T6_R4_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T6_R4_Model::advance(int phase){

    setPos(588,275);//T3_R7
}
void T6_R4_Model::clear(){
    this->clear();
}


T6_R3_Model::T6_R3_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T6_R3_Model::advance(int phase){

    setPos(536,114);//T3_R7
}
void T6_R3_Model::clear(){
    this->clear();
}



T6_R2_Model::T6_R2_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T6_R2_Model::advance(int phase){
    setPos(320,75);
}
void T6_R2_Model::clear(){
    this->clear();
}


T6_R1_Model::T6_R1_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T6_R1_Model::advance(int phase){

    setPos(100,114);//T3_R7
}
void T6_R1_Model::clear(){
    this->clear();
}



T5_R0_Model::T5_R0_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T5_R0_Model::advance(int phase){

    setPos(588,275);//T3_R7
}
void T5_R0_Model::clear(){
    this->clear();
}



T5_R1_Model::T5_R1_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T5_R1_Model::advance(int phase){
    setPos(536,436);//T3_R7
}
void T5_R1_Model::clear(){
    this->clear();
}



T5_R2_Model::T5_R2_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T5_R2_Model::advance(int phase){

    setPos(320,476);//T3_R7
}
void T5_R2_Model::clear(){
    this->clear();
}


T5_R3_Model::T5_R3_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T5_R3_Model::advance(int phase){

    setPos(100,436);//T3_R7
}
void T5_R3_Model::clear(){
    this->clear();
}



T5_R4_Model::T5_R4_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T5_R4_Model::advance(int phase){

    setPos(50,276);//T3_R7
}
void T5_R4_Model::clear(){
    this->clear();
}

T5_R5_Model::T5_R5_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T5_R5_Model::advance(int phase){
    setPos(100,114);//T3_R7
}
void T5_R5_Model::clear(){
    this->clear();
}

T5_R6_Model::T5_R6_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T5_R6_Model::advance(int phase){
    setPos(320,75);//T3_R7
}
void T5_R6_Model::clear(){
    this->clear();
}


T5_R7_Model::T5_R7_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T5_R7_Model::advance(int phase){
    setPos(536,114);//T3_R7
}
void T5_R7_Model::clear(){
    this->clear();
}

T4_R0_Model::T4_R0_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T4_R0_Model::advance(int phase){
    setPos(320,75);//T3_R7
}
void T4_R0_Model::clear(){
    this->clear();
}

T4_R1_Model::T4_R1_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T4_R1_Model::advance(int phase){
    setPos(536,114);//T3_R7
}
void T4_R1_Model::clear(){
    this->clear();
}

T4_R2_Model::T4_R2_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T4_R2_Model::advance(int phase){
    setPos(588,275);//T3_R7
}
void T4_R2_Model::clear(){
    this->clear();
}

T4_R3_Model::T4_R3_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T4_R3_Model::advance(int phase){
    setPos(536,436);//T3_R7
}
void T4_R3_Model::clear(){
    this->clear();
}

T4_R4_Model::T4_R4_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
    setPos(320,0);
}
void T4_R4_Model::advance(int phase){
    setPos(320,476);//T3_R7
}
void T4_R4_Model::clear(){
    this->clear();
}

T4_R5_Model::T4_R5_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T4_R5_Model::advance(int phase){
    setPos(100,436);//T3_R7
}
void T4_R5_Model::clear(){
    this->clear();
}

T4_R6_Model::T4_R6_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T4_R6_Model::advance(int phase){
    setPos(50,276);//T3_R7
}
void T4_R6_Model::clear(){
    this->clear();
}

T4_R7_Model::T4_R7_Model(const QString &fileName,QGraphicsScene *scene):QPixmapItem(fileName,scene)
{
}
void T4_R7_Model::advance(int phase){
    setPos(100,114);//T3_R7
}
void T4_R7_Model::clear(){
    this->clear();
}



