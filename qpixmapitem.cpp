/******************************************************************************
 * Project    : Smart Grid Table GUI
 * Author     : Dingzhu Chen
 *
 * Description: function implementations for the base class of all model icon classes
******************************************************************************/
#include "qpixmapitem.h"

QPixmapItem::QPixmapItem(const QString &fileName,QGraphicsScene *scene)
{
    pixmap.load(fileName);
    scene->addItem(this);
    //scene->removeItem(this);
}
QRectF QPixmapItem::boundingRect() const{
    return QRectF(-pixmap.width()/2,-pixmap.height(),pixmap.width(),pixmap.height());

}
void QPixmapItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
    painter->drawPixmap(-pixmap.width()/2,-pixmap.height(),pixmap);
}

void QPixmapItem::RemoveQitem(QGraphicsScene *scene){scene->removeItem(this);}
