/******************************************************************************
 * Project    : Smart Grid Table GUI
 * Author     : Dingzhu Chen
 *
 * Description: function implementations for all subsystem classes
******************************************************************************/
#include "Global_Modules.h"

table1::table1()
{
    this->setWindowIcon(QIcon("img/HAN.jpg"));//设置窗口图标
    //this->resize(this->width(),this->height());
    this->setWindowTitle("Table 1");//设置窗口标题名称
    this->setAutoFillBackground(true);//让窗体2铺满整个窗体把窗体1盖住
    this->setMouseTracking(true);
    //QPalette palette;//实例化一个调色板对象
    //this->setBackgroundBrush(QBrush(QImage("img/table1.png")));
    QPalette PAllbackground = this->palette();
    QImage fitimgpic=QImage("img/table1.png").scaled(this->width(),this->height(), Qt::IgnoreAspectRatio);
    this->setBackgroundBrush(QBrush(fitimgpic));
    this->setPalette(PAllbackground);

    table_1_scene=new QGraphicsScene;
    table_1_scene->setSceneRect(0,0,this->width(),this->height());//设置scene起始坐标和长宽
    this->setScene(table_1_scene);

    //T1_R5_MODULE=new T1_R5_Model("img/99.jpg",table_1_scene);

    //JSON_ITEM.a=88;
    this->timer = new QTimer;
    this->connect(timer, SIGNAL(timeout()),this->table_1_scene,SLOT(advance()));
    this->timer1 = new QTimer;
    this->connect(timer1, SIGNAL(timeout()),this->table_1_scene,SLOT(clear()));
    //global_instance.MQTT_RFID_VALUE
    //std::string s= std::to_string( (global_instance.ooo));
    std::string s = doubleToString(global_instance.ooo);
    std::cout<<"global_tables oo:=  "<<global_instance.ooo<<std::endl;
    std::cout<<"global_tables s :=  "<<s<<std::endl;
    global_instance.ttt=QString::fromStdString(s);
    std::cout<<"global_tables ttt :=  "<<global_instance.ttt.toStdString()<<std::endl;

    T1_R5_TextEdit = new QPlainTextEdit;
    T1_R5_TextEdit->move(210,60);
    T1_R5_TextEdit->setFixedWidth(80);
    T1_R5_TextEdit->setFixedHeight(40);
    //T1_R5_TextEdit->insertPlainText(global_instance.ttt);
    if(global_instance.T1_R5==1){
        //if(global_instance.Coal_PW_1071771887!=nullptr)
        T1_R5_TextEdit->insertPlainText(QString::number(global_instance.T1_R5_VALUE));
    }else {T1_R5_TextEdit->insertPlainText(nullptr);}
        T1_R5_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T1_R5_TextEdit->show();

    T1_R1_TextEdit = new QPlainTextEdit;
    T1_R1_TextEdit->move(400,60);
    T1_R1_TextEdit->setFixedWidth(80);
    T1_R1_TextEdit->setFixedHeight(40);
    //T1_R1_TextEdit->insertPlainText(global_instance.ttt);
    if(global_instance.T1_R1==1){
        //if(global_instance.Coal_PW_1071771887!=nullptr)
        T1_R1_TextEdit->insertPlainText(QString::number(global_instance.T1_R1_VALUE));
    }else {
        T1_R1_TextEdit->insertPlainText(nullptr);}
        T1_R1_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T1_R1_TextEdit->show();

    QLabel *nameLabel = new QLabel("T1_R5:");
    nameLabel->move(160,60);
    nameLabel->setFixedWidth(40);
    nameLabel->setFixedHeight(30);
    nameLabel->setParent(this);
    nameLabel->show();

    QLabel *T1_R1_Label = new QLabel("T1_R1:");
    T1_R1_Label->move(350,60);
    T1_R1_Label->setFixedWidth(40);
    T1_R1_Label->setFixedHeight(30);
    T1_R1_Label->setParent(this);
    T1_R1_Label->show();

    T1_R5_Btn =new QPushButton;
    T1_R5_Btn->resize(100, 25);
    T1_R5_Btn->move(160, 90);
    T1_R5_Btn->setText("confirm change");
    T1_R5_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
    T1_R5_Btn->setParent(this);
    T1_R5_Btn->show();
    connect(T1_R5_Btn,SIGNAL(clicked()),this, SLOT(T1_R5_Button_change()));

    QPushButton *T1_R1_Btn =new QPushButton;
    T1_R1_Btn->resize(100, 25);
    T1_R1_Btn->move(350, 90);
    T1_R1_Btn->setText("confirm change");
    T1_R1_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
    T1_R1_Btn->setParent(this);
    T1_R1_Btn->show();
    connect(T1_R1_Btn,SIGNAL(clicked()),this, SLOT(T1_R1_Button_change()));

    timer = new QTimer;
    this->connect(timer, SIGNAL(timeout()),table_1_scene,SLOT(advance()));
    //timer->start(100);
}

void table1::mouseMoveEvent(QMouseEvent *event){
    QPoint p;
    p=event->pos();//Returns the position of the mouse cursorReturns the position of the mouse cursor
    //画线
    QLine line(this->width()/2, this->height(),p.x(),p.y());
    QLineF linef(line);
}


void table1::mousePressEvent(QMouseEvent *event){
    Table1=new table1;
    Table1->show();
}


void table1::showrealtimestatus(){
}

void table1::T1_R5_Button_change(){
    QString qqq=T1_R5_TextEdit->toPlainText();
    global_instance.T1_R5_VALUE=qqq.toDouble();
    emit T1_R5_clicked(999);
}


void table1::T1_R1_Button_change(){
    QString qqq=T1_R1_TextEdit->toPlainText();
    global_instance.T1_R1_VALUE=qqq.toDouble();
    emit T1_R1_clicked(999);
}

void table1::T1_R5_TextData_change(){
        global_table_instance.TABLE1->T1_R5_TextEdit->clear();
        if(global_instance.T1_R5_VALUE!=NULL){
                if(global_instance.T1_R5==1){
                    //if(global_instance.In_Ex_PW_1842809804!=NULL)
                    T1_R5_TextEdit->insertPlainText(QString::number(global_instance.T1_R5_VALUE));
                }
                else if(global_instance.T1_R5==0) {
                    //T1_R5_TextEdit->insertPlainText("emmmmm");
                    global_instance.T1_R5_VALUE=NULL;
                }
        }
}

void table1::T1_R1_TextData_change(){
        global_table_instance.TABLE1->T1_R1_TextEdit->clear();
        if(global_instance.T1_R1_VALUE!=NULL){
                if(global_instance.T1_R1==1){
                    //if(global_instance.In_Ex_PW_1842809804!=NULL)
                    T1_R1_TextEdit->insertPlainText(QString::number(global_instance.T1_R1_VALUE));
                }
                else if(global_instance.T1_R1==0) {
                    //T1_R5_TextEdit->insertPlainText("emmmmm");
                    global_instance.T1_R1_VALUE=NULL;
                }
        }
}







table2::table2()
{        this->setWindowIcon(QIcon("img/HAN.jpg"));//设置窗口图标
         this->setWindowTitle("Table 2");//设置窗口标题名称

         QImage fitimgpic=QImage("img/table2.png").scaled(this->width(),this->height(), Qt::IgnoreAspectRatio);
         this->setBackgroundBrush(QBrush(fitimgpic));

         table_2_scene=new QGraphicsScene;
         table_2_scene->setSceneRect(0,0,this->width(),this->height());//设置scene起始坐标和长宽
         this->setScene(table_2_scene);
}

void table2::mouseMoveEvent(QMouseEvent *event){

    QPoint p;
    p=event->pos();//Returns the position of the mouse cursorReturns the position of the mouse cursor

    //画线
    QLine line(this->width()/2, this->height(),p.x(),p.y());
    QLineF linef(line);

    //Gun->setRotation(90-linef.angle());
    //qDebug("angle=%f",90-linef.angle());
}


void table2::mousePressEvent(QMouseEvent *event){
     //qDebug("hhhhhhhhhhhh");
//    Table2=new table2;
//    Table2->show();
    emit table2_clicked(333);


}


















table3::table3()
{
    this->setWindowIcon(QIcon("img/HAN.jpg"));//设置窗口图标
    //this->resize(this->width(),this->height());
    this->setWindowTitle("Table 3");//设置窗口标题名称

    //this->setAutoFillBackground(true);//让窗体2铺满整个窗体把窗体1盖住
    this->setMouseTracking(true);

    //QPalette palette;//实例化一个调色板对象
    //this->setBackgroundBrush(QBrush(QImage("E:/table3.png")));
    //QPainter painter(this);
    //painter.drawPixmap(0,0,this->width(),this->height(),QPixmap("E:/table3.png"));

    QPalette PAllbackground = this->palette();
    QImage fitimgpic=QImage("img/table3.png").scaled(this->width(),this->height(), Qt::IgnoreAspectRatio);
    this->setBackgroundBrush(QBrush(fitimgpic));
    this->setPalette(PAllbackground);
    table_3_scene=new QGraphicsScene;
    table_3_scene->setSceneRect(0,0,this->width(),this->height());//设置scene起始坐标和长宽
    this->setScene(table_3_scene);

    T3_R0_TextEdit = new QPlainTextEdit;
    T3_R0_TextEdit->move(441,108);
    T3_R0_TextEdit->setFixedWidth(80);
    T3_R0_TextEdit->setFixedHeight(30);
    //nameLineEdit->insertPlainText(QString::fromStdString(s));
    T3_R0_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
    T3_R0_TextEdit->show();

    QPushButton *T3_R0_Btn =new QPushButton;
    //QPalette *T6_R1_Btn =new QPalette;
    T3_R0_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
    T3_R0_Btn->resize(35, 25);
    T3_R0_Btn->move(441,81);
    T3_R0_Btn->setText("OK");
    //T3_R1_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
    T3_R0_Btn->setParent(this);
    T3_R0_Btn->show();
    connect(T3_R0_Btn,SIGNAL(clicked()),this, SLOT(T3_R0_Button_change()));

    T3_R1_TextEdit = new QPlainTextEdit;
    T3_R1_TextEdit->move(441,193);
    T3_R1_TextEdit->setFixedWidth(80);
    T3_R1_TextEdit->setFixedHeight(30);
    //nameLineEdit->insertPlainText(QString::fromStdString(s));
    T3_R1_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
    T3_R1_TextEdit->show();
    QPushButton *T3_R1_Btn =new QPushButton;
    //QPalette *T6_R1_Btn =new QPalette;
    T3_R1_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
    T3_R1_Btn->resize(35, 25);
    T3_R1_Btn->move(441,166);
    T3_R1_Btn->setText("OK");
    //T3_R1_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
    T3_R1_Btn->setParent(this);
    T3_R1_Btn->show();
    connect(T3_R1_Btn,SIGNAL(clicked()),this, SLOT(T3_R1_Button_change()));

    T3_R2_TextEdit = new QPlainTextEdit;
    T3_R2_TextEdit->move(441,278);
    T3_R2_TextEdit->setFixedWidth(80);
    T3_R2_TextEdit->setFixedHeight(30);
    //nameLineEdit->insertPlainText(QString::fromStdString(s));
    T3_R2_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
    T3_R2_TextEdit->show();
    QPushButton *T3_R2_Btn =new QPushButton;
    //QPalette *T6_R1_Btn =new QPalette;
    T3_R2_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
    T3_R2_Btn->resize(35, 25);
    T3_R2_Btn->move(441,251);
    T3_R2_Btn->setText("OK");
    //T3_R1_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
    T3_R2_Btn->setParent(this);
    T3_R2_Btn->show();
    connect(T3_R2_Btn,SIGNAL(clicked()),this, SLOT(T3_R2_Button_change()));

    T3_R3_TextEdit = new QPlainTextEdit;
    T3_R3_TextEdit->move(441,363);
    T3_R3_TextEdit->setFixedWidth(80);
    T3_R3_TextEdit->setFixedHeight(30);
    //nameLineEdit->insertPlainText(QString::fromStdString(s));
    T3_R3_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
    T3_R3_TextEdit->show();
    QPushButton *T3_R3_Btn =new QPushButton;
    //QPalette *T6_R1_Btn =new QPalette;
    T3_R3_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
    T3_R3_Btn->resize(35, 25);
    T3_R3_Btn->move(441,336);
    T3_R3_Btn->setText("OK");
    //T3_R1_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
    T3_R3_Btn->setParent(this);
    T3_R3_Btn->show();
    connect(T3_R3_Btn,SIGNAL(clicked()),this, SLOT(T3_R3_Button_change()));

    T3_R4_TextEdit = new QPlainTextEdit;
    T3_R4_TextEdit->move(120,363);
    T3_R4_TextEdit->setFixedWidth(80);
    T3_R4_TextEdit->setFixedHeight(30);
    //nameLineEdit->insertPlainText(QString::fromStdString(s));
    T3_R4_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
    T3_R4_TextEdit->show();
    QPushButton *T3_R4_Btn =new QPushButton;
    //QPalette *T6_R1_Btn =new QPalette;
    T3_R4_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
    T3_R4_Btn->resize(35, 25);
    T3_R4_Btn->move(120,336);
    T3_R4_Btn->setText("OK");
    //T3_R1_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
    T3_R4_Btn->setParent(this);
    T3_R4_Btn->show();
    connect(T3_R4_Btn,SIGNAL(clicked()),this, SLOT(T3_R4_Button_change()));

    T3_R5_TextEdit = new QPlainTextEdit;
    T3_R5_TextEdit->move(120,278);
    T3_R5_TextEdit->setFixedWidth(80);
    T3_R5_TextEdit->setFixedHeight(30);
    //nameLineEdit->insertPlainText(QString::fromStdString(s));
    T3_R5_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
    T3_R5_TextEdit->show();
    QPushButton *T3_R5_Btn =new QPushButton;
    //QPalette *T6_R1_Btn =new QPalette;
    T3_R5_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
    T3_R5_Btn->resize(35, 25);
    T3_R5_Btn->move(120,251);
    T3_R5_Btn->setText("OK");
    //T3_R1_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
    T3_R5_Btn->setParent(this);
    T3_R5_Btn->show();
    connect(T3_R5_Btn,SIGNAL(clicked()),this, SLOT(T3_R5_Button_change()));

    T3_R6_TextEdit = new QPlainTextEdit;
    T3_R6_TextEdit->move(120,193);
    T3_R6_TextEdit->setFixedWidth(80);
    T3_R6_TextEdit->setFixedHeight(30);
    //nameLineEdit->insertPlainText(QString::fromStdString(s));
    T3_R6_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
    T3_R6_TextEdit->show();
    QPushButton *T3_R6_Btn =new QPushButton;
    //QPalette *T6_R1_Btn =new QPalette;
    T3_R6_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
    T3_R6_Btn->resize(35, 25);
    T3_R6_Btn->move(120,166);
    T3_R6_Btn->setText("OK");
    //T3_R1_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
    T3_R6_Btn->setParent(this);
    T3_R6_Btn->show();
    connect(T3_R6_Btn,SIGNAL(clicked()),this, SLOT(T3_R6_Button_change()));

    T3_R7_TextEdit = new QPlainTextEdit;
    T3_R7_TextEdit->move(120,108);
    T3_R7_TextEdit->setFixedWidth(80);
    T3_R7_TextEdit->setFixedHeight(30);
    //nameLineEdit->insertPlainText(QString::fromStdString(s));
    T3_R7_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
    T3_R7_TextEdit->show();
    QPushButton *T3_R7_Btn =new QPushButton;
    //QPalette *T6_R1_Btn =new QPalette;
    T3_R7_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
    T3_R7_Btn->resize(35, 25);
    T3_R7_Btn->move(120,81);
    T3_R7_Btn->setText("OK");
    //T3_R1_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
    T3_R7_Btn->setParent(this);
    T3_R7_Btn->show();
    connect(T3_R7_Btn,SIGNAL(clicked()),this, SLOT(T3_R7_Button_change()));

         this->timer = new QTimer;
         this->connect(timer, SIGNAL(timeout()),this->table_3_scene,SLOT(advance()));
         this->timer1 = new QTimer;
         this->connect(timer1, SIGNAL(timeout()),this->table_3_scene,SLOT(clear()));
}
void table3::mouseMoveEvent(QMouseEvent *event){
    QPoint p;
    p=event->pos();//Returns the position of the mouse cursorReturns the position of the mouse cursor
    //画线
    QLine line(this->width()/2, this->height(),p.x(),p.y());
    QLineF linef(line);
    //Gun->setRotation(90-linef.angle());
    //qDebug("angle=%f",90-linef.angle());
}

void table3::mousePressEvent(QMouseEvent *event){
    Table3=new table3;
    Table3->show();
}


void table3::T3_R5_TextData_change(){
        global_table_instance.TABLE3->T3_R5_TextEdit->clear();
        if(global_instance.T3_R5_VALUE!=NULL){
                if(global_instance.T3_R5==1){
                    T3_R5_TextEdit->insertPlainText(QString::number(global_instance.T3_R5_VALUE));
                }
                else if(global_instance.T3_R5==0) {
                    global_instance.T3_R5_VALUE=NULL;
                }
        }
}

void table3:: T3_R6_TextData_change(){
    global_table_instance.TABLE3->T3_R6_TextEdit->clear();
    if(global_instance.T3_R6_VALUE!=NULL){
            if(global_instance.T3_R6==1){
                T3_R6_TextEdit->insertPlainText(QString::number(global_instance.T3_R6_VALUE));
            }
            else if(global_instance.T3_R6==0) {
                global_instance.T3_R6_VALUE=NULL;
            }
    }
}

void table3:: T3_R7_TextData_change(){
    global_table_instance.TABLE3->T3_R7_TextEdit->clear();
    if(global_instance.T3_R7_VALUE!=NULL){
            if(global_instance.T3_R7==1){
                T3_R7_TextEdit->insertPlainText(QString::number(global_instance.T3_R7_VALUE));
            }
            else if(global_instance.T3_R7==0) {
                global_instance.T3_R7_VALUE=NULL;
            }
    }
}

void table3:: T3_R4_TextData_change(){
    global_table_instance.TABLE3->T3_R4_TextEdit->clear();
    if(global_instance.T3_R4_VALUE!=NULL){
            if(global_instance.T3_R4==1){
                T3_R4_TextEdit->insertPlainText(QString::number(global_instance.T3_R4_VALUE));
            }
            else if(global_instance.T3_R4==0) {
                global_instance.T3_R4_VALUE=NULL;
            }
    }
}

void table3:: T3_R3_TextData_change(){
    global_table_instance.TABLE3->T3_R3_TextEdit->clear();
    if(global_instance.T3_R3_VALUE!=NULL){
            if(global_instance.T3_R3==1){
                T3_R3_TextEdit->insertPlainText(QString::number(global_instance.T3_R3_VALUE));
            }
            else if(global_instance.T3_R3==0) {
                global_instance.T3_R3_VALUE=NULL;
            }
    }
}

void table3:: T3_R2_TextData_change(){
    global_table_instance.TABLE3->T3_R2_TextEdit->clear();
    if(global_instance.T3_R2_VALUE!=NULL){
            if(global_instance.T3_R2==1){
                T3_R2_TextEdit->insertPlainText(QString::number(global_instance.T3_R2_VALUE));
            }
            else if(global_instance.T3_R2==0) {
                global_instance.T3_R2_VALUE=NULL;
            }
    }
}

void table3:: T3_R1_TextData_change(){
    global_table_instance.TABLE3->T3_R1_TextEdit->clear();
    if(global_instance.T3_R1_VALUE!=NULL){
            if(global_instance.T3_R1==1){
                T3_R1_TextEdit->insertPlainText(QString::number(global_instance.T3_R1_VALUE));
            }
            else if(global_instance.T3_R1==0) {
                global_instance.T3_R1_VALUE=NULL;
            }
    }
}


void table3:: T3_R0_TextData_change(){
    global_table_instance.TABLE3->T3_R0_TextEdit->clear();
    if(global_instance.T3_R0_VALUE!=NULL){
            if(global_instance.T3_R0==1){
                T3_R0_TextEdit->insertPlainText(QString::number(global_instance.T3_R0_VALUE));
            }
            else if(global_instance.T3_R0==0) {
                global_instance.T3_R0_VALUE=NULL;
            }
    }
}

void table3::T3_R0_Button_change(){
    QString qqq=T3_R0_TextEdit->toPlainText();
    global_instance.T3_R0_VALUE=qqq.toDouble();
    emit T3_R0_clicked(999);
}

void table3::T3_R1_Button_change(){
    QString qqq=T3_R1_TextEdit->toPlainText();
    global_instance.T3_R1_VALUE=qqq.toDouble();
    emit T3_R1_clicked(999);
}
void table3::T3_R2_Button_change(){
    QString qqq=T3_R2_TextEdit->toPlainText();
    global_instance.T3_R2_VALUE=qqq.toDouble();
    emit T3_R2_clicked(999);
}
void table3::T3_R3_Button_change(){
    QString qqq=T3_R3_TextEdit->toPlainText();
    global_instance.T3_R3_VALUE=qqq.toDouble();
    emit T3_R3_clicked(999);
}
void table3::T3_R4_Button_change(){
    QString qqq=T3_R4_TextEdit->toPlainText();
    global_instance.T3_R4_VALUE=qqq.toDouble();
    emit T3_R4_clicked(999);
}
void table3::T3_R5_Button_change(){
    QString qqq=T3_R5_TextEdit->toPlainText();
    global_instance.T3_R5_VALUE=qqq.toDouble();
    emit T3_R5_clicked(999);
}

void table3::T3_R6_Button_change(){
    QString qqq=T3_R6_TextEdit->toPlainText();
    global_instance.T3_R6_VALUE=qqq.toDouble();
    emit T3_R6_clicked(999);
}
void table3::T3_R7_Button_change(){
    QString qqq=T3_R7_TextEdit->toPlainText();
    global_instance.T3_R7_VALUE=qqq.toDouble();
    emit T3_R7_clicked(999);
}



table6::table6()
{        this->setWindowIcon(QIcon("img/HAN.jpg"));//设置窗口图标
         this->setWindowTitle("Table 6");//设置窗口标题名称
         this->setMouseTracking(true);

         QPalette PAllbackground = this->palette();
         QImage fitimgpic=QImage("img/table6.png").scaled(this->width(),this->height(), Qt::IgnoreAspectRatio);
         this->setBackgroundBrush(QBrush(fitimgpic));
         this->setPalette(PAllbackground);

         table_6_scene=new QGraphicsScene;
         table_6_scene->setSceneRect(0,0,this->width(),this->height());//设置scene起始坐标和长宽
         this->setScene(table_6_scene);

            T6_R1_TextEdit = new QPlainTextEdit;
            T6_R1_TextEdit->move(60,129);
            T6_R1_TextEdit->setFixedWidth(80);
            T6_R1_TextEdit->setFixedHeight(30);
            //nameLineEdit->insertPlainText(QString::fromStdString(s));
            T6_R1_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
            T6_R1_TextEdit->show();

            QPushButton *T6_R1_Btn =new QPushButton;
            //QPalette *T6_R1_Btn =new QPalette;
            T6_R1_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
            T6_R1_Btn->resize(35, 25);
            T6_R1_Btn->move(20, 129);
            T6_R1_Btn->setText("OK");
            //T6_R1_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
            T6_R1_Btn->setParent(this);
            T6_R1_Btn->show();
            connect(T6_R1_Btn,SIGNAL(clicked()),this, SLOT(T6_R1_Button_change()));

            T6_R2_TextEdit = new QPlainTextEdit;
            T6_R2_TextEdit->move(375,45);
            T6_R2_TextEdit->setFixedWidth(80);
            T6_R2_TextEdit->setFixedHeight(30);
            //nameLineEdit->insertPlainText(QString::fromStdString(s));
            T6_R2_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
            T6_R2_TextEdit->show();

            QPushButton *T6_R2_Btn =new QPushButton;
            //QPalette *T6_R2_Btn =new QPalette;
            T6_R2_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
            T6_R2_Btn->resize(35, 25);
            T6_R2_Btn->move(375, 15);
            T6_R2_Btn->setText("OK");
            //T6_R2_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
            T6_R2_Btn->setParent(this);
            T6_R2_Btn->show();
            connect(T6_R2_Btn,SIGNAL(clicked()),this, SLOT(T6_R2_Button_change()));

            T6_R3_TextEdit = new QPlainTextEdit;
            T6_R3_TextEdit->move(528,129);
            T6_R3_TextEdit->setFixedWidth(80);
            T6_R3_TextEdit->setFixedHeight(30);
            //nameLineEdit->insertPlainText(QString::fromStdString(s));
            T6_R3_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
            T6_R3_TextEdit->show();

            QPushButton *T6_R3_Btn =new QPushButton;
            //QPalette *T6_R3_Btn =new QPalette;
            T6_R3_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
            T6_R3_Btn->resize(35, 25);
            T6_R3_Btn->move(488, 129);
            T6_R3_Btn->setText("OK");
            //T6_R3_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
            T6_R3_Btn->setParent(this);
            T6_R3_Btn->show();
            connect(T6_R3_Btn,SIGNAL(clicked()),this, SLOT(T6_R3_Button_change()));
            T6_R4_TextEdit = new QPlainTextEdit;
            T6_R4_TextEdit->move(560,280);
            T6_R4_TextEdit->setFixedWidth(80);
            T6_R4_TextEdit->setFixedHeight(30);
            //nameLineEdit->insertPlainText(QString::fromStdString(s));
            T6_R4_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
            T6_R4_TextEdit->show();

            QPushButton *T6_R4_Btn =new QPushButton;
            //QPalette *T6_R4_Btn =new QPalette;
            T6_R4_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
            T6_R4_Btn->resize(35, 25);
            T6_R4_Btn->move(520, 280);
            T6_R4_Btn->setText("OK");
            //T6_R4_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
            T6_R4_Btn->setParent(this);
            T6_R4_Btn->show();
            connect(T6_R4_Btn,SIGNAL(clicked()),this, SLOT(T6_R4_Button_change()));

            T6_R5_TextEdit = new QPlainTextEdit;
            T6_R5_TextEdit->move(556,450);
            T6_R5_TextEdit->setFixedWidth(80);
            T6_R5_TextEdit->setFixedHeight(30);
            //nameLineEdit->insertPlainText(QString::fromStdString(s));
            T6_R5_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
            T6_R5_TextEdit->show();

            QPushButton *T6_R5_Btn =new QPushButton;
            //QPalette *T6_R5_Btn =new QPalette;
            T6_R5_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
            T6_R5_Btn->resize(35, 25);
            T6_R5_Btn->move(518, 456);
            T6_R5_Btn->setText("OK");
            //T6_R5_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
            T6_R5_Btn->setParent(this);
            T6_R5_Btn->show();
            connect(T6_R5_Btn,SIGNAL(clicked()),this, SLOT(T6_R5_Button_change()));

            T6_R6_TextEdit = new QPlainTextEdit;
            T6_R6_TextEdit->move(380,450);
            T6_R6_TextEdit->setFixedWidth(80);
            T6_R6_TextEdit->setFixedHeight(30);
            //nameLineEdit->insertPlainText(QString::fromStdString(s));
            T6_R6_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
            T6_R6_TextEdit->show();

            QPushButton *T6_R6_Btn =new QPushButton;
            //QPalette *T6_R6_Btn =new QPalette;
            T6_R6_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
            T6_R6_Btn->resize(35, 25);
            T6_R6_Btn->move(370, 426);
            T6_R6_Btn->setText("OK");
            //T6_R6_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
            T6_R6_Btn->setParent(this);
            T6_R6_Btn->show();
            connect(T6_R6_Btn,SIGNAL(clicked()),this, SLOT(T6_R6_Button_change()));

            T6_R7_TextEdit = new QPlainTextEdit;
            T6_R7_TextEdit->move(116,450);
            T6_R7_TextEdit->setFixedWidth(80);
            T6_R7_TextEdit->setFixedHeight(30);
            //nameLineEdit->insertPlainText(QString::fromStdString(s));
            T6_R7_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
            T6_R7_TextEdit->show();

            QPushButton *T6_R7_Btn =new QPushButton;
            //QPalette *T6_R7_Btn =new QPalette;
            T6_R7_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
            T6_R7_Btn->resize(35, 25);
            T6_R7_Btn->move(60, 456);
            T6_R7_Btn->setText("OK");
            //T6_R7_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
            T6_R7_Btn->setParent(this);
            T6_R7_Btn->show();
            connect(T6_R7_Btn,SIGNAL(clicked()),this, SLOT(T6_R7_Button_change()));

              this->timer = new QTimer;
              this->connect(timer, SIGNAL(timeout()),this->table_6_scene,SLOT(advance()));
              this->timer1 = new QTimer;
              this->connect(timer1, SIGNAL(timeout()),this->table_6_scene,SLOT(clear()));
}
void table6::mouseMoveEvent(QMouseEvent *event){
    QPoint p;
    p=event->pos();//Returns the position of the mouse cursorReturns the position of the mouse cursor

    //画线
    QLine line(this->width()/2, this->height(),p.x(),p.y());
    QLineF linef(line);
    //Gun->setRotation(90-linef.angle());
    //qDebug("angle=%f",90-linef.angle());
}

void table6::mousePressEvent(QMouseEvent *event){
     //qDebug("hhhhhhhhhhhh");
    Table6=new table6;
    Table6->show();
}

void table6::T6_R1_Button_change(){
    QString qqq=T6_R1_TextEdit->toPlainText();
    global_instance.T6_R1_VALUE=qqq.toDouble();
    emit T6_R1_clicked(999);
}
void table6::T6_R2_Button_change(){
    QString qqq=T6_R2_TextEdit->toPlainText();
    global_instance.T6_R2_VALUE=qqq.toDouble();
    emit T6_R2_clicked(999);
}
void table6::T6_R3_Button_change(){
    QString qqq=T6_R3_TextEdit->toPlainText();
    global_instance.T6_R3_VALUE=qqq.toDouble();
    emit T6_R3_clicked(999);
}

void table6::T6_R4_Button_change(){
    QString qqq=T6_R4_TextEdit->toPlainText();
    global_instance.T6_R4_VALUE=qqq.toDouble();
    emit T6_R4_clicked(999);
}
void table6::T6_R5_Button_change(){
    QString qqq=T6_R5_TextEdit->toPlainText();
    global_instance.T6_R5_VALUE=qqq.toDouble();
    emit T6_R5_clicked(999);
}

void table6::T6_R6_Button_change(){
    QString qqq=T6_R6_TextEdit->toPlainText();
    global_instance.T6_R6_VALUE=qqq.toDouble();
    emit T6_R6_clicked(999);
}

void table6::T6_R7_Button_change(){
    QString qqq=T6_R7_TextEdit->toPlainText();
    global_instance.T6_R7_VALUE=qqq.toDouble();
    emit T6_R7_clicked(999);
}


void table6:: T6_R1_TextData_change(){
    global_table_instance.TABLE6->T6_R1_TextEdit->clear();
    if(global_instance.T6_R1_VALUE!=NULL){
            if(global_instance.T6_R1==1){
                T6_R1_TextEdit->insertPlainText(QString::number(global_instance.T6_R1_VALUE));
            }
            else if(global_instance.T6_R1==0) {
                global_instance.T6_R1_VALUE=NULL;
            }
    }
}
void table6:: T6_R2_TextData_change(){
    global_table_instance.TABLE6->T6_R2_TextEdit->clear();
    if(global_instance.T6_R2_VALUE!=NULL){
            if(global_instance.T6_R2==1){
                T6_R2_TextEdit->insertPlainText(QString::number(global_instance.T6_R2_VALUE));
            }
            else if(global_instance.T6_R2==0) {
                global_instance.T6_R2_VALUE=NULL;
            }
    }
}
void table6:: T6_R3_TextData_change(){
    global_table_instance.TABLE6->T6_R3_TextEdit->clear();
    if(global_instance.T6_R3_VALUE!=NULL){
            if(global_instance.T6_R3==1){
                T6_R3_TextEdit->insertPlainText(QString::number(global_instance.T6_R3_VALUE));
            }
            else if(global_instance.T6_R3==0) {
                global_instance.T6_R3_VALUE=NULL;
            }
    }
}
void table6:: T6_R4_TextData_change(){
    global_table_instance.TABLE6->T6_R4_TextEdit->clear();
    if(global_instance.T6_R4_VALUE!=NULL){
            if(global_instance.T6_R4==1){
                T6_R4_TextEdit->insertPlainText(QString::number(global_instance.T6_R4_VALUE));
            }
            else if(global_instance.T6_R4==0) {
                global_instance.T6_R4_VALUE=NULL;
            }
    }
}
void table6:: T6_R5_TextData_change(){
    global_table_instance.TABLE6->T6_R5_TextEdit->clear();
    if(global_instance.T6_R5_VALUE!=NULL){
            if(global_instance.T6_R5==1){
                T6_R5_TextEdit->insertPlainText(QString::number(global_instance.T6_R5_VALUE));
            }
            else if(global_instance.T6_R5==0) {
                global_instance.T6_R5_VALUE=NULL;
            }
    }
}
void table6:: T6_R6_TextData_change(){
    global_table_instance.TABLE6->T6_R6_TextEdit->clear();
    if(global_instance.T6_R6_VALUE!=NULL){
            if(global_instance.T6_R6==1){
                T6_R6_TextEdit->insertPlainText(QString::number(global_instance.T6_R6_VALUE));
            }
            else if(global_instance.T6_R6==0) {
                global_instance.T6_R6_VALUE=NULL;
            }
    }
}

void table6:: T6_R7_TextData_change(){
    global_table_instance.TABLE6->T6_R7_TextEdit->clear();
    if(global_instance.T6_R7_VALUE!=NULL){
            if(global_instance.T6_R7==1){
                T6_R7_TextEdit->insertPlainText(QString::number(global_instance.T6_R7_VALUE));
            }
            else if(global_instance.T6_R7==0) {
                global_instance.T6_R7_VALUE=NULL;
            }
    }
}


table4::table4()
{
        this->setWindowIcon(QIcon("img/HAN.jpg"));//设置窗口图标
        this->setWindowTitle("Table 4");//设置窗口标题名称
        this->setMouseTracking(true);

        QPalette PAllbackground = this->palette();
        QImage fitimgpic=QImage("img/table4.png").scaled(this->width(),this->height(), Qt::IgnoreAspectRatio);
        this->setBackgroundBrush(QBrush(fitimgpic));
        this->setPalette(PAllbackground);

         table_4_scene=new QGraphicsScene;
         table_4_scene->setSceneRect(0,0,this->width(),this->height());//设置scene起始坐标和长宽
         this->setScene(table_4_scene);

        T4_R1_TextEdit = new QPlainTextEdit;
        T4_R1_TextEdit->move(528,129);
        T4_R1_TextEdit->setFixedWidth(80);
        T4_R1_TextEdit->setFixedHeight(30);
        //nameLineEdit->insertPlainText(QString::fromStdString(s));
        T4_R1_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T4_R1_TextEdit->show();

        QPushButton *T4_R1_Btn =new QPushButton;
        //QPalette *T4_R1_Btn =new QPalette;
        T4_R1_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
        T4_R1_Btn->resize(35, 25);
        T4_R1_Btn->move(488,129);
        T4_R1_Btn->setText("OK");
        //T4_R1_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
        T4_R1_Btn->setParent(this);
        T4_R1_Btn->show();
        connect(T4_R1_Btn,SIGNAL(clicked()),this, SLOT(T4_R1_Button_change()));

        T4_R2_TextEdit = new QPlainTextEdit;
        T4_R2_TextEdit->move(560,280);
        T4_R2_TextEdit->setFixedWidth(80);
        T4_R2_TextEdit->setFixedHeight(30);
        //nameLineEdit->insertPlainText(QString::fromStdString(s));
        T4_R2_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T4_R2_TextEdit->show();

        QPushButton *T4_R2_Btn =new QPushButton;
        //QPalette *T4_R2_Btn =new QPalette;
        T4_R2_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
        T4_R2_Btn->resize(35, 25);
        T4_R2_Btn->move(520,280);
        T4_R2_Btn->setText("OK");
        //T4_R2_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
        T4_R2_Btn->setParent(this);
        T4_R2_Btn->show();
        connect(T4_R2_Btn,SIGNAL(clicked()),this, SLOT(T4_R2_Button_change()));

        T4_R3_TextEdit = new QPlainTextEdit;
        T4_R3_TextEdit->move(556,450);
        T4_R3_TextEdit->setFixedWidth(80);
        T4_R3_TextEdit->setFixedHeight(30);
        //nameLineEdit->insertPlainText(QString::fromStdString(s));
        T4_R3_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T4_R3_TextEdit->show();

        QPushButton *T4_R3_Btn =new QPushButton;
        //QPalette *T4_R3_Btn =new QPalette;
        T4_R3_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
        T4_R3_Btn->resize(35, 25);
        T4_R3_Btn->move(518,456);
        T4_R3_Btn->setText("OK");
        //T4_R3_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
        T4_R3_Btn->setParent(this);
        T4_R3_Btn->show();
        connect(T4_R3_Btn,SIGNAL(clicked()),this, SLOT(T4_R3_Button_change()));

        T4_R4_TextEdit = new QPlainTextEdit;
        T4_R4_TextEdit->move(380,450);
        T4_R4_TextEdit->setFixedWidth(80);
        T4_R4_TextEdit->setFixedHeight(30);
        //nameLineEdit->insertPlainText(QString::fromStdString(s));
        T4_R4_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T4_R4_TextEdit->show();

        QPushButton *T4_R4_Btn =new QPushButton;
        //QPalette *T4_R4_Btn =new QPalette;
        T4_R4_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
        T4_R4_Btn->resize(35, 25);
        T4_R4_Btn->move(370,426);
        T4_R4_Btn->setText("OK");
        //T4_R4_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
        T4_R4_Btn->setParent(this);
        T4_R4_Btn->show();
        connect(T4_R4_Btn,SIGNAL(clicked()),this, SLOT(T4_R4_Button_change()));

        T4_R5_TextEdit = new QPlainTextEdit;
        T4_R5_TextEdit->move(116,450);
        T4_R5_TextEdit->setFixedWidth(80);
        T4_R5_TextEdit->setFixedHeight(30);
        //nameLineEdit->insertPlainText(QString::fromStdString(s));
        T4_R5_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T4_R5_TextEdit->show();

        QPushButton *T4_R5_Btn =new QPushButton;
        //QPalette *T4_R5_Btn =new QPalette;
        T4_R5_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
        T4_R5_Btn->resize(35, 25);
        T4_R5_Btn->move(60,456);
        T4_R5_Btn->setText("OK");
        //T4_R5_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
        T4_R5_Btn->setParent(this);
        T4_R5_Btn->show();
        connect(T4_R5_Btn,SIGNAL(clicked()),this, SLOT(T4_R5_Button_change()));
        T4_R6_TextEdit = new QPlainTextEdit;
        T4_R6_TextEdit->move(50,280);
        T4_R6_TextEdit->setFixedWidth(80);
        T4_R6_TextEdit->setFixedHeight(30);
        //nameLineEdit->insertPlainText(QString::fromStdString(s));
        T4_R6_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T4_R6_TextEdit->show();

        QPushButton *T4_R6_Btn =new QPushButton;
        //QPalette *T4_R6_Btn =new QPalette;
        T4_R6_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
        T4_R6_Btn->resize(35, 25);
        T4_R6_Btn->move(10,280);
        T4_R6_Btn->setText("OK");
        //T4_R6_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
        T4_R6_Btn->setParent(this);
        T4_R6_Btn->show();
        connect(T4_R6_Btn,SIGNAL(clicked()),this, SLOT(T4_R6_Button_change()));
        T4_R7_TextEdit = new QPlainTextEdit;
        T4_R7_TextEdit->move(60,129);
        T4_R7_TextEdit->setFixedWidth(80);
        T4_R7_TextEdit->setFixedHeight(30);
        //nameLineEdit->insertPlainText(QString::fromStdString(s));
        T4_R7_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T4_R7_TextEdit->show();

        QPushButton *T4_R7_Btn =new QPushButton;
        //QPalette *T4_R7_Btn =new QPalette;
        T4_R7_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
        T4_R7_Btn->resize(35, 25);
        T4_R7_Btn->move(20,129);
        T4_R7_Btn->setText("OK");
        //T4_R7_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
        T4_R7_Btn->setParent(this);
        T4_R7_Btn->show();
        connect(T4_R7_Btn,SIGNAL(clicked()),this, SLOT(T4_R7_Button_change()));

        //timer
        this->timer = new QTimer;
        this->connect(timer, SIGNAL(timeout()),this->table_4_scene,SLOT(advance()));
        this->timer1 = new QTimer;
        this->connect(timer1, SIGNAL(timeout()),this->table_4_scene,SLOT(clear()));
}
void table4::mouseMoveEvent(QMouseEvent *event){
    QPoint p;
    p=event->pos();//Returns the position of the mouse cursorReturns the position of the mouse cursor
    //画线
    QLine line(this->width()/2, this->height(),p.x(),p.y());
    QLineF linef(line);
    //Gun->setRotation(90-linef.angle());
    //qDebug("angle=%f",90-linef.angle());
}
void table4::mousePressEvent(QMouseEvent *event){
    Table4=new table4;
    Table4->show();

}
void table4::T4_R1_Button_change(){
    QString qqq=T4_R1_TextEdit->toPlainText();
    global_instance.T4_R1_VALUE=qqq.toDouble();
    emit T4_R1_clicked(999);
}
void table4::T4_R2_Button_change(){
    QString qqq=T4_R2_TextEdit->toPlainText();
    global_instance.T4_R2_VALUE=qqq.toDouble();
    emit T4_R2_clicked(999);
}
void table4::T4_R3_Button_change(){
    QString qqq=T4_R3_TextEdit->toPlainText();
    global_instance.T4_R3_VALUE=qqq.toDouble();
    emit T4_R3_clicked(999);
}
void table4::T4_R4_Button_change(){
    QString qqq=T4_R4_TextEdit->toPlainText();
    global_instance.T4_R4_VALUE=qqq.toDouble();
    emit T4_R4_clicked(999);
}
void table4::T4_R5_Button_change(){
    QString qqq=T4_R5_TextEdit->toPlainText();
    global_instance.T4_R5_VALUE=qqq.toDouble();
    emit T4_R5_clicked(999);
}
void table4::T4_R6_Button_change(){
    QString qqq=T4_R6_TextEdit->toPlainText();
    global_instance.T4_R6_VALUE=qqq.toDouble();
    emit T4_R6_clicked(999);
}
void table4::T4_R7_Button_change(){
    QString qqq=T4_R7_TextEdit->toPlainText();
    global_instance.T4_R7_VALUE=qqq.toDouble();
    emit T4_R7_clicked(999);
}




void table4:: T4_R1_TextData_change(){
    global_table_instance.TABLE4->T4_R1_TextEdit->clear();
    if(global_instance.T4_R1_VALUE!=NULL){
            if(global_instance.T4_R1==1){
                T4_R1_TextEdit->insertPlainText(QString::number(global_instance.T4_R1_VALUE));
            }
            else if(global_instance.T4_R1==0) {
                global_instance.T4_R1_VALUE=NULL;
            }
    }
}
void table4:: T4_R2_TextData_change(){
    global_table_instance.TABLE4->T4_R2_TextEdit->clear();
    if(global_instance.T4_R2_VALUE!=NULL){
            if(global_instance.T4_R2==1){
                T4_R2_TextEdit->insertPlainText(QString::number(global_instance.T4_R2_VALUE));
            }
            else if(global_instance.T4_R2==0) {
                global_instance.T4_R2_VALUE=NULL;
            }
    }
}
void table4:: T4_R3_TextData_change(){
    global_table_instance.TABLE4->T4_R3_TextEdit->clear();
    if(global_instance.T4_R3_VALUE!=NULL){
            if(global_instance.T4_R3==1){
                T4_R3_TextEdit->insertPlainText(QString::number(global_instance.T4_R3_VALUE));
            }
            else if(global_instance.T4_R3==0) {
                global_instance.T4_R3_VALUE=NULL;
            }
    }
}
void table4:: T4_R4_TextData_change(){
    global_table_instance.TABLE4->T4_R4_TextEdit->clear();
    if(global_instance.T4_R4_VALUE!=NULL){
            if(global_instance.T4_R4==1){
                T4_R4_TextEdit->insertPlainText(QString::number(global_instance.T4_R4_VALUE));
            }
            else if(global_instance.T4_R4==0) {
                global_instance.T4_R4_VALUE=NULL;
            }
    }
}
void table4:: T4_R5_TextData_change(){
    global_table_instance.TABLE4->T4_R5_TextEdit->clear();
    if(global_instance.T4_R5_VALUE!=NULL){
            if(global_instance.T4_R5==1){
                T4_R5_TextEdit->insertPlainText(QString::number(global_instance.T4_R5_VALUE));
            }
            else if(global_instance.T4_R5==0) {
                global_instance.T4_R5_VALUE=NULL;
            }
    }
}
void table4:: T4_R6_TextData_change(){
    global_table_instance.TABLE4->T4_R6_TextEdit->clear();
    if(global_instance.T4_R6_VALUE!=NULL){
            if(global_instance.T4_R6==1){
                T4_R6_TextEdit->insertPlainText(QString::number(global_instance.T4_R6_VALUE));
            }
            else if(global_instance.T4_R6==0) {
                global_instance.T4_R6_VALUE=NULL;
            }
    }
}


void table4:: T4_R7_TextData_change(){
    global_table_instance.TABLE4->T4_R7_TextEdit->clear();
    if(global_instance.T4_R7_VALUE!=NULL){
            if(global_instance.T4_R7==1){
                T4_R7_TextEdit->insertPlainText(QString::number(global_instance.T4_R7_VALUE));
            }
            else if(global_instance.T4_R7==0) {
                global_instance.T4_R7_VALUE=NULL;
            }
    }
}



table5::table5()
{
        this->setWindowIcon(QIcon("img/HAN.jpg"));//设置窗口图标
        this->setWindowTitle("Table 5");//设置窗口标题名称
        this->setMouseTracking(true);

        QPalette PAllbackground = this->palette();
        QImage fitimgpic=QImage("img/table5.png").scaled(this->width(),this->height(), Qt::IgnoreAspectRatio);
        this->setBackgroundBrush(QBrush(fitimgpic));
        this->setPalette(PAllbackground);


        table_5_scene=new QGraphicsScene;
        table_5_scene->setSceneRect(0,0,this->width(),this->height());//设置scene起始坐标和长宽
        this->setScene(table_5_scene);

        T5_R1_TextEdit = new QPlainTextEdit;
        T5_R1_TextEdit->move(556,450);
        T5_R1_TextEdit->setFixedWidth(80);
        T5_R1_TextEdit->setFixedHeight(30);
        //nameLineEdit->insertPlainText(QString::fromStdString(s));
        T5_R1_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T5_R1_TextEdit->show();

        QPushButton *T5_R1_Btn =new QPushButton;
        //QPalette *T5_R1_Btn =new QPalette;
        T5_R1_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
        T5_R1_Btn->resize(35, 25);
        T5_R1_Btn->move(518,456);
        T5_R1_Btn->setText("OK");
        //T5_R1_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
        T5_R1_Btn->setParent(this);
        T5_R1_Btn->show();
        connect(T5_R1_Btn,SIGNAL(clicked()),this, SLOT(T5_R1_Button_change()));

        T5_R2_TextEdit = new QPlainTextEdit;
        T5_R2_TextEdit->move(380,450);
        T5_R2_TextEdit->setFixedWidth(80);
        T5_R2_TextEdit->setFixedHeight(30);
        //nameLineEdit->insertPlainText(QString::fromStdString(s));
        T5_R2_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T5_R2_TextEdit->show();

        QPushButton *T5_R2_Btn =new QPushButton;
        //QPalette *T5_R2_Btn =new QPalette;
        T5_R2_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
        T5_R2_Btn->resize(35, 25);
        T5_R2_Btn->move(370,426);
        T5_R2_Btn->setText("OK");
        //T5_R2_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
        T5_R2_Btn->setParent(this);
        T5_R2_Btn->show();
        connect(T5_R2_Btn,SIGNAL(clicked()),this, SLOT(T5_R2_Button_change()));

        T5_R3_TextEdit = new QPlainTextEdit;
        T5_R3_TextEdit->move(116,450);
        T5_R3_TextEdit->setFixedWidth(80);
        T5_R3_TextEdit->setFixedHeight(30);
        //nameLineEdit->insertPlainText(QString::fromStdString(s));
        T5_R3_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T5_R3_TextEdit->show();

        QPushButton *T5_R3_Btn =new QPushButton;
        //QPalette *T5_R3_Btn =new QPalette;
        T5_R3_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
        T5_R3_Btn->resize(35, 25);
        T5_R3_Btn->move(60,456);
        T5_R3_Btn->setText("OK");
        //T5_R3_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
        T5_R3_Btn->setParent(this);
        T5_R3_Btn->show();
        connect(T5_R3_Btn,SIGNAL(clicked()),this, SLOT(T5_R3_Button_change()));

        T5_R4_TextEdit = new QPlainTextEdit;
        T5_R4_TextEdit->move(50,280);
        T5_R4_TextEdit->setFixedWidth(80);
        T5_R4_TextEdit->setFixedHeight(30);
        //nameLineEdit->insertPlainText(QString::fromStdString(s));
        T5_R4_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T5_R4_TextEdit->show();

        QPushButton *T5_R4_Btn =new QPushButton;
        //QPalette *T5_R4_Btn =new QPalette;
        T5_R4_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
        T5_R4_Btn->resize(35, 25);
        T5_R4_Btn->move(10,280);
        T5_R4_Btn->setText("OK");
        //T5_R4_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
        T5_R4_Btn->setParent(this);
        T5_R4_Btn->show();
        connect(T5_R4_Btn,SIGNAL(clicked()),this, SLOT(T5_R4_Button_change()));

        T5_R5_TextEdit = new QPlainTextEdit;
        T5_R5_TextEdit->move(60,129);
        T5_R5_TextEdit->setFixedWidth(80);
        T5_R5_TextEdit->setFixedHeight(30);
        //nameLineEdit->insertPlainText(QString::fromStdString(s));
        T5_R5_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T5_R5_TextEdit->show();

        QPushButton *T5_R5_Btn =new QPushButton;
        //QPalette *T5_R5_Btn =new QPalette;
        T5_R5_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
        T5_R5_Btn->resize(35, 25);
        T5_R5_Btn->move(20,129);
        T5_R5_Btn->setText("OK");
        //T5_R5_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
        T5_R5_Btn->setParent(this);
        T5_R5_Btn->show();
        connect(T5_R5_Btn,SIGNAL(clicked()),this, SLOT(T5_R5_Button_change()));
        T5_R6_TextEdit = new QPlainTextEdit;
        T5_R6_TextEdit->move(375,45);
        T5_R6_TextEdit->setFixedWidth(80);
        T5_R6_TextEdit->setFixedHeight(30);
        //nameLineEdit->insertPlainText(QString::fromStdString(s));
        T5_R6_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T5_R6_TextEdit->show();

        QPushButton *T5_R6_Btn =new QPushButton;
        //QPalette *T5_R6_Btn =new QPalette;
        T5_R6_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
        T5_R6_Btn->resize(35, 25);
        T5_R6_Btn->move(375,15);
        T5_R6_Btn->setText("OK");
        //T5_R6_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
        T5_R6_Btn->setParent(this);
        T5_R6_Btn->show();
        connect(T5_R6_Btn,SIGNAL(clicked()),this, SLOT(T5_R6_Button_change()));
        T5_R7_TextEdit = new QPlainTextEdit;
        T5_R7_TextEdit->move(528,129);
        T5_R7_TextEdit->setFixedWidth(80);
        T5_R7_TextEdit->setFixedHeight(30);
        //nameLineEdit->insertPlainText(QString::fromStdString(s));
        T5_R7_TextEdit->setParent(this);//Don't forget to indicate its parent else it will be displayed as independent widget.
        T5_R7_TextEdit->show();

        QPushButton *T5_R7_Btn =new QPushButton;
        //QPalette *T5_R7_Btn =new QPalette;
        T5_R7_Btn->setStyleSheet("background-color: rgb(255, 255, 0);");
        T5_R7_Btn->resize(35, 25);
        T5_R7_Btn->move(488,129);
        T5_R7_Btn->setText("OK");
        //T5_R7_Btn->setStyleSheet("text-color: rgb(255, 255, 0);");
        T5_R7_Btn->setParent(this);
        T5_R7_Btn->show();
        connect(T5_R7_Btn,SIGNAL(clicked()),this, SLOT(T5_R7_Button_change()));


         //场景类里添加item coal_powerplant
         //Coal_powerplant=new coal_powerplant("img/99.jpg",table_5_scene);
         //Coal_powerplant是item.Item坐标跟场景scene坐标一样，以这中点为中心，y正方向朝下。
         //table5是view, 坐标原点在左上角
         //Coal_powerplant->setPos(this->width()/2,this->height());//此处item在view内部,故设置坐标时候用view坐标
         //coal_powerplant->setPos(100,0);
         //item一定要被添加到scene中去
         //table_5_scene->addItem(Coal_powerplant);
         //Gun=new gun("img/999.jpg",table_5_scene);
         //Gun->setPos(this->width()/2,this->height());


        this->timer = new QTimer;
        this->connect(timer, SIGNAL(timeout()),this->table_5_scene,SLOT(advance()));
        this->timer1 = new QTimer;
        this->connect(timer1, SIGNAL(timeout()),this->table_5_scene,SLOT(clear()));
}
void table5::mouseMoveEvent(QMouseEvent *event){

    QPoint p;
    p=event->pos();//Returns the position of the mouse cursorReturns the position of the mouse cursor

    //画线
    QLine line(this->width()/2, this->height(),p.x(),p.y());
    QLineF linef(line);

    //Gun->setRotation(90-linef.angle());
    //qDebug("angle=%f",90-linef.angle());
}

void table5::mousePressEvent(QMouseEvent *event){
    Table5=new table5;
    Table5->show();
}

void table5::T5_R1_Button_change(){
    QString qqq=T5_R1_TextEdit->toPlainText();
    global_instance.T5_R1_VALUE=qqq.toDouble();
    emit T5_R1_clicked(999);
}
void table5::T5_R2_Button_change(){
    QString qqq=T5_R2_TextEdit->toPlainText();
    global_instance.T5_R2_VALUE=qqq.toDouble();
    emit T5_R2_clicked(999);
}
void table5::T5_R3_Button_change(){
    QString qqq=T5_R3_TextEdit->toPlainText();
    global_instance.T5_R3_VALUE=qqq.toDouble();
    emit T5_R3_clicked(999);
}
void table5::T5_R4_Button_change(){
    QString qqq=T5_R4_TextEdit->toPlainText();
    global_instance.T5_R4_VALUE=qqq.toDouble();
    emit T5_R4_clicked(999);
}
void table5::T5_R5_Button_change(){
    QString qqq=T5_R5_TextEdit->toPlainText();
    global_instance.T5_R5_VALUE=qqq.toDouble();
    emit T5_R5_clicked(999);
}
void table5::T5_R6_Button_change(){
    QString qqq=T5_R6_TextEdit->toPlainText();
    global_instance.T5_R6_VALUE=qqq.toDouble();
    emit T5_R6_clicked(999);
}
void table5::T5_R7_Button_change(){
    QString qqq=T5_R7_TextEdit->toPlainText();
    global_instance.T5_R7_VALUE=qqq.toDouble();
    emit T5_R7_clicked(999);
}

void table5:: T5_R1_TextData_change(){
    global_table_instance.TABLE5->T5_R1_TextEdit->clear();
    //global_table_instance.TABLE1->T3_R0_TextEdit->insertPlainText(global_instance.T3_R0_VALUE);

    if(global_instance.T5_R1_VALUE!=NULL){
            if(global_instance.T5_R1==1){
                //if(global_instance.In_Ex_PW_1842809804!=NULL)
                T5_R1_TextEdit->insertPlainText(QString::number(global_instance.T5_R1_VALUE));
            }
            else if(global_instance.T5_R1==0) {
                //T1_R0_TextEdit->insertPlainText("emmmmm");
                global_instance.T5_R1_VALUE=NULL;
            }
    }
}
void table5:: T5_R2_TextData_change(){
    global_table_instance.TABLE5->T5_R2_TextEdit->clear();
    if(global_instance.T5_R2_VALUE!=NULL){
            if(global_instance.T5_R2==1){
                T5_R2_TextEdit->insertPlainText(QString::number(global_instance.T5_R2_VALUE));
            }
            else if(global_instance.T5_R2==0) {
                global_instance.T5_R2_VALUE=NULL;
            }
    }
}
void table5:: T5_R3_TextData_change(){
    global_table_instance.TABLE5->T5_R3_TextEdit->clear();
    if(global_instance.T5_R3_VALUE!=NULL){
            if(global_instance.T5_R3==1){
                T5_R3_TextEdit->insertPlainText(QString::number(global_instance.T5_R3_VALUE));
            }
            else if(global_instance.T5_R3==0) {
                global_instance.T5_R3_VALUE=NULL;
            }
    }
}
void table5:: T5_R4_TextData_change(){
    global_table_instance.TABLE5->T5_R4_TextEdit->clear();
    if(global_instance.T5_R4_VALUE!=NULL){
            if(global_instance.T5_R4==1){
                T5_R4_TextEdit->insertPlainText(QString::number(global_instance.T5_R4_VALUE));
            }
            else if(global_instance.T5_R4==0) {
                global_instance.T5_R4_VALUE=NULL;
            }
    }
}
void table5:: T5_R5_TextData_change(){
    global_table_instance.TABLE5->T5_R5_TextEdit->clear();
    if(global_instance.T5_R5_VALUE!=NULL){
            if(global_instance.T5_R5==1){
                T5_R5_TextEdit->insertPlainText(QString::number(global_instance.T5_R5_VALUE));
            }
            else if(global_instance.T5_R5==0) {
                global_instance.T5_R5_VALUE=NULL;
            }
    }
}
void table5:: T5_R6_TextData_change(){
    global_table_instance.TABLE5->T5_R6_TextEdit->clear();
    if(global_instance.T5_R6_VALUE!=NULL){
            if(global_instance.T5_R6==1){
                T5_R6_TextEdit->insertPlainText(QString::number(global_instance.T5_R6_VALUE));
            }
            else if(global_instance.T5_R6==0) {
                global_instance.T5_R6_VALUE=NULL;
            }
    }
}
void table5:: T5_R7_TextData_change(){
    global_table_instance.TABLE5->T5_R7_TextEdit->clear();
    if(global_instance.T5_R7_VALUE!=NULL){
            if(global_instance.T5_R7==1){
                T5_R7_TextEdit->insertPlainText(QString::number(global_instance.T5_R7_VALUE));
            }
            else if(global_instance.T5_R7==0) {
                global_instance.T5_R7_VALUE=NULL;
            }
    }
}

dynamic_chart::dynamic_chart()
{
    global_instance.Dynamic_Gen_Line = new QSplineSeries();
    global_instance.Dynamic_Load_Line= new QSplineSeries();
    global_instance.Coal_Gen_Line = new QSplineSeries();
    global_instance.Wind_Solar_Line = new QSplineSeries();
    global_instance.In_Export_Line = new QSplineSeries();
    global_instance.Gas_Gen_Line = new QSplineSeries();
    global_instance.Volcano_Line = new QSplineSeries();
    global_instance.Nuclear_Line = new QSplineSeries();
    global_instance.Solar_Line = new QSplineSeries();

    global_instance.Proc_Fac_Line = new QSplineSeries();
    global_instance.Car_Char1_Line = new QSplineSeries();
    global_instance.Chemi_Fac_Line = new QSplineSeries();
    global_instance.Hydro_Sta_Line = new QSplineSeries();
    global_instance.HAN_Line = new QSplineSeries();
    global_instance.Car_Char2_Line = new QSplineSeries();
    global_instance.T_House1_Line = new QSplineSeries();
    global_instance.T_House2_Line = new QSplineSeries();
    global_instance.T_House3_Line = new QSplineSeries();
    global_instance.T_House4_Line = new QSplineSeries();
    global_instance.T_House5_Line = new QSplineSeries();
    global_instance.T_House6_Line = new QSplineSeries();
    global_instance.House1_Line = new QSplineSeries();
    global_instance.House2_Line = new QSplineSeries();
    global_instance.House3_Line = new QSplineSeries();
    global_instance.House4_Line = new QSplineSeries();
    global_instance.House5_Line = new QSplineSeries();
    global_instance.Stor_House1_Line = new QSplineSeries();
    global_instance.Stor_House2_Line = new QSplineSeries();
    global_instance.Appartment1_Line = new QSplineSeries();
    global_instance.Appartment2_Line = new QSplineSeries();
    global_instance.Appartment3_Line = new QSplineSeries();
    global_instance.Appartment4_Line = new QSplineSeries();
    global_instance.Appartment5_Line = new QSplineSeries();
    global_instance.Appartment6_Line = new QSplineSeries();
    global_instance.Shop_Mall_Line = new QSplineSeries();
    global_instance.Hospital_Line = new QSplineSeries();

    global_instance.Dynamic_Gen_Line->setName("GEN_SUM");
    global_instance.Dynamic_Load_Line->setName("LOAD_SUM");
    global_instance.Coal_Gen_Line ->setName("GCoal");
    global_instance.Wind_Solar_Line->setName("GW&Sola");
    global_instance.In_Export_Line->setName("GI/O");
    global_instance.Gas_Gen_Line->setName("GGas");
    global_instance.Volcano_Line->setName("GVoc");
    global_instance.Nuclear_Line->setName("GNuk");
    global_instance.Solar_Line->setName("GSola");

    global_instance.Proc_Fac_Line->setName("LPFac");
    global_instance.Car_Char1_Line->setName("LCarC1");
    global_instance.Chemi_Fac_Line->setName("LCheFac");
    global_instance.Hydro_Sta_Line->setName("LHySta");
    global_instance.HAN_Line->setName("LHAN");
    global_instance.Car_Char2_Line->setName("LCarC2");
    global_instance.T_House1_Line->setName("LTHou1");
    global_instance.T_House2_Line->setName("LTHou2");
    global_instance.T_House3_Line->setName("LTHou3");
    global_instance.T_House4_Line->setName("LTHou4");
    global_instance.T_House5_Line->setName("LTHou5");
    global_instance.T_House6_Line->setName("LTHou6");
    global_instance.House1_Line->setName("LHou1");
    global_instance.House2_Line->setName("LHou2");
    global_instance.House3_Line->setName("LHou3");
    global_instance.House4_Line->setName("LHou4");
    global_instance.House5_Line->setName("LHou5");
    global_instance.Stor_House1_Line->setName("LStoH1");
    global_instance.Stor_House2_Line->setName("LStoH2");
    global_instance.Appartment1_Line->setName("LAp1");
    global_instance.Appartment2_Line->setName("LAp2");
    global_instance.Appartment3_Line->setName("LAp3");
    global_instance.Appartment4_Line->setName("LAp4");
    global_instance.Appartment5_Line->setName("LAp5");
    global_instance.Appartment6_Line->setName("LAp6");
    global_instance.Shop_Mall_Line->setName("LSpMal");
    global_instance.Hospital_Line->setName("LHosp");

    QColor black(1,1,1);
    QColor red(255,0,0);
    global_instance.Dynamic_Gen_Line->setColor(red);
    global_instance.Dynamic_Load_Line->setColor(black);

//    global_instance.In_Export_Line->setName("In_Ex");
//    global_instance.Gas_Gen_Line->setName("Gas");
//    global_instance.Volcano_Line->setName("Volcano");
//    global_instance.Nuclear_Line->setName("Nuclear");
//    global_instance.Solar_Line->setName("Solar");

//    global_instance.Coal_Gen_Line->append(0, 6);
//    global_instance.Coal_Gen_Line->append(2, 4);
//    global_instance.Coal_Gen_Line->append(3, 8);
//    global_instance.Coal_Gen_Line->append(7, 4);
//    global_instance.Coal_Gen_Line->append(10, 5);
//    *global_instance.Coal_Gen_Line << QPointF(11, 1) << QPointF(13, 3) << QPointF(17, 6) << QPointF(18, 3) << QPointF(20, 2);

//    *global_instance.Wind_Solar_Line<<  QPointF(23, 0);
//    *global_instance.Dynamic_Gen_Line<<QPointF(0, 1) << QPointF(5, 3) << QPointF(8, 6) << QPointF(10, 3) << QPointF(11, 1) << QPointF(13, 3) << QPointF(17, 6) << QPointF(18, 3) << QPointF(20, 2)<< QPointF(23, 10);
    *global_instance.Dynamic_Load_Line<<  QPointF(23, 0);

    global_instance.chart = new QChart();
//  global_instance.chart->legend()->setColor(Qt::black);
    global_instance.chart->legend()->ensureVisible(0,0,0,0);
    global_instance.chart->legend()->setAlignment(Qt::AlignTop);
    //connectMarkers();
    //global_instance.chart->addSeries(global_instance.Dynamic_Gen_Line);

    global_instance.chart->addSeries(global_instance.Dynamic_Gen_Line);
    global_instance.chart->addSeries(global_instance.Dynamic_Load_Line);
//    global_instance.chart->addSeries(global_instance.Wind_Solar_Line);
//    global_instance.chart->addSeries(global_instance.Proc_Fac_Line);
//    global_instance.chart->addSeries(global_instance.Car_Char1_Line);
//    global_instance.chart->addSeries(global_instance.Chemi_Fac_Line);
//    global_instance.chart->addSeries(global_instance.Hydro_Sta_Line);
//    global_instance.chart->addSeries(global_instance.HAN_Line);
//    global_instance.chart->addSeries(global_instance.Car_Char2_Line);
//    global_instance.chart->addSeries(global_instance.T_House1_Line);
//    global_instance.chart->addSeries(global_instance.T_House2_Line);
//    global_instance.chart->addSeries(global_instance.T_House3_Line);

    global_instance.chart->createDefaultAxes();
    ///global_instance.chart->axes(Qt::Vertical).first()->setRange(0, 33);
    ///global_instance.chart->setMargins(QMargins(0,0,0,0));

    global_instance.chartView = new QChartView(global_instance.chart);
    this->resize(1200,720);
    this->setCentralWidget(global_instance.chartView);
    global_instance.chartView->setRenderHint(QPainter::Antialiasing);
//    chart_scene=new QGraphicsScene;
//    chart_scene->setSceneRect(0,0,this->width(),this->height());//设置scene起始坐标和长宽
}


void dynamic_chart::connectMarkers()
{
    // Connect all markers to handler
    const auto markers = global_instance.chart->legend()->markers();
    for (QLegendMarker *marker : markers) {
        // Disconnect possible existing connection to avoid multiple connections
        disconnect(marker, &QLegendMarker::clicked,this, &dynamic_chart::handleMarkerClicked);
        QObject::connect(marker, &QLegendMarker::clicked, this, &dynamic_chart::handleMarkerClicked);
    }
}


void dynamic_chart::handleMarkerClicked()
{
    if(sender()==NULL){return;}




    QLegendMarker* marker = qobject_cast<QLegendMarker*> (sender());
    Q_ASSERT(marker);
    std::cout<<"labelllll nameeeeeee=========:"<<marker->label().toStdString()<<std::endl;
   /* switch (marker->type())
    {
    case QLegendMarker::LegendMarkerTypeXY:
        {
        // Toggle visibility of series
        marker->series()->setVisible(!marker->series()->isVisible());

        // Turn legend marker back to visible, since hiding series also hides the marker
        // and we don't want it to happen now.
        marker->setVisible(true);
        // Dim the marker, if series is not visible
        qreal alpha = 1.0;

        if (!marker->series()->isVisible())
            alpha = 0.5;

        QColor color;
        QBrush brush = marker->labelBrush();
        color = brush.color();
        color.setAlphaF(alpha);
        brush.setColor(color);
        marker->setLabelBrush(brush);

        brush = marker->brush();
        color = brush.color();
        color.setAlphaF(alpha);
        brush.setColor(color);
        marker->setBrush(brush);

        QPen pen = marker->pen();
        color = pen.color();
        color.setAlphaF(alpha);
        pen.setColor(color);
        marker->setPen(pen);
        break;
        }
    default:
        {
        std::cout << "Unknown marker type";
        break;
        }
    }*/


    //if(global_instance.show_all_gens==false&&(global_instance.Dynamic_Load_Line->isVisible())){global_instance.show_all_gens=false;}
    /*Show sum of gens andall other individual generator modules
    But all loads are blocked including the sum of loads*/

    //===========================================================================
//        if(global_instance.show_all_gens==true&&(global_instance.Dynamic_Load_Line->isVisible())){

//            global_instance.show_all_gens=false;
//             std::cout<<"show all other gens"<<std::endl;
//             global_instance.Dynamic_Gen_Line->setVisible(true);
//             global_instance.Dynamic_Load_Line->setVisible(true);
//             global_instance.Dynamic_Load_Line->hide();

//             global_instance.chart->removeSeries(global_instance.Proc_Fac_Line);
//             global_instance.chart->removeSeries(global_instance.Car_Char1_Line);
//             global_instance.chart->removeSeries(global_instance.Chemi_Fac_Line);
//             global_instance.chart->removeSeries(global_instance.Hydro_Sta_Line);
//             global_instance.chart->removeSeries(global_instance.HAN_Line);
//             global_instance.chart->removeSeries(global_instance.Car_Char2_Line);
//             global_instance.chart->removeSeries(global_instance.T_House1_Line);
//             global_instance.chart->removeSeries(global_instance.T_House2_Line);
//             global_instance.chart->removeSeries(global_instance.T_House3_Line);
//             global_instance.chart->removeSeries(global_instance.T_House4_Line);
//             global_instance.chart->removeSeries(global_instance.T_House5_Line);
//             global_instance.chart->removeSeries(global_instance.T_House6_Line);
//             global_instance.chart->removeSeries(global_instance.House1_Line);
//             global_instance.chart->removeSeries(global_instance.House2_Line);
//             global_instance.chart->removeSeries(global_instance.House3_Line);
//             global_instance.chart->removeSeries(global_instance.House4_Line);
//             global_instance.chart->removeSeries(global_instance.House5_Line);
//             global_instance.chart->removeSeries(global_instance.Stor_House1_Line);
//             global_instance.chart->removeSeries(global_instance.Stor_House2_Line);
//             global_instance.chart->removeSeries(global_instance.Appartment1_Line);
//             global_instance.chart->removeSeries(global_instance.Appartment2_Line);
//             global_instance.chart->removeSeries(global_instance.Appartment3_Line);
//             global_instance.chart->removeSeries(global_instance.Appartment4_Line);
//             global_instance.chart->removeSeries(global_instance.Appartment5_Line);
//             global_instance.chart->removeSeries(global_instance.Appartment6_Line);
//             global_instance.chart->removeSeries(global_instance.Shop_Mall_Line);
//             global_instance.chart->removeSeries(global_instance.Hospital_Line);


//             if(global_instance.Dy_Unknown_MAP.count(1071771887)!=0){
//             global_instance.Coal_Gen_Line->clear();
//             global_instance.Dy_Unknown_MAP.find(1071771887);
//             global_instance.Coal_Gen_List=*global_instance.Dy_Unknown_MAP.find(1071771887);
//             global_instance.Coal_Gen_Line->append(global_instance.Coal_Gen_List);
//             global_instance.chart->addSeries(global_instance.Coal_Gen_Line);
//             global_instance.Coal_Gen_Line->setVisible(true);
//             }
//             if(global_instance.Dy_Unknown_MAP.count(1071741103)!=0){
//             global_instance.Wind_Solar_Line->clear();
//             global_instance.Dy_Unknown_MAP.find(1071741103);
//             global_instance.Wind_Solar_List=*global_instance.Dy_Unknown_MAP.find(1071741103);
//             global_instance.Wind_Solar_Line->append(global_instance.Wind_Solar_List);
//             global_instance.chart->addSeries(global_instance.Wind_Solar_Line);
//             global_instance.Wind_Solar_Line->setVisible(true);
//             }
//             if(global_instance.Dy_Unknown_MAP.count(1842809804)!=0){
//             global_instance.In_Export_Line->clear();
//             global_instance.Dy_Unknown_MAP.find(1842809804);
//             global_instance.In_Export_List=*global_instance.Dy_Unknown_MAP.find(1842809804);
//             global_instance.In_Export_Line->append(global_instance.In_Export_List);
//             global_instance.chart->addSeries(global_instance.In_Export_Line);
//             global_instance.In_Export_Line->setVisible(true);
//             }
//             if(global_instance.Dy_Unknown_MAP.count(1857348684)!=0){
//             global_instance.Nuclear_Line->clear();
//             global_instance.Dy_Unknown_MAP.find(1857348684);
//             global_instance.Nuclear_List=*global_instance.Dy_Unknown_MAP.find(1857348684);
//             global_instance.Nuclear_Line->append(global_instance.Nuclear_List);
//             global_instance.chart->addSeries(global_instance.Nuclear_Line);
//             global_instance.Nuclear_Line->setVisible(true);
//             }
//             if(global_instance.Dy_Unknown_MAP.count(1071766506)!=0){
//             global_instance.Solar_Line->clear();
//             global_instance.Dy_Unknown_MAP.find(1071766506);
//             global_instance.Solar_List=*global_instance.Dy_Unknown_MAP.find(1071766506);
//             global_instance.Solar_Line->append(global_instance.Solar_List);
//             global_instance.chart->addSeries(global_instance.Solar_Line);
//             global_instance.Solar_Line->setVisible(true);
//             }
//             if(global_instance.Dy_Unknown_MAP.count(1071966141)!=0){
//             global_instance.Gas_Gen_Line->clear();
//             global_instance.Dy_Unknown_MAP.find(1071966141);
//             global_instance.Gas_Gen_List=*global_instance.Dy_Unknown_MAP.find(1071966141);
//             global_instance.Gas_Gen_Line->append(global_instance.Gas_Gen_List);
//             global_instance.chart->addSeries(global_instance.Gas_Gen_Line);
//             global_instance.Gas_Gen_Line->setVisible(true);
//             }
//             if(global_instance.Dy_Unknown_MAP.count(2041806607)!=0){
//             global_instance.Volcano_Line->clear();
//             global_instance.Dy_Unknown_MAP.find(2041806607);
//             global_instance.Volcano_List=*global_instance.Dy_Unknown_MAP.find(2041806607);
//             global_instance.Volcano_Line->append(global_instance.Volcano_List);
//             global_instance.chart->addSeries(global_instance.Volcano_Line);
//             global_instance.Volcano_Line->setVisible(true);
//             }

//             global_instance.chart->createDefaultAxes();
//             ///global_instance.chart->axes(Qt::Vertical).first()->setRange(0, ((global_instance.Max_G_Sum)+0.5));
//             ///global_instance.chart->setMargins(QMargins(0,0,0,0));
//             //global_instance.Dynamic_Load_Line->setVisible(false);
//             //global_instance.Dynamic_Gen_Line->setVisible(true);




        //}







    //=====================================================










    if(marker->label()=="GEN_SUM"&&global_instance.show_all_gens==false)
    {
        std::cout<<"show all other Gens:-------------------------------------------"<<std::endl;
        std::cout<<"show all gens:==="<<global_instance.show_all_gens<<std::endl;

         std::cout<<"show all other gens"<<std::endl;
         global_instance.Dynamic_Gen_Line->setVisible(true);
         global_instance.Dynamic_Load_Line->setVisible(true);
         global_instance.Dynamic_Load_Line->hide();

         global_instance.chart->removeSeries(global_instance.Proc_Fac_Line);
         global_instance.chart->removeSeries(global_instance.Car_Char1_Line);
         global_instance.chart->removeSeries(global_instance.Chemi_Fac_Line);
         global_instance.chart->removeSeries(global_instance.Hydro_Sta_Line);
         global_instance.chart->removeSeries(global_instance.HAN_Line);
         global_instance.chart->removeSeries(global_instance.Car_Char2_Line);
         global_instance.chart->removeSeries(global_instance.T_House1_Line);
         global_instance.chart->removeSeries(global_instance.T_House2_Line);
         global_instance.chart->removeSeries(global_instance.T_House3_Line);
         global_instance.chart->removeSeries(global_instance.T_House4_Line);
         global_instance.chart->removeSeries(global_instance.T_House5_Line);
         global_instance.chart->removeSeries(global_instance.T_House6_Line);
         global_instance.chart->removeSeries(global_instance.House1_Line);
         global_instance.chart->removeSeries(global_instance.House2_Line);
         global_instance.chart->removeSeries(global_instance.House3_Line);
         global_instance.chart->removeSeries(global_instance.House4_Line);
         global_instance.chart->removeSeries(global_instance.House5_Line);
         global_instance.chart->removeSeries(global_instance.Stor_House1_Line);
         global_instance.chart->removeSeries(global_instance.Stor_House2_Line);
         global_instance.chart->removeSeries(global_instance.Appartment1_Line);
         global_instance.chart->removeSeries(global_instance.Appartment2_Line);
         global_instance.chart->removeSeries(global_instance.Appartment3_Line);
         global_instance.chart->removeSeries(global_instance.Appartment4_Line);
         global_instance.chart->removeSeries(global_instance.Appartment5_Line);
         global_instance.chart->removeSeries(global_instance.Appartment6_Line);
         global_instance.chart->removeSeries(global_instance.Shop_Mall_Line);
         global_instance.chart->removeSeries(global_instance.Hospital_Line);


         if(global_instance.Dy_Unknown_MAP.count(1071771887)!=0){
         global_instance.Coal_Gen_Line->clear();
         global_instance.Dy_Unknown_MAP.find(1071771887);
         global_instance.Coal_Gen_List=*global_instance.Dy_Unknown_MAP.find(1071771887);
         global_instance.Coal_Gen_Line->append(global_instance.Coal_Gen_List);
         global_instance.chart->addSeries(global_instance.Coal_Gen_Line);
         global_instance.Coal_Gen_Line->setVisible(true);
         }
         if(global_instance.Dy_Unknown_MAP.count(1071741103)!=0){
         global_instance.Wind_Solar_Line->clear();
         global_instance.Dy_Unknown_MAP.find(1071741103);
         global_instance.Wind_Solar_List=*global_instance.Dy_Unknown_MAP.find(1071741103);
         global_instance.Wind_Solar_Line->append(global_instance.Wind_Solar_List);
         global_instance.chart->addSeries(global_instance.Wind_Solar_Line);
         global_instance.Wind_Solar_Line->setVisible(true);
         }

         if(global_instance.Dy_Unknown_MAP.count(1842809804)!=0){
         global_instance.In_Export_Line->clear();
         global_instance.Dy_Unknown_MAP.find(1842809804);
         global_instance.In_Export_List=*global_instance.Dy_Unknown_MAP.find(1842809804);
         global_instance.In_Export_Line->append(global_instance.In_Export_List);
         global_instance.chart->addSeries(global_instance.In_Export_Line);
         global_instance.In_Export_Line->setVisible(true);
         }
         if(global_instance.Dy_Unknown_MAP.count(1857348684)!=0){
         global_instance.Nuclear_Line->clear();
         global_instance.Dy_Unknown_MAP.find(1857348684);
         global_instance.Nuclear_List=*global_instance.Dy_Unknown_MAP.find(1857348684);
         global_instance.Nuclear_Line->append(global_instance.Nuclear_List);
         global_instance.chart->addSeries(global_instance.Nuclear_Line);
         global_instance.Nuclear_Line->setVisible(true);
         }
         if(global_instance.Dy_Unknown_MAP.count(1071766506)!=0){
         global_instance.Solar_Line->clear();
         global_instance.Dy_Unknown_MAP.find(1071766506);
         global_instance.Solar_List=*global_instance.Dy_Unknown_MAP.find(1071766506);
         global_instance.Solar_Line->append(global_instance.Solar_List);
         global_instance.chart->addSeries(global_instance.Solar_Line);
         global_instance.Solar_Line->setVisible(true);
         }
         if(global_instance.Dy_Unknown_MAP.count(1071966141)!=0){
         global_instance.Gas_Gen_Line->clear();
         global_instance.Dy_Unknown_MAP.find(1071966141);
         global_instance.Gas_Gen_List=*global_instance.Dy_Unknown_MAP.find(1071966141);
         global_instance.Gas_Gen_Line->append(global_instance.Gas_Gen_List);
         global_instance.chart->addSeries(global_instance.Gas_Gen_Line);
         global_instance.Gas_Gen_Line->setVisible(true);
         }
         if(global_instance.Dy_Unknown_MAP.count(2041806607)!=0){
         global_instance.Volcano_Line->clear();
         global_instance.Dy_Unknown_MAP.find(2041806607);
         global_instance.Volcano_List=*global_instance.Dy_Unknown_MAP.find(2041806607);
         global_instance.Volcano_Line->append(global_instance.Volcano_List);
         global_instance.chart->addSeries(global_instance.Volcano_Line);
         global_instance.Volcano_Line->setVisible(true);
         }

         global_instance.chart->createDefaultAxes();
         ///global_instance.chart->axes(Qt::Vertical).first()->setRange(0, ((global_instance.Max_G_Sum)+0.5));
         ///global_instance.chart->setMargins(QMargins(0,0,0,0));
         //global_instance.Dynamic_Load_Line->setVisible(false);
         //global_instance.Dynamic_Gen_Line->setVisible(true);
         global_instance.show_all_gens=true;
        return;
    }

    //only show sum of gens and loads and block all other individual modules
    if(marker->label()=="GEN_SUM"&&global_instance.show_all_gens==true)
    {std::cout<<"show Gens and loads sum:-------------------------------------------"<<std::endl;
        std::cout<<"show all gens:==="<<global_instance.show_all_gens<<std::endl;

        global_instance.Dynamic_Gen_Line->setVisible(true);
        global_instance.Dynamic_Load_Line->setVisible(true);
        global_instance.chart->removeSeries(global_instance.Solar_Line);
        global_instance.chart->removeSeries(global_instance.Nuclear_Line);
        global_instance.chart->removeSeries(global_instance.In_Export_Line);
        global_instance.chart->removeSeries(global_instance.Wind_Solar_Line);
        global_instance.chart->removeSeries(global_instance.Coal_Gen_Line);
        global_instance.chart->removeSeries(global_instance.Gas_Gen_Line);
        global_instance.chart->removeSeries(global_instance.Volcano_Line);

        global_instance.chart->removeSeries(global_instance.Proc_Fac_Line);
        global_instance.chart->removeSeries(global_instance.Car_Char1_Line);
        global_instance.chart->removeSeries(global_instance.Chemi_Fac_Line);
        global_instance.chart->removeSeries(global_instance.Hydro_Sta_Line);
        global_instance.chart->removeSeries(global_instance.HAN_Line);
        global_instance.chart->removeSeries(global_instance.Car_Char2_Line);
        global_instance.chart->removeSeries(global_instance.T_House1_Line);
        global_instance.chart->removeSeries(global_instance.T_House2_Line);
        global_instance.chart->removeSeries(global_instance.T_House3_Line);
        global_instance.chart->removeSeries(global_instance.T_House4_Line);
        global_instance.chart->removeSeries(global_instance.T_House5_Line);
        global_instance.chart->removeSeries(global_instance.T_House6_Line);
        global_instance.chart->removeSeries(global_instance.House1_Line);
        global_instance.chart->removeSeries(global_instance.House2_Line);
        global_instance.chart->removeSeries(global_instance.House3_Line);
        global_instance.chart->removeSeries(global_instance.House4_Line);
        global_instance.chart->removeSeries(global_instance.House5_Line);
        global_instance.chart->removeSeries(global_instance.Stor_House1_Line);
        global_instance.chart->removeSeries(global_instance.Stor_House2_Line);
        global_instance.chart->removeSeries(global_instance.Appartment1_Line);
        global_instance.chart->removeSeries(global_instance.Appartment2_Line);
        global_instance.chart->removeSeries(global_instance.Appartment3_Line);
        global_instance.chart->removeSeries(global_instance.Appartment4_Line);
        global_instance.chart->removeSeries(global_instance.Appartment5_Line);
        global_instance.chart->removeSeries(global_instance.Appartment6_Line);
        global_instance.chart->removeSeries(global_instance.Shop_Mall_Line);
        global_instance.chart->removeSeries(global_instance.Hospital_Line);
        global_instance.chart->createDefaultAxes();//auto Y axis
        global_instance.chart->setMargins(QMargins(0,0,0,0));
        //global_instance.chart->removeAllSeries
        global_instance.show_all_gens=false;
        return;
    }

    /*Show sum of loads and all other individual load modules
    But all gens are blocked including the sum of gens*/
   if(marker->label()=="LOAD_SUM"&&global_instance.show_all_loads==false)

    {global_instance.show_all_loads=true;
        std::cout<<"show all other loads"<<std::endl;
        global_instance.Dynamic_Load_Line->setVisible(true);
        global_instance.Dynamic_Gen_Line->setVisible(true);
        global_instance.Dynamic_Gen_Line->hide();

        global_instance.chart->removeSeries(global_instance.Solar_Line);
        global_instance.chart->removeSeries(global_instance.Nuclear_Line);
        global_instance.chart->removeSeries(global_instance.In_Export_Line);
        global_instance.chart->removeSeries(global_instance.Wind_Solar_Line);
        global_instance.chart->removeSeries(global_instance.Coal_Gen_Line);
        global_instance.chart->removeSeries(global_instance.Gas_Gen_Line);
        global_instance.chart->removeSeries(global_instance.Volcano_Line);

        //removeSeries


//        global_instance.Solar_Line->setVisible(false);
//        global_instance.Nuclear_Line->setVisible(false);
//        global_instance.In_Export_Line->setVisible(false);
//        global_instance.Wind_Solar_Line->setVisible(false);
//        global_instance.Coal_Gen_Line->setVisible(false);
//        global_instance.Gas_Gen_Line->setVisible(false);
//        global_instance.Volcano_Line->setVisible(false);



        if(global_instance.Dy_Unknown_MAP.count(1072066624)!=0){
            global_instance.Proc_Fac_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1072066624);
            global_instance.Proc_Fac_List=*global_instance.Dy_Unknown_MAP.find(1072066624);
            global_instance.Proc_Fac_Line->append(global_instance.Proc_Fac_List);
            global_instance.chart->addSeries(global_instance.Proc_Fac_Line);
            global_instance.Proc_Fac_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(1071780135)!=0){
            global_instance.Car_Char1_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1071780135);
            global_instance.Car_Char1_List=*global_instance.Dy_Unknown_MAP.find(1071780135);
            global_instance.Car_Char1_Line->append(global_instance.Car_Char1_List);
            global_instance.chart->addSeries(global_instance.Car_Char1_Line);
            global_instance.Car_Char1_Line->setVisible(true);
        }


        if(global_instance.Dy_Unknown_MAP.count(1072051546)!=0){
            global_instance.Chemi_Fac_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1072051546);
            global_instance.Chemi_Fac_List=*global_instance.Dy_Unknown_MAP.find(1072051546);
            global_instance.Chemi_Fac_Line->append(global_instance.Chemi_Fac_List);
            global_instance.chart->addSeries(global_instance.Chemi_Fac_Line);
            global_instance.Chemi_Fac_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(1071991074)!=0){
            global_instance.Hydro_Sta_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1071991074);
            global_instance.Hydro_Sta_List=*global_instance.Dy_Unknown_MAP.find(1071991074);
            global_instance.Hydro_Sta_Line->append(global_instance.Hydro_Sta_List);
            global_instance.chart->addSeries(global_instance.Hydro_Sta_Line);
            global_instance.Hydro_Sta_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(1071729488)!=0){
            global_instance.HAN_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1071729488);
            global_instance.HAN_List=*global_instance.Dy_Unknown_MAP.find(1071729488);
            global_instance.HAN_Line->append(global_instance.HAN_List);
            global_instance.chart->addSeries(global_instance.HAN_Line);
            global_instance.HAN_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(1071769060)!=0){
            global_instance.Car_Char2_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1071769060);
            global_instance.Car_Char2_List=*global_instance.Dy_Unknown_MAP.find(1071769060);
            global_instance.Car_Char2_Line->append(global_instance.Car_Char2_List);
            global_instance.chart->addSeries(global_instance.Car_Char2_Line);
            global_instance.Car_Char2_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(1071985354)!=0){
            global_instance.T_House1_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1071985354);
            global_instance.T_House1_List=*global_instance.Dy_Unknown_MAP.find(1071985354);
            global_instance.T_House1_Line->append(global_instance.T_House1_List);
            global_instance.chart->addSeries(global_instance.T_House1_Line);
            global_instance.T_House1_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(1072063229)!=0){
            global_instance.T_House2_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1072063229);
            global_instance.T_House2_List=*global_instance.Dy_Unknown_MAP.find(1072063229);
            global_instance.T_House2_Line->append(global_instance.T_House2_List);
            global_instance.chart->addSeries(global_instance.T_House1_Line);
            global_instance.T_House2_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(1072056604)!=0){
            global_instance.T_House3_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1072056604);
            global_instance.T_House1_List=*global_instance.Dy_Unknown_MAP.find(1072056604);
            global_instance.T_House3_Line->append(global_instance.T_House1_List);
            global_instance.chart->addSeries(global_instance.T_House3_Line);
            global_instance.T_House3_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(699168295)!=0){
            global_instance.T_House4_Line->clear();
            global_instance.Dy_Unknown_MAP.find(699168295);
            global_instance.Volcano_List=*global_instance.Dy_Unknown_MAP.find(699168295);
            global_instance.T_House4_Line->append(global_instance.Volcano_List);
            global_instance.chart->addSeries(global_instance.T_House4_Line);
            global_instance.T_House4_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(2305939010)!=0){
            global_instance.T_House5_Line->clear();
            global_instance.Dy_Unknown_MAP.find(2305939010);
            global_instance.T_House5_List=*global_instance.Dy_Unknown_MAP.find(2305939010);
            global_instance.T_House5_Line->append(global_instance.T_House5_List);
            global_instance.chart->addSeries(global_instance.T_House5_Line);
            global_instance.T_House5_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(2836102489)!=0){
            global_instance.T_House6_Line->clear();
            global_instance.Dy_Unknown_MAP.find(2836102489);
            global_instance.T_House6_List=*global_instance.Dy_Unknown_MAP.find(2836102489);
            global_instance.T_House6_Line->append(global_instance.T_House6_List);
            global_instance.chart->addSeries(global_instance.T_House6_Line);
            global_instance.T_House6_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(1071986763)!=0){
            global_instance.House1_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1071986763);
            global_instance.House1_List=*global_instance.Dy_Unknown_MAP.find(1071986763);
            global_instance.House1_Line->append(global_instance.House1_List);
            global_instance.chart->addSeries(global_instance.House1_Line);
            global_instance.House1_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(1072063101)!=0){
            global_instance.House2_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1072063101);
            global_instance.House2_List=*global_instance.Dy_Unknown_MAP.find(1072063101);
            global_instance.House2_Line->append(global_instance.House2_List);
            global_instance.chart->addSeries(global_instance.House2_Line);
            global_instance.House2_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(2298616020)!=0){
            global_instance.House3_Line->clear();
            global_instance.Dy_Unknown_MAP.find(2298616020);
            global_instance.House3_List=*global_instance.Dy_Unknown_MAP.find(2298616020);
            global_instance.House3_Line->append(global_instance.House3_List);
            global_instance.chart->addSeries(global_instance.House3_Line);
            global_instance.House3_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(1071733508)!=0){
            global_instance.House4_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1071733508);
            global_instance.House4_List=*global_instance.Dy_Unknown_MAP.find(1071733508);
            global_instance.House4_Line->append(global_instance.House4_List);
            global_instance.chart->addSeries(global_instance.House4_Line);
            global_instance.House4_Line->setVisible(true);
        }


        if(global_instance.Dy_Unknown_MAP.count(3914547211)!=0){
            global_instance.House5_Line->clear();
            global_instance.Dy_Unknown_MAP.find(3914547211);
            global_instance.House5_List=*global_instance.Dy_Unknown_MAP.find(3914547211);
            global_instance.House5_Line->append(global_instance.House5_List);
            global_instance.chart->addSeries(global_instance.House5_Line);
            global_instance.House5_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(1071773816)!=0){
            global_instance.Stor_House1_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1071773816);
            global_instance.Stor_House1_List=*global_instance.Dy_Unknown_MAP.find(1071773816);
            global_instance.Stor_House1_Line->append(global_instance.Stor_House1_List);
            global_instance.chart->addSeries(global_instance.Stor_House1_Line);
            global_instance.Stor_House1_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(1071780377)!=0){
            global_instance.Stor_House2_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1071780377);
            global_instance.Stor_House2_List=*global_instance.Dy_Unknown_MAP.find(1071780377);
            global_instance.Stor_House2_Line->append(global_instance.Stor_House2_List);
            global_instance.chart->addSeries(global_instance.Stor_House2_Line);
            global_instance.Stor_House2_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(1071770985)!=0){
            global_instance.Appartment1_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1071770985);
            global_instance.Appartment1_List=*global_instance.Dy_Unknown_MAP.find(1071770985);
            global_instance.Appartment1_Line->append(global_instance.Appartment1_List);
            global_instance.chart->addSeries(global_instance.Appartment1_Line);
            global_instance.Appartment1_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(7304921)!=0){
            global_instance.Appartment2_Line->clear();
            global_instance.Dy_Unknown_MAP.find(7304921);
            global_instance.Appartment2_List=*global_instance.Dy_Unknown_MAP.find(7304921);
            global_instance.Appartment2_Line->append(global_instance.Appartment2_List);
            global_instance.chart->addSeries(global_instance.Appartment2_Line);
            global_instance.Appartment2_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(593072732)!=0){
            global_instance.Appartment3_Line->clear();
            global_instance.Dy_Unknown_MAP.find(593072732);
            global_instance.Appartment3_List=*global_instance.Dy_Unknown_MAP.find(593072732);
            global_instance.Appartment3_Line->append(global_instance.Appartment3_List);
            global_instance.chart->addSeries(global_instance.Appartment3_Line);
            global_instance.Appartment3_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(1071753108)!=0){
            global_instance.Appartment4_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1071753108);
            global_instance.Appartment4_List=*global_instance.Dy_Unknown_MAP.find(1071753108);
            global_instance.Appartment4_Line->append(global_instance.Appartment4_List);
            global_instance.chart->addSeries(global_instance.Appartment4_Line);
            global_instance.Appartment4_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(2836099420)!=0){
            global_instance.Appartment5_Line->clear();
            global_instance.Dy_Unknown_MAP.find(2836099420);
            global_instance.Appartment5_List=*global_instance.Dy_Unknown_MAP.find(2836099420);
            global_instance.Appartment5_Line->append(global_instance.Appartment5_List);
            global_instance.chart->addSeries(global_instance.Appartment5_Line);
            global_instance.Appartment5_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(1840834860)!=0){
            global_instance.Appartment6_Line->clear();
            global_instance.Dy_Unknown_MAP.find(1840834860);
            global_instance.Appartment6_List=*global_instance.Dy_Unknown_MAP.find(1840834860);
            global_instance.Appartment6_Line->append(global_instance.Appartment6_List);
            global_instance.chart->addSeries(global_instance.Appartment6_Line);
            global_instance.Appartment6_Line->setVisible(true);

        }

        if(global_instance.Dy_Unknown_MAP.count(597999943)!=0){
            global_instance.Shop_Mall_Line->clear();
            global_instance.Dy_Unknown_MAP.find(597999943);
            global_instance.Shop_Mall_List=*global_instance.Dy_Unknown_MAP.find(597999943);
            global_instance.Shop_Mall_Line->append(global_instance.Shop_Mall_List);
            global_instance.chart->addSeries(global_instance.Shop_Mall_Line);
            global_instance.Shop_Mall_Line->setVisible(true);
        }

        if(global_instance.Dy_Unknown_MAP.count(8538681)!=0){
            global_instance.Hospital_Line->clear();
            global_instance.Dy_Unknown_MAP.find(8538681);
            global_instance.Hospital_List=*global_instance.Dy_Unknown_MAP.find(8538681);
            global_instance.Hospital_Line->append(global_instance.Hospital_List);
            global_instance.chart->addSeries(global_instance.Hospital_Line);
            global_instance.Hospital_Line->setVisible(true);
        }

        global_instance.chart->createDefaultAxes();
        ///global_instance.chart->axes(Qt::Vertical).first()->setRange(0, ((global_instance.Max_L_Sum)+0.5));
        ///global_instance.chart->setMargins(QMargins(0,0,0,0));

        return;
   }

   //only show sum of gens and loads and block all other individual modules
   if(marker->label()=="LOAD_SUM"&&global_instance.show_all_loads==true)
   {
       global_instance.show_all_loads=false;
       global_instance.Dynamic_Load_Line->setVisible(true);
       global_instance.Dynamic_Gen_Line->setVisible(true);


       global_instance.chart->removeSeries(global_instance.Solar_Line);
       global_instance.chart->removeSeries(global_instance.Nuclear_Line);
       global_instance.chart->removeSeries(global_instance.In_Export_Line);
       global_instance.chart->removeSeries(global_instance.Wind_Solar_Line);
       global_instance.chart->removeSeries(global_instance.Coal_Gen_Line);
       global_instance.chart->removeSeries(global_instance.Gas_Gen_Line);
       global_instance.chart->removeSeries(global_instance.Volcano_Line);
       global_instance.chart->removeSeries(global_instance.Proc_Fac_Line);
       global_instance.chart->removeSeries(global_instance.Car_Char1_Line);
       global_instance.chart->removeSeries(global_instance.Chemi_Fac_Line);
       global_instance.chart->removeSeries(global_instance.Hydro_Sta_Line);
       global_instance.chart->removeSeries(global_instance.HAN_Line);
       global_instance.chart->removeSeries(global_instance.Car_Char2_Line);
       global_instance.chart->removeSeries(global_instance.T_House1_Line);
       global_instance.chart->removeSeries(global_instance.T_House2_Line);
       global_instance.chart->removeSeries(global_instance.T_House3_Line);
       global_instance.chart->removeSeries(global_instance.T_House4_Line);
       global_instance.chart->removeSeries(global_instance.T_House5_Line);
       global_instance.chart->removeSeries(global_instance.T_House6_Line);
       global_instance.chart->removeSeries(global_instance.House1_Line);
       global_instance.chart->removeSeries(global_instance.House2_Line);
       global_instance.chart->removeSeries(global_instance.House3_Line);
       global_instance.chart->removeSeries(global_instance.House4_Line);
       global_instance.chart->removeSeries(global_instance.House5_Line);
       global_instance.chart->removeSeries(global_instance.Stor_House1_Line);
       global_instance.chart->removeSeries(global_instance.Stor_House2_Line);
       global_instance.chart->removeSeries(global_instance.Appartment1_Line);
       global_instance.chart->removeSeries(global_instance.Appartment2_Line);
       global_instance.chart->removeSeries(global_instance.Appartment3_Line);
       global_instance.chart->removeSeries(global_instance.Appartment4_Line);
       global_instance.chart->removeSeries(global_instance.Appartment5_Line);
       global_instance.chart->removeSeries(global_instance.Appartment6_Line);
       global_instance.chart->removeSeries(global_instance.Shop_Mall_Line);
       global_instance.chart->removeSeries(global_instance.Hospital_Line);

       global_instance.chart->createDefaultAxes();
       global_instance.chart->setMargins(QMargins(0,0,0,0));
       std::cout<<"show sum"<<std::endl;
       return;
   }
}

void dynamic_chart::chart_data_change(){
//    QList<QPointF>::iterator i;
//        for (i = global_instance.Chemi_Fac_List.begin(); i != global_instance.Chemi_Fac_List.end(); ++i){
//            std::cout<<"House3_List:"<<std::endl;
//            qDebug() << *i;}

    //if(global_instance.Dynamic_Gen_Received==true){
        global_instance.chart->removeSeries(global_instance.Dynamic_Gen_Line);
        global_instance.Dynamic_Gen_Line->clear();
        global_instance.Dynamic_Gen_Line->append(global_instance.Dynamic_Gen_List);
        global_instance.chart->addSeries(global_instance.Dynamic_Gen_Line);

        ///global_instance.chart->axes(Qt::Vertical).first()->setRange(0, ((global_instance.Max_G_Sum)+0.5));
std::cout<<"show gens sum!!!!!!!!!!!!!!"<<std::endl;
        connectMarkers();
    //}
    //if(global_instance.Dynamic_Load_Received==true){
        global_instance.chart->removeSeries(global_instance.Dynamic_Load_Line);
        global_instance.Dynamic_Load_Line->clear();
        global_instance.Dynamic_Load_Line->append(global_instance.Dynamic_Load_List);
        global_instance.chart->addSeries(global_instance.Dynamic_Load_Line);
        ///global_instance.chart->axes(Qt::Vertical).first()->setRange(0, ((global_instance.Max_L_Sum)+0.5));
std::cout<<"show loads sum!!!!!!!!!!!!!!"<<std::endl;
        connectMarkers();
   // }

        global_instance.Dynamic_Load_Line->setVisible(true);

        //global_instance.Dynamic_Gen_Line->setVisible(true);
    global_instance.chart->createDefaultAxes();
    //global_instance.chart->axes(Qt::Vertical).first()->setRange(0, ((global_instance.Max_G_Sum)+0.5));
    ///global_instance.chart->setMargins(QMargins(0,0,0,0));
    //connectMarkers();




/*    if(global_instance.Dy_Unknown_MAP.count(1842809804)!=0){
    global_instance.In_Export_Line->clear();
    global_instance.Dy_Unknown_MAP.find(1842809804);
    global_instance.In_Export_List=*global_instance.Dy_Unknown_MAP.find(1842809804);
    global_instance.In_Export_Line->append(global_instance.In_Export_List);
    global_instance.chart->addSeries(global_instance.In_Export_Line);
    global_instance.In_Export_Line->setVisible(false);
    }
    if(global_instance.Dy_Unknown_MAP.count(1857348684)!=0){
    global_instance.Nuclear_Line->clear();
    global_instance.Dy_Unknown_MAP.find(1857348684);
    global_instance.Nuclear_List=*global_instance.Dy_Unknown_MAP.find(1857348684);
    global_instance.Nuclear_Line->append(global_instance.Nuclear_List);
    global_instance.chart->addSeries(global_instance.Nuclear_Line);
    global_instance.Nuclear_Line->setVisible(false);
    }
    if(global_instance.Dy_Unknown_MAP.count(1071766506)!=0){
    global_instance.Solar_Line->clear();
    global_instance.Dy_Unknown_MAP.find(1071766506);
    global_instance.Solar_List=*global_instance.Dy_Unknown_MAP.find(1071766506);
    global_instance.Solar_Line->append(global_instance.Solar_List);
    global_instance.chart->addSeries(global_instance.Solar_Line);
    global_instance.Solar_Line->setVisible(false);
    }
    if(global_instance.Dy_Unknown_MAP.count(1071966141)!=0){
    global_instance.Gas_Gen_Line->clear();
    global_instance.Dy_Unknown_MAP.find(1071966141);
    global_instance.Gas_Gen_List=*global_instance.Dy_Unknown_MAP.find(1071966141);
    global_instance.Gas_Gen_Line->append(global_instance.Gas_Gen_List);
    global_instance.chart->addSeries(global_instance.Gas_Gen_Line);
    global_instance.Gas_Gen_Line->setVisible(false);
    }
    if(global_instance.Dy_Unknown_MAP.count(2041806607)!=0){
    global_instance.Volcano_Line->clear();
    global_instance.Dy_Unknown_MAP.find(2041806607);
    global_instance.Volcano_List=*global_instance.Dy_Unknown_MAP.find(2041806607);
    global_instance.Volcano_Line->append(global_instance.Volcano_List);
    global_instance.chart->addSeries(global_instance.Volcano_Line);
    global_instance.Volcano_Line->setVisible(false);
    }




    if(global_instance.Dy_Unknown_MAP.count(1072066624)!=0){
        global_instance.Proc_Fac_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1072066624);
        global_instance.Proc_Fac_List=*global_instance.Dy_Unknown_MAP.find(1072066624);
        global_instance.Proc_Fac_Line->append(global_instance.Proc_Fac_List);
        global_instance.chart->addSeries(global_instance.Proc_Fac_Line);
        global_instance.Proc_Fac_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(1071780135)!=0){
        global_instance.Car_Char1_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1071780135);
        global_instance.Car_Char1_List=*global_instance.Dy_Unknown_MAP.find(1071780135);
        global_instance.Car_Char1_Line->append(global_instance.Car_Char1_List);
        global_instance.chart->addSeries(global_instance.Car_Char1_Line);
        global_instance.Car_Char1_Line->setVisible(false);
    }


    if(global_instance.Dy_Unknown_MAP.count(1072051546)!=0){
        global_instance.Chemi_Fac_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1072051546);
        global_instance.Chemi_Fac_List=*global_instance.Dy_Unknown_MAP.find(1072051546);
        global_instance.Chemi_Fac_Line->append(global_instance.Chemi_Fac_List);
        global_instance.chart->addSeries(global_instance.Chemi_Fac_Line);
        global_instance.Chemi_Fac_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(1071991074)!=0){
        global_instance.Hydro_Sta_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1071991074);
        global_instance.Hydro_Sta_List=*global_instance.Dy_Unknown_MAP.find(1071991074);
        global_instance.Hydro_Sta_Line->append(global_instance.Hydro_Sta_List);
        global_instance.chart->addSeries(global_instance.Hydro_Sta_Line);
        global_instance.Hydro_Sta_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(1071729488)!=0){
        global_instance.HAN_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1071729488);
        global_instance.HAN_List=*global_instance.Dy_Unknown_MAP.find(1071729488);
        global_instance.HAN_Line->append(global_instance.HAN_List);
        global_instance.chart->addSeries(global_instance.HAN_Line);
        global_instance.HAN_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(1071769060)!=0){
        global_instance.Car_Char2_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1071769060);
        global_instance.Car_Char2_List=*global_instance.Dy_Unknown_MAP.find(1071769060);
        global_instance.Car_Char2_Line->append(global_instance.Car_Char2_List);
        global_instance.chart->addSeries(global_instance.Car_Char2_Line);
        global_instance.Car_Char2_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(1071985354)!=0){
        global_instance.T_House1_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1071985354);
        global_instance.T_House1_List=*global_instance.Dy_Unknown_MAP.find(1071985354);
        global_instance.T_House1_Line->append(global_instance.T_House1_List);
        global_instance.chart->addSeries(global_instance.T_House1_Line);
        global_instance.T_House1_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(1072063229)!=0){
        global_instance.T_House2_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1072063229);
        global_instance.T_House2_List=*global_instance.Dy_Unknown_MAP.find(1072063229);
        global_instance.T_House2_Line->append(global_instance.T_House2_List);
        global_instance.chart->addSeries(global_instance.T_House1_Line);
        global_instance.T_House2_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(1072056604)!=0){
        global_instance.T_House3_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1072056604);
        global_instance.T_House1_List=*global_instance.Dy_Unknown_MAP.find(1072056604);
        global_instance.T_House3_Line->append(global_instance.T_House1_List);
        global_instance.chart->addSeries(global_instance.T_House3_Line);
        global_instance.T_House3_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(699168295)!=0){
        global_instance.T_House4_Line->clear();
        global_instance.Dy_Unknown_MAP.find(699168295);
        global_instance.Volcano_List=*global_instance.Dy_Unknown_MAP.find(699168295);
        global_instance.T_House4_Line->append(global_instance.Volcano_List);
        global_instance.chart->addSeries(global_instance.T_House4_Line);
        global_instance.T_House4_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(2305939010)!=0){
        global_instance.T_House5_Line->clear();
        global_instance.Dy_Unknown_MAP.find(2305939010);
        global_instance.T_House5_List=*global_instance.Dy_Unknown_MAP.find(2305939010);
        global_instance.T_House5_Line->append(global_instance.T_House5_List);
        global_instance.chart->addSeries(global_instance.T_House5_Line);
        global_instance.T_House5_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(2836102489)!=0){
        global_instance.T_House6_Line->clear();
        global_instance.Dy_Unknown_MAP.find(2836102489);
        global_instance.T_House6_List=*global_instance.Dy_Unknown_MAP.find(2836102489);
        global_instance.T_House6_Line->append(global_instance.T_House6_List);
        global_instance.chart->addSeries(global_instance.T_House6_Line);
        global_instance.T_House6_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(1071986763)!=0){
        global_instance.House1_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1071986763);
        global_instance.House1_List=*global_instance.Dy_Unknown_MAP.find(1071986763);
        global_instance.House1_Line->append(global_instance.House1_List);
        global_instance.chart->addSeries(global_instance.House1_Line);
        global_instance.House1_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(1072063101)!=0){
        global_instance.House2_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1072063101);
        global_instance.House2_List=*global_instance.Dy_Unknown_MAP.find(1072063101);
        global_instance.House2_Line->append(global_instance.House2_List);
        global_instance.chart->addSeries(global_instance.House2_Line);
        global_instance.House2_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(2298616020)!=0){
        global_instance.House3_Line->clear();
        global_instance.Dy_Unknown_MAP.find(2298616020);
        global_instance.House3_List=*global_instance.Dy_Unknown_MAP.find(2298616020);
        global_instance.House3_Line->append(global_instance.House3_List);
        global_instance.chart->addSeries(global_instance.House3_Line);
        global_instance.House3_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(1071733508)!=0){
        global_instance.House4_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1071733508);
        global_instance.House4_List=*global_instance.Dy_Unknown_MAP.find(1071733508);
        global_instance.House4_Line->append(global_instance.House4_List);
        global_instance.chart->addSeries(global_instance.House4_Line);
        global_instance.House4_Line->setVisible(false);
    }


    if(global_instance.Dy_Unknown_MAP.count(3914547211)!=0){
        global_instance.House5_Line->clear();
        global_instance.Dy_Unknown_MAP.find(3914547211);
        global_instance.House5_List=*global_instance.Dy_Unknown_MAP.find(3914547211);
        global_instance.House5_Line->append(global_instance.House5_List);
        global_instance.chart->addSeries(global_instance.House5_Line);
        global_instance.House5_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(1071773816)!=0){
        global_instance.Stor_House1_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1071773816);
        global_instance.Stor_House1_List=*global_instance.Dy_Unknown_MAP.find(1071773816);
        global_instance.Stor_House1_Line->append(global_instance.Stor_House1_List);
        global_instance.chart->addSeries(global_instance.Stor_House1_Line);
        global_instance.Stor_House1_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(1071780377)!=0){
        global_instance.Stor_House2_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1071780377);
        global_instance.Stor_House2_List=*global_instance.Dy_Unknown_MAP.find(1071780377);
        global_instance.Stor_House2_Line->append(global_instance.Stor_House2_List);
        global_instance.chart->addSeries(global_instance.Stor_House2_Line);
        global_instance.Stor_House2_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(1071770985)!=0){
        global_instance.Appartment1_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1071770985);
        global_instance.Appartment1_List=*global_instance.Dy_Unknown_MAP.find(1071770985);
        global_instance.Appartment1_Line->append(global_instance.Appartment1_List);
        global_instance.chart->addSeries(global_instance.Appartment1_Line);
        global_instance.Appartment1_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(7304921)!=0){
        global_instance.Appartment2_Line->clear();
        global_instance.Dy_Unknown_MAP.find(7304921);
        global_instance.Appartment2_List=*global_instance.Dy_Unknown_MAP.find(7304921);
        global_instance.Appartment2_Line->append(global_instance.Appartment2_List);
        global_instance.chart->addSeries(global_instance.Appartment2_Line);
        global_instance.Appartment2_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(593072732)!=0){
        global_instance.Appartment3_Line->clear();
        global_instance.Dy_Unknown_MAP.find(593072732);
        global_instance.Appartment3_List=*global_instance.Dy_Unknown_MAP.find(593072732);
        global_instance.Appartment3_Line->append(global_instance.Appartment3_List);
        global_instance.chart->addSeries(global_instance.Appartment3_Line);
        global_instance.Appartment3_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(1071753108)!=0){
        global_instance.Appartment4_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1071753108);
        global_instance.Appartment4_List=*global_instance.Dy_Unknown_MAP.find(1071753108);
        global_instance.Appartment4_Line->append(global_instance.Appartment4_List);
        global_instance.chart->addSeries(global_instance.Appartment4_Line);
        global_instance.Appartment4_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(2836099420)!=0){
        global_instance.Appartment5_Line->clear();
        global_instance.Dy_Unknown_MAP.find(2836099420);
        global_instance.Appartment5_List=*global_instance.Dy_Unknown_MAP.find(2836099420);
        global_instance.Appartment5_Line->append(global_instance.Appartment5_List);
        global_instance.chart->addSeries(global_instance.Appartment5_Line);
        global_instance.Appartment5_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(1840834860)!=0){
        global_instance.Appartment6_Line->clear();
        global_instance.Dy_Unknown_MAP.find(1840834860);
        global_instance.Appartment6_List=*global_instance.Dy_Unknown_MAP.find(1840834860);
        global_instance.Appartment6_Line->append(global_instance.Appartment6_List);
        global_instance.chart->addSeries(global_instance.Appartment6_Line);
        global_instance.Appartment6_Line->setVisible(false);

    }

    if(global_instance.Dy_Unknown_MAP.count(597999943)!=0){
        global_instance.Shop_Mall_Line->clear();
        global_instance.Dy_Unknown_MAP.find(597999943);
        global_instance.Shop_Mall_List=*global_instance.Dy_Unknown_MAP.find(597999943);
        global_instance.Shop_Mall_Line->append(global_instance.Shop_Mall_List);
        global_instance.chart->addSeries(global_instance.Shop_Mall_Line);
        global_instance.Shop_Mall_Line->setVisible(false);
    }

    if(global_instance.Dy_Unknown_MAP.count(8538681)!=0){
        global_instance.Hospital_Line->clear();
        global_instance.Dy_Unknown_MAP.find(8538681);
        global_instance.Hospital_List=*global_instance.Dy_Unknown_MAP.find(8538681);
        global_instance.Hospital_Line->append(global_instance.Hospital_List);
        global_instance.chart->addSeries(global_instance.Hospital_Line);
        global_instance.Hospital_Line->setVisible(false);
    }



*/







}

global_tables::global_tables()
{
}
