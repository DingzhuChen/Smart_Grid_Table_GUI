QT += widgets
requires(qtConfig(fontcombobox))
QT += charts
greaterThan(QT_MAJOR_VERSION, 4): QT += charts
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
SOURCES += main.cpp \
    Global_Models.cpp \
    global_items.cpp \
    Global_Modules.cpp \
    mainwindow.cpp \
    qpixmapitem.cpp \
    slidersgroup.cpp \


SOURCES += customproxy.cpp
HEADERS += customproxy.h \
    Global_Models.h \
    Slider_Window.h \
    Global_Modules.h \
    global_items.h \
    mainwindow.h \
    qpixmapitem.h \
    slidersgroup.h \







build_all:!build_pass {
    CONFIG -= build_all
    CONFIG += release
}

# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/graphicsview
INSTALLS += target


LIBS += -L$$PWD/Libs/Mqtt/x64/lib/ -lQt5Mqttd
INCLUDEPATH += $$PWD/Libs/Mqtt/include

win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/Libs/Mqtt/x64/lib/ -lQt5Mqttd
