/******************************************************************************
 * Project    : Smart Grid Table GUI
 * Author     : Dingzhu Chen
 *
 * Description: header file for all model icon classes of all RFID
                position on all table
******************************************************************************/
#ifndef GLOBAL_MODELS_H
#define GLOBAL_MODELS_H
#include <QGraphicsItem>
#include <QRectF>
#include <QPixmap>
#include <QPainter>
#include <qpixmapitem.h>
#include <QGraphicsScene>
#include <QPlainTextEdit>
#include "global_items.h"
#include <QGraphicsScene>
#include <QIcon>
#include <QPalette>
#include "qpixmapitem.h"
#include <QTimer>
#include <QMouseEvent>
#include <QWidget>
#include <QLine>
#include <QLinef>
#include <QDebug>
#include <QtWidgets>
#include <QTranslator>
#include <QLocale>
#include <QLibraryInfo>
#include <QDebug>
#include <stdlib.h>
//#include "global_tables.h"

class T1_R5_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T1_R5_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T1_R1_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T1_R1_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T3_R0_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T3_R0_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T3_R1_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T3_R1_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T3_R2_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T3_R2_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T3_R3_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T3_R3_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T3_R4_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T3_R4_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T3_R5_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T3_R5_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T3_R6_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T3_R6_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T3_R7_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T3_R7_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T6_R0_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T6_R0_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T6_R1_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T6_R1_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T6_R2_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T6_R2_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T6_R3_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T6_R3_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T6_R4_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T6_R4_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T6_R5_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T6_R5_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T6_R6_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T6_R6_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T6_R7_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T6_R7_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T5_R0_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T5_R0_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T5_R1_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T5_R1_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T5_R2_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T5_R2_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T5_R3_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T5_R3_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T5_R4_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T5_R4_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T5_R5_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T5_R5_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T5_R6_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T5_R6_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T5_R7_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T5_R7_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T4_R0_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T4_R0_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T4_R1_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T4_R1_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T4_R2_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T4_R2_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T4_R3_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T4_R3_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T4_R4_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T4_R4_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T4_R5_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T4_R5_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};


class T4_R6_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T4_R6_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

class T4_R7_Model:public QPixmapItem//(const QString &fileName):QPixmapItem(fileName)
{//Q_OBJECT
public:
        QPlainTextEdit *nameLineEdit;
        int q;
        T4_R7_Model(const QString &fileName,QGraphicsScene *scene);
        //void advance(int phase);
        QPainter *painter;
//public slots:
        void advance(int phase);
        void clear();
};

#endif // GLOBAL_MODELS_H
