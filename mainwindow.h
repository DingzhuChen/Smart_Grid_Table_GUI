/******************************************************************************
 * Project    : Smart Grid Table GUI
 * Author     : Dingzhu Chen
 *
 * Description: header file for mainwindow
******************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QtAlgorithms>
#include <QMainWindow>
#include "customproxy.h"
#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsView>
#include "global_items.h"
#include "Global_Modules.h"
#include<QtMqtt/QMqttClient>
#include <Slider_Window.h>

class mainwindow : public QMainWindow
{Q_OBJECT
public: void show_table2();
    explicit mainwindow(QWidget *parent = nullptr);
    //QGraphicsScene scene;
    void mousePressEvent(QMouseEvent *event);
    //void Publish_P_Set();
    void Change_Json_Gen(QString RFID_Tag_No, double p_nom_max_value);
    void Change_Json_Load(QString RFID_Tag_No, double p_set_value);
    QGraphicsScene *scene;
    double larger=0;
    CustomProxy *proxy;

    //const QString RFID_Tag;
    QString Dynamic_RFID;
    QString Dynamic_RFID_1;
    QString Dynamic_RFID_2;
    QString Dynamic_RFID_3;
    QString Dynamic_RFID_4;
    QString Dynamic_RFID_5;
    QString Dynamic_RFID_6;
    QString Dynamic_RFID_7;
    QString Dynamic_RFID_8;
    QString Dynamic_RFID_9;
    QString Dynamic_RFID_10;
    QString Dynamic_RFID_11;
    QString Dynamic_RFID_12;
    QString Dynamic_RFID_13;
    QString Dynamic_RFID_14;
    QString Dynamic_RFID_15;
    QString Dynamic_RFID_16;
    QString Dynamic_RFID_17;
    QString Dynamic_RFID_18;
    QString Dynamic_RFID_19;
    QString Dynamic_RFID_20;
    QString Dynamic_RFID_21;
    QString Dynamic_RFID_22;
    QString Dynamic_RFID_23;
    QString Dynamic_RFID_24;
    QString Dynamic_RFID_25;
    QString Dynamic_RFID_26;
    QString Dynamic_RFID_27;
    QString Dynamic_RFID_28;
    QString Dynamic_RFID_29;

    QString Model_Type;
    QString RFID_Tag;
    QString command;
    QString table;
    QString RFID;
signals:
    /*signals don't have return value,they can have parameters,
    signal functions don't need implementations, they only need to be declare*/
    void sig_model_placed(int a);
    void p_nom_max(int c);
    void T3R0_p_nom_max(int e);
    void T3R1_p_nom_max(int f);
    void T3R2_p_nom_max(int g);
    void T3R3_p_nom_max(int h);
    void T3R4_p_nom_max(int i);
    void T3R5_p_nom_max(int j);
    void T3R6_p_nom_max(int k);
    void T3R7_p_nom_max(int l);

    void chart_update(int b);
    void T1R1_PNM(int d);

private:
    //const int gridheight = 4;
    //const int gridwidth = 3;
    //QGraphicsScene *scene;
    QGraphicsView *view1;
    QWidget *widget;
    //create a timer
    QTimer *timer_calendar;
    QMqttClient *tables_topic_client;
    QMqttClient *GUI_topic_client;
public slots:
    //void connectMarkers();
    void change_size();
    void timer_Update();//slot function for timer update
    void Dynamic_Chart_Change();
    //void Dynamic_Chart_Switch();

    void P_Nom_Max_True();
    void T3R0_P_Nom_Max_True();
    void T3R1_P_Nom_Max_True();
    void T3R2_P_Nom_Max_True();
    void T3R3_P_Nom_Max_True();
    void T3R4_P_Nom_Max_True();
    void T3R5_P_Nom_Max_True();
    void T3R6_P_Nom_Max_True();
    void T3R7_P_Nom_Max_True();


    void Mainwindow_Data_Change();
    void T1_R5_Pub();
    void T1_R1_Pub();

    void T3_R0_Pub();
    void T3_R1_Pub();
    void T3_R2_Pub();
    void T3_R3_Pub();
    void T3_R4_Pub();
    void T3_R5_Pub();
    void T3_R6_Pub();
    void T3_R7_Pub();

    void T4_R1_Pub();
    void T4_R2_Pub();
    void T4_R3_Pub();
    void T4_R4_Pub();
    void T4_R5_Pub();
    void T4_R6_Pub();
    void T4_R7_Pub();

    void T5_R1_Pub();
    void T5_R2_Pub();
    void T5_R3_Pub();
    void T5_R4_Pub();
    void T5_R5_Pub();
    void T5_R6_Pub();
    void T5_R7_Pub();

    void T6_R1_Pub();
    void T6_R2_Pub();
    void T6_R3_Pub();
    void T6_R4_Pub();
    void T6_R5_Pub();
    void T6_R6_Pub();
    void T6_R7_Pub();
    //void show_layout();

    void emit_model_placed();
void emit_T3R0_p_nom_max();
void emit_T3R1_p_nom_max();
void emit_T3R2_p_nom_max();
void emit_T3R3_p_nom_max();
void emit_T3R4_p_nom_max();
void emit_T3R5_p_nom_max();
void emit_T3R6_p_nom_max();
void emit_T3R7_p_nom_max();

    void emit_chart_update();
void command_Pub();

protected:
    //void mousePressEvent(QMouseEvent *event);
};

#endif // MAINWINDOW_H
