/******************************************************************************
 * Project    : Smart Grid Table GUI
 * Author     : Dingzhu Chen
 *
 * Description: function implementations for mainwindow
******************************************************************************/
#include "mainwindow.h"

/*constructor for main window*/
mainwindow::mainwindow(QWidget *parent)
    : QMainWindow{parent}
{    scene= new QGraphicsScene;
    table1 a;table2 b;table3 c;table4 d;table5 e;table6 f;dynamic_chart g;ComboBox_Window h;Slider_Window i;
    a.Table1 = new table1;
    global_table_instance.TABLE1=a.Table1;
    b.Table2 = new table2;
    global_table_instance.TABLE2=b.Table2;
    c.Table3 = new table3;
    global_table_instance.TABLE3=c.Table3;
    d.Table4 = new table4;
    global_table_instance.TABLE4=d.Table4;
    e.Table5 = new table5;
    global_table_instance.TABLE5=e.Table5;
    f.Table6 = new table6;
    global_table_instance.TABLE6=f.Table6;
    g.Dynamic_Chart= new dynamic_chart;
    global_table_instance.chart=g.Dynamic_Chart;
    h.commands_box=new ComboBox_Window;
    h.commands_box->resize(600, 300);

    global_table_instance.commands_box=h.commands_box;
    i.GUI_slider = new Slider_Window;

    global_table_instance.slider= i.GUI_slider;
    global_table_instance.slider->resize(300,400);

//MQTT STARTED=========================================================================================================
    tables_topic_client = new QMqttClient(this);
    QString const HOST="broker.hivemq.com";
    quint16 PORT= 1883;
    tables_topic_client->setHostname(HOST);
    tables_topic_client->setPort(PORT);
    tables_topic_client->connectToHost();
    QString const subtopic ="SmartDemoTable/+/Outgoing" ;
    QMessageBox::question(this, QLatin1String("sure connection?"), QLatin1String("Sure to connect topic SmartDemoTable/+/Outgoing?"));
    tables_topic_client->subscribe(subtopic,0);

     GUI_topic_client = new QMqttClient(this);
     GUI_topic_client->setHostname(HOST);
     GUI_topic_client->setPort(PORT);
     GUI_topic_client->connectToHost();
     QString const subtopic1 ="SmartDemoTable/GUI/Ingoing" ;
     QMessageBox::question(this, QLatin1String("sure connection?"), QLatin1String("Sure to connect topic SmartDemoTable/GUI/Ingoing?"));
     GUI_topic_client->subscribe(subtopic1);

     connect(GUI_topic_client, &QMqttClient::messageReceived, this, [=](const QByteArray &message, const QMqttTopicName &topic)
     {
        //std::cout<<"{{{{{{{{{{{{{{{{ count()========================: "<<message.split('{').count()<<std::endl;
        if (message.split('{').count()>2)//{qDebug("No content");}
        {
            Model_Type=message.split(':')[0].split('"')[1].split('_')[1];
            int Mes_No =message.split(':').count();
            std::cout<<"Mes_No count()========================: "<<Mes_No<<std::endl;
            switch(Mes_No){
            case 26:
            {Dynamic_RFID=message.split(':')[0].split('"')[1];}
                break;
            case 51:
            {Dynamic_RFID=message.split(':')[0].split('"')[1];
            Dynamic_RFID_1=message.split(':')[25].split('"')[1];}
                break;
            case 76:
            {Dynamic_RFID=message.split(':')[0].split('"')[1];
            Dynamic_RFID_1=message.split(':')[25].split('"')[1];
            Dynamic_RFID_2=message.split(':')[50].split('"')[1];
            }
                break;
            case 101:
            {Dynamic_RFID=message.split(':')[0].split('"')[1];
            Dynamic_RFID_1=message.split(':')[25].split('"')[1];
            Dynamic_RFID_2=message.split(':')[50].split('"')[1];
            Dynamic_RFID_3=message.split(':')[75].split('"')[1];
            }
                break;
            case 126:
            {Dynamic_RFID=message.split(':')[0].split('"')[1];
            Dynamic_RFID_1=message.split(':')[25].split('"')[1];
            Dynamic_RFID_2=message.split(':')[50].split('"')[1];
            Dynamic_RFID_3=message.split(':')[75].split('"')[1];
            Dynamic_RFID_4=message.split(':')[100].split('"')[1];
            }
                break;
            case 151:
            {Dynamic_RFID=message.split(':')[0].split('"')[1];
            Dynamic_RFID_1=message.split(':')[25].split('"')[1];
            Dynamic_RFID_2=message.split(':')[50].split('"')[1];
            Dynamic_RFID_3=message.split(':')[75].split('"')[1];
            Dynamic_RFID_4=message.split(':')[100].split('"')[1];
            Dynamic_RFID_5=message.split(':')[125].split('"')[1];
            }
                break;
            case 176:
            {Dynamic_RFID=message.split(':')[0].split('"')[1];
            Dynamic_RFID_1=message.split(':')[25].split('"')[1];
            Dynamic_RFID_2=message.split(':')[50].split('"')[1];
            Dynamic_RFID_3=message.split(':')[75].split('"')[1];
            Dynamic_RFID_4=message.split(':')[100].split('"')[1];
            Dynamic_RFID_5=message.split(':')[125].split('"')[1];
            Dynamic_RFID_6=message.split(':')[150].split('"')[1];

            }
                break;
            case 201:
            {Dynamic_RFID=message.split(':')[0].split('"')[1];
            Dynamic_RFID_1=message.split(':')[25].split('"')[1];
            Dynamic_RFID_2=message.split(':')[50].split('"')[1];
            Dynamic_RFID_3=message.split(':')[75].split('"')[1];
            Dynamic_RFID_4=message.split(':')[100].split('"')[1];
            Dynamic_RFID_5=message.split(':')[125].split('"')[1];
            Dynamic_RFID_6=message.split(':')[150].split('"')[1];
            Dynamic_RFID_7=message.split(':')[175].split('"')[1];
            }
            break;
            case 226:
            {Dynamic_RFID=message.split(':')[0].split('"')[1];
            Dynamic_RFID_1=message.split(':')[25].split('"')[1];
            Dynamic_RFID_2=message.split(':')[50].split('"')[1];
            Dynamic_RFID_3=message.split(':')[75].split('"')[1];
            Dynamic_RFID_4=message.split(':')[100].split('"')[1];
            Dynamic_RFID_5=message.split(':')[125].split('"')[1];
            Dynamic_RFID_6=message.split(':')[150].split('"')[1];
            Dynamic_RFID_7=message.split(':')[175].split('"')[1];
            Dynamic_RFID_8=message.split(':')[200].split('"')[1];
            }
            break;
            case 251:
            {Dynamic_RFID=message.split(':')[0].split('"')[1];
            Dynamic_RFID_1=message.split(':')[25].split('"')[1];
            Dynamic_RFID_2=message.split(':')[50].split('"')[1];
            Dynamic_RFID_3=message.split(':')[75].split('"')[1];
            Dynamic_RFID_4=message.split(':')[100].split('"')[1];
            Dynamic_RFID_5=message.split(':')[125].split('"')[1];
            Dynamic_RFID_6=message.split(':')[150].split('"')[1];
            Dynamic_RFID_7=message.split(':')[175].split('"')[1];
            Dynamic_RFID_8=message.split(':')[200].split('"')[1];
            Dynamic_RFID_9=message.split(':')[225].split('"')[1];
            }
            break;
            case 276:
            {Dynamic_RFID=message.split(':')[0].split('"')[1];
            Dynamic_RFID_1=message.split(':')[25].split('"')[1];
            Dynamic_RFID_2=message.split(':')[50].split('"')[1];
            Dynamic_RFID_3=message.split(':')[75].split('"')[1];
            Dynamic_RFID_4=message.split(':')[100].split('"')[1];
            Dynamic_RFID_5=message.split(':')[125].split('"')[1];
            Dynamic_RFID_6=message.split(':')[150].split('"')[1];
            Dynamic_RFID_7=message.split(':')[175].split('"')[1];
            Dynamic_RFID_8=message.split(':')[200].split('"')[1];
            Dynamic_RFID_9=message.split(':')[225].split('"')[1];
            Dynamic_RFID_10=message.split(':')[250].split('"')[1];
            }
            break;
            case 301:
            {Dynamic_RFID=message.split(':')[0].split('"')[1];
            Dynamic_RFID_1=message.split(':')[25].split('"')[1];
            Dynamic_RFID_2=message.split(':')[50].split('"')[1];
            Dynamic_RFID_3=message.split(':')[75].split('"')[1];
            Dynamic_RFID_4=message.split(':')[100].split('"')[1];
            Dynamic_RFID_5=message.split(':')[125].split('"')[1];
            Dynamic_RFID_6=message.split(':')[150].split('"')[1];
            Dynamic_RFID_7=message.split(':')[175].split('"')[1];
            Dynamic_RFID_8=message.split(':')[200].split('"')[1];
            Dynamic_RFID_9=message.split(':')[225].split('"')[1];
            Dynamic_RFID_10=message.split(':')[250].split('"')[1];
            Dynamic_RFID_11=message.split(':')[275].split('"')[1];
            }
            break;
            case 326:
            {Dynamic_RFID=message.split(':')[0].split('"')[1];
            Dynamic_RFID_1=message.split(':')[25].split('"')[1];
            Dynamic_RFID_2=message.split(':')[50].split('"')[1];
            Dynamic_RFID_3=message.split(':')[75].split('"')[1];
            Dynamic_RFID_4=message.split(':')[100].split('"')[1];
            Dynamic_RFID_5=message.split(':')[125].split('"')[1];
            Dynamic_RFID_6=message.split(':')[150].split('"')[1];
            Dynamic_RFID_7=message.split(':')[175].split('"')[1];
            Dynamic_RFID_8=message.split(':')[200].split('"')[1];
            Dynamic_RFID_9=message.split(':')[225].split('"')[1];
            Dynamic_RFID_10=message.split(':')[250].split('"')[1];
            Dynamic_RFID_11=message.split(':')[275].split('"')[1];
            Dynamic_RFID_12=message.split(':')[300].split('"')[1];
            }
            break;
            default:
                break;
        }
            if(Mes_No>=26){
                global_instance.if_new_received=global_instance.if_new_received+1;
                std::cout<<"the count number is:============="<<global_instance.if_new_received<<std::endl;
                if(global_instance.if_new_received==2){
                    global_instance.show_all_loads=false;
                    global_instance.show_all_gens=false;





                    global_instance.chart->removeSeries(global_instance.Dynamic_Load_Line);
                    global_instance.chart->removeSeries(global_instance.Dynamic_Gen_Line);

                    global_instance.chart->removeSeries(global_instance.Solar_Line);
                    global_instance.chart->removeSeries(global_instance.Nuclear_Line);
                    global_instance.chart->removeSeries(global_instance.In_Export_Line);
                    global_instance.chart->removeSeries(global_instance.Wind_Solar_Line);
                    global_instance.chart->removeSeries(global_instance.Coal_Gen_Line);
                    global_instance.chart->removeSeries(global_instance.Gas_Gen_Line);
                    global_instance.chart->removeSeries(global_instance.Volcano_Line);

                    global_instance.chart->removeSeries(global_instance.Proc_Fac_Line);
                    global_instance.chart->removeSeries(global_instance.Car_Char1_Line);
                    global_instance.chart->removeSeries(global_instance.Chemi_Fac_Line);
                    global_instance.chart->removeSeries(global_instance.Hydro_Sta_Line);
                    global_instance.chart->removeSeries(global_instance.HAN_Line);
                    global_instance.chart->removeSeries(global_instance.Car_Char2_Line);
                    global_instance.chart->removeSeries(global_instance.T_House1_Line);
                    global_instance.chart->removeSeries(global_instance.T_House2_Line);
                    global_instance.chart->removeSeries(global_instance.T_House3_Line);
                    global_instance.chart->removeSeries(global_instance.T_House4_Line);
                    global_instance.chart->removeSeries(global_instance.T_House5_Line);
                    global_instance.chart->removeSeries(global_instance.T_House6_Line);
                    global_instance.chart->removeSeries(global_instance.House1_Line);
                    global_instance.chart->removeSeries(global_instance.House2_Line);
                    global_instance.chart->removeSeries(global_instance.House3_Line);
                    global_instance.chart->removeSeries(global_instance.House4_Line);
                    global_instance.chart->removeSeries(global_instance.House5_Line);
                    global_instance.chart->removeSeries(global_instance.Stor_House1_Line);
                    global_instance.chart->removeSeries(global_instance.Stor_House2_Line);
                    global_instance.chart->removeSeries(global_instance.Appartment1_Line);
                    global_instance.chart->removeSeries(global_instance.Appartment2_Line);
                    global_instance.chart->removeSeries(global_instance.Appartment3_Line);
                    global_instance.chart->removeSeries(global_instance.Appartment4_Line);
                    global_instance.chart->removeSeries(global_instance.Appartment5_Line);
                    global_instance.chart->removeSeries(global_instance.Appartment6_Line);
                    global_instance.chart->removeSeries(global_instance.Shop_Mall_Line);
                    global_instance.chart->removeSeries(global_instance.Hospital_Line);

                    //global_instance.Dy_Unknown_MAP.clear();
                    global_instance.if_new_received=0;
                }
                global_instance.Lable_to_RFID.insert("1071771887_Gen0",1071771887);
                global_instance.Lable_to_RFID.insert("1071741103_Gen0",1071741103);
                global_instance.Lable_to_RFID.insert("1842809804_Gen0",1842809804);
                global_instance.Lable_to_RFID.insert("1071966141_Gen0",1071966141);
                global_instance.Lable_to_RFID.insert("2041806607_Gen0",2041806607);
                global_instance.Lable_to_RFID.insert("1857348684_Gen0",1857348684);
                global_instance.Lable_to_RFID.insert("1071766506_Gen0",1071766506);
                global_instance.Lable_to_RFID.insert("1072066624_Load0",1072066624);
                global_instance.Lable_to_RFID.insert("1071780135_Load0",1071780135);
                global_instance.Lable_to_RFID.insert("1072051546_Load0",1072051546);
                global_instance.Lable_to_RFID.insert("1071991074_Load0",1071991074);
                global_instance.Lable_to_RFID.insert("1071729488_Load0",1071729488);
                global_instance.Lable_to_RFID.insert("1071769060_Load0",1071769060);
                global_instance.Lable_to_RFID.insert("1071985354_Load0",1071985354);
                global_instance.Lable_to_RFID.insert("1072063229_Load0",1072063229);
                global_instance.Lable_to_RFID.insert("1072056604_Load0",1072056604);
                global_instance.Lable_to_RFID.insert("699168295_Load0",699168295);
                global_instance.Lable_to_RFID.insert("2305939010_Load0",2305939010);
                global_instance.Lable_to_RFID.insert("2836102489_Load0",2836102489);
                global_instance.Lable_to_RFID.insert("1071986763_Load0",1071986763);
                global_instance.Lable_to_RFID.insert("1072063101_Load0",1072063101);
                global_instance.Lable_to_RFID.insert("2298616020_Load0",2298616020);
                global_instance.Lable_to_RFID.insert("1071733508_Load0",1071733508);
                global_instance.Lable_to_RFID.insert("3914547211_Load0",3914547211);
                global_instance.Lable_to_RFID.insert("1071773816_Load0",1071773816);
                global_instance.Lable_to_RFID.insert("1071780377_Load0",1071780377);
                global_instance.Lable_to_RFID.insert("1071770985_Load0",1071770985);
                global_instance.Lable_to_RFID.insert("7304921_Load0",7304921);
                global_instance.Lable_to_RFID.insert("593072732_Load0",593072732);
                global_instance.Lable_to_RFID.insert("1071753108_Load0",1071753108);
                global_instance.Lable_to_RFID.insert("2836099420_Load0",2836099420);
                global_instance.Lable_to_RFID.insert("1840834860_Load0",1840834860);
                global_instance.Lable_to_RFID.insert("597999943_Load0",597999943);
                global_instance.Lable_to_RFID.insert("8538681_Load0",8538681);

                global_instance.Dynamic_RFID_No=*global_instance.Lable_to_RFID.find(Dynamic_RFID);
                global_instance.Dynamic_RFID_No_1=*global_instance.Lable_to_RFID.find(Dynamic_RFID_1);
                global_instance.Dynamic_RFID_No_2=*global_instance.Lable_to_RFID.find(Dynamic_RFID_2);
                global_instance.Dynamic_RFID_No_3=*global_instance.Lable_to_RFID.find(Dynamic_RFID_3);
                global_instance.Dynamic_RFID_No_4=*global_instance.Lable_to_RFID.find(Dynamic_RFID_4);
                global_instance.Dynamic_RFID_No_5=*global_instance.Lable_to_RFID.find(Dynamic_RFID_5);
                global_instance.Dynamic_RFID_No_6=*global_instance.Lable_to_RFID.find(Dynamic_RFID_6);
                global_instance.Dynamic_RFID_No_7=*global_instance.Lable_to_RFID.find(Dynamic_RFID_7);
                global_instance.Dynamic_RFID_No_8=*global_instance.Lable_to_RFID.find(Dynamic_RFID_8);
                global_instance.Dynamic_RFID_No_9=*global_instance.Lable_to_RFID.find(Dynamic_RFID_9);
                global_instance.Dynamic_RFID_No_10=*global_instance.Lable_to_RFID.find(Dynamic_RFID_10);
                global_instance.Dynamic_RFID_No_11=*global_instance.Lable_to_RFID.find(Dynamic_RFID_11);
                global_instance.Dynamic_RFID_No_12=*global_instance.Lable_to_RFID.find(Dynamic_RFID_12);
                global_instance.Dynamic_RFID_No_13=*global_instance.Lable_to_RFID.find(Dynamic_RFID_13);
                global_instance.Dynamic_RFID_No_14=*global_instance.Lable_to_RFID.find(Dynamic_RFID_14);
                global_instance.Dynamic_RFID_No_15=*global_instance.Lable_to_RFID.find(Dynamic_RFID_15);
                global_instance.Dynamic_RFID_No_16=*global_instance.Lable_to_RFID.find(Dynamic_RFID_16);
                global_instance.Dynamic_RFID_No_17=*global_instance.Lable_to_RFID.find(Dynamic_RFID_17);
                global_instance.Dynamic_RFID_No_18=*global_instance.Lable_to_RFID.find(Dynamic_RFID_18);
                global_instance.Dynamic_RFID_No_19=*global_instance.Lable_to_RFID.find(Dynamic_RFID_19);
                global_instance.Dynamic_RFID_No_20=*global_instance.Lable_to_RFID.find(Dynamic_RFID_20);

                global_instance.Dynamic_RFID_No_21=*global_instance.Lable_to_RFID.find(Dynamic_RFID_21);
                global_instance.Dynamic_RFID_No_22=*global_instance.Lable_to_RFID.find(Dynamic_RFID_22);
                global_instance.Dynamic_RFID_No_23=*global_instance.Lable_to_RFID.find(Dynamic_RFID_23);
                global_instance.Dynamic_RFID_No_24=*global_instance.Lable_to_RFID.find(Dynamic_RFID_24);
                global_instance.Dynamic_RFID_No_25=*global_instance.Lable_to_RFID.find(Dynamic_RFID_25);
                global_instance.Dynamic_RFID_No_26=*global_instance.Lable_to_RFID.find(Dynamic_RFID_26);
                global_instance.Dynamic_RFID_No_27=*global_instance.Lable_to_RFID.find(Dynamic_RFID_27);
                global_instance.Dynamic_RFID_No_28=*global_instance.Lable_to_RFID.find(Dynamic_RFID_28);
                global_instance.Dynamic_RFID_No_29=*global_instance.Lable_to_RFID.find(Dynamic_RFID_29);



        switch(Mes_No){
            case 26://if(Mes_No==26)
            {
            if(Model_Type=="Gen0"){
                global_instance.Dy_Unknown_MAP.clear();
                global_instance.Dynamic_Gen_Received=true;
                global_instance.Dynamic_Gen_List.clear();
                global_instance.Max_G_Sum=0;
                larger=0;
                std::string s =message.split(':')[2].split(',')[0].toStdString();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+2].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Dynamic_Gen_List.append(QPointF(i,d));
                      if(d > global_instance.Max_L_Sum){larger=d;}
                      else{larger=global_instance.Max_L_Sum;}
                      global_instance.Max_L_Sum = larger;
                  }
                //emit_chart_update();
            }

            if(Model_Type=="Load0"){
                global_instance.Dynamic_Load_Received=true;
                global_instance.Dynamic_Load_List.clear();
                global_instance.Max_L_Sum=0;
                larger=0;
                std::string s =message.split(':')[2].split(',')[0].toStdString();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+2].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Dynamic_Load_List.append(QPointF(i,d));
                      if(d > global_instance.Max_L_Sum){larger=d;}
                      else{larger=global_instance.Max_L_Sum;}
                      global_instance.Max_L_Sum = larger;
                  }
                emit_chart_update();
            }

                global_instance.Unknown_Model_List.clear();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+2].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No, global_instance.Unknown_Model_List);
                //emit_chart_update();

            }
            break;

            case 51://if (Mes_No==51)
            {

            if(Model_Type=="Gen0"){
                global_instance.Dy_Unknown_MAP.clear();
                global_instance.Dynamic_Gen_Received=true;
                global_instance.Dynamic_Gen_List.clear();

                global_instance.Max_G_Sum=0;
                larger=0;
                std::string s =message.split(':')[2].split(',')[0].toStdString();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                    double d_1 =std::stod(data_1);
                    std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                      double d_2 =std::stod(data_2);
                      double sum=d_1+d_2;
                      global_instance.Dynamic_Gen_List.append(QPointF(i,sum));

                      if(sum > global_instance.Max_G_Sum){larger=sum;}
                      else{larger=global_instance.Max_G_Sum;}
                      global_instance.Max_G_Sum = larger;
                  }
                std::cout<<"maximummmmmm valueeee======= :"<<global_instance.Max_G_Sum<<std::endl;
                //emit_chart_update();
            }
            if(Model_Type=="Load0"){
                global_instance.Dynamic_Load_Received=true;
                global_instance.Dynamic_Load_List.clear();
                global_instance.Max_L_Sum=0;
                larger=0;
                std::string s =message.split(':')[2].split(',')[0].toStdString();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                    double d_1 =std::stod(data_1);
                    std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                      double d_2 =std::stod(data_2);
                      double sum=d_1+d_2;
                      global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                      if(sum > global_instance.Max_L_Sum){larger=sum;}
                      else{larger=global_instance.Max_L_Sum;}
                      global_instance.Max_L_Sum = larger;
                  }
                emit_chart_update();
            }

                global_instance.Unknown_Model_List.clear();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+2].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                    {std::string s =message.split(':')[i+27].split(',')[0].toStdString();
                         double d =std::stod(s);
                         global_instance.Unknown_Model_List.append(QPointF(i,d));

                    }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_1, global_instance.Unknown_Model_List);
                //emit_chart_update();

            }
            break;

            case 76://if (Mes_No==76)
            {

            if(Model_Type=="Gen0"){
                global_instance.Dy_Unknown_MAP.clear();
                global_instance.Dynamic_Gen_Received=true;
                global_instance.Dynamic_Gen_List.clear();
                global_instance.Max_G_Sum=0;
                larger=0;
                std::string s =message.split(':')[2].split(',')[0].toStdString();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                    double d_1 =std::stod(data_1);
                    std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                      double d_2 =std::stod(data_2);
                      std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                      double d_3 =std::stod(data_3);
                      double sum=d_1+d_2+d_3;
                      global_instance.Dynamic_Gen_List.append(QPointF(i,sum));
                      if(sum > global_instance.Max_G_Sum){larger=sum;}
                      else{larger=global_instance.Max_G_Sum;}
                      global_instance.Max_G_Sum = larger;
                  }
                //emit_chart_update();
            }
            if(Model_Type=="Load0"){
                global_instance.Dynamic_Load_Received=true;
                global_instance.Dynamic_Load_List.clear();
                global_instance.Max_L_Sum=0;
                larger=0;
                std::string s =message.split(':')[2].split(',')[0].toStdString();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                    double d_1 =std::stod(data_1);
                    std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                      double d_2 =std::stod(data_2);
                      std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                      double sum=d_1+d_2+d_3;
                      global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                      if(sum > global_instance.Max_L_Sum){larger=sum;}
                      else{larger=global_instance.Max_L_Sum;}
                      global_instance.Max_L_Sum = larger;
                  }
                emit_chart_update();
            }

                global_instance.Unknown_Model_List.clear();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+2].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                    {std::string s =message.split(':')[i+27].split(',')[0].toStdString();
                         double d =std::stod(s);
                         global_instance.Unknown_Model_List.append(QPointF(i,d));
                    }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_1, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+52].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_2, global_instance.Unknown_Model_List);
                //emit_chart_update();

            }
            break;

            case 101://if (Mes_No==101)
            {
            if(Model_Type=="Gen0"){
                global_instance.Dy_Unknown_MAP.clear();
                global_instance.Dynamic_Gen_Received=true;
                global_instance.Dynamic_Gen_List.clear();
                global_instance.Max_G_Sum=0;
                larger=0;
                std::string s =message.split(':')[2].split(',')[0].toStdString();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                    double d_1 =std::stod(data_1);
                    std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                    double d_2 =std::stod(data_2);
                    std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                    double d_3 =std::stod(data_3);
                    std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                    double d_4 =std::stod(data_4);
                    double sum=d_1+d_2+d_3+d_4;
                    global_instance.Dynamic_Gen_List.append(QPointF(i,sum));
                    if(sum > global_instance.Max_G_Sum){larger=sum;}
                    else{larger=global_instance.Max_G_Sum;}
                    global_instance.Max_G_Sum = larger;
                  }
                //emit_chart_update();
            }
            if(Model_Type=="Load0"){
                global_instance.Dynamic_Load_Received=true;
                global_instance.Dynamic_Load_List.clear();
                global_instance.Max_L_Sum=0;
                larger=0;
                std::string s =message.split(':')[2].split(',')[0].toStdString();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                    double d_1 =std::stod(data_1);
                    std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                      double d_2 =std::stod(data_2);
                      std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                          double d_4 =std::stod(data_4);
                      double sum=d_1+d_2+d_3+d_4;
                      global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                      if(sum > global_instance.Max_L_Sum){larger=sum;}
                      else{larger=global_instance.Max_L_Sum;}
                      global_instance.Max_L_Sum = larger;
                  }
                emit_chart_update();
            }

                global_instance.Unknown_Model_List.clear();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+2].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                    {std::string s =message.split(':')[i+27].split(',')[0].toStdString();
                         double d =std::stod(s);
                         global_instance.Unknown_Model_List.append(QPointF(i,d));
                    }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_1, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+52].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_2, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+77].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_3, global_instance.Unknown_Model_List);
                //emit_chart_update();

            }
            break;

            case 126://if (Mes_No==101)
            {
            if(Model_Type=="Gen0"){
                global_instance.Dy_Unknown_MAP.clear();
                global_instance.Dynamic_Gen_Received=true;
                global_instance.Dynamic_Gen_List.clear();
                global_instance.Max_G_Sum=0;
                larger=0;
                std::string s =message.split(':')[2].split(',')[0].toStdString();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                    double d_1 =std::stod(data_1);
                    std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                    double d_2 =std::stod(data_2);
                    std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                    double d_3 =std::stod(data_3);
                    std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                    double d_4 =std::stod(data_4);
                    std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                    double d_5 =std::stod(data_5);
                    double sum=d_1+d_2+d_3+d_4+d_5;
                    global_instance.Dynamic_Gen_List.append(QPointF(i,sum));
                    if(sum > global_instance.Max_G_Sum){larger=sum;}
                    else{larger=global_instance.Max_G_Sum;}
                    global_instance.Max_G_Sum = larger;
                  }
               // emit_chart_update();
            }
            if(Model_Type=="Load0"){
                global_instance.Dynamic_Load_Received=true;
                global_instance.Dynamic_Load_List.clear();
                global_instance.Max_L_Sum=0;
                larger=0;
                std::string s =message.split(':')[2].split(',')[0].toStdString();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                    double d_1 =std::stod(data_1);
                    std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                    double d_2 =std::stod(data_2);
                    std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                    double d_3 =std::stod(data_3);
                    std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                    double d_4 =std::stod(data_4);
                    std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                    double d_5 =std::stod(data_5);
                    double sum=d_1+d_2+d_3+d_4+d_5;
                    global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                    if(sum > global_instance.Max_L_Sum){larger=sum;}
                    else{larger=global_instance.Max_L_Sum;}
                    global_instance.Max_L_Sum = larger;
                  }
                emit_chart_update();
            }

                global_instance.Unknown_Model_List.clear();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+2].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                    {std::string s =message.split(':')[i+27].split(',')[0].toStdString();
                         double d =std::stod(s);
                         global_instance.Unknown_Model_List.append(QPointF(i,d));

                    }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_1, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+52].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_2, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+77].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_3, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+102].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_4, global_instance.Unknown_Model_List);
                //emit_chart_update();

            }
            break;

            case 151://if (Mes_No==101)
            {
                global_instance.Unknown_Model_List.clear();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+2].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                    {std::string s =message.split(':')[i+27].split(',')[0].toStdString();
                         double d =std::stod(s);
                         global_instance.Unknown_Model_List.append(QPointF(i,d));

                    }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_1, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+52].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_2, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+77].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_3, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+102].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_4, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+127].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_5, global_instance.Unknown_Model_List);
                //emit_chart_update();

                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 176://if (Mes_No==101)
            {
                global_instance.Unknown_Model_List.clear();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+2].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                    {std::string s =message.split(':')[i+27].split(',')[0].toStdString();
                         double d =std::stod(s);
                         global_instance.Unknown_Model_List.append(QPointF(i,d));

                    }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_1, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+52].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_2, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+77].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_3, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+102].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_4, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+127].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_5, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+152].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_6, global_instance.Unknown_Model_List);
                //emit_chart_update();
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();

                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));

                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 201://if (Mes_No==101)
            {
                global_instance.Unknown_Model_List.clear();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+2].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                    {std::string s =message.split(':')[i+27].split(',')[0].toStdString();
                         double d =std::stod(s);
                         global_instance.Unknown_Model_List.append(QPointF(i,d));

                    }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_1, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+52].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_2, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+77].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_3, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+102].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_4, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+127].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_5, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+152].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_6, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+177].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_7, global_instance.Unknown_Model_List);
                //emit_chart_update();
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();

                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));

                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 226://if (Mes_No==101)
            {
                global_instance.Unknown_Model_List.clear();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+2].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                    {std::string s =message.split(':')[i+27].split(',')[0].toStdString();
                         double d =std::stod(s);
                         global_instance.Unknown_Model_List.append(QPointF(i,d));

                    }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_1, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+52].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_2, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+77].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_3, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+102].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_4, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+127].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_5, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+152].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_6, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+177].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_7, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+202].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_8, global_instance.Unknown_Model_List);
                //emit_chart_update();
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));

                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 251://if (Mes_No==101)
            {
                global_instance.Unknown_Model_List.clear();
                int Number_Size=24;
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+2].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                    {std::string s =message.split(':')[i+27].split(',')[0].toStdString();
                         double d =std::stod(s);
                         global_instance.Unknown_Model_List.append(QPointF(i,d));

                    }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_1, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+52].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_2, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+77].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_3, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+102].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_4, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+127].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_5, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+152].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_6, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+177].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_7, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+202].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_8, global_instance.Unknown_Model_List);
                //emit_chart_update();
                global_instance.Unknown_Model_List.clear();
                for(int i = 0;i<Number_Size;i++)
                  {std::string s =message.split(':')[i+227].split(',')[0].toStdString();
                      double d =std::stod(s);
                      global_instance.Unknown_Model_List.append(QPointF(i,d));
                  }
                global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_9, global_instance.Unknown_Model_List);
                //emit_chart_update();
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 276://if (Mes_No==101)
            {

            global_instance.Unknown_Model_List.clear();
            int Number_Size=24;
            for(int i = 0;i<Number_Size;i++)
              {std::string s =message.split(':')[i+2].split(',')[0].toStdString();
                  double d =std::stod(s);
                  global_instance.Unknown_Model_List.append(QPointF(i,d));
              }
            global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No, global_instance.Unknown_Model_List);
            //emit_chart_update();
            global_instance.Unknown_Model_List.clear();
            for(int i = 0;i<Number_Size;i++)
                {std::string s =message.split(':')[i+27].split(',')[0].toStdString();
                     double d =std::stod(s);
                     global_instance.Unknown_Model_List.append(QPointF(i,d));

                }
            global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_1, global_instance.Unknown_Model_List);
            //emit_chart_update();
            global_instance.Unknown_Model_List.clear();
            for(int i = 0;i<Number_Size;i++)
              {std::string s =message.split(':')[i+52].split(',')[0].toStdString();
                  double d =std::stod(s);
                  global_instance.Unknown_Model_List.append(QPointF(i,d));
              }
            global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_2, global_instance.Unknown_Model_List);
            //emit_chart_update();
            global_instance.Unknown_Model_List.clear();
            for(int i = 0;i<Number_Size;i++)
              {std::string s =message.split(':')[i+77].split(',')[0].toStdString();
                  double d =std::stod(s);
                  global_instance.Unknown_Model_List.append(QPointF(i,d));
              }
            global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_3, global_instance.Unknown_Model_List);
            //emit_chart_update();
            global_instance.Unknown_Model_List.clear();
            for(int i = 0;i<Number_Size;i++)
              {std::string s =message.split(':')[i+102].split(',')[0].toStdString();
                  double d =std::stod(s);
                  global_instance.Unknown_Model_List.append(QPointF(i,d));
              }
            global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_4, global_instance.Unknown_Model_List);
            //emit_chart_update();
            global_instance.Unknown_Model_List.clear();
            for(int i = 0;i<Number_Size;i++)
              {std::string s =message.split(':')[i+127].split(',')[0].toStdString();
                  double d =std::stod(s);
                  global_instance.Unknown_Model_List.append(QPointF(i,d));
              }
            global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_5, global_instance.Unknown_Model_List);
            //emit_chart_update();
            global_instance.Unknown_Model_List.clear();
            for(int i = 0;i<Number_Size;i++)
              {std::string s =message.split(':')[i+152].split(',')[0].toStdString();
                  double d =std::stod(s);
                  global_instance.Unknown_Model_List.append(QPointF(i,d));
              }
            global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_6, global_instance.Unknown_Model_List);
            //emit_chart_update();
            global_instance.Unknown_Model_List.clear();
            for(int i = 0;i<Number_Size;i++)
              {std::string s =message.split(':')[i+177].split(',')[0].toStdString();
                  double d =std::stod(s);
                  global_instance.Unknown_Model_List.append(QPointF(i,d));
              }
            global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_7, global_instance.Unknown_Model_List);
           // emit_chart_update();
            global_instance.Unknown_Model_List.clear();
            for(int i = 0;i<Number_Size;i++)
              {std::string s =message.split(':')[i+202].split(',')[0].toStdString();
                  double d =std::stod(s);
                  global_instance.Unknown_Model_List.append(QPointF(i,d));
              }
            global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_8, global_instance.Unknown_Model_List);
            //emit_chart_update();
            global_instance.Unknown_Model_List.clear();
            for(int i = 0;i<Number_Size;i++)
              {std::string s =message.split(':')[i+227].split(',')[0].toStdString();
                  double d =std::stod(s);
                  global_instance.Unknown_Model_List.append(QPointF(i,d));
              }
            global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_9, global_instance.Unknown_Model_List);
            //emit_chart_update();
            global_instance.Unknown_Model_List.clear();
            for(int i = 0;i<Number_Size;i++)
              {std::string s =message.split(':')[i+252].split(',')[0].toStdString();
                  double d =std::stod(s);
                  global_instance.Unknown_Model_List.append(QPointF(i,d));
              }
            global_instance.Dy_Unknown_MAP.insert(global_instance.Dynamic_RFID_No_10, global_instance.Unknown_Model_List);
            //emit_chart_update();
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 301://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 326://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        std::string data_13 =message.split(':')[i+302].split(',')[0].toStdString();
                        double d_13 =std::stod(data_13);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12+d_13;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 351://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        std::string data_13 =message.split(':')[i+302].split(',')[0].toStdString();
                        double d_13 =std::stod(data_13);
                        std::string data_14 =message.split(':')[i+327].split(',')[0].toStdString();
                        double d_14 =std::stod(data_14);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12+d_13+d_14;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 376://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        std::string data_13 =message.split(':')[i+302].split(',')[0].toStdString();
                        double d_13 =std::stod(data_13);
                        std::string data_14 =message.split(':')[i+327].split(',')[0].toStdString();
                        double d_14 =std::stod(data_14);
                        std::string data_15 =message.split(':')[i+352].split(',')[0].toStdString();
                        double d_15 =std::stod(data_15);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12+d_13+d_14+d_15;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 401://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        std::string data_13 =message.split(':')[i+302].split(',')[0].toStdString();
                        double d_13 =std::stod(data_13);
                        std::string data_14 =message.split(':')[i+327].split(',')[0].toStdString();
                        double d_14 =std::stod(data_14);
                        std::string data_15 =message.split(':')[i+352].split(',')[0].toStdString();
                        double d_15 =std::stod(data_15);
                        std::string data_16 =message.split(':')[i+377].split(',')[0].toStdString();
                        double d_16 =std::stod(data_16);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12+d_13+d_14+d_15+d_16;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 426://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        std::string data_13 =message.split(':')[i+302].split(',')[0].toStdString();
                        double d_13 =std::stod(data_13);
                        std::string data_14 =message.split(':')[i+327].split(',')[0].toStdString();
                        double d_14 =std::stod(data_14);
                        std::string data_15 =message.split(':')[i+352].split(',')[0].toStdString();
                        double d_15 =std::stod(data_15);
                        std::string data_16 =message.split(':')[i+377].split(',')[0].toStdString();
                        double d_16 =std::stod(data_16);
                        std::string data_17 =message.split(':')[i+402].split(',')[0].toStdString();
                        double d_17 =std::stod(data_17);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12+d_13+d_14+d_15+d_16+d_17;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 451://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        std::string data_13 =message.split(':')[i+302].split(',')[0].toStdString();
                        double d_13 =std::stod(data_13);
                        std::string data_14 =message.split(':')[i+327].split(',')[0].toStdString();
                        double d_14 =std::stod(data_14);
                        std::string data_15 =message.split(':')[i+352].split(',')[0].toStdString();
                        double d_15 =std::stod(data_15);
                        std::string data_16 =message.split(':')[i+377].split(',')[0].toStdString();
                        double d_16 =std::stod(data_16);
                        std::string data_17 =message.split(':')[i+402].split(',')[0].toStdString();
                        double d_17 =std::stod(data_17);
                        std::string data_18 =message.split(':')[i+427].split(',')[0].toStdString();
                        double d_18 =std::stod(data_18);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12+d_13+d_14+d_15+d_16+d_17+d_18;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 476://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        std::string data_13 =message.split(':')[i+302].split(',')[0].toStdString();
                        double d_13 =std::stod(data_13);
                        std::string data_14 =message.split(':')[i+327].split(',')[0].toStdString();
                        double d_14 =std::stod(data_14);
                        std::string data_15 =message.split(':')[i+352].split(',')[0].toStdString();
                        double d_15 =std::stod(data_15);
                        std::string data_16 =message.split(':')[i+377].split(',')[0].toStdString();
                        double d_16 =std::stod(data_16);
                        std::string data_17 =message.split(':')[i+402].split(',')[0].toStdString();
                        double d_17 =std::stod(data_17);
                        std::string data_18 =message.split(':')[i+427].split(',')[0].toStdString();
                        double d_18 =std::stod(data_18);
                        std::string data_19 =message.split(':')[i+452].split(',')[0].toStdString();
                        double d_19 =std::stod(data_19);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12+d_13+d_14+d_15+d_16+d_17+d_18+d_19;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 501://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        std::string data_13 =message.split(':')[i+302].split(',')[0].toStdString();
                        double d_13 =std::stod(data_13);
                        std::string data_14 =message.split(':')[i+327].split(',')[0].toStdString();
                        double d_14 =std::stod(data_14);
                        std::string data_15 =message.split(':')[i+352].split(',')[0].toStdString();
                        double d_15 =std::stod(data_15);
                        std::string data_16 =message.split(':')[i+377].split(',')[0].toStdString();
                        double d_16 =std::stod(data_16);
                        std::string data_17 =message.split(':')[i+402].split(',')[0].toStdString();
                        double d_17 =std::stod(data_17);
                        std::string data_18 =message.split(':')[i+427].split(',')[0].toStdString();
                        double d_18 =std::stod(data_18);
                        std::string data_19 =message.split(':')[i+452].split(',')[0].toStdString();
                        double d_19 =std::stod(data_19);
                        std::string data_20 =message.split(':')[i+477].split(',')[0].toStdString();
                        double d_20 =std::stod(data_20);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12+d_13+d_14+d_15+d_16+d_17+d_18+d_19+d_20;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 526://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        std::string data_13 =message.split(':')[i+302].split(',')[0].toStdString();
                        double d_13 =std::stod(data_13);
                        std::string data_14 =message.split(':')[i+327].split(',')[0].toStdString();
                        double d_14 =std::stod(data_14);
                        std::string data_15 =message.split(':')[i+352].split(',')[0].toStdString();
                        double d_15 =std::stod(data_15);
                        std::string data_16 =message.split(':')[i+377].split(',')[0].toStdString();
                        double d_16 =std::stod(data_16);
                        std::string data_17 =message.split(':')[i+402].split(',')[0].toStdString();
                        double d_17 =std::stod(data_17);
                        std::string data_18 =message.split(':')[i+427].split(',')[0].toStdString();
                        double d_18 =std::stod(data_18);
                        std::string data_19 =message.split(':')[i+452].split(',')[0].toStdString();
                        double d_19 =std::stod(data_19);
                        std::string data_20 =message.split(':')[i+477].split(',')[0].toStdString();
                        double d_20 =std::stod(data_20);
                        std::string data_21 =message.split(':')[i+502].split(',')[0].toStdString();
                        double d_21 =std::stod(data_21);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12+d_13+d_14+d_15+d_16+d_17+d_18+d_19+d_20+d_21;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 551://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        std::string data_13 =message.split(':')[i+302].split(',')[0].toStdString();
                        double d_13 =std::stod(data_13);
                        std::string data_14 =message.split(':')[i+327].split(',')[0].toStdString();
                        double d_14 =std::stod(data_14);
                        std::string data_15 =message.split(':')[i+352].split(',')[0].toStdString();
                        double d_15 =std::stod(data_15);
                        std::string data_16 =message.split(':')[i+377].split(',')[0].toStdString();
                        double d_16 =std::stod(data_16);
                        std::string data_17 =message.split(':')[i+402].split(',')[0].toStdString();
                        double d_17 =std::stod(data_17);
                        std::string data_18 =message.split(':')[i+427].split(',')[0].toStdString();
                        double d_18 =std::stod(data_18);
                        std::string data_19 =message.split(':')[i+452].split(',')[0].toStdString();
                        double d_19 =std::stod(data_19);
                        std::string data_20 =message.split(':')[i+477].split(',')[0].toStdString();
                        double d_20 =std::stod(data_20);
                        std::string data_21 =message.split(':')[i+502].split(',')[0].toStdString();
                        double d_21 =std::stod(data_21);
                        std::string data_22 =message.split(':')[i+527].split(',')[0].toStdString();
                        double d_22 =std::stod(data_22);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12+d_13+d_14+d_15+d_16+d_17+d_18+d_19+d_20+d_21+d_22;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 576://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        std::string data_13 =message.split(':')[i+302].split(',')[0].toStdString();
                        double d_13 =std::stod(data_13);
                        std::string data_14 =message.split(':')[i+327].split(',')[0].toStdString();
                        double d_14 =std::stod(data_14);
                        std::string data_15 =message.split(':')[i+352].split(',')[0].toStdString();
                        double d_15 =std::stod(data_15);
                        std::string data_16 =message.split(':')[i+377].split(',')[0].toStdString();
                        double d_16 =std::stod(data_16);
                        std::string data_17 =message.split(':')[i+402].split(',')[0].toStdString();
                        double d_17 =std::stod(data_17);
                        std::string data_18 =message.split(':')[i+427].split(',')[0].toStdString();
                        double d_18 =std::stod(data_18);
                        std::string data_19 =message.split(':')[i+452].split(',')[0].toStdString();
                        double d_19 =std::stod(data_19);
                        std::string data_20 =message.split(':')[i+477].split(',')[0].toStdString();
                        double d_20 =std::stod(data_20);
                        std::string data_21 =message.split(':')[i+502].split(',')[0].toStdString();
                        double d_21 =std::stod(data_21);
                        std::string data_22 =message.split(':')[i+527].split(',')[0].toStdString();
                        double d_22 =std::stod(data_22);
                        std::string data_23 =message.split(':')[i+552].split(',')[0].toStdString();
                        double d_23 =std::stod(data_23);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12+d_13+d_14+d_15+d_16+d_17+d_18+d_19+d_20+d_21+d_22+d_23;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;
            case 601://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        std::string data_13 =message.split(':')[i+302].split(',')[0].toStdString();
                        double d_13 =std::stod(data_13);
                        std::string data_14 =message.split(':')[i+327].split(',')[0].toStdString();
                        double d_14 =std::stod(data_14);
                        std::string data_15 =message.split(':')[i+352].split(',')[0].toStdString();
                        double d_15 =std::stod(data_15);
                        std::string data_16 =message.split(':')[i+377].split(',')[0].toStdString();
                        double d_16 =std::stod(data_16);
                        std::string data_17 =message.split(':')[i+402].split(',')[0].toStdString();
                        double d_17 =std::stod(data_17);
                        std::string data_18 =message.split(':')[i+427].split(',')[0].toStdString();
                        double d_18 =std::stod(data_18);
                        std::string data_19 =message.split(':')[i+452].split(',')[0].toStdString();
                        double d_19 =std::stod(data_19);
                        std::string data_20 =message.split(':')[i+477].split(',')[0].toStdString();
                        double d_20 =std::stod(data_20);
                        std::string data_21 =message.split(':')[i+502].split(',')[0].toStdString();
                        double d_21 =std::stod(data_21);
                        std::string data_22 =message.split(':')[i+527].split(',')[0].toStdString();
                        double d_22 =std::stod(data_22);
                        std::string data_23 =message.split(':')[i+552].split(',')[0].toStdString();
                        double d_23 =std::stod(data_23);
                        std::string data_24 =message.split(':')[i+577].split(',')[0].toStdString();
                        double d_24 =std::stod(data_24);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12+d_13+d_14+d_15+d_16+d_17+d_18+d_19+d_20+d_21+d_22+d_23+d_24;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;
            case 626://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        std::string data_13 =message.split(':')[i+302].split(',')[0].toStdString();
                        double d_13 =std::stod(data_13);
                        std::string data_14 =message.split(':')[i+327].split(',')[0].toStdString();
                        double d_14 =std::stod(data_14);
                        std::string data_15 =message.split(':')[i+352].split(',')[0].toStdString();
                        double d_15 =std::stod(data_15);
                        std::string data_16 =message.split(':')[i+377].split(',')[0].toStdString();
                        double d_16 =std::stod(data_16);
                        std::string data_17 =message.split(':')[i+402].split(',')[0].toStdString();
                        double d_17 =std::stod(data_17);
                        std::string data_18 =message.split(':')[i+427].split(',')[0].toStdString();
                        double d_18 =std::stod(data_18);
                        std::string data_19 =message.split(':')[i+452].split(',')[0].toStdString();
                        double d_19 =std::stod(data_19);
                        std::string data_20 =message.split(':')[i+477].split(',')[0].toStdString();
                        double d_20 =std::stod(data_20);
                        std::string data_21 =message.split(':')[i+502].split(',')[0].toStdString();
                        double d_21 =std::stod(data_21);
                        std::string data_22 =message.split(':')[i+527].split(',')[0].toStdString();
                        double d_22 =std::stod(data_22);
                        std::string data_23 =message.split(':')[i+552].split(',')[0].toStdString();
                        double d_23 =std::stod(data_23);
                        std::string data_24 =message.split(':')[i+577].split(',')[0].toStdString();
                        double d_24 =std::stod(data_24);
                        std::string data_25 =message.split(':')[i+602].split(',')[0].toStdString();
                        double d_25 =std::stod(data_25);

                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12+d_13+d_14+d_15+d_16+d_17+d_18+d_19+d_20+d_21+d_22+d_23+d_24+d_25;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 651://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        std::string data_13 =message.split(':')[i+302].split(',')[0].toStdString();
                        double d_13 =std::stod(data_13);
                        std::string data_14 =message.split(':')[i+327].split(',')[0].toStdString();
                        double d_14 =std::stod(data_14);
                        std::string data_15 =message.split(':')[i+352].split(',')[0].toStdString();
                        double d_15 =std::stod(data_15);
                        std::string data_16 =message.split(':')[i+377].split(',')[0].toStdString();
                        double d_16 =std::stod(data_16);
                        std::string data_17 =message.split(':')[i+402].split(',')[0].toStdString();
                        double d_17 =std::stod(data_17);
                        std::string data_18 =message.split(':')[i+427].split(',')[0].toStdString();
                        double d_18 =std::stod(data_18);
                        std::string data_19 =message.split(':')[i+452].split(',')[0].toStdString();
                        double d_19 =std::stod(data_19);
                        std::string data_20 =message.split(':')[i+477].split(',')[0].toStdString();
                        double d_20 =std::stod(data_20);
                        std::string data_21 =message.split(':')[i+502].split(',')[0].toStdString();
                        double d_21 =std::stod(data_21);
                        std::string data_22 =message.split(':')[i+527].split(',')[0].toStdString();
                        double d_22 =std::stod(data_22);
                        std::string data_23 =message.split(':')[i+552].split(',')[0].toStdString();
                        double d_23 =std::stod(data_23);
                        std::string data_24 =message.split(':')[i+577].split(',')[0].toStdString();
                        double d_24 =std::stod(data_24);
                        std::string data_25 =message.split(':')[i+602].split(',')[0].toStdString();
                        double d_25 =std::stod(data_25);
                        std::string data_26 =message.split(':')[i+627].split(',')[0].toStdString();
                        double d_26 =std::stod(data_26);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12+d_13+d_14+d_15+d_16+d_17+d_18+d_19+d_20+d_21+d_22+d_23+d_24+d_25+d_26;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            case 676://if (Mes_No==101)
            {
                if(Model_Type=="Load0"){
                    global_instance.Dynamic_Load_Received=true;
                    global_instance.Dynamic_Load_List.clear();
                    global_instance.Max_L_Sum=0;
                    larger=0;
                    std::string s =message.split(':')[2].split(',')[0].toStdString();
                    int Number_Size=24;
                    for(int i = 0;i<Number_Size;i++)
                      { std::string data_1 =message.split(':')[i+2].split(',')[0].toStdString();
                        double d_1 =std::stod(data_1);
                        std::string data_2 =message.split(':')[i+27].split(',')[0].toStdString();
                        double d_2 =std::stod(data_2);
                        std::string data_3 =message.split(':')[i+52].split(',')[0].toStdString();
                        double d_3 =std::stod(data_3);
                        std::string data_4 =message.split(':')[i+77].split(',')[0].toStdString();
                        double d_4 =std::stod(data_4);
                        std::string data_5 =message.split(':')[i+102].split(',')[0].toStdString();
                        double d_5 =std::stod(data_5);
                        std::string data_6 =message.split(':')[i+127].split(',')[0].toStdString();
                        double d_6 =std::stod(data_6);
                        std::string data_7 =message.split(':')[i+152].split(',')[0].toStdString();
                        double d_7 =std::stod(data_7);
                        std::string data_8 =message.split(':')[i+177].split(',')[0].toStdString();
                        double d_8 =std::stod(data_8);
                        std::string data_9 =message.split(':')[i+202].split(',')[0].toStdString();
                        double d_9 =std::stod(data_9);
                        std::string data_10 =message.split(':')[i+227].split(',')[0].toStdString();
                        double d_10 =std::stod(data_10);
                        std::string data_11 =message.split(':')[i+252].split(',')[0].toStdString();
                        double d_11 =std::stod(data_11);
                        std::string data_12 =message.split(':')[i+277].split(',')[0].toStdString();
                        double d_12 =std::stod(data_12);
                        std::string data_13 =message.split(':')[i+302].split(',')[0].toStdString();
                        double d_13 =std::stod(data_13);
                        std::string data_14 =message.split(':')[i+327].split(',')[0].toStdString();
                        double d_14 =std::stod(data_14);
                        std::string data_15 =message.split(':')[i+352].split(',')[0].toStdString();
                        double d_15 =std::stod(data_15);
                        std::string data_16 =message.split(':')[i+377].split(',')[0].toStdString();
                        double d_16 =std::stod(data_16);
                        std::string data_17 =message.split(':')[i+402].split(',')[0].toStdString();
                        double d_17 =std::stod(data_17);
                        std::string data_18 =message.split(':')[i+427].split(',')[0].toStdString();
                        double d_18 =std::stod(data_18);
                        std::string data_19 =message.split(':')[i+452].split(',')[0].toStdString();
                        double d_19 =std::stod(data_19);
                        std::string data_20 =message.split(':')[i+477].split(',')[0].toStdString();
                        double d_20 =std::stod(data_20);
                        std::string data_21 =message.split(':')[i+502].split(',')[0].toStdString();
                        double d_21 =std::stod(data_21);
                        std::string data_22 =message.split(':')[i+527].split(',')[0].toStdString();
                        double d_22 =std::stod(data_22);
                        std::string data_23 =message.split(':')[i+552].split(',')[0].toStdString();
                        double d_23 =std::stod(data_23);
                        std::string data_24 =message.split(':')[i+577].split(',')[0].toStdString();
                        double d_24 =std::stod(data_24);
                        std::string data_25 =message.split(':')[i+602].split(',')[0].toStdString();
                        double d_25 =std::stod(data_25);
                        std::string data_26 =message.split(':')[i+627].split(',')[0].toStdString();
                        double d_26 =std::stod(data_26);
                        std::string data_27 =message.split(':')[i+652].split(',')[0].toStdString();
                        double d_27 =std::stod(data_27);
                        double sum=d_1+d_2+d_3+d_4+d_5+d_6+d_7+d_8+d_9+d_10+d_11+d_12+d_13+d_14+d_15+d_16+d_17+d_18+d_19+d_20+d_21+d_22+d_23+d_24+d_25+d_26+d_27;
                        global_instance.Dynamic_Load_List.append(QPointF(i,sum));
                        if(sum > global_instance.Max_L_Sum){larger=sum;}
                        else{larger=global_instance.Max_L_Sum;}
                        global_instance.Max_L_Sum = larger;
                      }
                    emit_chart_update();
                }
            }
            break;

            default:

            break;
        }
        }
        }
     });


//注意此处lambda functor的[capture]必须是this或者&或者=，因为这个functor的implementation部分要用到mainwindow类成员emit_model_placed();
     connect(tables_topic_client, &QMqttClient::messageReceived, this,[this](const QByteArray &message, const QMqttTopicName &topic)
     {
        int mes_num =message.split(':').count();
        //std::cout<<"the num is================="<<mes_num<<std::endl;
        if(message=="Flow Config"){
            //qDebug("No");
        }
        else if(mes_num==5){
        if(message.split(':')[0].split('"')[1]=="RFID")
        {
             RFID_Tag=message.split(':')[2].split(',')[0].split('"')[1];
             command=message.split(':')[4].split('"')[1];
             table = message.split(':')[3].split(',')[0].split('"')[1];
             RFID = message.split(':')[1].split(',')[0].split('"')[1];

             global_instance.TABLE_Num =table;
             global_instance.RFID_Pos=RFID;
             std::cout<<"RFID: "<<RFID.toStdString()<<std::endl;
             std::cout<<"table: "<<table.toStdString()<<std::endl;

            if(table=="Table1"){global_instance.Table_No=1;}
            else if(table=="Table2"){global_instance.Table_No=2;}
            else if(table=="Table3"){global_instance.Table_No=3;}
            else if(table=="Table4"){global_instance.Table_No=4;}
            else if(table=="Table5"){global_instance.Table_No=5;}
            else if(table=="Table6"){global_instance.Table_No=6;}

            if (RFID=="RFID 0"){global_instance.RFID_No=0;}
            else if (RFID=="RFID 1"){global_instance.RFID_No=1;}
            else if (RFID=="RFID 2"){global_instance.RFID_No=2;}
            else if (RFID=="RFID 3"){global_instance.RFID_No=3;}
            else if (RFID=="RFID 4"){global_instance.RFID_No=4;}
            else if (RFID=="RFID 5"){global_instance.RFID_No=5;}
            else if (RFID=="RFID 6"){global_instance.RFID_No=6;}
            else if (RFID=="RFID 7"){global_instance.RFID_No=7;}
            //HV
            if(RFID_Tag=="1071771887"){global_instance.RFID_Tag_No=1071771887;}
            else if(RFID_Tag=="1842809804"){global_instance.RFID_Tag_No=1842809804;}
            else if(RFID_Tag=="1071966141"){global_instance.RFID_Tag_No=1071966141;}
            else if(RFID_Tag=="1857348684"){global_instance.RFID_Tag_No=1857348684;}
            else if(RFID_Tag=="2041806607"){global_instance.RFID_Tag_No=2041806607;}
            else if(RFID_Tag=="1071766506"){global_instance.RFID_Tag_No=1071766506;}
            else if(RFID_Tag=="1071741103"){global_instance.RFID_Tag_No=1071741103;}
            else if(RFID_Tag=="0"){global_instance.RFID_Tag_No=0;}
            //MV
            else if(RFID_Tag=="1071766506"){global_instance.RFID_Tag_No=1071766506;}
            else if(RFID_Tag=="1071741103"){global_instance.RFID_Tag_No=1071741103;}
            else if(RFID_Tag=="1072051546"){global_instance.RFID_Tag_No=1072051546;}
            else if(RFID_Tag=="1072066624"){global_instance.RFID_Tag_No=1072066624;}
            else if(RFID_Tag=="1071780135"){global_instance.RFID_Tag_No=1071780135;}
            else if(RFID_Tag=="1071769060"){global_instance.RFID_Tag_No=1071769060;}
            else if(RFID_Tag=="1071991074"){global_instance.RFID_Tag_No=1071991074;}
            else if(RFID_Tag=="1071729488"){global_instance.RFID_Tag_No=1071729488;}
            //LV
            else if(RFID_Tag=="1071985354"){global_instance.RFID_Tag_No=1071985354;}
            else if(RFID_Tag=="1072063229"){global_instance.RFID_Tag_No=1072063229;}
            else if(RFID_Tag=="1072056604"){global_instance.RFID_Tag_No=1072056604;}
            else if(RFID_Tag== "699168295"){global_instance.RFID_Tag_No= 699168295;}
            else if(RFID_Tag=="2305939010"){global_instance.RFID_Tag_No=2305939010;}
            else if(RFID_Tag=="2836102489"){global_instance.RFID_Tag_No=2836102489;}
            else if(RFID_Tag=="1071986763"){global_instance.RFID_Tag_No=1071986763;}
            else if(RFID_Tag=="1072063101"){global_instance.RFID_Tag_No=1072063101;}
            else if(RFID_Tag=="2298616020"){global_instance.RFID_Tag_No=2298616020;}
            else if(RFID_Tag=="1071733508"){global_instance.RFID_Tag_No=1071733508;}
            else if(RFID_Tag=="3914547211"){global_instance.RFID_Tag_No=3914547211;}
            else if(RFID_Tag=="1071773816"){global_instance.RFID_Tag_No=1071773816;}
            else if(RFID_Tag=="1071780377"){global_instance.RFID_Tag_No=1071780377;}
            else if(RFID_Tag=="1071770985"){global_instance.RFID_Tag_No=1071770985;}
            else if(RFID_Tag==   "7304921"){global_instance.RFID_Tag_No=   7304921;}
            else if(RFID_Tag== "593072732"){global_instance.RFID_Tag_No= 593072732;}
            else if(RFID_Tag=="1071753108"){global_instance.RFID_Tag_No=1071753108;}
            else if(RFID_Tag=="2836099420"){global_instance.RFID_Tag_No=2836099420;}
            else if(RFID_Tag=="1840834860"){global_instance.RFID_Tag_No=1840834860;}
            else if(RFID_Tag== "597999943"){global_instance.RFID_Tag_No= 597999943;}
            else if(RFID_Tag==   "8538681"){global_instance.RFID_Tag_No=   8538681;}
            //parsing table ESP32 mqtt
            switch(global_instance.Table_No){
                    case 1://if(table=="Table1")
                         {
                            switch(global_instance.RFID_No)
                                  {
                                    case 5://if(RFID=="RFID 5")
                                         {   switch(global_instance.RFID_Tag_No)
                                            {
                                                 case 1071771887://if(RFID_Tag=="1071771887")
                                                 {
                                                     global_instance.T1_R5_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                                     global_instance.T1_R5_RFID =RFID_Tag;
                                                     global_instance.T1_R5=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T1_R5 in MQTT()=:"<<global_instance.T1_R5<<std::endl;
                                                     global_table_instance.TABLE1->timer1->stop();
                                                     global_table_instance.TABLE1->T1_R5_MODULE=new T1_R5_Model("img/coal.jpg",global_table_instance.TABLE1->table_1_scene);
                                                     global_table_instance.TABLE1->timer->start(100);
                                                     qDebug("Model Placed");
                                                 }
                                                 break;
                                                 case 1842809804:
                                                 {
                                                     //global_instance.In_Ex_PW_1842809804=  *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                                     global_instance.T1_R5_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                                     global_instance.T1_R5_RFID =RFID_Tag;
                                                     global_instance.T1_R5=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T1_R5 in MQTT()=:"<<global_instance.T1_R5<<std::endl;
                                                     global_table_instance.TABLE1->timer1->stop();
                                                     global_table_instance.TABLE1->T1_R5_MODULE=new T1_R5_Model("img/In_Ex.jpg",global_table_instance.TABLE1->table_1_scene);
                                                     global_table_instance.TABLE1->timer->start(100);
                                                     qDebug("Model Placed");
                                                 }
                                                 break;
                                                    //else if(RFID_Tag=="1071966141")
                                                 case 1071966141:
                                                 {
                                                     global_instance.T1_R5_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                                     global_instance.T1_R5_RFID =RFID_Tag;
                                                     global_instance.T1_R5=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T1_R5 in MQTT()=:"<<global_instance.T1_R5<<std::endl;
                                                     global_table_instance.TABLE1->timer1->stop();
                                                     global_table_instance.TABLE1->T1_R5_MODULE=new T1_R5_Model("img/gas.jpg",global_table_instance.TABLE1->table_1_scene);
                                                     global_table_instance.TABLE1->timer->start(100);
                                                     qDebug("Model Placed");
                                                 }
                                                 break;
                                                    //else if(RFID_Tag=="1857348684")
                                                 case 1857348684:
                                                 {
                                                     global_instance.T1_R5_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                                     global_instance.T1_R5_RFID =RFID_Tag;
                                                     global_instance.T1_R5=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T1_R5 in MQTT()=:"<<global_instance.T1_R5<<std::endl;
                                                     global_table_instance.TABLE1->timer1->stop();
                                                     global_table_instance.TABLE1->T1_R5_MODULE=new T1_R5_Model("img/nuke.jpg",global_table_instance.TABLE1->table_1_scene);
                                                     global_table_instance.TABLE1->timer->start(100);
                                                     qDebug("Model Placed");
                                                 }
                                                 break;
                                                    //else if(RFID_Tag=="2041806607")
                                                 case 2041806607:
                                                 {
                                                     global_instance.T1_R5_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                                     global_instance.T1_R5_RFID =RFID_Tag;
                                                     global_instance.T1_R5=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T1_R5 in MQTT()=:"<<global_instance.T1_R5<<std::endl;
                                                     global_table_instance.TABLE1->timer1->stop();
                                                     global_table_instance.TABLE1->T1_R5_MODULE=new T1_R5_Model("img/volcano.jpg",global_table_instance.TABLE1->table_1_scene);
                                                     global_table_instance.TABLE1->timer->start(100);
                                                     qDebug("Model Placed");
                                                 }
                                                 break;
                                                    //else if(RFID_Tag=="1071766506")
                                                case 1071766506:
                                                 {
                                                     //MV cant in table 1.  "Solar farm small 2"
                                                 }
                                                break;
                                                    //else if(RFID_Tag=="1071741103")
                                                 case 1071741103:
                                                 {
                                                     //MV cant in table 1.  "Wind and solar"
                                                 }
                                                break;

                                                    //else if(global_instance.T1_R5==1&&RFID_Tag=="0")
                                                case 0:
                                                     if(global_instance.T1_R5==1)
                                                 {
                                                     global_instance.T1_R5_RFID =nullptr;
                                                     global_instance.T1_R5=0;
                                                     emit_model_placed();
                                                     global_table_instance.TABLE1->timer->stop();
                                                     global_table_instance.TABLE1->T1_R5_MODULE->RemoveQitem(global_table_instance.TABLE1->table_1_scene);
                                                     //没啥用global_table_instance.TABLE1->table_1_scene->removeItem(global_table_instance.TABLE1->T1_R1_MODULE);
                                                     //没啥用delete global_table_instance.TABLE1->T1_R1_MODULE;
                                                     //global_table_instance.TABLE1->timer1->start(100);
                                                     qDebug("Model Removed");
                                                 }
                                                break;
                                                default:
                                                    //else{qDebug("No");}
                                                    qDebug("No");
                                                    break;
                                            }
                                        }
                                    break;
                                    case 1://else if(RFID=="RFID 1")
                                        {   switch(global_instance.RFID_Tag_No)
                                           {
                                                case 1071771887://if(RFID_Tag=="1071771887")
                                                {
                                                    //global_instance.Coal_PW_1071771887=  *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                                    global_instance.T1_R1_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                                    //global_instance.T1_R1_RFID ="1071771887";
                                                    global_instance.T1_R1_RFID =RFID_Tag;
                                                    global_instance.T1_R1=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T1_R1 in MQTT()=:"<<global_instance.T1_R1<<std::endl;
                                                    //global_instance.Coal_PW_1071771887=global_instance.MQTT_RFID_VALUE;
                                                    //if(RFID_Tag==global_instance.MQTT_RFID);
                                                    //std::cout<<"reallllllllllllltime=:" <<global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag)->toStdString()<<std::endl;
                                                    global_table_instance.TABLE1->timer1->stop();
                                                    global_table_instance.TABLE1->T1_R1_MODULE=new T1_R1_Model("img/coal.jpg",global_table_instance.TABLE1->table_1_scene);
                                                    global_table_instance.TABLE1->timer->start(100);

                                                    qDebug("Model Placed");
                                                    //qDeleteAll((&message)->begin(),(&message)->end());
                                                }
                                                break;
                                                   //else if(RFID_Tag=="1842809804")
                                                case 1842809804:
                                                {
                                                    //global_instance.In_Ex_PW_1842809804=  *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                                    global_instance.T1_R1_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                                    global_instance.T1_R1_RFID =RFID_Tag;
                                                    global_instance.T1_R1=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T1_R1 in MQTT()=:"<<global_instance.T1_R1<<std::endl;
                                                    global_table_instance.TABLE1->timer1->stop();
                                                    global_table_instance.TABLE1->T1_R1_MODULE=new T1_R1_Model("img/In_Ex.jpg",global_table_instance.TABLE1->table_1_scene);
                                                    global_table_instance.TABLE1->timer->start(100);
                                                    qDebug("Model Placed");
                                                }
                                                break;
                                                   //else if(RFID_Tag=="1071966141")
                                                case 1071966141:
                                                {
                                                    global_instance.T1_R1_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                                    global_instance.T1_R1_RFID =RFID_Tag;
                                                    global_instance.T1_R1=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T1_R1 in MQTT()=:"<<global_instance.T1_R1<<std::endl;
                                                    global_table_instance.TABLE1->timer1->stop();
                                                    global_table_instance.TABLE1->T1_R1_MODULE=new T1_R1_Model("img/gas.jpg",global_table_instance.TABLE1->table_1_scene);
                                                    global_table_instance.TABLE1->timer->start(100);
                                                    qDebug("Model Placed");
                                                }
                                                break;
                                                   //else if(RFID_Tag=="1857348684")
                                                case 1857348684:
                                                {
                                                    global_instance.T1_R1_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                                    global_instance.T1_R1_RFID =RFID_Tag;
                                                    global_instance.T1_R1=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T1_R1 in MQTT()=:"<<global_instance.T1_R1<<std::endl;
                                                    global_table_instance.TABLE1->timer1->stop();
                                                    global_table_instance.TABLE1->T1_R1_MODULE=new T1_R1_Model("img/nuke.jpg",global_table_instance.TABLE1->table_1_scene);
                                                    global_table_instance.TABLE1->timer->start(100);
                                                    qDebug("Model Placed");
                                                }
                                                break;
                                                   //else if(RFID_Tag=="2041806607")
                                                case 2041806607:
                                                {
                                                    global_instance.T1_R1_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                                    global_instance.T1_R1_RFID =RFID_Tag;
                                                    global_instance.T1_R1=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T1_R1 in MQTT()=:"<<global_instance.T1_R1<<std::endl;
                                                    global_table_instance.TABLE1->timer1->stop();
                                                    global_table_instance.TABLE1->T1_R1_MODULE=new T1_R1_Model("img/volcano.jpg",global_table_instance.TABLE1->table_1_scene);
                                                    global_table_instance.TABLE1->timer->start(100);
                                                    qDebug("Model Placed");
                                                }
                                                break;
                                                   //else if(RFID_Tag=="1071766506")
                                               case 1071766506:
                                                {
                                                    //MV cant in table 1.  "Solar farm small 2"
                                                }
                                               break;
                                                   //else if(RFID_Tag=="1071741103")
                                                case 1071741103:
                                                {
                                                    //MV cant in table 1.  "Wind and solar"
                                                }
                                               break;
                                                   //else if(global_instance.T1_R1==1&&RFID_Tag=="0")
                                               case 0:
                                                    if(global_instance.T1_R1==1)
                                                {
                                                    global_instance.T1_R1_RFID =nullptr;
                                                    global_instance.T1_R1=0;
                                                    emit_model_placed();
                                                    global_table_instance.TABLE1->timer->stop();
                                                    global_table_instance.TABLE1->T1_R1_MODULE->RemoveQitem(global_table_instance.TABLE1->table_1_scene);
                                                    //没啥用global_table_instance.TABLE1->table_1_scene->removeItem(global_table_instance.TABLE1->T1_R1_MODULE);
                                                    //没啥用delete global_table_instance.TABLE1->T1_R1_MODULE;
                                                    //global_table_instance.TABLE1->timer1->start(100);
                                                    qDebug("Model Removed");
                                                }
                                               break;
                                               default:
                                                   //else{qDebug("No");}
                                                   qDebug("No");
                                                   break;
                                           }
                                       }
                                    break;
                                    default:

                                    break;
                                  }
                         }
                    break;
                    //table1 finished==================================================================
                    case 3://if(table=="Table3")
                         {
                            switch(global_instance.RFID_No)
                                    {
                                     case 0://if(RFID=="RFID 0")
                                     {switch(global_instance.RFID_Tag_No)
                                        {
                                         case 1071766506://if(RFID_Tag=="1071766506")
                                         {emit_T3R0_p_nom_max();
                                             global_instance.T3_R0_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                             global_instance.T3_R0_RFID =RFID_Tag;
                                             global_instance.T3_R0=1;
                                             emit_model_placed();
                                             std::cout<<"global_instance.T3_R0 in MQTT()=:"<<global_instance.T3_R0<<std::endl;
                                             global_table_instance.TABLE3->timer1->stop();
                                             global_table_instance.TABLE3->T3_R0_MODULE=new T3_R0_Model("img/solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                             global_table_instance.TABLE3->timer->start(100);
                                             qDebug("Model Placed");
                                         }
                                         break;
                                         case 1071741103://else if(RFID_Tag=="1071741103")
                                         {
                                             emit_T3R0_p_nom_max();
                                             global_instance.T3_R0_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                             global_instance.T3_R0_RFID =RFID_Tag;
                                             global_instance.T3_R0=1;
                                             emit_model_placed();
                                             std::cout<<"global_instance.T3_R0 in MQTT()=:"<<global_instance.T3_R0<<std::endl;
                                             global_table_instance.TABLE3->timer1->stop();
                                             global_table_instance.TABLE3->T3_R0_MODULE=new T3_R0_Model("img/wind_solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                             global_table_instance.TABLE3->timer->start(100);
                                         }
                                         break;
                                         case 1072051546://else if(RFID_Tag=="1072051546")
                                         {
                                             global_instance.T3_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                             global_instance.T3_R0_RFID =RFID_Tag;
                                             global_instance.T3_R0=1;
                                             emit_model_placed();
                                             std::cout<<"global_instance.T3_R0 in MQTT()=:"<<global_instance.T3_R0<<std::endl;
                                             global_table_instance.TABLE3->timer1->stop();
                                             global_table_instance.TABLE3->T3_R0_MODULE=new T3_R0_Model("img/chemical_factory.jpg",global_table_instance.TABLE3->table_3_scene);
                                             global_table_instance.TABLE3->timer->start(100);
                                         }
                                         break;
                                         case 1072066624://else if(RFID_Tag=="1072066624")
                                         {
                                             global_instance.T3_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                             global_instance.T3_R0_RFID =RFID_Tag;
                                             global_instance.T3_R0=1;
                                             emit_model_placed();
                                             std::cout<<"global_instance.T3_R0 in MQTT()=:"<<global_instance.T3_R0<<std::endl;
                                             global_table_instance.TABLE3->timer1->stop();
                                             global_table_instance.TABLE3->T3_R0_MODULE=new T3_R0_Model("img/processing_fac.jpg",global_table_instance.TABLE3->table_3_scene);
                                             global_table_instance.TABLE3->timer->start(100);
                                         }
                                         break;
                                         case 1071780135://else if(RFID_Tag=="1071780135")
                                         {
                                             global_instance.T3_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                             global_instance.T3_R0_RFID =RFID_Tag;
                                             global_instance.T3_R0=1;
                                             emit_model_placed();
                                             std::cout<<"global_instance.T3_R0 in MQTT()=:"<<global_instance.T3_R0<<std::endl;
                                             global_table_instance.TABLE3->timer1->stop();
                                             global_table_instance.TABLE3->T3_R0_MODULE=new T3_R0_Model("img/car_charge_1.jpg",global_table_instance.TABLE3->table_3_scene);
                                             global_table_instance.TABLE3->timer->start(100);
                                         }
                                         break;
                                         case 1071769060://else if(RFID_Tag=="1071769060")
                                         {
                                             global_instance.T3_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                             global_instance.T3_R0_RFID =RFID_Tag;
                                             global_instance.T3_R0=1;
                                             emit_model_placed();
                                             std::cout<<"global_instance.T3_R0 in MQTT()=:"<<global_instance.T3_R0<<std::endl;
                                             global_table_instance.TABLE3->timer1->stop();
                                             global_table_instance.TABLE3->T3_R0_MODULE=new T3_R0_Model("img/car_charge_2.jpg",global_table_instance.TABLE3->table_3_scene);
                                             global_table_instance.TABLE3->timer->start(100);
                                         }
                                         break;
                                         case 1071991074://else if(RFID_Tag=="1071991074")
                                         {
                                             global_instance.T3_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                             global_instance.T3_R0_RFID =RFID_Tag;
                                             global_instance.T3_R0=1;
                                             emit_model_placed();
                                             std::cout<<"global_instance.T3_R0 in MQTT()=:"<<global_instance.T3_R0<<std::endl;
                                             global_table_instance.TABLE3->timer1->stop();
                                             global_table_instance.TABLE3->T3_R0_MODULE=new T3_R0_Model("img/hydro_station.jpg",global_table_instance.TABLE3->table_3_scene);
                                             global_table_instance.TABLE3->timer->start(100);
                                         }
                                         break;
                                         case 1071729488://else if(RFID_Tag=="1071729488")
                                         {
                                             global_instance.T3_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                             global_instance.T3_R0_RFID =RFID_Tag;
                                             global_instance.T3_R0=1;
                                             emit_model_placed();
                                             std::cout<<"global_instance.T3_R0 in MQTT()=:"<<global_instance.T3_R0<<std::endl;
                                             global_table_instance.TABLE3->timer1->stop();
                                             global_table_instance.TABLE3->T3_R0_MODULE=new T3_R0_Model("img/HAN_module.jpg",global_table_instance.TABLE3->table_3_scene);
                                             global_table_instance.TABLE3->timer->start(100);
                                         }
                                         break;
                                         case 0://else if(global_instance.T3_R0==1&&RFID_Tag=="0")
                                             if(global_instance.T3_R0==1)
                                             {
                                             {
                                                 global_instance.T3_R0_RFID =nullptr;
                                                 global_instance.T3_R0=0;
                                                 emit_model_placed();

                                                 global_table_instance.TABLE3->T3_R0_MODULE->RemoveQitem(global_table_instance.TABLE3->table_3_scene);
                                                 //global_table_instance.TABLE1->timer->stop();
                                                 //global_table_instance.TABLE1->timer1->start(100);
                                                 qDebug("Model Removed");
                                             }
                                         }
                                                 break;
                                            default:
                                            qDebug("No");
                                         //else{qDebug("No");}
                                        break;
                                        }
                                    }
                                     break;
                                     case 7://if(RFID=="RFID 7")
                                    {switch(global_instance.RFID_Tag_No)
                                       {
                                        case 1071766506://if(RFID_Tag=="1071766506")
                                        {emit_T3R7_p_nom_max();
                                            global_instance.T3_R7_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                            global_instance.T3_R7_RFID =RFID_Tag;
                                            global_instance.T3_R7=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R7 in MQTT()=:"<<global_instance.T3_R7<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R7_MODULE=new T3_R7_Model("img/solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                            qDebug("Model Placed");
                                        }
                                        break;
                                        case 1071741103://else if(RFID_Tag=="1071741103")
                                        {
                                             emit_T3R7_p_nom_max();
                                            global_instance.T3_R7_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                            global_instance.T3_R7_RFID =RFID_Tag;
                                            global_instance.T3_R7=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R7 in MQTT()=:"<<global_instance.T3_R7<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R7_MODULE=new T3_R7_Model("img/wind_solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1072051546://else if(RFID_Tag=="1072051546")
                                        {
                                            global_instance.T3_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R7_RFID =RFID_Tag;
                                            global_instance.T3_R7=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R7 in MQTT()=:"<<global_instance.T3_R7<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R7_MODULE=new T3_R7_Model("img/chemical_factory.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1072066624://else if(RFID_Tag=="1072066624")
                                        {
                                            global_instance.T3_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R7_RFID =RFID_Tag;
                                            global_instance.T3_R7=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R7 in MQTT()=:"<<global_instance.T3_R7<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R7_MODULE=new T3_R7_Model("img/processing_fac.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071780135://else if(RFID_Tag=="1071780135")
                                        {
                                            global_instance.T3_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R7_RFID =RFID_Tag;
                                            global_instance.T3_R7=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R7 in MQTT()=:"<<global_instance.T3_R7<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R7_MODULE=new T3_R7_Model("img/car_charge_1.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071769060://else if(RFID_Tag=="1071769060")
                                        {
                                            global_instance.T3_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R7_RFID =RFID_Tag;
                                            global_instance.T3_R7=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R7 in MQTT()=:"<<global_instance.T3_R7<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R7_MODULE=new T3_R7_Model("img/car_charge_2.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071991074://else if(RFID_Tag=="1071991074")
                                        {
                                            global_instance.T3_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R7_RFID =RFID_Tag;
                                            global_instance.T3_R7=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R7 in MQTT()=:"<<global_instance.T3_R7<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R7_MODULE=new T3_R7_Model("img/hydro_station.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071729488://else if(RFID_Tag=="1071729488")
                                        {
                                            global_instance.T3_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R7_RFID =RFID_Tag;
                                            global_instance.T3_R7=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R7 in MQTT()=:"<<global_instance.T3_R7<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R7_MODULE=new T3_R7_Model("img/HAN_module.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 0://else if(global_instance.T3_R7==1&&RFID_Tag=="0")
                                    global_instance.T3R7_p_nom_max=false;
                                            if(global_instance.T3_R7==1){

                                        {
                                            global_instance.T3_R7_RFID =nullptr;
                                            global_instance.T3_R7=0;
                                            emit_model_placed();

                                            global_table_instance.TABLE3->T3_R7_MODULE->RemoveQitem(global_table_instance.TABLE3->table_3_scene);
                                            //global_table_instance.TABLE1->timer->stop();
                                            //global_table_instance.TABLE1->timer1->start(100);
                                            qDebug("Model Removed");
                                        }
                                        }
                                                break;
                                           default:
                                           qDebug("No");
                                        //else{qDebug("No");}
                                       break;
                                       }
                                   }
                                     break;
                                     case 6://if(RFID=="RFID 6")
                                    {switch(global_instance.RFID_Tag_No)
                                       {
                                        case 1071766506://if(RFID_Tag=="1071766506")
                                        {emit_T3R6_p_nom_max();
                                            global_instance.T3_R6_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                            global_instance.T3_R6_RFID =RFID_Tag;
                                            global_instance.T3_R6=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R6 in MQTT()=:"<<global_instance.T3_R6<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R6_MODULE=new T3_R6_Model("img/solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                            qDebug("Model Placed");
                                        }
                                        break;
                                        case 1071741103://else if(RFID_Tag=="1071741103")
                                        {
                                             emit_T3R6_p_nom_max();
                                            global_instance.T3_R6_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                            global_instance.T3_R6_RFID =RFID_Tag;
                                            global_instance.T3_R6=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R6 in MQTT()=:"<<global_instance.T3_R6<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R6_MODULE=new T3_R6_Model("img/wind_solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1072051546://else if(RFID_Tag=="1072051546")
                                        {
                                            global_instance.T3_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R6_RFID =RFID_Tag;
                                            global_instance.T3_R6=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R6 in MQTT()=:"<<global_instance.T3_R6<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R6_MODULE=new T3_R6_Model("img/chemical_factory.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1072066624://else if(RFID_Tag=="1072066624")
                                        {
                                            global_instance.T3_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R6_RFID =RFID_Tag;
                                            global_instance.T3_R6=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R6 in MQTT()=:"<<global_instance.T3_R6<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R6_MODULE=new T3_R6_Model("img/processing_fac.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071780135://else if(RFID_Tag=="1071780135")
                                        {
                                            global_instance.T3_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R6_RFID =RFID_Tag;
                                            global_instance.T3_R6=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R6 in MQTT()=:"<<global_instance.T3_R6<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R6_MODULE=new T3_R6_Model("img/car_charge_1.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071769060://else if(RFID_Tag=="1071769060")
                                        {
                                            global_instance.T3_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R6_RFID =RFID_Tag;
                                            global_instance.T3_R6=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R6 in MQTT()=:"<<global_instance.T3_R6<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R6_MODULE=new T3_R6_Model("img/car_charge_2.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071991074://else if(RFID_Tag=="1071991074")
                                        {
                                            global_instance.T3_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R6_RFID =RFID_Tag;
                                            global_instance.T3_R6=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R6 in MQTT()=:"<<global_instance.T3_R6<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R6_MODULE=new T3_R6_Model("img/hydro_station.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071729488://else if(RFID_Tag=="1071729488")
                                        {
                                            global_instance.T3_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R6_RFID =RFID_Tag;
                                            global_instance.T3_R6=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R6 in MQTT()=:"<<global_instance.T3_R6<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R6_MODULE=new T3_R6_Model("img/HAN_module.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 0://else if(global_instance.T3_R6==1&&RFID_Tag=="0")
                                    global_instance.T3R6_p_nom_max=false;
                                            if(global_instance.T3_R6==1){
                                        {
                                            global_instance.T3_R6_RFID =nullptr;
                                            global_instance.T3_R6=0;
                                            emit_model_placed();

                                            global_table_instance.TABLE3->T3_R6_MODULE->RemoveQitem(global_table_instance.TABLE3->table_3_scene);
                                            //global_table_instance.TABLE1->timer->stop();
                                            //global_table_instance.TABLE1->timer1->start(100);
                                            qDebug("Model Removed");
                                        }
                                        }
                                                break;
                                           default:
                                           qDebug("No");
                                        //else{qDebug("No");}
                                       break;
                                       }
                                   }
                                     break;
                                     case 5:if(RFID=="RFID 5")
                                        {switch(global_instance.RFID_Tag_No)
                                           {
                                            case 1071766506://if(RFID_Tag=="1071766506")
                                            {emit_T3R5_p_nom_max();
                                                global_instance.T3_R5_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                                global_instance.T3_R5_RFID =RFID_Tag;
                                                global_instance.T3_R5=1;
                                                emit_model_placed();
                                                std::cout<<"global_instance.T3_R5 in MQTT()=:"<<global_instance.T3_R5<<std::endl;
                                                global_table_instance.TABLE3->timer1->stop();
                                                global_table_instance.TABLE3->T3_R5_MODULE=new T3_R5_Model("img/solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                                global_table_instance.TABLE3->timer->start(100);
                                                qDebug("Model Placed");
                                            }
                                            break;
                                            case 1071741103://else if(RFID_Tag=="1071741103")
                                            {
                                             emit_T3R5_p_nom_max();
                                                global_instance.T3_R5_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                                global_instance.T3_R5_RFID =RFID_Tag;
                                                global_instance.T3_R5=1;
                                                emit_model_placed();
                                                std::cout<<"global_instance.T3_R5 in MQTT()=:"<<global_instance.T3_R5<<std::endl;
                                                global_table_instance.TABLE3->timer1->stop();
                                                global_table_instance.TABLE3->T3_R5_MODULE=new T3_R5_Model("img/wind_solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                                global_table_instance.TABLE3->timer->start(100);
                                            }
                                            break;
                                            case 1072051546://else if(RFID_Tag=="1072051546")
                                            {
                                                global_instance.T3_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                global_instance.T3_R5_RFID =RFID_Tag;
                                                global_instance.T3_R5=1;
                                                emit_model_placed();
                                                std::cout<<"global_instance.T3_R5 in MQTT()=:"<<global_instance.T3_R5<<std::endl;
                                                global_table_instance.TABLE3->timer1->stop();
                                                global_table_instance.TABLE3->T3_R5_MODULE=new T3_R5_Model("img/chemical_factory.jpg",global_table_instance.TABLE3->table_3_scene);
                                                global_table_instance.TABLE3->timer->start(100);
                                            }
                                            break;
                                            case 1072066624://else if(RFID_Tag=="1072066624")
                                            {
                                                global_instance.T3_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                global_instance.T3_R5_RFID =RFID_Tag;
                                                global_instance.T3_R5=1;
                                                emit_model_placed();
                                                std::cout<<"global_instance.T3_R5 in MQTT()=:"<<global_instance.T3_R5<<std::endl;
                                                global_table_instance.TABLE3->timer1->stop();
                                                global_table_instance.TABLE3->T3_R5_MODULE=new T3_R5_Model("img/processing_fac.jpg",global_table_instance.TABLE3->table_3_scene);
                                                global_table_instance.TABLE3->timer->start(100);
                                            }
                                            break;
                                            case 1071780135://else if(RFID_Tag=="1071780135")
                                            {
                                                global_instance.T3_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                global_instance.T3_R5_RFID =RFID_Tag;
                                                global_instance.T3_R5=1;
                                                emit_model_placed();
                                                std::cout<<"global_instance.T3_R5 in MQTT()=:"<<global_instance.T3_R5<<std::endl;
                                                global_table_instance.TABLE3->timer1->stop();
                                                global_table_instance.TABLE3->T3_R5_MODULE=new T3_R5_Model("img/car_charge_1.jpg",global_table_instance.TABLE3->table_3_scene);
                                                global_table_instance.TABLE3->timer->start(100);
                                            }
                                            break;
                                            case 1071769060://else if(RFID_Tag=="1071769060")
                                            {
                                                global_instance.T3_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                global_instance.T3_R5_RFID =RFID_Tag;
                                                global_instance.T3_R5=1;
                                                emit_model_placed();
                                                std::cout<<"global_instance.T3_R5 in MQTT()=:"<<global_instance.T3_R5<<std::endl;
                                                global_table_instance.TABLE3->timer1->stop();
                                                global_table_instance.TABLE3->T3_R5_MODULE=new T3_R5_Model("img/car_charge_2.jpg",global_table_instance.TABLE3->table_3_scene);
                                                global_table_instance.TABLE3->timer->start(100);
                                            }
                                            break;
                                            case 1071991074://else if(RFID_Tag=="1071991074")
                                            {
                                                global_instance.T3_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                global_instance.T3_R5_RFID =RFID_Tag;
                                                global_instance.T3_R5=1;
                                                emit_model_placed();
                                                std::cout<<"global_instance.T3_R5 in MQTT()=:"<<global_instance.T3_R5<<std::endl;
                                                global_table_instance.TABLE3->timer1->stop();
                                                global_table_instance.TABLE3->T3_R5_MODULE=new T3_R5_Model("img/hydro_station.jpg",global_table_instance.TABLE3->table_3_scene);
                                                global_table_instance.TABLE3->timer->start(100);
                                            }
                                            break;
                                            case 1071729488://else if(RFID_Tag=="1071729488")
                                            {
                                                global_instance.T3_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                global_instance.T3_R5_RFID =RFID_Tag;
                                                global_instance.T3_R5=1;
                                                emit_model_placed();
                                                std::cout<<"global_instance.T3_R5 in MQTT()=:"<<global_instance.T3_R5<<std::endl;
                                                global_table_instance.TABLE3->timer1->stop();
                                                global_table_instance.TABLE3->T3_R5_MODULE=new T3_R5_Model("img/HAN_module.jpg",global_table_instance.TABLE3->table_3_scene);
                                                global_table_instance.TABLE3->timer->start(100);
                                            }
                                            break;
                                            case 0://else if(global_instance.T3_R5==1&&RFID_Tag=="0")
                                        global_instance.T3R5_p_nom_max=false;
                                                if(global_instance.T3_R5==1){
                                            {
                                                global_instance.T3_R5_RFID =nullptr;
                                                global_instance.T3_R5=0;
                                                emit_model_placed();

                                                global_table_instance.TABLE3->T3_R5_MODULE->RemoveQitem(global_table_instance.TABLE3->table_3_scene);
                                                //global_table_instance.TABLE1->timer->stop();
                                                //global_table_instance.TABLE1->timer1->start(100);
                                                qDebug("Model Removed");
                                            }
                                            }
                                                    break;
                                               default:
                                               qDebug("No");
                                            //else{qDebug("No");}
                                           break;
                                           }
                                       }
                                     break;
                                     case 4://if(RFID=="RFID 4")
                                    {switch(global_instance.RFID_Tag_No)
                                       {
                                        case 1071766506://if(RFID_Tag=="1071766506")
                                        {emit_T3R4_p_nom_max();
                                            global_instance.T3_R4_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                            global_instance.T3_R4_RFID =RFID_Tag;
                                            global_instance.T3_R4=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R4 in MQTT()=:"<<global_instance.T3_R4<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R4_MODULE=new T3_R4_Model("img/solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                            qDebug("Model Placed");
                                        }
                                        break;
                                        case 1071741103://else if(RFID_Tag=="1071741103")
                                        {
                                             emit_T3R4_p_nom_max();
                                            global_instance.T3_R4_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                            global_instance.T3_R4_RFID =RFID_Tag;
                                            global_instance.T3_R4=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R4 in MQTT()=:"<<global_instance.T3_R4<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R4_MODULE=new T3_R4_Model("img/wind_solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1072051546://else if(RFID_Tag=="1072051546")
                                        {
                                            global_instance.T3_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R4_RFID =RFID_Tag;
                                            global_instance.T3_R4=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R4 in MQTT()=:"<<global_instance.T3_R4<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R4_MODULE=new T3_R4_Model("img/chemical_factory.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1072066624://else if(RFID_Tag=="1072066624")
                                        {
                                            global_instance.T3_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R4_RFID =RFID_Tag;
                                            global_instance.T3_R4=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R4 in MQTT()=:"<<global_instance.T3_R4<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R4_MODULE=new T3_R4_Model("img/processing_fac.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071780135://else if(RFID_Tag=="1071780135")
                                        {
                                            global_instance.T3_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R4_RFID =RFID_Tag;
                                            global_instance.T3_R4=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R4 in MQTT()=:"<<global_instance.T3_R4<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R4_MODULE=new T3_R4_Model("img/car_charge_1.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071769060://else if(RFID_Tag=="1071769060")
                                        {
                                            global_instance.T3_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R4_RFID =RFID_Tag;
                                            global_instance.T3_R4=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R4 in MQTT()=:"<<global_instance.T3_R4<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R4_MODULE=new T3_R4_Model("img/car_charge_2.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071991074://else if(RFID_Tag=="1071991074")
                                        {
                                            global_instance.T3_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R4_RFID =RFID_Tag;
                                            global_instance.T3_R4=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R4 in MQTT()=:"<<global_instance.T3_R4<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R4_MODULE=new T3_R4_Model("img/hydro_station.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071729488://else if(RFID_Tag=="1071729488")
                                        {
                                            global_instance.T3_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R4_RFID =RFID_Tag;
                                            global_instance.T3_R4=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R4 in MQTT()=:"<<global_instance.T3_R4<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R4_MODULE=new T3_R4_Model("img/HAN_module.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 0://else if(global_instance.T3_R4==1&&RFID_Tag=="0")
                                    global_instance.T3R4_p_nom_max=false;
                                            if(global_instance.T3_R4==1){
                                        {
                                            global_instance.T3_R4_RFID =nullptr;
                                            global_instance.T3_R4=0;
                                            emit_model_placed();

                                            global_table_instance.TABLE3->T3_R4_MODULE->RemoveQitem(global_table_instance.TABLE3->table_3_scene);
                                            //global_table_instance.TABLE1->timer->stop();
                                            //global_table_instance.TABLE1->timer1->start(100);
                                            qDebug("Model Removed");
                                        }
                                        }
                                                break;
                                           default:
                                           qDebug("No");
                                        //else{qDebug("No");}
                                       break;
                                       }
                                   }
                                     break;
                                     case 3://if(RFID=="RFID 3")
                                    {switch(global_instance.RFID_Tag_No)
                                       {
                                        case 1071766506://if(RFID_Tag=="1071766506")
                                        {emit_T3R3_p_nom_max();
                                            global_instance.T3_R3_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                            global_instance.T3_R3_RFID =RFID_Tag;
                                            global_instance.T3_R3=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R3 in MQTT()=:"<<global_instance.T3_R3<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R3_MODULE=new T3_R3_Model("img/solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                            qDebug("Model Placed");
                                        }
                                        break;
                                        case 1071741103://else if(RFID_Tag=="1071741103")
                                        {
                                             emit_T3R3_p_nom_max();
                                            global_instance.T3_R3_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                            global_instance.T3_R3_RFID =RFID_Tag;
                                            global_instance.T3_R3=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R3 in MQTT()=:"<<global_instance.T3_R3<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R3_MODULE=new T3_R3_Model("img/wind_solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1072051546://else if(RFID_Tag=="1072051546")
                                        {
                                            global_instance.T3_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R3_RFID =RFID_Tag;
                                            global_instance.T3_R3=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R3 in MQTT()=:"<<global_instance.T3_R3<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R3_MODULE=new T3_R3_Model("img/chemical_factory.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1072066624://else if(RFID_Tag=="1072066624")
                                        {
                                            global_instance.T3_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R3_RFID =RFID_Tag;
                                            global_instance.T3_R3=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R3 in MQTT()=:"<<global_instance.T3_R3<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R3_MODULE=new T3_R3_Model("img/processing_fac.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071780135://else if(RFID_Tag=="1071780135")
                                        {
                                            global_instance.T3_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R3_RFID =RFID_Tag;
                                            global_instance.T3_R3=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R3 in MQTT()=:"<<global_instance.T3_R3<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R3_MODULE=new T3_R3_Model("img/car_charge_1.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071769060://else if(RFID_Tag=="1071769060")
                                        {
                                            global_instance.T3_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R3_RFID =RFID_Tag;
                                            global_instance.T3_R3=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R3 in MQTT()=:"<<global_instance.T3_R3<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R3_MODULE=new T3_R3_Model("img/car_charge_2.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071991074://else if(RFID_Tag=="1071991074")
                                        {
                                            global_instance.T3_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R3_RFID =RFID_Tag;
                                            global_instance.T3_R3=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R3 in MQTT()=:"<<global_instance.T3_R3<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R3_MODULE=new T3_R3_Model("img/hydro_station.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071729488://else if(RFID_Tag=="1071729488")
                                        {
                                            global_instance.T3_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R3_RFID =RFID_Tag;
                                            global_instance.T3_R3=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R3 in MQTT()=:"<<global_instance.T3_R3<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R3_MODULE=new T3_R3_Model("img/HAN_module.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 0://else if(global_instance.T3_R3==1&&RFID_Tag=="0")
                                    global_instance.T3R3_p_nom_max=false;
                                            if(global_instance.T3_R3==1){
                                        {
                                            global_instance.T3_R3_RFID =nullptr;
                                            global_instance.T3_R3=0;
                                            emit_model_placed();

                                            global_table_instance.TABLE3->T3_R3_MODULE->RemoveQitem(global_table_instance.TABLE3->table_3_scene);
                                            //global_table_instance.TABLE1->timer->stop();
                                            //global_table_instance.TABLE1->timer1->start(100);
                                            qDebug("Model Removed");
                                        }
                                        }
                                                break;
                                           default:
                                           qDebug("No");
                                        //else{qDebug("No");}
                                       break;
                                       }
                                   }
                                     break;
                                     case 2://if(RFID=="RFID 2")
                                    {switch(global_instance.RFID_Tag_No)
                                       {
                                        case 1071766506://if(RFID_Tag=="1071766506")
                                        {emit_T3R2_p_nom_max();
                                            global_instance.T3_R2_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                            global_instance.T3_R2_RFID =RFID_Tag;
                                            global_instance.T3_R2=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R2 in MQTT()=:"<<global_instance.T3_R2<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R2_MODULE=new T3_R2_Model("img/solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                            qDebug("Model Placed");
                                        }
                                        break;
                                        case 1071741103://else if(RFID_Tag=="1071741103")
                                        {
                                             emit_T3R2_p_nom_max();
                                            global_instance.T3_R2_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                            global_instance.T3_R2_RFID =RFID_Tag;
                                            global_instance.T3_R2=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R2 in MQTT()=:"<<global_instance.T3_R2<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R2_MODULE=new T3_R2_Model("img/wind_solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1072051546://else if(RFID_Tag=="1072051546")
                                        {
                                            global_instance.T3_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R2_RFID =RFID_Tag;
                                            global_instance.T3_R2=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R2 in MQTT()=:"<<global_instance.T3_R2<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R2_MODULE=new T3_R2_Model("img/chemical_factory.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1072066624://else if(RFID_Tag=="1072066624")
                                        {
                                            global_instance.T3_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R2_RFID =RFID_Tag;
                                            global_instance.T3_R2=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R2 in MQTT()=:"<<global_instance.T3_R2<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R2_MODULE=new T3_R2_Model("img/processing_fac.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071780135://else if(RFID_Tag=="1071780135")
                                        {
                                            global_instance.T3_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R2_RFID =RFID_Tag;
                                            global_instance.T3_R2=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R2 in MQTT()=:"<<global_instance.T3_R2<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R2_MODULE=new T3_R2_Model("img/car_charge_1.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071769060://else if(RFID_Tag=="1071769060")
                                        {
                                            global_instance.T3_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R2_RFID =RFID_Tag;
                                            global_instance.T3_R2=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R2 in MQTT()=:"<<global_instance.T3_R2<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R2_MODULE=new T3_R2_Model("img/car_charge_2.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071991074://else if(RFID_Tag=="1071991074")
                                        {
                                            global_instance.T3_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R2_RFID =RFID_Tag;
                                            global_instance.T3_R2=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R2 in MQTT()=:"<<global_instance.T3_R2<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R2_MODULE=new T3_R2_Model("img/hydro_station.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071729488://else if(RFID_Tag=="1071729488")
                                        {
                                            global_instance.T3_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R2_RFID =RFID_Tag;
                                            global_instance.T3_R2=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R2 in MQTT()=:"<<global_instance.T3_R2<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R2_MODULE=new T3_R2_Model("img/HAN_module.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 0://else if(global_instance.T3_R2==1&&RFID_Tag=="0")
                                            global_instance.T3R2_p_nom_max=false;
                                            if(global_instance.T3_R2==1){
                                        {
                                            global_instance.T3_R2_RFID =nullptr;
                                            global_instance.T3_R2=0;
                                            emit_model_placed();

                                            global_table_instance.TABLE3->T3_R2_MODULE->RemoveQitem(global_table_instance.TABLE3->table_3_scene);
                                            //global_table_instance.TABLE1->timer->stop();
                                            //global_table_instance.TABLE1->timer1->start(100);
                                            qDebug("Model Removed");
                                        }
                                        }
                                                break;
                                           default:
                                           qDebug("No");
                                        //else{qDebug("No");}
                                       break;
                                       }
                                   }
                                     break;
                                     case 1://if(RFID=="RFID 1")
                                    {switch(global_instance.RFID_Tag_No)
                                       {
                                        case 1071766506://if(RFID_Tag=="1071766506")
                                        {emit_T3R1_p_nom_max();
                                            global_instance.T3_R1_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                            global_instance.T3_R1_RFID =RFID_Tag;
                                            global_instance.T3_R1=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R1 in MQTT()=:"<<global_instance.T3_R1<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R1_MODULE=new T3_R1_Model("img/solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                            qDebug("Model Placed");
                                        }
                                        break;
                                        case 1071741103://else if(RFID_Tag=="1071741103")
                                        {
                                             emit_T3R1_p_nom_max();
                                            global_instance.T3_R1_VALUE= *(global_instance.MQTT_JSON_MAP_GEN.find(RFID_Tag));
                                            global_instance.T3_R1_RFID =RFID_Tag;
                                            global_instance.T3_R1=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R1 in MQTT()=:"<<global_instance.T3_R1<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R1_MODULE=new T3_R1_Model("img/wind_solar.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1072051546://else if(RFID_Tag=="1072051546")
                                        {
                                            global_instance.T3_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R1_RFID =RFID_Tag;
                                            global_instance.T3_R1=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R1 in MQTT()=:"<<global_instance.T3_R1<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R1_MODULE=new T3_R1_Model("img/chemical_factory.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1072066624://else if(RFID_Tag=="1072066624")
                                        {
                                            global_instance.T3_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R1_RFID =RFID_Tag;
                                            global_instance.T3_R1=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R1 in MQTT()=:"<<global_instance.T3_R1<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R1_MODULE=new T3_R1_Model("img/processing_fac.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071780135://else if(RFID_Tag=="1071780135")
                                        {
                                            global_instance.T3_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R1_RFID =RFID_Tag;
                                            global_instance.T3_R1=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R1 in MQTT()=:"<<global_instance.T3_R1<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R1_MODULE=new T3_R1_Model("img/car_charge_1.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071769060://else if(RFID_Tag=="1071769060")
                                        {
                                            global_instance.T3_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R1_RFID =RFID_Tag;
                                            global_instance.T3_R1=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R1 in MQTT()=:"<<global_instance.T3_R1<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R1_MODULE=new T3_R1_Model("img/car_charge_2.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071991074://else if(RFID_Tag=="1071991074")
                                        {
                                            global_instance.T3_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R1_RFID =RFID_Tag;
                                            global_instance.T3_R1=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R1 in MQTT()=:"<<global_instance.T3_R1<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R1_MODULE=new T3_R1_Model("img/hydro_station.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 1071729488://else if(RFID_Tag=="1071729488")
                                        {
                                            global_instance.T3_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T3_R1_RFID =RFID_Tag;
                                            global_instance.T3_R1=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T3_R1 in MQTT()=:"<<global_instance.T3_R1<<std::endl;
                                            global_table_instance.TABLE3->timer1->stop();
                                            global_table_instance.TABLE3->T3_R1_MODULE=new T3_R1_Model("img/HAN_module.jpg",global_table_instance.TABLE3->table_3_scene);
                                            global_table_instance.TABLE3->timer->start(100);
                                        }
                                        break;
                                        case 0://else if(global_instance.T3_R1==1&&RFID_Tag=="0")
                                            if(global_instance.T3_R1==1){

                                        {global_instance.T3R1_p_nom_max=false;
                                            global_instance.T3_R1_RFID =nullptr;
                                            global_instance.T3_R1=0;
                                            emit_model_placed();

                                            global_table_instance.TABLE3->T3_R1_MODULE->RemoveQitem(global_table_instance.TABLE3->table_3_scene);
                                            //global_table_instance.TABLE1->timer->stop();
                                            //global_table_instance.TABLE1->timer1->start(100);
                                            qDebug("Model Removed");
                                        }
                                        }
                                                break;
                                           default:
                                           qDebug("No");
                                        //else{qDebug("No");}
                                       break;
                                       }
                                   }
                                     break;
                                     default:

                                     break;
                                    }
                         }
                    break;
                    //table3 finished==================================================================
                    case 4://if(table=="TABLE4")
                    {switch(global_instance.RFID_No)
                       {
                        /*case 0://if(RFID=="RFID 0")
                        {switch(global_instance.RFID_Tag_No)
                           {
                            case 1071985354://if(RFID_Tag=="1071985354")
                            {
                                global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T4_R0_RFID =RFID_Tag;
                                global_instance.T4_R0=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                global_table_instance.TABLE4->timer1->stop();
                                global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                                global_table_instance.TABLE4->timer->start(100);
                            }
                            break;
                            case 1072063229://else if(RFID_Tag=="1072063229")
                            {
                                global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T4_R0_RFID =RFID_Tag;
                                global_instance.T4_R0=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                global_table_instance.TABLE4->timer1->stop();
                                global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                                global_table_instance.TABLE4->timer->start(100);
                            }
                            break;
                            case 1072056604://else if(RFID_Tag=="1072056604")
                            {
                                global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T4_R0_RFID =RFID_Tag;
                                global_instance.T4_R0=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                global_table_instance.TABLE4->timer1->stop();
                                global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE4->table_4_scene);
                                global_table_instance.TABLE4->timer->start(100);
                            }
                            break;
                            case 699168295://else if(RFID_Tag=="699168295")
                            {
                                global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T4_R0_RFID =RFID_Tag;
                                global_instance.T4_R0=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                global_table_instance.TABLE4->timer1->stop();
                                global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE4->table_4_scene);
                                global_table_instance.TABLE4->timer->start(100);
                            }
                            break;
                            case 2305939010://else if(RFID_Tag=="2305939010")
                            {
                                global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T4_R0_RFID =RFID_Tag;
                                global_instance.T4_R0=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                global_table_instance.TABLE4->timer1->stop();
                                global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE4->table_4_scene);
                                global_table_instance.TABLE4->timer->start(100);
                            }
                            break;
                            case  2836102489: //else if(RFID_Tag=="2836102489")
                            {
                                global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T4_R0_RFID =RFID_Tag;
                                global_instance.T4_R0=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                global_table_instance.TABLE4->timer1->stop();
                                global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE4->table_4_scene);
                                global_table_instance.TABLE4->timer->start(100);
                            }
                            break;
                            case  1071986763://else if(RFID_Tag=="1071986763")
                    {
                                global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T4_R0_RFID =RFID_Tag;
                                global_instance.T4_R0=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                global_table_instance.TABLE4->timer1->stop();
                                global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/house1.jpg",global_table_instance.TABLE4->table_4_scene);
                                global_table_instance.TABLE4->timer->start(100);
                            }
                            break;
                            case  1072063101://else if(RFID_Tag=="1072063101")
                                   {
                                        global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T4_R0_RFID =RFID_Tag;
                                        global_instance.T4_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                        global_table_instance.TABLE4->timer1->stop();
                                        global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/house2.jpg",global_table_instance.TABLE4->table_4_scene);
                                        global_table_instance.TABLE4->timer->start(100);
                                    }
                            break;
                            case  2298616020://else if(RFID_Tag=="2298616020")
                           {
                                        global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T4_R0_RFID =RFID_Tag;
                                        global_instance.T4_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                        global_table_instance.TABLE4->timer1->stop();
                                        global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/house3.jpg",global_table_instance.TABLE4->table_4_scene);
                                        global_table_instance.TABLE4->timer->start(100);
                                    }
                            break;
                            case  1071733508://else if(RFID_Tag=="1071733508")
                           {
                                        global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T4_R0_RFID =RFID_Tag;
                                        global_instance.T4_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                        global_table_instance.TABLE4->timer1->stop();
                                        global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/house4.jpg",global_table_instance.TABLE4->table_4_scene);
                                        global_table_instance.TABLE4->timer->start(100);
                                    }
                            break;
                            case  3914547211://else if(RFID_Tag=="3914547211")
                           {
                                        global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T4_R0_RFID =RFID_Tag;
                                        global_instance.T4_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                        global_table_instance.TABLE4->timer1->stop();
                                        global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/house5.jpg",global_table_instance.TABLE4->table_4_scene);
                                        global_table_instance.TABLE4->timer->start(100);
                                    }
                            break;
                            case  1071773816://else if(RFID_Tag=="1071773816")
                           {
                                        global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T4_R0_RFID =RFID_Tag;
                                        global_instance.T4_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                        global_table_instance.TABLE4->timer1->stop();
                                        global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/Storage_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                                        global_table_instance.TABLE4->timer->start(100);
                                    }
                            break;
                            case  1071780377://else if(RFID_Tag=="1071780377")
                           {
                                        global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T4_R0_RFID =RFID_Tag;
                                        global_instance.T4_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                        global_table_instance.TABLE4->timer1->stop();
                                        global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/Storage_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                                        global_table_instance.TABLE4->timer->start(100);
                                    }
                            break;
                            case  1071770985://else if(RFID_Tag=="1071770985")
                           {
                                        global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T4_R0_RFID =RFID_Tag;
                                        global_instance.T4_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                        global_table_instance.TABLE4->timer1->stop();
                                        global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/apartment1.jpg",global_table_instance.TABLE4->table_4_scene);
                                        global_table_instance.TABLE4->timer->start(100);
                                    }
                            break;
                            case  7304921://else if(RFID_Tag=="7304921")
                           {
                                        global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T4_R0_RFID =RFID_Tag;
                                        global_instance.T4_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                        global_table_instance.TABLE4->timer1->stop();
                                        global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/apartment2.jpg",global_table_instance.TABLE4->table_4_scene);
                                        global_table_instance.TABLE4->timer->start(100);
                                    }
                            break;
                            case  593072732://else if(RFID_Tag=="593072732")
                           {
                                        global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T4_R0_RFID =RFID_Tag;
                                        global_instance.T4_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                        global_table_instance.TABLE4->timer1->stop();
                                        global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/apartment3.jpg",global_table_instance.TABLE4->table_4_scene);
                                        global_table_instance.TABLE4->timer->start(100);
                                    }
                            break;
                            case  1071753108://else if(RFID_Tag=="1071753108")
                           {
                                        global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T4_R0_RFID =RFID_Tag;
                                        global_instance.T4_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                        global_table_instance.TABLE4->timer1->stop();
                                        global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/apartment4.jpg",global_table_instance.TABLE4->table_4_scene);
                                        global_table_instance.TABLE4->timer->start(100);
                                    }
                            break;
                            case  2836099420://else if(RFID_Tag=="2836099420")
                           {
                                        global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T4_R0_RFID =RFID_Tag;
                                        global_instance.T4_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                        global_table_instance.TABLE4->timer1->stop();
                                        global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                                        global_table_instance.TABLE4->timer->start(100);
                                    }
                            break;
                            case  1840834860://else if(RFID_Tag=="1840834860")
                           {
                                        global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T4_R0_RFID =RFID_Tag;
                                        global_instance.T4_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                        global_table_instance.TABLE4->timer1->stop();
                                        global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                                        global_table_instance.TABLE4->timer->start(100);
                                    }
                            break;
                            case  597999943://else if(RFID_Tag=="597999943")
                           {
                                        global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T4_R0_RFID =RFID_Tag;
                                        global_instance.T4_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                        global_table_instance.TABLE4->timer1->stop();
                                        global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/shopping_mall.jpg",global_table_instance.TABLE4->table_4_scene);
                                        global_table_instance.TABLE4->timer->start(100);
                                    }
                            break;
                            case  8538681://else if(RFID_Tag=="8538681")
                           {
                                        global_instance.T4_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T4_R0_RFID =RFID_Tag;
                                        global_instance.T4_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T4_R0 in MQTT()=:"<<global_instance.T4_R0<<std::endl;
                                        global_table_instance.TABLE4->timer1->stop();
                                        global_table_instance.TABLE4->T4_R0_MODULE=new T4_R0_Model("img/hospital.jpg",global_table_instance.TABLE4->table_4_scene);
                                        global_table_instance.TABLE4->timer->start(100);
                                    }
                            break;
                            case  0://else if(global_instance.T4_R0==1&&RFID_Tag=="0")
                            if(global_instance.T4_R0==1){
                           {
                                        global_instance.T4_R0_RFID =nullptr;
                                        global_instance.T4_R0=0;
                                        emit_model_placed();

                                        global_table_instance.TABLE4->T4_R0_MODULE->RemoveQitem(global_table_instance.TABLE4->table_4_scene);
                                        //global_table_instance.TABLE1->timer->stop();
                                        //global_table_instance.TABLE1->timer1->start(100);
                                        qDebug("Model Removed");
                                    }
                            }
                            break;
                            default://else{qDebug("No");}
                                  qDebug("No");
                            break;
                       }
                    }
                        break;*/
                        case 7://if(RFID=="RFID 7")
                        {switch(global_instance.RFID_Tag_No)
                              {
                               case 1071985354://if(RFID_Tag=="1071985354")
                               {
                                   global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                   global_instance.T4_R7_RFID =RFID_Tag;
                                   global_instance.T4_R7=1;
                                   emit_model_placed();
                                   std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                   global_table_instance.TABLE4->timer1->stop();
                                   global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                                   global_table_instance.TABLE4->timer->start(100);
                               }
                               break;
                               case 1072063229://else if(RFID_Tag=="1072063229")
                               {
                                   global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                   global_instance.T4_R7_RFID =RFID_Tag;
                                   global_instance.T4_R7=1;
                                   emit_model_placed();
                                   std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                   global_table_instance.TABLE4->timer1->stop();
                                   global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                                   global_table_instance.TABLE4->timer->start(100);
                               }
                               break;
                               case 1072056604://else if(RFID_Tag=="1072056604")
                               {
                                   global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                   global_instance.T4_R7_RFID =RFID_Tag;
                                   global_instance.T4_R7=1;
                                   emit_model_placed();
                                   std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                   global_table_instance.TABLE4->timer1->stop();
                                   global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE4->table_4_scene);
                                   global_table_instance.TABLE4->timer->start(100);
                               }
                               break;
                               case 699168295://else if(RFID_Tag=="699168295")
                               {
                                   global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                   global_instance.T4_R7_RFID =RFID_Tag;
                                   global_instance.T4_R7=1;
                                   emit_model_placed();
                                   std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                   global_table_instance.TABLE4->timer1->stop();
                                   global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE4->table_4_scene);
                                   global_table_instance.TABLE4->timer->start(100);
                               }
                               break;
                               case 2305939010://else if(RFID_Tag=="2305939010")
                               {
                                   global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                   global_instance.T4_R7_RFID =RFID_Tag;
                                   global_instance.T4_R7=1;
                                   emit_model_placed();
                                   std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                   global_table_instance.TABLE4->timer1->stop();
                                   global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE4->table_4_scene);
                                   global_table_instance.TABLE4->timer->start(100);
                               }
                               break;
                               case  2836102489: //else if(RFID_Tag=="2836102489")
                               {
                                   global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                   global_instance.T4_R7_RFID =RFID_Tag;
                                   global_instance.T4_R7=1;
                                   emit_model_placed();
                                   std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                   global_table_instance.TABLE4->timer1->stop();
                                   global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE4->table_4_scene);
                                   global_table_instance.TABLE4->timer->start(100);
                               }
                               break;
                               case  1071986763://else if(RFID_Tag=="1071986763")
                       {
                                   global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                   global_instance.T4_R7_RFID =RFID_Tag;
                                   global_instance.T4_R7=1;
                                   emit_model_placed();
                                   std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                   global_table_instance.TABLE4->timer1->stop();
                                   global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/house1.jpg",global_table_instance.TABLE4->table_4_scene);
                                   global_table_instance.TABLE4->timer->start(100);
                               }
                               break;
                               case  1072063101://else if(RFID_Tag=="1072063101")
                                      {
                                           global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R7_RFID =RFID_Tag;
                                           global_instance.T4_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/house2.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                               break;
                               case  2298616020://else if(RFID_Tag=="2298616020")
                              {
                                           global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R7_RFID =RFID_Tag;
                                           global_instance.T4_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/house3.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                               break;
                               case  1071733508://else if(RFID_Tag=="1071733508")
                              {
                                           global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R7_RFID =RFID_Tag;
                                           global_instance.T4_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/house4.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                               break;
                               case  3914547211://else if(RFID_Tag=="3914547211")
                              {
                                           global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R7_RFID =RFID_Tag;
                                           global_instance.T4_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/house5.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                               break;
                               case  1071773816://else if(RFID_Tag=="1071773816")
                              {
                                           global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R7_RFID =RFID_Tag;
                                           global_instance.T4_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/Storage_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                               break;
                               case  1071780377://else if(RFID_Tag=="1071780377")
                              {
                                           global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R7_RFID =RFID_Tag;
                                           global_instance.T4_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/Storage_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                               break;
                               case  1071770985://else if(RFID_Tag=="1071770985")
                              {
                                           global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R7_RFID =RFID_Tag;
                                           global_instance.T4_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/apartment1.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                               break;
                               case  7304921://else if(RFID_Tag=="7304921")
                              {
                                           global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R7_RFID =RFID_Tag;
                                           global_instance.T4_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/apartment2.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                               break;
                               case  593072732://else if(RFID_Tag=="593072732")
                              {
                                           global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R7_RFID =RFID_Tag;
                                           global_instance.T4_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/apartment3.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                               break;
                               case  1071753108://else if(RFID_Tag=="1071753108")
                              {
                                           global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R7_RFID =RFID_Tag;
                                           global_instance.T4_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/apartment4.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                               break;
                               case  2836099420://else if(RFID_Tag=="2836099420")
                              {
                                           global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R7_RFID =RFID_Tag;
                                           global_instance.T4_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                               break;
                               case  1840834860://else if(RFID_Tag=="1840834860")
                              {
                                           global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R7_RFID =RFID_Tag;
                                           global_instance.T4_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                               break;
                               case  597999943://else if(RFID_Tag=="597999943")
                              {
                                           global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R7_RFID =RFID_Tag;
                                           global_instance.T4_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/shopping_mall.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                               break;
                               case  8538681://else if(RFID_Tag=="8538681")
                              {
                                           global_instance.T4_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R7_RFID =RFID_Tag;
                                           global_instance.T4_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R7 in MQTT()=:"<<global_instance.T4_R7<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R7_MODULE=new T4_R7_Model("img/hospital.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }

                               break;
                               case  0://else if(global_instance.T4_R7==1&&RFID_Tag=="0")
                               if(global_instance.T4_R7==1){
                              {
                                           global_instance.T4_R7_RFID =nullptr;
                                           global_instance.T4_R7=0;
                                           emit_model_placed();

                                           global_table_instance.TABLE4->T4_R7_MODULE->RemoveQitem(global_table_instance.TABLE4->table_4_scene);
                                           //global_table_instance.TABLE1->timer->stop();
                                           //global_table_instance.TABLE1->timer1->start(100);
                                           qDebug("Model Removed");
                                       }
                               }
                               break;
                               default://else{qDebug("No");}
                                     qDebug("No");
                               break;
                          }
                    }
                        break;
                        case 6://if(RFID=="RFID 6")
                       {switch(global_instance.RFID_Tag_No)
                          {
                           case 1071985354://if(RFID_Tag=="1071985354")
                           {
                               global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                               global_instance.T4_R6_RFID =RFID_Tag;
                               global_instance.T4_R6=1;
                               emit_model_placed();
                               std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                               global_table_instance.TABLE4->timer1->stop();
                               global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                               global_table_instance.TABLE4->timer->start(100);
                           }
                           break;
                           case 1072063229://else if(RFID_Tag=="1072063229")
                           {
                               global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                               global_instance.T4_R6_RFID =RFID_Tag;
                               global_instance.T4_R6=1;
                               emit_model_placed();
                               std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                               global_table_instance.TABLE4->timer1->stop();
                               global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                               global_table_instance.TABLE4->timer->start(100);
                           }
                           break;
                           case 1072056604://else if(RFID_Tag=="1072056604")
                           {
                               global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                               global_instance.T4_R6_RFID =RFID_Tag;
                               global_instance.T4_R6=1;
                               emit_model_placed();
                               std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                               global_table_instance.TABLE4->timer1->stop();
                               global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE4->table_4_scene);
                               global_table_instance.TABLE4->timer->start(100);
                           }
                           break;
                           case 699168295://else if(RFID_Tag=="699168295")
                           {
                               global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                               global_instance.T4_R6_RFID =RFID_Tag;
                               global_instance.T4_R6=1;
                               emit_model_placed();
                               std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                               global_table_instance.TABLE4->timer1->stop();
                               global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE4->table_4_scene);
                               global_table_instance.TABLE4->timer->start(100);
                           }
                           break;
                           case 2305939010://else if(RFID_Tag=="2305939010")
                           {
                               global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                               global_instance.T4_R6_RFID =RFID_Tag;
                               global_instance.T4_R6=1;
                               emit_model_placed();
                               std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                               global_table_instance.TABLE4->timer1->stop();
                               global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE4->table_4_scene);
                               global_table_instance.TABLE4->timer->start(100);
                           }
                           break;
                           case  2836102489: //else if(RFID_Tag=="2836102489")
                           {
                               global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                               global_instance.T4_R6_RFID =RFID_Tag;
                               global_instance.T4_R6=1;
                               emit_model_placed();
                               std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                               global_table_instance.TABLE4->timer1->stop();
                               global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE4->table_4_scene);
                               global_table_instance.TABLE4->timer->start(100);
                           }
                           break;
                           case  1071986763://else if(RFID_Tag=="1071986763")
                    {
                               global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                               global_instance.T4_R6_RFID =RFID_Tag;
                               global_instance.T4_R6=1;
                               emit_model_placed();
                               std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                               global_table_instance.TABLE4->timer1->stop();
                               global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/house1.jpg",global_table_instance.TABLE4->table_4_scene);
                               global_table_instance.TABLE4->timer->start(100);
                           }
                           break;
                           case  1072063101://else if(RFID_Tag=="1072063101")
                                  {
                                       global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T4_R6_RFID =RFID_Tag;
                                       global_instance.T4_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                                       global_table_instance.TABLE4->timer1->stop();
                                       global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/house2.jpg",global_table_instance.TABLE4->table_4_scene);
                                       global_table_instance.TABLE4->timer->start(100);
                                   }
                           break;
                           case  2298616020://else if(RFID_Tag=="2298616020")
                          {
                                       global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T4_R6_RFID =RFID_Tag;
                                       global_instance.T4_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                                       global_table_instance.TABLE4->timer1->stop();
                                       global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/house3.jpg",global_table_instance.TABLE4->table_4_scene);
                                       global_table_instance.TABLE4->timer->start(100);
                                   }
                           break;
                           case  1071733508://else if(RFID_Tag=="1071733508")
                          {
                                       global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T4_R6_RFID =RFID_Tag;
                                       global_instance.T4_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                                       global_table_instance.TABLE4->timer1->stop();
                                       global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/house4.jpg",global_table_instance.TABLE4->table_4_scene);
                                       global_table_instance.TABLE4->timer->start(100);
                                   }
                           break;
                           case  3914547211://else if(RFID_Tag=="3914547211")
                          {
                                       global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T4_R6_RFID =RFID_Tag;
                                       global_instance.T4_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                                       global_table_instance.TABLE4->timer1->stop();
                                       global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/house5.jpg",global_table_instance.TABLE4->table_4_scene);
                                       global_table_instance.TABLE4->timer->start(100);
                                   }
                           break;
                           case  1071773816://else if(RFID_Tag=="1071773816")
                          {
                                       global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T4_R6_RFID =RFID_Tag;
                                       global_instance.T4_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                                       global_table_instance.TABLE4->timer1->stop();
                                       global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/Storage_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                                       global_table_instance.TABLE4->timer->start(100);
                                   }
                           break;
                           case  1071780377://else if(RFID_Tag=="1071780377")
                          {
                                       global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T4_R6_RFID =RFID_Tag;
                                       global_instance.T4_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                                       global_table_instance.TABLE4->timer1->stop();
                                       global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/Storage_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                                       global_table_instance.TABLE4->timer->start(100);
                                   }
                           break;
                           case  1071770985://else if(RFID_Tag=="1071770985")
                          {
                                       global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T4_R6_RFID =RFID_Tag;
                                       global_instance.T4_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                                       global_table_instance.TABLE4->timer1->stop();
                                       global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/apartment1.jpg",global_table_instance.TABLE4->table_4_scene);
                                       global_table_instance.TABLE4->timer->start(100);
                                   }
                           break;
                           case  7304921://else if(RFID_Tag=="7304921")
                          {
                                       global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T4_R6_RFID =RFID_Tag;
                                       global_instance.T4_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                                       global_table_instance.TABLE4->timer1->stop();
                                       global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/apartment2.jpg",global_table_instance.TABLE4->table_4_scene);
                                       global_table_instance.TABLE4->timer->start(100);
                                   }
                           break;
                           case  593072732://else if(RFID_Tag=="593072732")
                          {
                                       global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T4_R6_RFID =RFID_Tag;
                                       global_instance.T4_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                                       global_table_instance.TABLE4->timer1->stop();
                                       global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/apartment3.jpg",global_table_instance.TABLE4->table_4_scene);
                                       global_table_instance.TABLE4->timer->start(100);
                                   }
                           break;
                           case  1071753108://else if(RFID_Tag=="1071753108")
                          {
                                       global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T4_R6_RFID =RFID_Tag;
                                       global_instance.T4_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                                       global_table_instance.TABLE4->timer1->stop();
                                       global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/apartment4.jpg",global_table_instance.TABLE4->table_4_scene);
                                       global_table_instance.TABLE4->timer->start(100);
                                   }
                           break;
                           case  2836099420://else if(RFID_Tag=="2836099420")
                          {
                                       global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T4_R6_RFID =RFID_Tag;
                                       global_instance.T4_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                                       global_table_instance.TABLE4->timer1->stop();
                                       global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                                       global_table_instance.TABLE4->timer->start(100);
                                   }
                           break;
                           case  1840834860://else if(RFID_Tag=="1840834860")
                          {
                                       global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T4_R6_RFID =RFID_Tag;
                                       global_instance.T4_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                                       global_table_instance.TABLE4->timer1->stop();
                                       global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                                       global_table_instance.TABLE4->timer->start(100);
                                   }
                           break;
                           case  597999943://else if(RFID_Tag=="597999943")
                          {
                                       global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T4_R6_RFID =RFID_Tag;
                                       global_instance.T4_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                                       global_table_instance.TABLE4->timer1->stop();
                                       global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/shopping_mall.jpg",global_table_instance.TABLE4->table_4_scene);
                                       global_table_instance.TABLE4->timer->start(100);
                                   }
                           break;
                           case  8538681://else if(RFID_Tag=="8538681")
                          {
                                       global_instance.T4_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T4_R6_RFID =RFID_Tag;
                                       global_instance.T4_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T4_R6 in MQTT()=:"<<global_instance.T4_R6<<std::endl;
                                       global_table_instance.TABLE4->timer1->stop();
                                       global_table_instance.TABLE4->T4_R6_MODULE=new T4_R6_Model("img/hospital.jpg",global_table_instance.TABLE4->table_4_scene);
                                       global_table_instance.TABLE4->timer->start(100);
                                   }

                           break;
                           case  0://else if(global_instance.T4_R6==1&&RFID_Tag=="0")
                           if(global_instance.T4_R6==1){
                          {
                                       global_instance.T4_R6_RFID =nullptr;
                                       global_instance.T4_R6=0;
                                       emit_model_placed();

                                       global_table_instance.TABLE4->T4_R6_MODULE->RemoveQitem(global_table_instance.TABLE4->table_4_scene);
                                       //global_table_instance.TABLE1->timer->stop();
                                       //global_table_instance.TABLE1->timer1->start(100);
                                       qDebug("Model Removed");
                                   }
                           }
                           break;
                           default://else{qDebug("No");}
                                 qDebug("No");
                           break;
                      }
                    }
                        break;
                        case 5://if(RFID=="RFID 5")
                    {switch(global_instance.RFID_Tag_No)
                    {
                    case 1071985354://if(RFID_Tag=="1071985354")
                    {
                    global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R5_RFID =RFID_Tag;
                    global_instance.T4_R5=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 1072063229://else if(RFID_Tag=="1072063229")
                    {
                    global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R5_RFID =RFID_Tag;
                    global_instance.T4_R5=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 1072056604://else if(RFID_Tag=="1072056604")
                    {
                    global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R5_RFID =RFID_Tag;
                    global_instance.T4_R5=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 699168295://else if(RFID_Tag=="699168295")
                    {
                    global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R5_RFID =RFID_Tag;
                    global_instance.T4_R5=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 2305939010://else if(RFID_Tag=="2305939010")
                    {
                    global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R5_RFID =RFID_Tag;
                    global_instance.T4_R5=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case  2836102489: //else if(RFID_Tag=="2836102489")
                    {
                    global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R5_RFID =RFID_Tag;
                    global_instance.T4_R5=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case  1071986763://else if(RFID_Tag=="1071986763")
                    {
                    global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R5_RFID =RFID_Tag;
                    global_instance.T4_R5=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/house1.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case  1072063101://else if(RFID_Tag=="1072063101")
                      {
                           global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R5_RFID =RFID_Tag;
                           global_instance.T4_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/house2.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  2298616020://else if(RFID_Tag=="2298616020")
                    {
                           global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R5_RFID =RFID_Tag;
                           global_instance.T4_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/house3.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071733508://else if(RFID_Tag=="1071733508")
                    {
                           global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R5_RFID =RFID_Tag;
                           global_instance.T4_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/house4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  3914547211://else if(RFID_Tag=="3914547211")
                    {
                           global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R5_RFID =RFID_Tag;
                           global_instance.T4_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/house5.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071773816://else if(RFID_Tag=="1071773816")
                    {
                           global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R5_RFID =RFID_Tag;
                           global_instance.T4_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/Storage_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071780377://else if(RFID_Tag=="1071780377")
                    {
                           global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R5_RFID =RFID_Tag;
                           global_instance.T4_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/Storage_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071770985://else if(RFID_Tag=="1071770985")
                    {
                           global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R5_RFID =RFID_Tag;
                           global_instance.T4_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/apartment1.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  7304921://else if(RFID_Tag=="7304921")
                    {
                           global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R5_RFID =RFID_Tag;
                           global_instance.T4_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/apartment2.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  593072732://else if(RFID_Tag=="593072732")
                    {
                           global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R5_RFID =RFID_Tag;
                           global_instance.T4_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/apartment3.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071753108://else if(RFID_Tag=="1071753108")
                    {
                           global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R5_RFID =RFID_Tag;
                           global_instance.T4_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/apartment4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  2836099420://else if(RFID_Tag=="2836099420")
                    {
                           global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R5_RFID =RFID_Tag;
                           global_instance.T4_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1840834860://else if(RFID_Tag=="1840834860")
                    {
                           global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R5_RFID =RFID_Tag;
                           global_instance.T4_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  597999943://else if(RFID_Tag=="597999943")
                    {
                           global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R5_RFID =RFID_Tag;
                           global_instance.T4_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/shopping_mall.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  8538681://else if(RFID_Tag=="8538681")
                    {
                           global_instance.T4_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R5_RFID =RFID_Tag;
                           global_instance.T4_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R5 in MQTT()=:"<<global_instance.T4_R5<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R5_MODULE=new T4_R5_Model("img/hospital.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  0://else if(global_instance.T4_R5==1&&RFID_Tag=="0")
                    if(global_instance.T4_R5==1){
                    {
                           global_instance.T4_R5_RFID =nullptr;
                           global_instance.T4_R5=0;
                           emit_model_placed();

                           global_table_instance.TABLE4->T4_R5_MODULE->RemoveQitem(global_table_instance.TABLE4->table_4_scene);
                           //global_table_instance.TABLE1->timer->stop();
                           //global_table_instance.TABLE1->timer1->start(100);
                           qDebug("Model Removed");
                       }
                    }
                    break;
                    default://else{qDebug("No");}
                     qDebug("No");
                    break;
                    }
                    }
                        break;
                        case 4://if(RFID=="RFID 4")
                             {switch(global_instance.RFID_Tag_No)
                                {
                                    case 1071985354://if(RFID_Tag=="1071985354")
                                    {
                                    global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                    global_instance.T4_R4_RFID =RFID_Tag;
                                    global_instance.T4_R4=1;
                                    emit_model_placed();
                                    std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                    global_table_instance.TABLE4->timer1->stop();
                                    global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                                    global_table_instance.TABLE4->timer->start(100);
                                    }
                                    break;
                                    case 1072063229://else if(RFID_Tag=="1072063229")
                                    {
                                    global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                    global_instance.T4_R4_RFID =RFID_Tag;
                                    global_instance.T4_R4=1;
                                    emit_model_placed();
                                    std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                    global_table_instance.TABLE4->timer1->stop();
                                    global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                                    global_table_instance.TABLE4->timer->start(100);
                                    }
                                    break;
                                    case 1072056604://else if(RFID_Tag=="1072056604")
                                    {
                                    global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                    global_instance.T4_R4_RFID =RFID_Tag;
                                    global_instance.T4_R4=1;
                                    emit_model_placed();
                                    std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                    global_table_instance.TABLE4->timer1->stop();
                                    global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE4->table_4_scene);
                                    global_table_instance.TABLE4->timer->start(100);
                                    }
                                    break;
                                    case 699168295://else if(RFID_Tag=="699168295")
                                    {
                                    global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                    global_instance.T4_R4_RFID =RFID_Tag;
                                    global_instance.T4_R4=1;
                                    emit_model_placed();
                                    std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                    global_table_instance.TABLE4->timer1->stop();
                                    global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE4->table_4_scene);
                                    global_table_instance.TABLE4->timer->start(100);
                                    }
                                    break;
                                    case 2305939010://else if(RFID_Tag=="2305939010")
                                    {
                                    global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                    global_instance.T4_R4_RFID =RFID_Tag;
                                    global_instance.T4_R4=1;
                                    emit_model_placed();
                                    std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                    global_table_instance.TABLE4->timer1->stop();
                                    global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE4->table_4_scene);
                                    global_table_instance.TABLE4->timer->start(100);
                                    }
                                    break;
                                    case  2836102489: //else if(RFID_Tag=="2836102489")
                                    {
                                    global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                    global_instance.T4_R4_RFID =RFID_Tag;
                                    global_instance.T4_R4=1;
                                    emit_model_placed();
                                    std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                    global_table_instance.TABLE4->timer1->stop();
                                    global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE4->table_4_scene);
                                    global_table_instance.TABLE4->timer->start(100);
                                    }
                                    break;
                                    case  1071986763://else if(RFID_Tag=="1071986763")
                                    {
                                    global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                    global_instance.T4_R4_RFID =RFID_Tag;
                                    global_instance.T4_R4=1;
                                    emit_model_placed();
                                    std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                    global_table_instance.TABLE4->timer1->stop();
                                    global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/house1.jpg",global_table_instance.TABLE4->table_4_scene);
                                    global_table_instance.TABLE4->timer->start(100);
                                    }
                                    break;
                                    case  1072063101://else if(RFID_Tag=="1072063101")
                                      {
                                           global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R4_RFID =RFID_Tag;
                                           global_instance.T4_R4=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/house2.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                                    break;
                                    case  2298616020://else if(RFID_Tag=="2298616020")
                                    {
                                           global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R4_RFID =RFID_Tag;
                                           global_instance.T4_R4=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/house3.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                                    break;
                                    case  1071733508://else if(RFID_Tag=="1071733508")
                                    {
                                           global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R4_RFID =RFID_Tag;
                                           global_instance.T4_R4=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/house4.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                                    break;
                                    case  3914547211://else if(RFID_Tag=="3914547211")
                                    {
                                           global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R4_RFID =RFID_Tag;
                                           global_instance.T4_R4=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/house5.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                                    break;
                                    case  1071773816://else if(RFID_Tag=="1071773816")
                                    {
                                           global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R4_RFID =RFID_Tag;
                                           global_instance.T4_R4=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/Storage_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                                    break;
                                    case  1071780377://else if(RFID_Tag=="1071780377")
                                    {
                                           global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R4_RFID =RFID_Tag;
                                           global_instance.T4_R4=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/Storage_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                                    break;
                                    case  1071770985://else if(RFID_Tag=="1071770985")
                                    {
                                           global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R4_RFID =RFID_Tag;
                                           global_instance.T4_R4=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/apartment1.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                                    break;
                                    case  7304921://else if(RFID_Tag=="7304921")
                                    {
                                           global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R4_RFID =RFID_Tag;
                                           global_instance.T4_R4=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/apartment2.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                                    break;
                                    case  593072732://else if(RFID_Tag=="593072732")
                                    {
                                           global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R4_RFID =RFID_Tag;
                                           global_instance.T4_R4=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/apartment3.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                                    break;
                                    case  1071753108://else if(RFID_Tag=="1071753108")
                                    {
                                           global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R4_RFID =RFID_Tag;
                                           global_instance.T4_R4=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/apartment4.jpg",global_table_instance.TABLE4->table_4_scene);
                                           std::cout<<"new module"<<std::endl;
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                                    break;
                                    case  2836099420://else if(RFID_Tag=="2836099420")
                                    {
                                           global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R4_RFID =RFID_Tag;
                                           global_instance.T4_R4=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                                    break;
                                    case  1840834860://else if(RFID_Tag=="1840834860")
                                    {
                                           global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R4_RFID =RFID_Tag;
                                           global_instance.T4_R4=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                                    break;
                                    case  597999943://else if(RFID_Tag=="597999943")
                                    {
                                           global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R4_RFID =RFID_Tag;
                                           global_instance.T4_R4=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/shopping_mall.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                                    break;
                                    case  8538681://else if(RFID_Tag=="8538681")
                                    {
                                           global_instance.T4_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T4_R4_RFID =RFID_Tag;
                                           global_instance.T4_R4=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T4_R4 in MQTT()=:"<<global_instance.T4_R4<<std::endl;
                                           global_table_instance.TABLE4->timer1->stop();
                                           global_table_instance.TABLE4->T4_R4_MODULE=new T4_R4_Model("img/hospital.jpg",global_table_instance.TABLE4->table_4_scene);
                                           global_table_instance.TABLE4->timer->start(100);
                                       }
                                    break;
                                    case  0://else if(global_instance.T4_R4==1&&RFID_Tag=="0")
                                    if(global_instance.T4_R4==1){
                                    {
                                           global_instance.T4_R4_RFID =nullptr;
                                           global_instance.T4_R4=0;
                                           emit_model_placed();

                                           global_table_instance.TABLE4->T4_R4_MODULE->RemoveQitem(global_table_instance.TABLE4->table_4_scene);
                                           //global_table_instance.TABLE1->timer->stop();
                                           //global_table_instance.TABLE1->timer1->start(100);
                                           qDebug("Model Removed");
                                       }
                                    }
                                    break;
                                    default://else{qDebug("No");}
                                     qDebug("No");
                                    break;
                                    }
                    }
                        break;
                        case 3://if(RFID=="RFID 3")
                    {switch(global_instance.RFID_Tag_No)
                    {
                    case 1071985354://if(RFID_Tag=="1071985354")
                    {
                    global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R3_RFID =RFID_Tag;
                    global_instance.T4_R3=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 1072063229://else if(RFID_Tag=="1072063229")
                    {
                    global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R3_RFID =RFID_Tag;
                    global_instance.T4_R3=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 1072056604://else if(RFID_Tag=="1072056604")
                    {
                    global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R3_RFID =RFID_Tag;
                    global_instance.T4_R3=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 699168295://else if(RFID_Tag=="699168295")
                    {
                    global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R3_RFID =RFID_Tag;
                    global_instance.T4_R3=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 2305939010://else if(RFID_Tag=="2305939010")
                    {
                    global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R3_RFID =RFID_Tag;
                    global_instance.T4_R3=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case  2836102489: //else if(RFID_Tag=="2836102489")
                    {
                    global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R3_RFID =RFID_Tag;
                    global_instance.T4_R3=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case  1071986763://else if(RFID_Tag=="1071986763")
                    {
                    global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R3_RFID =RFID_Tag;
                    global_instance.T4_R3=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/house1.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case  1072063101://else if(RFID_Tag=="1072063101")
                      {
                           global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R3_RFID =RFID_Tag;
                           global_instance.T4_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/house2.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  2298616020://else if(RFID_Tag=="2298616020")
                    {
                           global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R3_RFID =RFID_Tag;
                           global_instance.T4_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/house3.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071733508://else if(RFID_Tag=="1071733508")
                    {
                           global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R3_RFID =RFID_Tag;
                           global_instance.T4_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/house4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  3914547211://else if(RFID_Tag=="3914547211")
                    {
                           global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R3_RFID =RFID_Tag;
                           global_instance.T4_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/house5.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071773816://else if(RFID_Tag=="1071773816")
                    {
                           global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R3_RFID =RFID_Tag;
                           global_instance.T4_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/Storage_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071780377://else if(RFID_Tag=="1071780377")
                    {
                           global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R3_RFID =RFID_Tag;
                           global_instance.T4_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/Storage_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071770985://else if(RFID_Tag=="1071770985")
                    {
                           global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R3_RFID =RFID_Tag;
                           global_instance.T4_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/apartment1.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  7304921://else if(RFID_Tag=="7304921")
                    {
                           global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R3_RFID =RFID_Tag;
                           global_instance.T4_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/apartment2.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  593072732://else if(RFID_Tag=="593072732")
                    {
                           global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R3_RFID =RFID_Tag;
                           global_instance.T4_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/apartment3.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071753108://else if(RFID_Tag=="1071753108")
                    {
                           global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R3_RFID =RFID_Tag;
                           global_instance.T4_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/apartment4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  2836099420://else if(RFID_Tag=="2836099420")
                    {
                           global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R3_RFID =RFID_Tag;
                           global_instance.T4_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1840834860://else if(RFID_Tag=="1840834860")
                    {
                           global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R3_RFID =RFID_Tag;
                           global_instance.T4_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  597999943://else if(RFID_Tag=="597999943")
                    {
                           global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R3_RFID =RFID_Tag;
                           global_instance.T4_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/shopping_mall.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  8538681://else if(RFID_Tag=="8538681")
                    {
                           global_instance.T4_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R3_RFID =RFID_Tag;
                           global_instance.T4_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R3 in MQTT()=:"<<global_instance.T4_R3<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R3_MODULE=new T4_R3_Model("img/hospital.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }

                    break;
                    case  0://else if(global_instance.T4_R3==1&&RFID_Tag=="0")
                    if(global_instance.T4_R3==1){
                    {
                           global_instance.T4_R3_RFID =nullptr;
                           global_instance.T4_R3=0;
                           emit_model_placed();

                           global_table_instance.TABLE4->T4_R3_MODULE->RemoveQitem(global_table_instance.TABLE4->table_4_scene);
                           //global_table_instance.TABLE1->timer->stop();
                           //global_table_instance.TABLE1->timer1->start(100);
                           qDebug("Model Removed");
                       }
                    }
                    break;
                    default://else{qDebug("No");}
                     qDebug("No");
                    break;
                    }
                    }
                        break;
                        case 2://if(RFID=="RFID 2")
                    {switch(global_instance.RFID_Tag_No)
                    {
                    case 1071985354://if(RFID_Tag=="1071985354")
                    {
                    global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R2_RFID =RFID_Tag;
                    global_instance.T4_R2=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 1072063229://else if(RFID_Tag=="1072063229")
                    {
                    global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R2_RFID =RFID_Tag;
                    global_instance.T4_R2=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 1072056604://else if(RFID_Tag=="1072056604")
                    {
                    global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R2_RFID =RFID_Tag;
                    global_instance.T4_R2=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 699168295://else if(RFID_Tag=="699168295")
                    {
                    global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R2_RFID =RFID_Tag;
                    global_instance.T4_R2=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 2305939010://else if(RFID_Tag=="2305939010")
                    {
                    global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R2_RFID =RFID_Tag;
                    global_instance.T4_R2=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case  2836102489: //else if(RFID_Tag=="2836102489")
                    {
                    global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R2_RFID =RFID_Tag;
                    global_instance.T4_R2=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case  1071986763://else if(RFID_Tag=="1071986763")
                    {
                    global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R2_RFID =RFID_Tag;
                    global_instance.T4_R2=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/house1.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case  1072063101://else if(RFID_Tag=="1072063101")
                      {
                           global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R2_RFID =RFID_Tag;
                           global_instance.T4_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/house2.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  2298616020://else if(RFID_Tag=="2298616020")
                    {
                           global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R2_RFID =RFID_Tag;
                           global_instance.T4_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/house3.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071733508://else if(RFID_Tag=="1071733508")
                    {
                           global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R2_RFID =RFID_Tag;
                           global_instance.T4_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/house4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  3914547211://else if(RFID_Tag=="3914547211")
                    {
                           global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R2_RFID =RFID_Tag;
                           global_instance.T4_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/house5.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071773816://else if(RFID_Tag=="1071773816")
                    {
                           global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R2_RFID =RFID_Tag;
                           global_instance.T4_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/Storage_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071780377://else if(RFID_Tag=="1071780377")
                    {
                           global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R2_RFID =RFID_Tag;
                           global_instance.T4_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/Storage_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071770985://else if(RFID_Tag=="1071770985")
                    {
                           global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R2_RFID =RFID_Tag;
                           global_instance.T4_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/apartment1.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  7304921://else if(RFID_Tag=="7304921")
                    {
                           global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R2_RFID =RFID_Tag;
                           global_instance.T4_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/apartment2.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  593072732://else if(RFID_Tag=="593072732")
                    {
                           global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R2_RFID =RFID_Tag;
                           global_instance.T4_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/apartment3.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071753108://else if(RFID_Tag=="1071753108")
                    {
                           global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R2_RFID =RFID_Tag;
                           global_instance.T4_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/apartment4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  2836099420://else if(RFID_Tag=="2836099420")
                    {
                           global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R2_RFID =RFID_Tag;
                           global_instance.T4_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1840834860://else if(RFID_Tag=="1840834860")
                    {
                           global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R2_RFID =RFID_Tag;
                           global_instance.T4_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  597999943://else if(RFID_Tag=="597999943")
                    {
                           global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R2_RFID =RFID_Tag;
                           global_instance.T4_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/shopping_mall.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  8538681://else if(RFID_Tag=="8538681")
                    {
                           global_instance.T4_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R2_RFID =RFID_Tag;
                           global_instance.T4_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R2 in MQTT()=:"<<global_instance.T4_R2<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R2_MODULE=new T4_R2_Model("img/hospital.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }

                    break;
                    case  0://else if(global_instance.T4_R2==1&&RFID_Tag=="0")
                    if(global_instance.T4_R2==1){
                    {
                           global_instance.T4_R2_RFID =nullptr;
                           global_instance.T4_R2=0;
                           emit_model_placed();

                           global_table_instance.TABLE4->T4_R2_MODULE->RemoveQitem(global_table_instance.TABLE4->table_4_scene);
                           //global_table_instance.TABLE1->timer->stop();
                           //global_table_instance.TABLE1->timer1->start(100);
                           qDebug("Model Removed");
                       }
                    }
                    break;
                    default://else{qDebug("No");}
                     qDebug("No");
                    break;
                    }
                    }
                        break;
                        case 1://if(RFID=="RFID 1")
                    {switch(global_instance.RFID_Tag_No)
                    {
                    case 1071985354://if(RFID_Tag=="1071985354")
                    {
                    global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R1_RFID =RFID_Tag;
                    global_instance.T4_R1=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 1072063229://else if(RFID_Tag=="1072063229")
                    {
                    global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R1_RFID =RFID_Tag;
                    global_instance.T4_R1=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 1072056604://else if(RFID_Tag=="1072056604")
                    {
                    global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R1_RFID =RFID_Tag;
                    global_instance.T4_R1=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 699168295://else if(RFID_Tag=="699168295")
                    {
                    global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R1_RFID =RFID_Tag;
                    global_instance.T4_R1=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case 2305939010://else if(RFID_Tag=="2305939010")
                    {
                    global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R1_RFID =RFID_Tag;
                    global_instance.T4_R1=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case  2836102489: //else if(RFID_Tag=="2836102489")
                    {
                    global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R1_RFID =RFID_Tag;
                    global_instance.T4_R1=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case  1071986763://else if(RFID_Tag=="1071986763")
                    {
                    global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T4_R1_RFID =RFID_Tag;
                    global_instance.T4_R1=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                    global_table_instance.TABLE4->timer1->stop();
                    global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/house1.jpg",global_table_instance.TABLE4->table_4_scene);
                    global_table_instance.TABLE4->timer->start(100);
                    }
                    break;
                    case  1072063101://else if(RFID_Tag=="1072063101")
                      {
                           global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R1_RFID =RFID_Tag;
                           global_instance.T4_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/house2.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  2298616020://else if(RFID_Tag=="2298616020")
                    {
                           global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R1_RFID =RFID_Tag;
                           global_instance.T4_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/house3.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071733508://else if(RFID_Tag=="1071733508")
                    {
                           global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R1_RFID =RFID_Tag;
                           global_instance.T4_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/house4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  3914547211://else if(RFID_Tag=="3914547211")
                    {
                           global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R1_RFID =RFID_Tag;
                           global_instance.T4_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/house5.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071773816://else if(RFID_Tag=="1071773816")
                    {
                           global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R1_RFID =RFID_Tag;
                           global_instance.T4_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/Storage_house_1.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071780377://else if(RFID_Tag=="1071780377")
                    {
                           global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R1_RFID =RFID_Tag;
                           global_instance.T4_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/Storage_house_2.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071770985://else if(RFID_Tag=="1071770985")
                    {
                           global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R1_RFID =RFID_Tag;
                           global_instance.T4_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/apartment1.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  7304921://else if(RFID_Tag=="7304921")
                    {
                           global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R1_RFID =RFID_Tag;
                           global_instance.T4_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/apartment2.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  593072732://else if(RFID_Tag=="593072732")
                    {
                           global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R1_RFID =RFID_Tag;
                           global_instance.T4_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/apartment3.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1071753108://else if(RFID_Tag=="1071753108")
                    {
                           global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R1_RFID =RFID_Tag;
                           global_instance.T4_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/apartment4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  2836099420://else if(RFID_Tag=="2836099420")
                    {
                           global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R1_RFID =RFID_Tag;
                           global_instance.T4_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  1840834860://else if(RFID_Tag=="1840834860")
                    {
                           global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R1_RFID =RFID_Tag;
                           global_instance.T4_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/apartmenT4.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  597999943://else if(RFID_Tag=="597999943")
                    {
                           global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R1_RFID =RFID_Tag;
                           global_instance.T4_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/shopping_mall.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }
                    break;
                    case  8538681://else if(RFID_Tag=="8538681")
                    {
                           global_instance.T4_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T4_R1_RFID =RFID_Tag;
                           global_instance.T4_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T4_R1 in MQTT()=:"<<global_instance.T4_R1<<std::endl;
                           global_table_instance.TABLE4->timer1->stop();
                           global_table_instance.TABLE4->T4_R1_MODULE=new T4_R1_Model("img/hospital.jpg",global_table_instance.TABLE4->table_4_scene);
                           global_table_instance.TABLE4->timer->start(100);
                       }

                    break;
                    case  0://else if(global_instance.T4_R1==1&&RFID_Tag=="0")
                    if(global_instance.T4_R1==1){
                    {
                           global_instance.T4_R1_RFID =nullptr;
                           global_instance.T4_R1=0;
                           emit_model_placed();

                           global_table_instance.TABLE4->T4_R1_MODULE->RemoveQitem(global_table_instance.TABLE4->table_4_scene);
                           //global_table_instance.TABLE1->timer->stop();
                           //global_table_instance.TABLE1->timer1->start(100);
                           qDebug("Model Removed");
                       }
                    }
                    break;
                    default://else{qDebug("No");}
                     qDebug("No");
                    break;
                    }
                    }
                        break;
                        default:

                        break;
                       }
                    }
                    break;
                    //table4 finished==================================================================
                    case 5://if(table=="Table5")
                    {switch(global_instance.RFID_No)
                       {
                        case 0://if(RFID=="RFID 0")
                        {switch(global_instance.RFID_Tag_No)
                           {
                            case 1071985354://if(RFID_Tag=="1071985354")
                            {
                                global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T5_R0_RFID =RFID_Tag;
                                global_instance.T5_R0=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                global_table_instance.TABLE5->timer1->stop();
                                global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                                global_table_instance.TABLE5->timer->start(100);
                            }
                            break;
                            case 1072063229://else if(RFID_Tag=="1072063229")
                            {
                                global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T5_R0_RFID =RFID_Tag;
                                global_instance.T5_R0=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                global_table_instance.TABLE5->timer1->stop();
                                global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                                global_table_instance.TABLE5->timer->start(100);
                            }
                            break;
                            case 1072056604://else if(RFID_Tag=="1072056604")
                            {
                                global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T5_R0_RFID =RFID_Tag;
                                global_instance.T5_R0=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                global_table_instance.TABLE5->timer1->stop();
                                global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE5->table_5_scene);
                                global_table_instance.TABLE5->timer->start(100);
                            }
                            break;
                            case 699168295://else if(RFID_Tag=="699168295")
                            {
                                global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T5_R0_RFID =RFID_Tag;
                                global_instance.T5_R0=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                global_table_instance.TABLE5->timer1->stop();
                                global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE5->table_5_scene);
                                global_table_instance.TABLE5->timer->start(100);
                            }
                            break;
                            case 2305939010://else if(RFID_Tag=="2305939010")
                            {
                                global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T5_R0_RFID =RFID_Tag;
                                global_instance.T5_R0=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                global_table_instance.TABLE5->timer1->stop();
                                global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE5->table_5_scene);
                                global_table_instance.TABLE5->timer->start(100);
                            }
                            break;
                            case  2836102489: //else if(RFID_Tag=="2836102489")
                            {
                                global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T5_R0_RFID =RFID_Tag;
                                global_instance.T5_R0=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                global_table_instance.TABLE5->timer1->stop();
                                global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE5->table_5_scene);
                                global_table_instance.TABLE5->timer->start(100);
                            }
                            break;
                            case  1071986763://else if(RFID_Tag=="1071986763")
                    {
                                global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T5_R0_RFID =RFID_Tag;
                                global_instance.T5_R0=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                global_table_instance.TABLE5->timer1->stop();
                                global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/house1.jpg",global_table_instance.TABLE5->table_5_scene);
                                global_table_instance.TABLE5->timer->start(100);
                            }
                            break;
                            case  1072063101://else if(RFID_Tag=="1072063101")
                                   {
                                        global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T5_R0_RFID =RFID_Tag;
                                        global_instance.T5_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                        global_table_instance.TABLE5->timer1->stop();
                                        global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/house2.jpg",global_table_instance.TABLE5->table_5_scene);
                                        global_table_instance.TABLE5->timer->start(100);
                                    }
                            break;
                            case  2298616020://else if(RFID_Tag=="2298616020")
                           {
                                        global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T5_R0_RFID =RFID_Tag;
                                        global_instance.T5_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                        global_table_instance.TABLE5->timer1->stop();
                                        global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/house3.jpg",global_table_instance.TABLE5->table_5_scene);
                                        global_table_instance.TABLE5->timer->start(100);
                                    }
                            break;
                            case  1071733508://else if(RFID_Tag=="1071733508")
                           {
                                        global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T5_R0_RFID =RFID_Tag;
                                        global_instance.T5_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                        global_table_instance.TABLE5->timer1->stop();
                                        global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/house4.jpg",global_table_instance.TABLE5->table_5_scene);
                                        global_table_instance.TABLE5->timer->start(100);
                                    }
                            break;
                            case  3914547211://else if(RFID_Tag=="3914547211")
                           {
                                        global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T5_R0_RFID =RFID_Tag;
                                        global_instance.T5_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                        global_table_instance.TABLE5->timer1->stop();
                                        global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/house5.jpg",global_table_instance.TABLE5->table_5_scene);
                                        global_table_instance.TABLE5->timer->start(100);
                                    }
                            break;
                            case  1071773816://else if(RFID_Tag=="1071773816")
                           {
                                        global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T5_R0_RFID =RFID_Tag;
                                        global_instance.T5_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                        global_table_instance.TABLE5->timer1->stop();
                                        global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/Storage_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                                        global_table_instance.TABLE5->timer->start(100);
                                    }
                            break;
                            case  1071780377://else if(RFID_Tag=="1071780377")
                           {
                                        global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T5_R0_RFID =RFID_Tag;
                                        global_instance.T5_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                        global_table_instance.TABLE5->timer1->stop();
                                        global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/Storage_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                                        global_table_instance.TABLE5->timer->start(100);
                                    }
                            break;
                            case  1071770985://else if(RFID_Tag=="1071770985")
                           {
                                        global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T5_R0_RFID =RFID_Tag;
                                        global_instance.T5_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                        global_table_instance.TABLE5->timer1->stop();
                                        global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/apartment1.jpg",global_table_instance.TABLE5->table_5_scene);
                                        global_table_instance.TABLE5->timer->start(100);
                                    }
                            break;
                            case  7304921://else if(RFID_Tag=="7304921")
                           {
                                        global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T5_R0_RFID =RFID_Tag;
                                        global_instance.T5_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                        global_table_instance.TABLE5->timer1->stop();
                                        global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/apartment2.jpg",global_table_instance.TABLE5->table_5_scene);
                                        global_table_instance.TABLE5->timer->start(100);
                                    }
                            break;
                            case  593072732://else if(RFID_Tag=="593072732")
                           {
                                        global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T5_R0_RFID =RFID_Tag;
                                        global_instance.T5_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                        global_table_instance.TABLE5->timer1->stop();
                                        global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/apartment3.jpg",global_table_instance.TABLE5->table_5_scene);
                                        global_table_instance.TABLE5->timer->start(100);
                                    }
                            break;
                            case  1071753108://else if(RFID_Tag=="1071753108")
                           {
                                        global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T5_R0_RFID =RFID_Tag;
                                        global_instance.T5_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                        global_table_instance.TABLE5->timer1->stop();
                                        global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/apartment4.jpg",global_table_instance.TABLE5->table_5_scene);
                                        global_table_instance.TABLE5->timer->start(100);
                                    }
                            break;
                            case  2836099420://else if(RFID_Tag=="2836099420")
                           {
                                        global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T5_R0_RFID =RFID_Tag;
                                        global_instance.T5_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                        global_table_instance.TABLE5->timer1->stop();
                                        global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/apartment5.jpg",global_table_instance.TABLE5->table_5_scene);
                                        global_table_instance.TABLE5->timer->start(100);
                                    }
                            break;
                            case  1840834860://else if(RFID_Tag=="1840834860")
                           {
                                        global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T5_R0_RFID =RFID_Tag;
                                        global_instance.T5_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                        global_table_instance.TABLE5->timer1->stop();
                                        global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/apartment6.jpg",global_table_instance.TABLE5->table_5_scene);
                                        global_table_instance.TABLE5->timer->start(100);
                                    }
                            break;
                            case  597999943://else if(RFID_Tag=="597999943")
                           {
                                        global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T5_R0_RFID =RFID_Tag;
                                        global_instance.T5_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                        global_table_instance.TABLE5->timer1->stop();
                                        global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/shopping_mall.jpg",global_table_instance.TABLE5->table_5_scene);
                                        global_table_instance.TABLE5->timer->start(100);
                                    }
                            break;
                            case  8538681://else if(RFID_Tag=="8538681")
                           {
                                        global_instance.T5_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T5_R0_RFID =RFID_Tag;
                                        global_instance.T5_R0=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T5_R0 in MQTT()=:"<<global_instance.T5_R0<<std::endl;
                                        global_table_instance.TABLE5->timer1->stop();
                                        global_table_instance.TABLE5->T5_R0_MODULE=new T5_R0_Model("img/hospital.jpg",global_table_instance.TABLE5->table_5_scene);
                                        global_table_instance.TABLE5->timer->start(100);
                                    }

                            break;
                            case  0://else if(global_instance.T5_R0==1&&RFID_Tag=="0")
                            if(global_instance.T5_R0==1){
                           {
                                        global_instance.T5_R0_RFID =nullptr;
                                        global_instance.T5_R0=0;
                                        emit_model_placed();

                                        global_table_instance.TABLE5->T5_R0_MODULE->RemoveQitem(global_table_instance.TABLE5->table_5_scene);
                                        //global_table_instance.TABLE1->timer->stop();
                                        //global_table_instance.TABLE1->timer1->start(100);
                                        qDebug("Model Removed");
                                    }
                            }
                            break;
                            default://else{qDebug("No");}
                                  qDebug("No");
                            break;
                       }

                    }
                        break;
                        case 7://if(RFID=="RFID 7")
                        {switch(global_instance.RFID_Tag_No)
                              {
                               case 1071985354://if(RFID_Tag=="1071985354")
                               {
                                   global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                   global_instance.T5_R7_RFID =RFID_Tag;
                                   global_instance.T5_R7=1;
                                   emit_model_placed();
                                   std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                   global_table_instance.TABLE5->timer1->stop();
                                   global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                                   global_table_instance.TABLE5->timer->start(100);
                               }
                               break;
                               case 1072063229://else if(RFID_Tag=="1072063229")
                               {
                                   global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                   global_instance.T5_R7_RFID =RFID_Tag;
                                   global_instance.T5_R7=1;
                                   emit_model_placed();
                                   std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                   global_table_instance.TABLE5->timer1->stop();
                                   global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                                   global_table_instance.TABLE5->timer->start(100);
                               }
                               break;
                               case 1072056604://else if(RFID_Tag=="1072056604")
                               {
                                   global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                   global_instance.T5_R7_RFID =RFID_Tag;
                                   global_instance.T5_R7=1;
                                   emit_model_placed();
                                   std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                   global_table_instance.TABLE5->timer1->stop();
                                   global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE5->table_5_scene);
                                   global_table_instance.TABLE5->timer->start(100);
                               }
                               break;
                               case 699168295://else if(RFID_Tag=="699168295")
                               {
                                   global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                   global_instance.T5_R7_RFID =RFID_Tag;
                                   global_instance.T5_R7=1;
                                   emit_model_placed();
                                   std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                   global_table_instance.TABLE5->timer1->stop();
                                   global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE5->table_5_scene);
                                   global_table_instance.TABLE5->timer->start(100);
                               }
                               break;
                               case 2305939010://else if(RFID_Tag=="2305939010")
                               {
                                   global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                   global_instance.T5_R7_RFID =RFID_Tag;
                                   global_instance.T5_R7=1;
                                   emit_model_placed();
                                   std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                   global_table_instance.TABLE5->timer1->stop();
                                   global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE5->table_5_scene);
                                   global_table_instance.TABLE5->timer->start(100);
                               }
                               break;
                               case  2836102489: //else if(RFID_Tag=="2836102489")
                               {
                                   global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                   global_instance.T5_R7_RFID =RFID_Tag;
                                   global_instance.T5_R7=1;
                                   emit_model_placed();
                                   std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                   global_table_instance.TABLE5->timer1->stop();
                                   global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE5->table_5_scene);
                                   global_table_instance.TABLE5->timer->start(100);
                               }
                               break;
                               case  1071986763://else if(RFID_Tag=="1071986763")
                       {
                                   global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                   global_instance.T5_R7_RFID =RFID_Tag;
                                   global_instance.T5_R7=1;
                                   emit_model_placed();
                                   std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                   global_table_instance.TABLE5->timer1->stop();
                                   global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/house1.jpg",global_table_instance.TABLE5->table_5_scene);
                                   global_table_instance.TABLE5->timer->start(100);
                               }
                               break;
                               case  1072063101://else if(RFID_Tag=="1072063101")
                                      {
                                           global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T5_R7_RFID =RFID_Tag;
                                           global_instance.T5_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                           global_table_instance.TABLE5->timer1->stop();
                                           global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/house2.jpg",global_table_instance.TABLE5->table_5_scene);
                                           global_table_instance.TABLE5->timer->start(100);
                                       }
                               break;
                               case  2298616020://else if(RFID_Tag=="2298616020")
                              {
                                           global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T5_R7_RFID =RFID_Tag;
                                           global_instance.T5_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                           global_table_instance.TABLE5->timer1->stop();
                                           global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/house3.jpg",global_table_instance.TABLE5->table_5_scene);
                                           global_table_instance.TABLE5->timer->start(100);
                                       }
                               break;
                               case  1071733508://else if(RFID_Tag=="1071733508")
                              {
                                           global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T5_R7_RFID =RFID_Tag;
                                           global_instance.T5_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                           global_table_instance.TABLE5->timer1->stop();
                                           global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/house4.jpg",global_table_instance.TABLE5->table_5_scene);
                                           global_table_instance.TABLE5->timer->start(100);
                                       }
                               break;
                               case  3914547211://else if(RFID_Tag=="3914547211")
                              {
                                           global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T5_R7_RFID =RFID_Tag;
                                           global_instance.T5_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                           global_table_instance.TABLE5->timer1->stop();
                                           global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/house5.jpg",global_table_instance.TABLE5->table_5_scene);
                                           global_table_instance.TABLE5->timer->start(100);
                                       }
                               break;
                               case  1071773816://else if(RFID_Tag=="1071773816")


                              {
                                           global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T5_R7_RFID =RFID_Tag;
                                           global_instance.T5_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                           global_table_instance.TABLE5->timer1->stop();
                                           global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/Storage_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                                           global_table_instance.TABLE5->timer->start(100);
                                       }
                               break;
                               case  1071780377://else if(RFID_Tag=="1071780377")


                              {
                                           global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T5_R7_RFID =RFID_Tag;
                                           global_instance.T5_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                           global_table_instance.TABLE5->timer1->stop();
                                           global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/Storage_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                                           global_table_instance.TABLE5->timer->start(100);
                                       }
                               break;
                               case  1071770985://else if(RFID_Tag=="1071770985")
                              {
                                           global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T5_R7_RFID =RFID_Tag;
                                           global_instance.T5_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                           global_table_instance.TABLE5->timer1->stop();
                                           global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/apartment1.jpg",global_table_instance.TABLE5->table_5_scene);
                                           global_table_instance.TABLE5->timer->start(100);
                                       }
                               break;
                               case  7304921://else if(RFID_Tag=="7304921")
                              {
                                           global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T5_R7_RFID =RFID_Tag;
                                           global_instance.T5_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                           global_table_instance.TABLE5->timer1->stop();
                                           global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/apartment2.jpg",global_table_instance.TABLE5->table_5_scene);
                                           global_table_instance.TABLE5->timer->start(100);
                                       }
                               break;
                               case  593072732://else if(RFID_Tag=="593072732")
                              {
                                           global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T5_R7_RFID =RFID_Tag;
                                           global_instance.T5_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                           global_table_instance.TABLE5->timer1->stop();
                                           global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/apartment3.jpg",global_table_instance.TABLE5->table_5_scene);
                                           global_table_instance.TABLE5->timer->start(100);
                                       }
                               break;
                               case  1071753108://else if(RFID_Tag=="1071753108")
                              {
                                           global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T5_R7_RFID =RFID_Tag;
                                           global_instance.T5_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                           global_table_instance.TABLE5->timer1->stop();
                                           global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/apartment4.jpg",global_table_instance.TABLE5->table_5_scene);
                                           global_table_instance.TABLE5->timer->start(100);
                                       }
                               break;
                               case  2836099420://else if(RFID_Tag=="2836099420")
                              {
                                           global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T5_R7_RFID =RFID_Tag;
                                           global_instance.T5_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                           global_table_instance.TABLE5->timer1->stop();
                                           global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/apartment5.jpg",global_table_instance.TABLE5->table_5_scene);
                                           global_table_instance.TABLE5->timer->start(100);
                                       }
                               break;
                               case  1840834860://else if(RFID_Tag=="1840834860")
                              {
                                           global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T5_R7_RFID =RFID_Tag;
                                           global_instance.T5_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                           global_table_instance.TABLE5->timer1->stop();
                                           global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/apartment6.jpg",global_table_instance.TABLE5->table_5_scene);
                                           global_table_instance.TABLE5->timer->start(100);
                                       }
                               break;
                               case  597999943://else if(RFID_Tag=="597999943")
                              {
                                           global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T5_R7_RFID =RFID_Tag;
                                           global_instance.T5_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                           global_table_instance.TABLE5->timer1->stop();
                                           global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/shopping_mall.jpg",global_table_instance.TABLE5->table_5_scene);
                                           global_table_instance.TABLE5->timer->start(100);
                                       }
                               break;
                               case  8538681://else if(RFID_Tag=="8538681")
                              {
                                           global_instance.T5_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                           global_instance.T5_R7_RFID =RFID_Tag;
                                           global_instance.T5_R7=1;
                                           emit_model_placed();
                                           std::cout<<"global_instance.T5_R7 in MQTT()=:"<<global_instance.T5_R7<<std::endl;
                                           global_table_instance.TABLE5->timer1->stop();
                                           global_table_instance.TABLE5->T5_R7_MODULE=new T5_R7_Model("img/hospital.jpg",global_table_instance.TABLE5->table_5_scene);
                                           global_table_instance.TABLE5->timer->start(100);
                                       }

                               break;
                               case  0://else if(global_instance.T5_R7==1&&RFID_Tag=="0")
                               if(global_instance.T5_R7==1){
                              {
                                           global_instance.T5_R7_RFID =nullptr;
                                           global_instance.T5_R7=0;
                                           emit_model_placed();

                                           global_table_instance.TABLE5->T5_R7_MODULE->RemoveQitem(global_table_instance.TABLE5->table_5_scene);
                                           //global_table_instance.TABLE1->timer->stop();
                                           //global_table_instance.TABLE1->timer1->start(100);
                                           qDebug("Model Removed");
                                       }
                               }
                               break;
                               default://else{qDebug("No");}
                                     qDebug("No");
                               break;
                          }
                    }
                        break;
                        case 6://if(RFID=="RFID 6")
                       {switch(global_instance.RFID_Tag_No)
                          {
                           case 1071985354://if(RFID_Tag=="1071985354")
                           {
                               global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                               global_instance.T5_R6_RFID =RFID_Tag;
                               global_instance.T5_R6=1;
                               emit_model_placed();
                               std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                               global_table_instance.TABLE5->timer1->stop();
                               global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                               global_table_instance.TABLE5->timer->start(100);
                           }
                           break;
                           case 1072063229://else if(RFID_Tag=="1072063229")
                           {
                               global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                               global_instance.T5_R6_RFID =RFID_Tag;
                               global_instance.T5_R6=1;
                               emit_model_placed();
                               std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                               global_table_instance.TABLE5->timer1->stop();
                               global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                               global_table_instance.TABLE5->timer->start(100);
                           }
                           break;
                           case 1072056604://else if(RFID_Tag=="1072056604")
                           {
                               global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                               global_instance.T5_R6_RFID =RFID_Tag;
                               global_instance.T5_R6=1;
                               emit_model_placed();
                               std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                               global_table_instance.TABLE5->timer1->stop();
                               global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE5->table_5_scene);
                               global_table_instance.TABLE5->timer->start(100);
                           }
                           break;
                           case 699168295://else if(RFID_Tag=="699168295")
                           {
                               global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                               global_instance.T5_R6_RFID =RFID_Tag;
                               global_instance.T5_R6=1;
                               emit_model_placed();
                               std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                               global_table_instance.TABLE5->timer1->stop();
                               global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE5->table_5_scene);
                               global_table_instance.TABLE5->timer->start(100);
                           }
                           break;
                           case 2305939010://else if(RFID_Tag=="2305939010")
                           {
                               global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                               global_instance.T5_R6_RFID =RFID_Tag;
                               global_instance.T5_R6=1;
                               emit_model_placed();
                               std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                               global_table_instance.TABLE5->timer1->stop();
                               global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE5->table_5_scene);
                               global_table_instance.TABLE5->timer->start(100);
                           }
                           break;
                           case  2836102489: //else if(RFID_Tag=="2836102489")
                           {
                               global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                               global_instance.T5_R6_RFID =RFID_Tag;
                               global_instance.T5_R6=1;
                               emit_model_placed();
                               std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                               global_table_instance.TABLE5->timer1->stop();
                               global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE5->table_5_scene);
                               global_table_instance.TABLE5->timer->start(100);
                           }
                           break;
                           case  1071986763://else if(RFID_Tag=="1071986763")
                    {
                               global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                               global_instance.T5_R6_RFID =RFID_Tag;
                               global_instance.T5_R6=1;
                               emit_model_placed();
                               std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                               global_table_instance.TABLE5->timer1->stop();
                               global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/house1.jpg",global_table_instance.TABLE5->table_5_scene);
                               global_table_instance.TABLE5->timer->start(100);
                           }
                           break;
                           case  1072063101://else if(RFID_Tag=="1072063101")
                                  {
                                       global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T5_R6_RFID =RFID_Tag;
                                       global_instance.T5_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                                       global_table_instance.TABLE5->timer1->stop();
                                       global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/house2.jpg",global_table_instance.TABLE5->table_5_scene);
                                       global_table_instance.TABLE5->timer->start(100);
                                   }
                           break;
                           case  2298616020://else if(RFID_Tag=="2298616020")
                          {
                                       global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T5_R6_RFID =RFID_Tag;
                                       global_instance.T5_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                                       global_table_instance.TABLE5->timer1->stop();
                                       global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/house3.jpg",global_table_instance.TABLE5->table_5_scene);
                                       global_table_instance.TABLE5->timer->start(100);
                                   }
                           break;
                           case  1071733508://else if(RFID_Tag=="1071733508")
                          {
                                       global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T5_R6_RFID =RFID_Tag;
                                       global_instance.T5_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                                       global_table_instance.TABLE5->timer1->stop();
                                       global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/house4.jpg",global_table_instance.TABLE5->table_5_scene);
                                       global_table_instance.TABLE5->timer->start(100);
                                   }
                           break;
                           case  3914547211://else if(RFID_Tag=="3914547211")
                          {
                                       global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T5_R6_RFID =RFID_Tag;
                                       global_instance.T5_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                                       global_table_instance.TABLE5->timer1->stop();
                                       global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/house5.jpg",global_table_instance.TABLE5->table_5_scene);
                                       global_table_instance.TABLE5->timer->start(100);
                                   }
                           break;
                           case  1071773816://else if(RFID_Tag=="1071773816")
                          {
                                       global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T5_R6_RFID =RFID_Tag;
                                       global_instance.T5_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                                       global_table_instance.TABLE5->timer1->stop();
                                       global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/Storage_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                                       global_table_instance.TABLE5->timer->start(100);
                                   }
                           break;
                           case  1071780377://else if(RFID_Tag=="1071780377")
                          {
                                       global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T5_R6_RFID =RFID_Tag;
                                       global_instance.T5_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                                       global_table_instance.TABLE5->timer1->stop();
                                       global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/Storage_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                                       global_table_instance.TABLE5->timer->start(100);
                                   }
                           break;
                           case  1071770985://else if(RFID_Tag=="1071770985")
                          {
                                       global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T5_R6_RFID =RFID_Tag;
                                       global_instance.T5_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                                       global_table_instance.TABLE5->timer1->stop();
                                       global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/apartment1.jpg",global_table_instance.TABLE5->table_5_scene);
                                       global_table_instance.TABLE5->timer->start(100);
                                   }
                           break;
                           case  7304921://else if(RFID_Tag=="7304921")
                          {
                                       global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T5_R6_RFID =RFID_Tag;
                                       global_instance.T5_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                                       global_table_instance.TABLE5->timer1->stop();
                                       global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/apartment2.jpg",global_table_instance.TABLE5->table_5_scene);
                                       global_table_instance.TABLE5->timer->start(100);
                                   }
                           break;
                           case  593072732://else if(RFID_Tag=="593072732")
                          {
                                       global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T5_R6_RFID =RFID_Tag;
                                       global_instance.T5_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                                       global_table_instance.TABLE5->timer1->stop();
                                       global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/apartment3.jpg",global_table_instance.TABLE5->table_5_scene);
                                       global_table_instance.TABLE5->timer->start(100);
                                   }
                           break;
                           case  1071753108://else if(RFID_Tag=="1071753108")
                          {
                                       global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T5_R6_RFID =RFID_Tag;
                                       global_instance.T5_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                                       global_table_instance.TABLE5->timer1->stop();
                                       global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/apartment4.jpg",global_table_instance.TABLE5->table_5_scene);
                                       global_table_instance.TABLE5->timer->start(100);
                                   }
                           break;
                           case  2836099420://else if(RFID_Tag=="2836099420")
                          {
                                       global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T5_R6_RFID =RFID_Tag;
                                       global_instance.T5_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                                       global_table_instance.TABLE5->timer1->stop();
                                       global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/apartment5.jpg",global_table_instance.TABLE5->table_5_scene);
                                       global_table_instance.TABLE5->timer->start(100);
                                   }
                           break;
                           case  1840834860://else if(RFID_Tag=="1840834860")
                          {
                                       global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T5_R6_RFID =RFID_Tag;
                                       global_instance.T5_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                                       global_table_instance.TABLE5->timer1->stop();
                                       global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/apartment6.jpg",global_table_instance.TABLE5->table_5_scene);
                                       global_table_instance.TABLE5->timer->start(100);
                                   }
                           break;
                           case  597999943://else if(RFID_Tag=="597999943")
                          {
                                       global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T5_R6_RFID =RFID_Tag;
                                       global_instance.T5_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                                       global_table_instance.TABLE5->timer1->stop();
                                       global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/shopping_mall.jpg",global_table_instance.TABLE5->table_5_scene);
                                       global_table_instance.TABLE5->timer->start(100);
                                   }
                           break;
                           case  8538681://else if(RFID_Tag=="8538681")
                          {
                                       global_instance.T5_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                       global_instance.T5_R6_RFID =RFID_Tag;
                                       global_instance.T5_R6=1;
                                       emit_model_placed();
                                       std::cout<<"global_instance.T5_R6 in MQTT()=:"<<global_instance.T5_R6<<std::endl;
                                       global_table_instance.TABLE5->timer1->stop();
                                       global_table_instance.TABLE5->T5_R6_MODULE=new T5_R6_Model("img/hospital.jpg",global_table_instance.TABLE5->table_5_scene);
                                       global_table_instance.TABLE5->timer->start(100);
                                   }

                           break;
                           case  0://else if(global_instance.T5_R6==1&&RFID_Tag=="0")
                           if(global_instance.T5_R6==1){
                          {
                                       global_instance.T5_R6_RFID =nullptr;
                                       global_instance.T5_R6=0;
                                       emit_model_placed();
                                       global_table_instance.TABLE5->T5_R6_MODULE->RemoveQitem(global_table_instance.TABLE5->table_5_scene);
                                       //global_table_instance.TABLE1->timer->stop();
                                       //global_table_instance.TABLE1->timer1->start(100);
                                       qDebug("Model Removed");
                                   }
                           }
                           break;
                           default://else{qDebug("No");}
                                 qDebug("No");
                           break;
                      }

                    }
                        break;
                        case 5://if(RFID=="RFID 5")
                    {switch(global_instance.RFID_Tag_No)
                    {
                    case 1071985354://if(RFID_Tag=="1071985354")
                    {
                    global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R5_RFID =RFID_Tag;
                    global_instance.T5_R5=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 1072063229://else if(RFID_Tag=="1072063229")
                    {
                    global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R5_RFID =RFID_Tag;
                    global_instance.T5_R5=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 1072056604://else if(RFID_Tag=="1072056604")
                    {
                    global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R5_RFID =RFID_Tag;
                    global_instance.T5_R5=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 699168295://else if(RFID_Tag=="699168295")
                    {
                    global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R5_RFID =RFID_Tag;
                    global_instance.T5_R5=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 2305939010://else if(RFID_Tag=="2305939010")
                    {
                    global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R5_RFID =RFID_Tag;
                    global_instance.T5_R5=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case  2836102489: //else if(RFID_Tag=="2836102489")


                    {
                    global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R5_RFID =RFID_Tag;
                    global_instance.T5_R5=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case  1071986763://else if(RFID_Tag=="1071986763")


                    {
                    global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R5_RFID =RFID_Tag;
                    global_instance.T5_R5=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/house1.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case  1072063101://else if(RFID_Tag=="1072063101")


                      {
                           global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R5_RFID =RFID_Tag;
                           global_instance.T5_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/house2.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  2298616020://else if(RFID_Tag=="2298616020")


                    {
                           global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R5_RFID =RFID_Tag;
                           global_instance.T5_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/house3.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071733508://else if(RFID_Tag=="1071733508")


                    {
                           global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R5_RFID =RFID_Tag;
                           global_instance.T5_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/house4.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  3914547211://else if(RFID_Tag=="3914547211")


                    {
                           global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R5_RFID =RFID_Tag;
                           global_instance.T5_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/house5.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071773816://else if(RFID_Tag=="1071773816")


                    {
                           global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R5_RFID =RFID_Tag;
                           global_instance.T5_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/Storage_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071780377://else if(RFID_Tag=="1071780377")


                    {
                           global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R5_RFID =RFID_Tag;
                           global_instance.T5_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/Storage_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071770985://else if(RFID_Tag=="1071770985")


                    {
                           global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R5_RFID =RFID_Tag;
                           global_instance.T5_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/apartment1.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  7304921://else if(RFID_Tag=="7304921")


                    {
                           global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R5_RFID =RFID_Tag;
                           global_instance.T5_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/apartment2.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  593072732://else if(RFID_Tag=="593072732")


                    {
                           global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R5_RFID =RFID_Tag;
                           global_instance.T5_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/apartment3.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071753108://else if(RFID_Tag=="1071753108")


                    {
                           global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R5_RFID =RFID_Tag;
                           global_instance.T5_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/apartment4.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  2836099420://else if(RFID_Tag=="2836099420")


                    {
                           global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R5_RFID =RFID_Tag;
                           global_instance.T5_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/apartment5.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1840834860://else if(RFID_Tag=="1840834860")


                    {
                           global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R5_RFID =RFID_Tag;
                           global_instance.T5_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/apartment6.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  597999943://else if(RFID_Tag=="597999943")


                    {
                           global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R5_RFID =RFID_Tag;
                           global_instance.T5_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/shopping_mall.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  8538681://else if(RFID_Tag=="8538681")


                    {
                           global_instance.T5_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R5_RFID =RFID_Tag;
                           global_instance.T5_R5=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R5 in MQTT()=:"<<global_instance.T5_R5<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R5_MODULE=new T5_R5_Model("img/hospital.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }

                    break;
                    case  0://else if(global_instance.T5_R5==1&&RFID_Tag=="0")
                    if(global_instance.T5_R5==1){

                    {
                           global_instance.T5_R5_RFID =nullptr;
                           global_instance.T5_R5=0;
                           emit_model_placed();

                           global_table_instance.TABLE5->T5_R5_MODULE->RemoveQitem(global_table_instance.TABLE5->table_5_scene);
                           //global_table_instance.TABLE1->timer->stop();
                           //global_table_instance.TABLE1->timer1->start(100);
                           qDebug("Model Removed");
                       }
                    }
                    break;
                    default://else{qDebug("No");}
                     qDebug("No");
                    break;
                    }

                    }
                        break;
                        case 4://if(RFID=="RFID 4")
                    {switch(global_instance.RFID_Tag_No)
                    {
                    case 1071985354://if(RFID_Tag=="1071985354")

                    {
                    global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R4_RFID =RFID_Tag;
                    global_instance.T5_R4=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 1072063229://else if(RFID_Tag=="1072063229")


                    {
                    global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R4_RFID =RFID_Tag;
                    global_instance.T5_R4=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 1072056604://else if(RFID_Tag=="1072056604")


                    {
                    global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R4_RFID =RFID_Tag;
                    global_instance.T5_R4=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 699168295://else if(RFID_Tag=="699168295")


                    {
                    global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R4_RFID =RFID_Tag;
                    global_instance.T5_R4=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 2305939010://else if(RFID_Tag=="2305939010")


                    {
                    global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R4_RFID =RFID_Tag;
                    global_instance.T5_R4=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case  2836102489: //else if(RFID_Tag=="2836102489")


                    {
                    global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R4_RFID =RFID_Tag;
                    global_instance.T5_R4=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case  1071986763://else if(RFID_Tag=="1071986763")


                    {
                    global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R4_RFID =RFID_Tag;
                    global_instance.T5_R4=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/house1.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case  1072063101://else if(RFID_Tag=="1072063101")


                      {
                           global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R4_RFID =RFID_Tag;
                           global_instance.T5_R4=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/house2.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  2298616020://else if(RFID_Tag=="2298616020")


                    {
                           global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R4_RFID =RFID_Tag;
                           global_instance.T5_R4=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/house3.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071733508://else if(RFID_Tag=="1071733508")


                    {
                           global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R4_RFID =RFID_Tag;
                           global_instance.T5_R4=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/house4.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  3914547211://else if(RFID_Tag=="3914547211")


                    {
                           global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R4_RFID =RFID_Tag;
                           global_instance.T5_R4=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/house5.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071773816://else if(RFID_Tag=="1071773816")


                    {
                           global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R4_RFID =RFID_Tag;
                           global_instance.T5_R4=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/Storage_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071780377://else if(RFID_Tag=="1071780377")


                    {
                           global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R4_RFID =RFID_Tag;
                           global_instance.T5_R4=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/Storage_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071770985://else if(RFID_Tag=="1071770985")


                    {
                           global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R4_RFID =RFID_Tag;
                           global_instance.T5_R4=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/apartment1.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  7304921://else if(RFID_Tag=="7304921")


                    {
                           global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R4_RFID =RFID_Tag;
                           global_instance.T5_R4=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/apartment2.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  593072732://else if(RFID_Tag=="593072732")


                    {
                           global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R4_RFID =RFID_Tag;
                           global_instance.T5_R4=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/apartment3.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071753108://else if(RFID_Tag=="1071753108")


                    {
                           global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R4_RFID =RFID_Tag;
                           global_instance.T5_R4=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/apartment4.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  2836099420://else if(RFID_Tag=="2836099420")


                    {
                           global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R4_RFID =RFID_Tag;
                           global_instance.T5_R4=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/apartment5.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1840834860://else if(RFID_Tag=="1840834860")


                    {
                           global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R4_RFID =RFID_Tag;
                           global_instance.T5_R4=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/apartment6.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  597999943://else if(RFID_Tag=="597999943")


                    {
                           global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R4_RFID =RFID_Tag;
                           global_instance.T5_R4=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/shopping_mall.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  8538681://else if(RFID_Tag=="8538681")


                    {
                           global_instance.T5_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R4_RFID =RFID_Tag;
                           global_instance.T5_R4=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R4 in MQTT()=:"<<global_instance.T5_R4<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R4_MODULE=new T5_R4_Model("img/hospital.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }

                    break;
                    case  0://else if(global_instance.T5_R4==1&&RFID_Tag=="0")
                    if(global_instance.T5_R4==1){

                    {
                           global_instance.T5_R4_RFID =nullptr;
                           global_instance.T5_R4=0;
                           emit_model_placed();

                           global_table_instance.TABLE5->T5_R4_MODULE->RemoveQitem(global_table_instance.TABLE5->table_5_scene);
                           //global_table_instance.TABLE1->timer->stop();
                           //global_table_instance.TABLE1->timer1->start(100);
                           qDebug("Model Removed");
                       }
                    }
                    break;
                    default://else{qDebug("No");}
                     qDebug("No");
                    break;
                    }

                    }
                        break;
                        case 3://if(RFID=="RFID 3")
                    {switch(global_instance.RFID_Tag_No)
                    {
                    case 1071985354://if(RFID_Tag=="1071985354")

                    {
                    global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R3_RFID =RFID_Tag;
                    global_instance.T5_R3=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 1072063229://else if(RFID_Tag=="1072063229")
                    {
                    global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R3_RFID =RFID_Tag;
                    global_instance.T5_R3=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 1072056604://else if(RFID_Tag=="1072056604")
                    {
                    global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R3_RFID =RFID_Tag;
                    global_instance.T5_R3=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 699168295://else if(RFID_Tag=="699168295")
                    {
                    global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R3_RFID =RFID_Tag;
                    global_instance.T5_R3=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 2305939010://else if(RFID_Tag=="2305939010")
                    {
                    global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R3_RFID =RFID_Tag;
                    global_instance.T5_R3=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case  2836102489: //else if(RFID_Tag=="2836102489")
                    {
                    global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R3_RFID =RFID_Tag;
                    global_instance.T5_R3=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case  1071986763://else if(RFID_Tag=="1071986763")
                    {
                    global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R3_RFID =RFID_Tag;
                    global_instance.T5_R3=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/house1.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case  1072063101://else if(RFID_Tag=="1072063101")
                      {
                           global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R3_RFID =RFID_Tag;
                           global_instance.T5_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/house2.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  2298616020://else if(RFID_Tag=="2298616020")
                    {
                           global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R3_RFID =RFID_Tag;
                           global_instance.T5_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/house3.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071733508://else if(RFID_Tag=="1071733508")
                    {
                           global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R3_RFID =RFID_Tag;
                           global_instance.T5_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/house4.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  3914547211://else if(RFID_Tag=="3914547211")
                    {
                           global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R3_RFID =RFID_Tag;
                           global_instance.T5_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/house5.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071773816://else if(RFID_Tag=="1071773816")
                    {
                           global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R3_RFID =RFID_Tag;
                           global_instance.T5_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/Storage_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071780377://else if(RFID_Tag=="1071780377")
                    {
                           global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R3_RFID =RFID_Tag;
                           global_instance.T5_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/Storage_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071770985://else if(RFID_Tag=="1071770985")
                    {
                           global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R3_RFID =RFID_Tag;
                           global_instance.T5_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/apartment1.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  7304921://else if(RFID_Tag=="7304921")
                    {
                           global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R3_RFID =RFID_Tag;
                           global_instance.T5_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/apartment2.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  593072732://else if(RFID_Tag=="593072732")
                    {
                           global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R3_RFID =RFID_Tag;
                           global_instance.T5_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/apartment3.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071753108://else if(RFID_Tag=="1071753108")
                    {
                           global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R3_RFID =RFID_Tag;
                           global_instance.T5_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/apartment4.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  2836099420://else if(RFID_Tag=="2836099420")
                    {
                           global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R3_RFID =RFID_Tag;
                           global_instance.T5_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/apartment5.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1840834860://else if(RFID_Tag=="1840834860")
                    {
                           global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R3_RFID =RFID_Tag;
                           global_instance.T5_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/apartment6.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  597999943://else if(RFID_Tag=="597999943")
                    {
                           global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R3_RFID =RFID_Tag;
                           global_instance.T5_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/shopping_mall.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  8538681://else if(RFID_Tag=="8538681")
                    {
                           global_instance.T5_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R3_RFID =RFID_Tag;
                           global_instance.T5_R3=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R3 in MQTT()=:"<<global_instance.T5_R3<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R3_MODULE=new T5_R3_Model("img/hospital.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  0://else if(global_instance.T5_R3==1&&RFID_Tag=="0")
                    if(global_instance.T5_R3==1){
                    {
                           global_instance.T5_R3_RFID =nullptr;
                           global_instance.T5_R3=0;
                           emit_model_placed();

                           global_table_instance.TABLE5->T5_R3_MODULE->RemoveQitem(global_table_instance.TABLE5->table_5_scene);
                           //global_table_instance.TABLE1->timer->stop();
                           //global_table_instance.TABLE1->timer1->start(100);
                           qDebug("Model Removed");
                       }
                    }
                    break;
                    default://else{qDebug("No");}
                     qDebug("No");
                    break;
                    }
                    }
                        break;
                        case 2://if(RFID=="RFID 2")
                    {switch(global_instance.RFID_Tag_No)
                    {
                    case 1071985354://if(RFID_Tag=="1071985354")

                    {
                    global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R2_RFID =RFID_Tag;
                    global_instance.T5_R2=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 1072063229://else if(RFID_Tag=="1072063229")
                    {
                    global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R2_RFID =RFID_Tag;
                    global_instance.T5_R2=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 1072056604://else if(RFID_Tag=="1072056604")
                    {
                    global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R2_RFID =RFID_Tag;
                    global_instance.T5_R2=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 699168295://else if(RFID_Tag=="699168295")
                    {
                    global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R2_RFID =RFID_Tag;
                    global_instance.T5_R2=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 2305939010://else if(RFID_Tag=="2305939010")
                    {
                    global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R2_RFID =RFID_Tag;
                    global_instance.T5_R2=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case  2836102489: //else if(RFID_Tag=="2836102489")
                    {
                    global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R2_RFID =RFID_Tag;
                    global_instance.T5_R2=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case  1071986763://else if(RFID_Tag=="1071986763")
                    {
                    global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R2_RFID =RFID_Tag;
                    global_instance.T5_R2=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/house1.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case  1072063101://else if(RFID_Tag=="1072063101")
                      {
                           global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R2_RFID =RFID_Tag;
                           global_instance.T5_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/house2.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  2298616020://else if(RFID_Tag=="2298616020")
                    {
                           global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R2_RFID =RFID_Tag;
                           global_instance.T5_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/house3.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071733508://else if(RFID_Tag=="1071733508")
                    {
                           global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R2_RFID =RFID_Tag;
                           global_instance.T5_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/house4.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  3914547211://else if(RFID_Tag=="3914547211")
                    {
                           global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R2_RFID =RFID_Tag;
                           global_instance.T5_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/house5.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071773816://else if(RFID_Tag=="1071773816")
                    {
                           global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R2_RFID =RFID_Tag;
                           global_instance.T5_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/Storage_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071780377://else if(RFID_Tag=="1071780377")
                    {
                           global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R2_RFID =RFID_Tag;
                           global_instance.T5_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/Storage_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071770985://else if(RFID_Tag=="1071770985")
                    {
                           global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R2_RFID =RFID_Tag;
                           global_instance.T5_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/apartment1.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  7304921://else if(RFID_Tag=="7304921")
                    {
                           global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R2_RFID =RFID_Tag;
                           global_instance.T5_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/apartment2.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  593072732://else if(RFID_Tag=="593072732")
                    {
                           global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R2_RFID =RFID_Tag;
                           global_instance.T5_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/apartment3.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071753108://else if(RFID_Tag=="1071753108")
                    {
                           global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R2_RFID =RFID_Tag;
                           global_instance.T5_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/apartment4.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  2836099420://else if(RFID_Tag=="2836099420")
                    {
                           global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R2_RFID =RFID_Tag;
                           global_instance.T5_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/apartment5.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1840834860://else if(RFID_Tag=="1840834860")
                    {
                           global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R2_RFID =RFID_Tag;
                           global_instance.T5_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/apartment6.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  597999943://else if(RFID_Tag=="597999943")
                    {
                           global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R2_RFID =RFID_Tag;
                           global_instance.T5_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/shopping_mall.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  8538681://else if(RFID_Tag=="8538681")
                    {
                           global_instance.T5_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R2_RFID =RFID_Tag;
                           global_instance.T5_R2=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R2 in MQTT()=:"<<global_instance.T5_R2<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R2_MODULE=new T5_R2_Model("img/hospital.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  0://else if(global_instance.T5_R2==1&&RFID_Tag=="0")
                    if(global_instance.T5_R2==1){
                    {
                           global_instance.T5_R2_RFID =nullptr;
                           global_instance.T5_R2=0;
                           emit_model_placed();

                           global_table_instance.TABLE5->T5_R2_MODULE->RemoveQitem(global_table_instance.TABLE5->table_5_scene);
                           //global_table_instance.TABLE1->timer->stop();
                           //global_table_instance.TABLE1->timer1->start(100);
                           qDebug("Model Removed");
                       }
                    }
                    break;
                    default://else{qDebug("No");}
                     qDebug("No");
                    break;
                    }
                    }
                        break;
                        case 1://if(RFID=="RFID 1")
                    {switch(global_instance.RFID_Tag_No)
                    {
                    case 1071985354://if(RFID_Tag=="1071985354")

                    {
                    global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R1_RFID =RFID_Tag;
                    global_instance.T5_R1=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 1072063229://else if(RFID_Tag=="1072063229")
                    {
                    global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R1_RFID =RFID_Tag;
                    global_instance.T5_R1=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 1072056604://else if(RFID_Tag=="1072056604")
                    {
                    global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R1_RFID =RFID_Tag;
                    global_instance.T5_R1=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 699168295://else if(RFID_Tag=="699168295")
                    {
                    global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R1_RFID =RFID_Tag;
                    global_instance.T5_R1=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case 2305939010://else if(RFID_Tag=="2305939010")
                    {
                    global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R1_RFID =RFID_Tag;
                    global_instance.T5_R1=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case  2836102489: //else if(RFID_Tag=="2836102489")
                    {
                    global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R1_RFID =RFID_Tag;
                    global_instance.T5_R1=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case  1071986763://else if(RFID_Tag=="1071986763")
                    {
                    global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                    global_instance.T5_R1_RFID =RFID_Tag;
                    global_instance.T5_R1=1;
                    emit_model_placed();
                    std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                    global_table_instance.TABLE5->timer1->stop();
                    global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/house1.jpg",global_table_instance.TABLE5->table_5_scene);
                    global_table_instance.TABLE5->timer->start(100);
                    }
                    break;
                    case  1072063101://else if(RFID_Tag=="1072063101")
                      {
                           global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R1_RFID =RFID_Tag;
                           global_instance.T5_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/house2.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  2298616020://else if(RFID_Tag=="2298616020")
                    {
                           global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R1_RFID =RFID_Tag;
                           global_instance.T5_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/house3.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071733508://else if(RFID_Tag=="1071733508")
                    {
                           global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R1_RFID =RFID_Tag;
                           global_instance.T5_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/house4.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  3914547211://else if(RFID_Tag=="3914547211")
                    {
                           global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R1_RFID =RFID_Tag;
                           global_instance.T5_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/house5.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071773816://else if(RFID_Tag=="1071773816")
                    {
                           global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R1_RFID =RFID_Tag;
                           global_instance.T5_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/Storage_house_1.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071780377://else if(RFID_Tag=="1071780377")
                    {
                           global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R1_RFID =RFID_Tag;
                           global_instance.T5_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/Storage_house_2.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071770985://else if(RFID_Tag=="1071770985")
                    {
                           global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R1_RFID =RFID_Tag;
                           global_instance.T5_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/apartment1.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  7304921://else if(RFID_Tag=="7304921")
                    {
                           global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R1_RFID =RFID_Tag;
                           global_instance.T5_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/apartment2.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  593072732://else if(RFID_Tag=="593072732")
                    {
                           global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R1_RFID =RFID_Tag;
                           global_instance.T5_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/apartment3.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1071753108://else if(RFID_Tag=="1071753108")
                    {
                           global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R1_RFID =RFID_Tag;
                           global_instance.T5_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/apartment4.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  2836099420://else if(RFID_Tag=="2836099420")
                    {
                           global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R1_RFID =RFID_Tag;
                           global_instance.T5_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/apartment5.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  1840834860://else if(RFID_Tag=="1840834860")
                    {
                           global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R1_RFID =RFID_Tag;
                           global_instance.T5_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/apartment6.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  597999943://else if(RFID_Tag=="597999943")
                    {
                           global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R1_RFID =RFID_Tag;
                           global_instance.T5_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/shopping_mall.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  8538681://else if(RFID_Tag=="8538681")
                    {
                           global_instance.T5_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                           global_instance.T5_R1_RFID =RFID_Tag;
                           global_instance.T5_R1=1;
                           emit_model_placed();
                           std::cout<<"global_instance.T5_R1 in MQTT()=:"<<global_instance.T5_R1<<std::endl;
                           global_table_instance.TABLE5->timer1->stop();
                           global_table_instance.TABLE5->T5_R1_MODULE=new T5_R1_Model("img/hospital.jpg",global_table_instance.TABLE5->table_5_scene);
                           global_table_instance.TABLE5->timer->start(100);
                       }
                    break;
                    case  0://else if(global_instance.T5_R1==1&&RFID_Tag=="0")
                    if(global_instance.T5_R1==1){
                    {
                           global_instance.T5_R1_RFID =nullptr;
                           global_instance.T5_R1=0;
                           emit_model_placed();

                           global_table_instance.TABLE5->T5_R1_MODULE->RemoveQitem(global_table_instance.TABLE5->table_5_scene);
                           //global_table_instance.TABLE1->timer->stop();
                           //global_table_instance.TABLE1->timer1->start(100);
                           qDebug("Model Removed");
                       }
                    }
                    break;
                    default://else{qDebug("No");}
                     qDebug("No");
                    break;
                    }
                    }
                        break;
                        default:

                        break;
                       }
                    }
                    break;
                    //table5 finished==================================================================
                    case 6://if(table=="Table6")
                                 {switch(global_instance.RFID_No)
                                    {
                                     case 0://if(RFID=="RFID 0")
                                     {switch(global_instance.RFID_Tag_No)
                                        {
                                         case 1071985354://if(RFID_Tag=="1071985354")
                                         {
                                             global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                             global_instance.T6_R0_RFID =RFID_Tag;
                                             global_instance.T6_R0=1;
                                             emit_model_placed();
                                             std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                             global_table_instance.TABLE6->timer1->stop();
                                             global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                             global_table_instance.TABLE6->timer->start(100);
                                         }
                                         break;
                                         case 1072063229://else if(RFID_Tag=="1072063229")


                                         {
                                             global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                             global_instance.T6_R0_RFID =RFID_Tag;
                                             global_instance.T6_R0=1;
                                             emit_model_placed();
                                             std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                             global_table_instance.TABLE6->timer1->stop();
                                             global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                             global_table_instance.TABLE6->timer->start(100);
                                         }
                                         break;
                                         case 1072056604://else if(RFID_Tag=="1072056604")
                                         {
                                             global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                             global_instance.T6_R0_RFID =RFID_Tag;
                                             global_instance.T6_R0=1;
                                             emit_model_placed();
                                             std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                             global_table_instance.TABLE6->timer1->stop();
                                             global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE6->table_6_scene);
                                             global_table_instance.TABLE6->timer->start(100);
                                         }
                                         break;
                                         case 699168295://else if(RFID_Tag=="699168295")
                                         {
                                             global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                             global_instance.T6_R0_RFID =RFID_Tag;
                                             global_instance.T6_R0=1;
                                             emit_model_placed();
                                             std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                             global_table_instance.TABLE6->timer1->stop();
                                             global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE6->table_6_scene);
                                             global_table_instance.TABLE6->timer->start(100);
                                         }
                                         break;
                                         case 2305939010://else if(RFID_Tag=="2305939010")
                                         {
                                             global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                             global_instance.T6_R0_RFID =RFID_Tag;
                                             global_instance.T6_R0=1;
                                             emit_model_placed();
                                             std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                             global_table_instance.TABLE6->timer1->stop();
                                             global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE6->table_6_scene);
                                             global_table_instance.TABLE6->timer->start(100);
                                         }
                                         break;
                                         case  2836102489: //else if(RFID_Tag=="2836102489")
                                         {
                                             global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                             global_instance.T6_R0_RFID =RFID_Tag;
                                             global_instance.T6_R0=1;
                                             emit_model_placed();
                                             std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                             global_table_instance.TABLE6->timer1->stop();
                                             global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE6->table_6_scene);
                                             global_table_instance.TABLE6->timer->start(100);
                                         }
                                         break;
                                         case  1071986763://else if(RFID_Tag=="1071986763")
                                {
                                             global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                             global_instance.T6_R0_RFID =RFID_Tag;
                                             global_instance.T6_R0=1;
                                             emit_model_placed();
                                             std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                             global_table_instance.TABLE6->timer1->stop();
                                             global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/house1.jpg",global_table_instance.TABLE6->table_6_scene);
                                             global_table_instance.TABLE6->timer->start(100);
                                         }
                                         break;
                                         case  1072063101://else if(RFID_Tag=="1072063101")
                                                {
                                                     global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                     global_instance.T6_R0_RFID =RFID_Tag;
                                                     global_instance.T6_R0=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                                     global_table_instance.TABLE6->timer1->stop();
                                                     global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/house2.jpg",global_table_instance.TABLE6->table_6_scene);
                                                     global_table_instance.TABLE6->timer->start(100);
                                                 }
                                         break;
                                         case  2298616020://else if(RFID_Tag=="2298616020")
                                        {
                                                     global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                     global_instance.T6_R0_RFID =RFID_Tag;
                                                     global_instance.T6_R0=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                                     global_table_instance.TABLE6->timer1->stop();
                                                     global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/house3.jpg",global_table_instance.TABLE6->table_6_scene);
                                                     global_table_instance.TABLE6->timer->start(100);
                                                 }
                                         break;
                                         case  1071733508://else if(RFID_Tag=="1071733508")
                                        {
                                                     global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                     global_instance.T6_R0_RFID =RFID_Tag;
                                                     global_instance.T6_R0=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                                     global_table_instance.TABLE6->timer1->stop();
                                                     global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/house4.jpg",global_table_instance.TABLE6->table_6_scene);
                                                     global_table_instance.TABLE6->timer->start(100);
                                                 }
                                         break;
                                         case  3914547211://else if(RFID_Tag=="3914547211")
                                        {
                                                     global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                     global_instance.T6_R0_RFID =RFID_Tag;
                                                     global_instance.T6_R0=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                                     global_table_instance.TABLE6->timer1->stop();
                                                     global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/house5.jpg",global_table_instance.TABLE6->table_6_scene);
                                                     global_table_instance.TABLE6->timer->start(100);
                                                 }
                                         break;
                                         case  1071773816://else if(RFID_Tag=="1071773816")
                                        {
                                                     global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                     global_instance.T6_R0_RFID =RFID_Tag;
                                                     global_instance.T6_R0=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                                     global_table_instance.TABLE6->timer1->stop();
                                                     global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/Storage_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                                     global_table_instance.TABLE6->timer->start(100);
                                                 }
                                         break;
                                         case  1071780377://else if(RFID_Tag=="1071780377")
                                        {
                                                     global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                     global_instance.T6_R0_RFID =RFID_Tag;
                                                     global_instance.T6_R0=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                                     global_table_instance.TABLE6->timer1->stop();
                                                     global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/Storage_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                                     global_table_instance.TABLE6->timer->start(100);
                                                 }
                                         break;
                                         case  1071770985://else if(RFID_Tag=="1071770985")
                                        {
                                                     global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                     global_instance.T6_R0_RFID =RFID_Tag;
                                                     global_instance.T6_R0=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                                     global_table_instance.TABLE6->timer1->stop();
                                                     global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/apartment1.jpg",global_table_instance.TABLE6->table_6_scene);
                                                     global_table_instance.TABLE6->timer->start(100);
                                                 }
                                         break;
                                         case  7304921://else if(RFID_Tag=="7304921")
                                        {
                                                     global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                     global_instance.T6_R0_RFID =RFID_Tag;
                                                     global_instance.T6_R0=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                                     global_table_instance.TABLE6->timer1->stop();
                                                     global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/apartment2.jpg",global_table_instance.TABLE6->table_6_scene);
                                                     global_table_instance.TABLE6->timer->start(100);
                                                 }
                                         break;
                                         case  593072732://else if(RFID_Tag=="593072732")


                                        {
                                                     global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                     global_instance.T6_R0_RFID =RFID_Tag;
                                                     global_instance.T6_R0=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                                     global_table_instance.TABLE6->timer1->stop();
                                                     global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/apartment3.jpg",global_table_instance.TABLE6->table_6_scene);
                                                     global_table_instance.TABLE6->timer->start(100);
                                                 }
                                         break;
                                         case  1071753108://else if(RFID_Tag=="1071753108")


                                        {
                                                     global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                     global_instance.T6_R0_RFID =RFID_Tag;
                                                     global_instance.T6_R0=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                                     global_table_instance.TABLE6->timer1->stop();
                                                     global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/apartment4.jpg",global_table_instance.TABLE6->table_6_scene);
                                                     global_table_instance.TABLE6->timer->start(100);
                                                 }
                                         break;
                                         case  2836099420://else if(RFID_Tag=="2836099420")


                                        {
                                                     global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                     global_instance.T6_R0_RFID =RFID_Tag;
                                                     global_instance.T6_R0=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                                     global_table_instance.TABLE6->timer1->stop();
                                                     global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/apartment5.jpg",global_table_instance.TABLE6->table_6_scene);
                                                     global_table_instance.TABLE6->timer->start(100);
                                                 }
                                         break;
                                         case  1840834860://else if(RFID_Tag=="1840834860")


                                        {
                                                     global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                     global_instance.T6_R0_RFID =RFID_Tag;
                                                     global_instance.T6_R0=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                                     global_table_instance.TABLE6->timer1->stop();
                                                     global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/apartment6.jpg",global_table_instance.TABLE6->table_6_scene);
                                                     global_table_instance.TABLE6->timer->start(100);
                                                 }
                                         break;
                                         case  597999943://else if(RFID_Tag=="597999943")


                                        {
                                                     global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                     global_instance.T6_R0_RFID =RFID_Tag;
                                                     global_instance.T6_R0=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                                     global_table_instance.TABLE6->timer1->stop();
                                                     global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/shopping_mall.jpg",global_table_instance.TABLE6->table_6_scene);
                                                     global_table_instance.TABLE6->timer->start(100);
                                                 }
                                         break;
                                         case  8538681://else if(RFID_Tag=="8538681")
                                        {
                                                     global_instance.T6_R0_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                     global_instance.T6_R0_RFID =RFID_Tag;
                                                     global_instance.T6_R0=1;
                                                     emit_model_placed();
                                                     std::cout<<"global_instance.T6_R0 in MQTT()=:"<<global_instance.T6_R0<<std::endl;
                                                     global_table_instance.TABLE6->timer1->stop();
                                                     global_table_instance.TABLE6->T6_R0_MODULE=new T6_R0_Model("img/hospital.jpg",global_table_instance.TABLE6->table_6_scene);
                                                     global_table_instance.TABLE6->timer->start(100);
                                                 }

                                         break;
                                         case  0://else if(global_instance.T6_R0==1&&RFID_Tag=="0")
                                         if(global_instance.T6_R0==1){
                                        {
                                                     global_instance.T6_R0_RFID =nullptr;
                                                     global_instance.T6_R0=0;
                                                     emit_model_placed();

                                                     global_table_instance.TABLE6->T6_R0_MODULE->RemoveQitem(global_table_instance.TABLE6->table_6_scene);
                                                     //global_table_instance.TABLE1->timer->stop();
                                                     //global_table_instance.TABLE1->timer1->start(100);
                                                     qDebug("Model Removed");
                                                 }
                                         }
                                         break;
                                         default://else{qDebug("No");}
                                               qDebug("No");
                                         break;
                                    }

                                }
                                     break;
                                     case 7://if(RFID=="RFID 7")
                                     {switch(global_instance.RFID_Tag_No)
                                           {
                                            case 1071985354://if(RFID_Tag=="1071985354")
                                            {
                                                global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                global_instance.T6_R7_RFID =RFID_Tag;
                                                global_instance.T6_R7=1;
                                                emit_model_placed();
                                                std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                global_table_instance.TABLE6->timer1->stop();
                                                global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                                global_table_instance.TABLE6->timer->start(100);
                                            }
                                            break;
                                            case 1072063229://else if(RFID_Tag=="1072063229")
                                            {
                                                global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                global_instance.T6_R7_RFID =RFID_Tag;
                                                global_instance.T6_R7=1;
                                                emit_model_placed();
                                                std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                global_table_instance.TABLE6->timer1->stop();
                                                global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                                global_table_instance.TABLE6->timer->start(100);
                                            }
                                            break;
                                            case 1072056604://else if(RFID_Tag=="1072056604")
                                            {
                                                global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                global_instance.T6_R7_RFID =RFID_Tag;
                                                global_instance.T6_R7=1;
                                                emit_model_placed();
                                                std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                global_table_instance.TABLE6->timer1->stop();
                                                global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE6->table_6_scene);
                                                global_table_instance.TABLE6->timer->start(100);
                                            }
                                            break;
                                            case 699168295://else if(RFID_Tag=="699168295")
                                            {
                                                global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                global_instance.T6_R7_RFID =RFID_Tag;
                                                global_instance.T6_R7=1;
                                                emit_model_placed();
                                                std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                global_table_instance.TABLE6->timer1->stop();
                                                global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE6->table_6_scene);
                                                global_table_instance.TABLE6->timer->start(100);
                                            }
                                            break;
                                            case 2305939010://else if(RFID_Tag=="2305939010")
                                            {
                                                global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                global_instance.T6_R7_RFID =RFID_Tag;
                                                global_instance.T6_R7=1;
                                                emit_model_placed();
                                                std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                global_table_instance.TABLE6->timer1->stop();
                                                global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE6->table_6_scene);
                                                global_table_instance.TABLE6->timer->start(100);
                                            }
                                            break;
                                            case  2836102489: //else if(RFID_Tag=="2836102489")
                                            {
                                                global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                global_instance.T6_R7_RFID =RFID_Tag;
                                                global_instance.T6_R7=1;
                                                emit_model_placed();
                                                std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                global_table_instance.TABLE6->timer1->stop();
                                                global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE6->table_6_scene);
                                                global_table_instance.TABLE6->timer->start(100);
                                            }
                                            break;
                                            case  1071986763://else if(RFID_Tag=="1071986763")
                                    {
                                                global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                global_instance.T6_R7_RFID =RFID_Tag;
                                                global_instance.T6_R7=1;
                                                emit_model_placed();
                                                std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                global_table_instance.TABLE6->timer1->stop();
                                                global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/house1.jpg",global_table_instance.TABLE6->table_6_scene);
                                                global_table_instance.TABLE6->timer->start(100);
                                            }
                                            break;
                                            case  1072063101://else if(RFID_Tag=="1072063101")
                                                   {
                                                        global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                        global_instance.T6_R7_RFID =RFID_Tag;
                                                        global_instance.T6_R7=1;
                                                        emit_model_placed();
                                                        std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                        global_table_instance.TABLE6->timer1->stop();
                                                        global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/house2.jpg",global_table_instance.TABLE6->table_6_scene);
                                                        global_table_instance.TABLE6->timer->start(100);
                                                    }
                                            break;
                                            case  2298616020://else if(RFID_Tag=="2298616020")
                                           {
                                                        global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                        global_instance.T6_R7_RFID =RFID_Tag;
                                                        global_instance.T6_R7=1;
                                                        emit_model_placed();
                                                        std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                        global_table_instance.TABLE6->timer1->stop();
                                                        global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/house3.jpg",global_table_instance.TABLE6->table_6_scene);
                                                        global_table_instance.TABLE6->timer->start(100);
                                                    }
                                            break;
                                            case  1071733508://else if(RFID_Tag=="1071733508")
                                           {
                                                        global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                        global_instance.T6_R7_RFID =RFID_Tag;
                                                        global_instance.T6_R7=1;
                                                        emit_model_placed();
                                                        std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                        global_table_instance.TABLE6->timer1->stop();
                                                        global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/house4.jpg",global_table_instance.TABLE6->table_6_scene);
                                                        global_table_instance.TABLE6->timer->start(100);
                                                    }
                                            break;
                                            case  3914547211://else if(RFID_Tag=="3914547211")
                                           {
                                                        global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                        global_instance.T6_R7_RFID =RFID_Tag;
                                                        global_instance.T6_R7=1;
                                                        emit_model_placed();
                                                        std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                        global_table_instance.TABLE6->timer1->stop();
                                                        global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/house5.jpg",global_table_instance.TABLE6->table_6_scene);
                                                        global_table_instance.TABLE6->timer->start(100);
                                                    }
                                            break;
                                            case  1071773816://else if(RFID_Tag=="1071773816")
                                           {
                                                        global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                        global_instance.T6_R7_RFID =RFID_Tag;
                                                        global_instance.T6_R7=1;
                                                        emit_model_placed();
                                                        std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                        global_table_instance.TABLE6->timer1->stop();
                                                        global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/Storage_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                                        global_table_instance.TABLE6->timer->start(100);
                                                    }
                                            break;
                                            case  1071780377://else if(RFID_Tag=="1071780377")
                                           {
                                                        global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                        global_instance.T6_R7_RFID =RFID_Tag;
                                                        global_instance.T6_R7=1;
                                                        emit_model_placed();
                                                        std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                        global_table_instance.TABLE6->timer1->stop();
                                                        global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/Storage_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                                        global_table_instance.TABLE6->timer->start(100);
                                                    }
                                            break;
                                            case  1071770985://else if(RFID_Tag=="1071770985")
                                           {
                                                        global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                        global_instance.T6_R7_RFID =RFID_Tag;
                                                        global_instance.T6_R7=1;
                                                        emit_model_placed();
                                                        std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                        global_table_instance.TABLE6->timer1->stop();
                                                        global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/apartment1.jpg",global_table_instance.TABLE6->table_6_scene);
                                                        global_table_instance.TABLE6->timer->start(100);
                                                    }
                                            break;
                                            case  7304921://else if(RFID_Tag=="7304921")
                                           {
                                                        global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                        global_instance.T6_R7_RFID =RFID_Tag;
                                                        global_instance.T6_R7=1;
                                                        emit_model_placed();
                                                        std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                        global_table_instance.TABLE6->timer1->stop();
                                                        global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/apartment2.jpg",global_table_instance.TABLE6->table_6_scene);
                                                        global_table_instance.TABLE6->timer->start(100);
                                                    }
                                            break;
                                            case  593072732://else if(RFID_Tag=="593072732")
                                           {
                                                        global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                        global_instance.T6_R7_RFID =RFID_Tag;
                                                        global_instance.T6_R7=1;
                                                        emit_model_placed();
                                                        std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                        global_table_instance.TABLE6->timer1->stop();
                                                        global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/apartment3.jpg",global_table_instance.TABLE6->table_6_scene);
                                                        global_table_instance.TABLE6->timer->start(100);
                                                    }
                                            break;
                                            case  1071753108://else if(RFID_Tag=="1071753108")
                                           {
                                                        global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                        global_instance.T6_R7_RFID =RFID_Tag;
                                                        global_instance.T6_R7=1;
                                                        emit_model_placed();
                                                        std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                        global_table_instance.TABLE6->timer1->stop();
                                                        global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/apartment4.jpg",global_table_instance.TABLE6->table_6_scene);
                                                        global_table_instance.TABLE6->timer->start(100);
                                                    }
                                            break;
                                            case  2836099420://else if(RFID_Tag=="2836099420")
                                           {
                                                        global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                        global_instance.T6_R7_RFID =RFID_Tag;
                                                        global_instance.T6_R7=1;
                                                        emit_model_placed();
                                                        std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                        global_table_instance.TABLE6->timer1->stop();
                                                        global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/apartment5.jpg",global_table_instance.TABLE6->table_6_scene);
                                                        global_table_instance.TABLE6->timer->start(100);
                                                    }
                                            break;
                                            case  1840834860://else if(RFID_Tag=="1840834860")
                                           {
                                                        global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                        global_instance.T6_R7_RFID =RFID_Tag;
                                                        global_instance.T6_R7=1;
                                                        emit_model_placed();
                                                        std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                        global_table_instance.TABLE6->timer1->stop();
                                                        global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/apartment6.jpg",global_table_instance.TABLE6->table_6_scene);
                                                        global_table_instance.TABLE6->timer->start(100);
                                                    }
                                            break;
                                            case  597999943://else if(RFID_Tag=="597999943")
                                           {
                                                        global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                        global_instance.T6_R7_RFID =RFID_Tag;
                                                        global_instance.T6_R7=1;
                                                        emit_model_placed();
                                                        std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                        global_table_instance.TABLE6->timer1->stop();
                                                        global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/shopping_mall.jpg",global_table_instance.TABLE6->table_6_scene);
                                                        global_table_instance.TABLE6->timer->start(100);
                                                    }
                                            break;
                                            case  8538681://else if(RFID_Tag=="8538681")
                                           {
                                                        global_instance.T6_R7_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                        global_instance.T6_R7_RFID =RFID_Tag;
                                                        global_instance.T6_R7=1;
                                                        emit_model_placed();
                                                        std::cout<<"global_instance.T6_R7 in MQTT()=:"<<global_instance.T6_R7<<std::endl;
                                                        global_table_instance.TABLE6->timer1->stop();
                                                        global_table_instance.TABLE6->T6_R7_MODULE=new T6_R7_Model("img/hospital.jpg",global_table_instance.TABLE6->table_6_scene);
                                                        global_table_instance.TABLE6->timer->start(100);
                                                    }

                                            break;
                                            case  0://else if(global_instance.T6_R7==1&&RFID_Tag=="0")
                                            if(global_instance.T6_R7==1){
                                           {
                                                        global_instance.T6_R7_RFID =nullptr;
                                                        global_instance.T6_R7=0;
                                                        emit_model_placed();

                                                        global_table_instance.TABLE6->T6_R7_MODULE->RemoveQitem(global_table_instance.TABLE6->table_6_scene);
                                                        //global_table_instance.TABLE1->timer->stop();
                                                        //global_table_instance.TABLE1->timer1->start(100);
                                                        qDebug("Model Removed");
                                                    }
                                            }
                                            break;
                                            default://else{qDebug("No");}
                                                  qDebug("No");
                                            break;
                                       }
                                }
                                     break;
                                     case 6://if(RFID=="RFID 6")
                                    {switch(global_instance.RFID_Tag_No)
                                       {
                                        case 1071985354://if(RFID_Tag=="1071985354")
                                        {
                                            global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T6_R6_RFID =RFID_Tag;
                                            global_instance.T6_R6=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                            global_table_instance.TABLE6->timer1->stop();
                                            global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                            global_table_instance.TABLE6->timer->start(100);
                                        }
                                        break;
                                        case 1072063229://else if(RFID_Tag=="1072063229")
                                        {
                                            global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T6_R6_RFID =RFID_Tag;
                                            global_instance.T6_R6=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                            global_table_instance.TABLE6->timer1->stop();
                                            global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                            global_table_instance.TABLE6->timer->start(100);
                                        }
                                        break;
                                        case 1072056604://else if(RFID_Tag=="1072056604")
                                        {
                                            global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T6_R6_RFID =RFID_Tag;
                                            global_instance.T6_R6=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                            global_table_instance.TABLE6->timer1->stop();
                                            global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE6->table_6_scene);
                                            global_table_instance.TABLE6->timer->start(100);
                                        }
                                        break;
                                        case 699168295://else if(RFID_Tag=="699168295")
                                        {
                                            global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T6_R6_RFID =RFID_Tag;
                                            global_instance.T6_R6=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                            global_table_instance.TABLE6->timer1->stop();
                                            global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE6->table_6_scene);
                                            global_table_instance.TABLE6->timer->start(100);
                                        }
                                        break;
                                        case 2305939010://else if(RFID_Tag=="2305939010")
                                        {
                                            global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T6_R6_RFID =RFID_Tag;
                                            global_instance.T6_R6=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                            global_table_instance.TABLE6->timer1->stop();
                                            global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE6->table_6_scene);
                                            global_table_instance.TABLE6->timer->start(100);
                                        }
                                        break;
                                        case  2836102489: //else if(RFID_Tag=="2836102489")
                                        {
                                            global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T6_R6_RFID =RFID_Tag;
                                            global_instance.T6_R6=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                            global_table_instance.TABLE6->timer1->stop();
                                            global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE6->table_6_scene);
                                            global_table_instance.TABLE6->timer->start(100);
                                        }
                                        break;
                                        case  1071986763://else if(RFID_Tag=="1071986763")
                                {
                                            global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                            global_instance.T6_R6_RFID =RFID_Tag;
                                            global_instance.T6_R6=1;
                                            emit_model_placed();
                                            std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                            global_table_instance.TABLE6->timer1->stop();
                                            global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/house1.jpg",global_table_instance.TABLE6->table_6_scene);
                                            global_table_instance.TABLE6->timer->start(100);
                                        }
                                        break;
                                        case  1072063101://else if(RFID_Tag=="1072063101")
                                               {
                                                    global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                    global_instance.T6_R6_RFID =RFID_Tag;
                                                    global_instance.T6_R6=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                                    global_table_instance.TABLE6->timer1->stop();
                                                    global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/house2.jpg",global_table_instance.TABLE6->table_6_scene);
                                                    global_table_instance.TABLE6->timer->start(100);
                                                }
                                        break;
                                        case  2298616020://else if(RFID_Tag=="2298616020")
                                       {
                                                    global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                    global_instance.T6_R6_RFID =RFID_Tag;
                                                    global_instance.T6_R6=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                                    global_table_instance.TABLE6->timer1->stop();
                                                    global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/house3.jpg",global_table_instance.TABLE6->table_6_scene);
                                                    global_table_instance.TABLE6->timer->start(100);
                                                }
                                        break;
                                        case  1071733508://else if(RFID_Tag=="1071733508")
                                       {
                                                    global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                    global_instance.T6_R6_RFID =RFID_Tag;
                                                    global_instance.T6_R6=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                                    global_table_instance.TABLE6->timer1->stop();
                                                    global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/house4.jpg",global_table_instance.TABLE6->table_6_scene);
                                                    global_table_instance.TABLE6->timer->start(100);
                                                }
                                        break;
                                        case  3914547211://else if(RFID_Tag=="3914547211")
                                       {
                                                    global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                    global_instance.T6_R6_RFID =RFID_Tag;
                                                    global_instance.T6_R6=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                                    global_table_instance.TABLE6->timer1->stop();
                                                    global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/house5.jpg",global_table_instance.TABLE6->table_6_scene);
                                                    global_table_instance.TABLE6->timer->start(100);
                                                }
                                        break;
                                        case  1071773816://else if(RFID_Tag=="1071773816")
                                       {
                                                    global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                    global_instance.T6_R6_RFID =RFID_Tag;
                                                    global_instance.T6_R6=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                                    global_table_instance.TABLE6->timer1->stop();
                                                    global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/Storage_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                                    global_table_instance.TABLE6->timer->start(100);
                                                }
                                        break;
                                        case  1071780377://else if(RFID_Tag=="1071780377")
                                       {
                                                    global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                    global_instance.T6_R6_RFID =RFID_Tag;
                                                    global_instance.T6_R6=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                                    global_table_instance.TABLE6->timer1->stop();
                                                    global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/Storage_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                                    global_table_instance.TABLE6->timer->start(100);
                                                }
                                        break;
                                        case  1071770985://else if(RFID_Tag=="1071770985")
                                       {
                                                    global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                    global_instance.T6_R6_RFID =RFID_Tag;
                                                    global_instance.T6_R6=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                                    global_table_instance.TABLE6->timer1->stop();
                                                    global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/apartment1.jpg",global_table_instance.TABLE6->table_6_scene);
                                                    global_table_instance.TABLE6->timer->start(100);
                                                }
                                        break;
                                        case  7304921://else if(RFID_Tag=="7304921")
                                       {
                                                    global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                    global_instance.T6_R6_RFID =RFID_Tag;
                                                    global_instance.T6_R6=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                                    global_table_instance.TABLE6->timer1->stop();
                                                    global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/apartment2.jpg",global_table_instance.TABLE6->table_6_scene);
                                                    global_table_instance.TABLE6->timer->start(100);
                                                }
                                        break;
                                        case  593072732://else if(RFID_Tag=="593072732")
                                       {
                                                    global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                    global_instance.T6_R6_RFID =RFID_Tag;
                                                    global_instance.T6_R6=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                                    global_table_instance.TABLE6->timer1->stop();
                                                    global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/apartment3.jpg",global_table_instance.TABLE6->table_6_scene);
                                                    global_table_instance.TABLE6->timer->start(100);
                                                }
                                        break;
                                        case  1071753108://else if(RFID_Tag=="1071753108")
                                       {
                                                    global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                    global_instance.T6_R6_RFID =RFID_Tag;
                                                    global_instance.T6_R6=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                                    global_table_instance.TABLE6->timer1->stop();
                                                    global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/apartment4.jpg",global_table_instance.TABLE6->table_6_scene);
                                                    global_table_instance.TABLE6->timer->start(100);
                                                }
                                        break;
                                        case  2836099420://else if(RFID_Tag=="2836099420")
                                       {
                                                    global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                    global_instance.T6_R6_RFID =RFID_Tag;
                                                    global_instance.T6_R6=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                                    global_table_instance.TABLE6->timer1->stop();
                                                    global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/apartment5.jpg",global_table_instance.TABLE6->table_6_scene);
                                                    global_table_instance.TABLE6->timer->start(100);
                                                }
                                        break;
                                        case  1840834860://else if(RFID_Tag=="1840834860")
                                       {
                                                    global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                    global_instance.T6_R6_RFID =RFID_Tag;
                                                    global_instance.T6_R6=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                                    global_table_instance.TABLE6->timer1->stop();
                                                    global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/apartment6.jpg",global_table_instance.TABLE6->table_6_scene);
                                                    global_table_instance.TABLE6->timer->start(100);
                                                }
                                        break;
                                        case  597999943://else if(RFID_Tag=="597999943")
                                       {
                                                    global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                    global_instance.T6_R6_RFID =RFID_Tag;
                                                    global_instance.T6_R6=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                                    global_table_instance.TABLE6->timer1->stop();
                                                    global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/shopping_mall.jpg",global_table_instance.TABLE6->table_6_scene);
                                                    global_table_instance.TABLE6->timer->start(100);
                                                }
                                        break;
                                        case  8538681://else if(RFID_Tag=="8538681")


                                       {
                                                    global_instance.T6_R6_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                                    global_instance.T6_R6_RFID =RFID_Tag;
                                                    global_instance.T6_R6=1;
                                                    emit_model_placed();
                                                    std::cout<<"global_instance.T6_R6 in MQTT()=:"<<global_instance.T6_R6<<std::endl;
                                                    global_table_instance.TABLE6->timer1->stop();
                                                    global_table_instance.TABLE6->T6_R6_MODULE=new T6_R6_Model("img/hospital.jpg",global_table_instance.TABLE6->table_6_scene);
                                                    global_table_instance.TABLE6->timer->start(100);
                                                }

                                        break;
                                        case  0://else if(global_instance.T6_R6==1&&RFID_Tag=="0")
                                        if(global_instance.T6_R6==1){

                                       {
                                                    global_instance.T6_R6_RFID =nullptr;
                                                    global_instance.T6_R6=0;
                                                    emit_model_placed();

                                                    global_table_instance.TABLE6->T6_R6_MODULE->RemoveQitem(global_table_instance.TABLE6->table_6_scene);
                                                    //global_table_instance.TABLE1->timer->stop();
                                                    //global_table_instance.TABLE1->timer1->start(100);
                                                    qDebug("Model Removed");
                                                }
                                        }
                                        break;
                                        default://else{qDebug("No");}
                                              qDebug("No");
                                        break;
                                   }

                                }
                                     break;
                                     case 5://if(RFID=="RFID 5")
                        {switch(global_instance.RFID_Tag_No)
                           {
                            case 1071985354://if(RFID_Tag=="1071985354")
                            {
                                global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R5_RFID =RFID_Tag;
                                global_instance.T6_R5=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 1072063229://else if(RFID_Tag=="1072063229")
                            {
                                global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R5_RFID =RFID_Tag;
                                global_instance.T6_R5=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 1072056604://else if(RFID_Tag=="1072056604")
                            {
                                global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R5_RFID =RFID_Tag;
                                global_instance.T6_R5=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 699168295://else if(RFID_Tag=="699168295")
                            {
                                global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R5_RFID =RFID_Tag;
                                global_instance.T6_R5=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 2305939010://else if(RFID_Tag=="2305939010")
                            {
                                global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R5_RFID =RFID_Tag;
                                global_instance.T6_R5=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case  2836102489: //else if(RFID_Tag=="2836102489")
                            {
                                global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R5_RFID =RFID_Tag;
                                global_instance.T6_R5=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case  1071986763://else if(RFID_Tag=="1071986763")


                    {
                                global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R5_RFID =RFID_Tag;
                                global_instance.T6_R5=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/house1.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case  1072063101://else if(RFID_Tag=="1072063101")
                                   {
                                        global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R5_RFID =RFID_Tag;
                                        global_instance.T6_R5=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/house2.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  2298616020://else if(RFID_Tag=="2298616020")
                           {
                                        global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R5_RFID =RFID_Tag;
                                        global_instance.T6_R5=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/house3.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071733508://else if(RFID_Tag=="1071733508")
                           {
                                        global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R5_RFID =RFID_Tag;
                                        global_instance.T6_R5=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/house4.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  3914547211://else if(RFID_Tag=="3914547211")
                           {
                                        global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R5_RFID =RFID_Tag;
                                        global_instance.T6_R5=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/house5.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071773816://else if(RFID_Tag=="1071773816")
                           {
                                        global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R5_RFID =RFID_Tag;
                                        global_instance.T6_R5=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/Storage_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071780377://else if(RFID_Tag=="1071780377")
                           {
                                        global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R5_RFID =RFID_Tag;
                                        global_instance.T6_R5=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/Storage_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071770985://else if(RFID_Tag=="1071770985")
                           {
                                        global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R5_RFID =RFID_Tag;
                                        global_instance.T6_R5=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/apartment1.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  7304921://else if(RFID_Tag=="7304921")
                           {
                                        global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R5_RFID =RFID_Tag;
                                        global_instance.T6_R5=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/apartment2.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  593072732://else if(RFID_Tag=="593072732")
                           {
                                        global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R5_RFID =RFID_Tag;
                                        global_instance.T6_R5=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/apartment3.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071753108://else if(RFID_Tag=="1071753108")
                           {
                                        global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R5_RFID =RFID_Tag;
                                        global_instance.T6_R5=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/apartment4.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  2836099420://else if(RFID_Tag=="2836099420")
                           {
                                        global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R5_RFID =RFID_Tag;
                                        global_instance.T6_R5=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/apartment5.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1840834860://else if(RFID_Tag=="1840834860")
                           {
                                        global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R5_RFID =RFID_Tag;
                                        global_instance.T6_R5=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/apartment6.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  597999943://else if(RFID_Tag=="597999943")
                           {
                                        global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R5_RFID =RFID_Tag;
                                        global_instance.T6_R5=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/shopping_mall.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  8538681://else if(RFID_Tag=="8538681")
                           {
                                        global_instance.T6_R5_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R5_RFID =RFID_Tag;
                                        global_instance.T6_R5=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R5 in MQTT()=:"<<global_instance.T6_R5<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R5_MODULE=new T6_R5_Model("img/hospital.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }

                            break;
                            case  0://else if(global_instance.T6_R5==1&&RFID_Tag=="0")
                            if(global_instance.T6_R5==1){
                           {
                                        global_instance.T6_R5_RFID =nullptr;
                                        global_instance.T6_R5=0;
                                        emit_model_placed();

                                        global_table_instance.TABLE6->T6_R5_MODULE->RemoveQitem(global_table_instance.TABLE6->table_6_scene);
                                        //global_table_instance.TABLE1->timer->stop();
                                        //global_table_instance.TABLE1->timer1->start(100);
                                        qDebug("Model Removed");
                                    }
                            }
                            break;
                            default://else{qDebug("No");}
                                  qDebug("No");
                            break;
                       }

                    }
                                     break;
                                     case 4://if(RFID=="RFID 4")
                        {switch(global_instance.RFID_Tag_No)
                           {
                            case 1071985354://if(RFID_Tag=="1071985354")
                            {
                                global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R4_RFID =RFID_Tag;
                                global_instance.T6_R4=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 1072063229://else if(RFID_Tag=="1072063229")
                            {
                                global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R4_RFID =RFID_Tag;
                                global_instance.T6_R4=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 1072056604://else if(RFID_Tag=="1072056604")
                            {
                                global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R4_RFID =RFID_Tag;
                                global_instance.T6_R4=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 699168295://else if(RFID_Tag=="699168295")
                            {
                                global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R4_RFID =RFID_Tag;
                                global_instance.T6_R4=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 2305939010://else if(RFID_Tag=="2305939010")
                            {
                                global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R4_RFID =RFID_Tag;
                                global_instance.T6_R4=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case  2836102489: //else if(RFID_Tag=="2836102489")
                            {
                                global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R4_RFID =RFID_Tag;
                                global_instance.T6_R4=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case  1071986763://else if(RFID_Tag=="1071986763")
                    {
                                global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R4_RFID =RFID_Tag;
                                global_instance.T6_R4=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/house1.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case  1072063101://else if(RFID_Tag=="1072063101")
                                   {
                                        global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R4_RFID =RFID_Tag;
                                        global_instance.T6_R4=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/house2.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  2298616020://else if(RFID_Tag=="2298616020")
                           {
                                        global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R4_RFID =RFID_Tag;
                                        global_instance.T6_R4=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/house3.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071733508://else if(RFID_Tag=="1071733508")
                           {
                                        global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R4_RFID =RFID_Tag;
                                        global_instance.T6_R4=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/house4.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  3914547211://else if(RFID_Tag=="3914547211")
                           {
                                        global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R4_RFID =RFID_Tag;
                                        global_instance.T6_R4=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/house5.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071773816://else if(RFID_Tag=="1071773816")
                           {
                                        global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R4_RFID =RFID_Tag;
                                        global_instance.T6_R4=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/Storage_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071780377://else if(RFID_Tag=="1071780377")
                           {
                                        global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R4_RFID =RFID_Tag;
                                        global_instance.T6_R4=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/Storage_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071770985://else if(RFID_Tag=="1071770985")
                           {
                                        global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R4_RFID =RFID_Tag;
                                        global_instance.T6_R4=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/apartment1.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  7304921://else if(RFID_Tag=="7304921")
                           {
                                        global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R4_RFID =RFID_Tag;
                                        global_instance.T6_R4=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/apartment2.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  593072732://else if(RFID_Tag=="593072732")
                           {
                                        global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R4_RFID =RFID_Tag;
                                        global_instance.T6_R4=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/apartment3.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071753108://else if(RFID_Tag=="1071753108")
                           {
                                        global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R4_RFID =RFID_Tag;
                                        global_instance.T6_R4=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/apartment4.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  2836099420://else if(RFID_Tag=="2836099420")
                           {
                                        global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R4_RFID =RFID_Tag;
                                        global_instance.T6_R4=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/apartment5.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1840834860://else if(RFID_Tag=="1840834860")
                           {
                                        global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R4_RFID =RFID_Tag;
                                        global_instance.T6_R4=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/apartment6.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  597999943://else if(RFID_Tag=="597999943")
                           {
                                        global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R4_RFID =RFID_Tag;
                                        global_instance.T6_R4=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/shopping_mall.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  8538681://else if(RFID_Tag=="8538681")
                           {
                                        global_instance.T6_R4_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R4_RFID =RFID_Tag;
                                        global_instance.T6_R4=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R4 in MQTT()=:"<<global_instance.T6_R4<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R4_MODULE=new T6_R4_Model("img/hospital.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }

                            break;
                            case  0://else if(global_instance.T6_R4==1&&RFID_Tag=="0")
                            if(global_instance.T6_R4==1){

                           {
                                        global_instance.T6_R4_RFID =nullptr;
                                        global_instance.T6_R4=0;
                                        emit_model_placed();

                                        global_table_instance.TABLE6->T6_R4_MODULE->RemoveQitem(global_table_instance.TABLE6->table_6_scene);
                                        //global_table_instance.TABLE1->timer->stop();
                                        //global_table_instance.TABLE1->timer1->start(100);
                                        qDebug("Model Removed");
                                    }
                            }
                            break;
                            default://else{qDebug("No");}
                                  qDebug("No");
                            break;
                       }

                    }
                                     break;
                                     case 3://if(RFID=="RFID 3")
                        {switch(global_instance.RFID_Tag_No)
                           {
                            case 1071985354://if(RFID_Tag=="1071985354")
                            {
                                global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R3_RFID =RFID_Tag;
                                global_instance.T6_R3=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 1072063229://else if(RFID_Tag=="1072063229")
                            {
                                global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R3_RFID =RFID_Tag;
                                global_instance.T6_R3=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 1072056604://else if(RFID_Tag=="1072056604")
                            {
                                global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R3_RFID =RFID_Tag;
                                global_instance.T6_R3=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 699168295://else if(RFID_Tag=="699168295")
                            {
                                global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R3_RFID =RFID_Tag;
                                global_instance.T6_R3=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 2305939010://else if(RFID_Tag=="2305939010")
                            {
                                global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R3_RFID =RFID_Tag;
                                global_instance.T6_R3=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case  2836102489: //else if(RFID_Tag=="2836102489")
                            {
                                global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R3_RFID =RFID_Tag;
                                global_instance.T6_R3=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case  1071986763://else if(RFID_Tag=="1071986763")
                    {
                                global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R3_RFID =RFID_Tag;
                                global_instance.T6_R3=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/house1.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case  1072063101://else if(RFID_Tag=="1072063101")
                                   {
                                        global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R3_RFID =RFID_Tag;
                                        global_instance.T6_R3=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/house2.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  2298616020://else if(RFID_Tag=="2298616020")
                           {
                                        global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R3_RFID =RFID_Tag;
                                        global_instance.T6_R3=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/house3.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071733508://else if(RFID_Tag=="1071733508")
                           {
                                        global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R3_RFID =RFID_Tag;
                                        global_instance.T6_R3=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/house4.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  3914547211://else if(RFID_Tag=="3914547211")
                           {
                                        global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R3_RFID =RFID_Tag;
                                        global_instance.T6_R3=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/house5.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071773816://else if(RFID_Tag=="1071773816")


                           {
                                        global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R3_RFID =RFID_Tag;
                                        global_instance.T6_R3=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/Storage_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071780377://else if(RFID_Tag=="1071780377")
                           {
                                        global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R3_RFID =RFID_Tag;
                                        global_instance.T6_R3=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/Storage_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071770985://else if(RFID_Tag=="1071770985")
                           {
                                        global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R3_RFID =RFID_Tag;
                                        global_instance.T6_R3=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/apartment1.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  7304921://else if(RFID_Tag=="7304921")
                           {
                                        global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R3_RFID =RFID_Tag;
                                        global_instance.T6_R3=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/apartment2.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  593072732://else if(RFID_Tag=="593072732")
                           {
                                        global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R3_RFID =RFID_Tag;
                                        global_instance.T6_R3=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/apartment3.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071753108://else if(RFID_Tag=="1071753108")
                           {
                                        global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R3_RFID =RFID_Tag;
                                        global_instance.T6_R3=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/apartment4.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  2836099420://else if(RFID_Tag=="2836099420")
                           {
                                        global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R3_RFID =RFID_Tag;
                                        global_instance.T6_R3=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/apartment5.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1840834860://else if(RFID_Tag=="1840834860")
                           {
                                        global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R3_RFID =RFID_Tag;
                                        global_instance.T6_R3=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/apartment6.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  597999943://else if(RFID_Tag=="597999943")
                           {
                                        global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R3_RFID =RFID_Tag;
                                        global_instance.T6_R3=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/shopping_mall.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  8538681://else if(RFID_Tag=="8538681")
                           {
                                        global_instance.T6_R3_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R3_RFID =RFID_Tag;
                                        global_instance.T6_R3=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R3 in MQTT()=:"<<global_instance.T6_R3<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R3_MODULE=new T6_R3_Model("img/hospital.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }

                            break;
                            case  0://else if(global_instance.T6_R3==1&&RFID_Tag=="0")
                            if(global_instance.T6_R3==1){

                           {
                                        global_instance.T6_R3_RFID =nullptr;
                                        global_instance.T6_R3=0;
                                        emit_model_placed();

                                        global_table_instance.TABLE6->T6_R3_MODULE->RemoveQitem(global_table_instance.TABLE6->table_6_scene);
                                        //global_table_instance.TABLE1->timer->stop();
                                        //global_table_instance.TABLE1->timer1->start(100);
                                        qDebug("Model Removed");
                                    }
                            }
                            break;
                            default://else{qDebug("No");}
                                  qDebug("No");
                            break;
                       }

                    }
                                     break;
                                     case 2://if(RFID=="RFID 2")
                        {switch(global_instance.RFID_Tag_No)
                           {
                            case 1071985354://if(RFID_Tag=="1071985354")
                            {
                                global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R2_RFID =RFID_Tag;
                                global_instance.T6_R2=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 1072063229://else if(RFID_Tag=="1072063229")
                            {
                                global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R2_RFID =RFID_Tag;
                                global_instance.T6_R2=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 1072056604://else if(RFID_Tag=="1072056604")
                            {
                                global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R2_RFID =RFID_Tag;
                                global_instance.T6_R2=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 699168295://else if(RFID_Tag=="699168295")
                            {
                                global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R2_RFID =RFID_Tag;
                                global_instance.T6_R2=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 2305939010://else if(RFID_Tag=="2305939010")
                            {
                                global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R2_RFID =RFID_Tag;
                                global_instance.T6_R2=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case  2836102489: //else if(RFID_Tag=="2836102489")
                            {
                                global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R2_RFID =RFID_Tag;
                                global_instance.T6_R2=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case  1071986763://else if(RFID_Tag=="1071986763")
                    {
                                global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R2_RFID =RFID_Tag;
                                global_instance.T6_R2=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/house1.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case  1072063101://else if(RFID_Tag=="1072063101")
                                   {
                                        global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R2_RFID =RFID_Tag;
                                        global_instance.T6_R2=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/house2.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  2298616020://else if(RFID_Tag=="2298616020")
                           {
                                        global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R2_RFID =RFID_Tag;
                                        global_instance.T6_R2=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/house3.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071733508://else if(RFID_Tag=="1071733508")
                           {
                                        global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R2_RFID =RFID_Tag;
                                        global_instance.T6_R2=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/house4.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  3914547211://else if(RFID_Tag=="3914547211")
                           {
                                        global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R2_RFID =RFID_Tag;
                                        global_instance.T6_R2=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/house5.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071773816://else if(RFID_Tag=="1071773816")
                           {
                                        global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R2_RFID =RFID_Tag;
                                        global_instance.T6_R2=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/Storage_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071780377://else if(RFID_Tag=="1071780377")
                           {
                                        global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R2_RFID =RFID_Tag;
                                        global_instance.T6_R2=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/Storage_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071770985://else if(RFID_Tag=="1071770985")
                           {
                                        global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R2_RFID =RFID_Tag;
                                        global_instance.T6_R2=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/apartment1.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  7304921://else if(RFID_Tag=="7304921")
                           {
                                        global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R2_RFID =RFID_Tag;
                                        global_instance.T6_R2=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/apartment2.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  593072732://else if(RFID_Tag=="593072732")
                           {
                                        global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R2_RFID =RFID_Tag;
                                        global_instance.T6_R2=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/apartment3.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071753108://else if(RFID_Tag=="1071753108")
                           {
                                        global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R2_RFID =RFID_Tag;
                                        global_instance.T6_R2=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/apartment4.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  2836099420://else if(RFID_Tag=="2836099420")
                           {
                                        global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R2_RFID =RFID_Tag;
                                        global_instance.T6_R2=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/apartment5.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1840834860://else if(RFID_Tag=="1840834860")
                           {
                                        global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R2_RFID =RFID_Tag;
                                        global_instance.T6_R2=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/apartment6.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  597999943://else if(RFID_Tag=="597999943")
                           {
                                        global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R2_RFID =RFID_Tag;
                                        global_instance.T6_R2=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/shopping_mall.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  8538681://else if(RFID_Tag=="8538681")
                           {
                                        global_instance.T6_R2_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R2_RFID =RFID_Tag;
                                        global_instance.T6_R2=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R2 in MQTT()=:"<<global_instance.T6_R2<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R2_MODULE=new T6_R2_Model("img/hospital.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }

                            break;
                            case  0://else if(global_instance.T6_R2==1&&RFID_Tag=="0")
                            if(global_instance.T6_R2==1){
                           {
                                        global_instance.T6_R2_RFID =nullptr;
                                        global_instance.T6_R2=0;
                                        emit_model_placed();

                                        global_table_instance.TABLE6->T6_R2_MODULE->RemoveQitem(global_table_instance.TABLE6->table_6_scene);
                                        //global_table_instance.TABLE1->timer->stop();
                                        //global_table_instance.TABLE1->timer1->start(100);
                                        qDebug("Model Removed");
                                    }
                            }
                            break;
                            default://else{qDebug("No");}
                                  qDebug("No");
                            break;
                       }

                    }
                                     break;
                                     case 1://if(RFID=="RFID 1")
                        {switch(global_instance.RFID_Tag_No)
                           {
                            case 1071985354://if(RFID_Tag=="1071985354")
                            {
                                global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R1_RFID =RFID_Tag;
                                global_instance.T6_R1=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/Terrace_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 1072063229://else if(RFID_Tag=="1072063229")
                            {
                                global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R1_RFID =RFID_Tag;
                                global_instance.T6_R1=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/Terrace_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 1072056604://else if(RFID_Tag=="1072056604")
                            {
                                global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R1_RFID =RFID_Tag;
                                global_instance.T6_R1=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/Terrace_house_3.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 699168295://else if(RFID_Tag=="699168295")
                            {
                                global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R1_RFID =RFID_Tag;
                                global_instance.T6_R1=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/Terrace_house_4.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case 2305939010://else if(RFID_Tag=="2305939010")
                            {
                                global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R1_RFID =RFID_Tag;
                                global_instance.T6_R1=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/Terrace_house_5.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case  2836102489: //else if(RFID_Tag=="2836102489")
                            {
                                global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R1_RFID =RFID_Tag;
                                global_instance.T6_R1=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/Terrace_house_6.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case  1071986763://else if(RFID_Tag=="1071986763")
                    {
                                global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                global_instance.T6_R1_RFID =RFID_Tag;
                                global_instance.T6_R1=1;
                                emit_model_placed();
                                std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                global_table_instance.TABLE6->timer1->stop();
                                global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/house1.jpg",global_table_instance.TABLE6->table_6_scene);
                                global_table_instance.TABLE6->timer->start(100);
                            }
                            break;
                            case  1072063101://else if(RFID_Tag=="1072063101")
                                   {
                                        global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R1_RFID =RFID_Tag;
                                        global_instance.T6_R1=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/house2.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  2298616020://else if(RFID_Tag=="2298616020")
                           {
                                        global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R1_RFID =RFID_Tag;
                                        global_instance.T6_R1=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/house3.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071733508://else if(RFID_Tag=="1071733508")
                           {
                                        global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R1_RFID =RFID_Tag;
                                        global_instance.T6_R1=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/house4.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  3914547211://else if(RFID_Tag=="3914547211")
                           {
                                        global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R1_RFID =RFID_Tag;
                                        global_instance.T6_R1=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/house5.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071773816://else if(RFID_Tag=="1071773816")
                           {
                                        global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R1_RFID =RFID_Tag;
                                        global_instance.T6_R1=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/Storage_house_1.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071780377://else if(RFID_Tag=="1071780377")
                           {
                                        global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R1_RFID =RFID_Tag;
                                        global_instance.T6_R1=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/Storage_house_2.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071770985://else if(RFID_Tag=="1071770985")
                           {
                                        global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R1_RFID =RFID_Tag;
                                        global_instance.T6_R1=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/apartment1.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  7304921://else if(RFID_Tag=="7304921")
                           {
                                        global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R1_RFID =RFID_Tag;
                                        global_instance.T6_R1=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/apartment2.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  593072732://else if(RFID_Tag=="593072732")
                           {
                                        global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R1_RFID =RFID_Tag;
                                        global_instance.T6_R1=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/apartment3.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1071753108://else if(RFID_Tag=="1071753108")
                           {
                                        global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R1_RFID =RFID_Tag;
                                        global_instance.T6_R1=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/apartment4.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  2836099420://else if(RFID_Tag=="2836099420")
                           {
                                        global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R1_RFID =RFID_Tag;
                                        global_instance.T6_R1=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/apartment5.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  1840834860://else if(RFID_Tag=="1840834860")
                           {
                                        global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R1_RFID =RFID_Tag;
                                        global_instance.T6_R1=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/apartment6.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  597999943://else if(RFID_Tag=="597999943")
                           {
                                        global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R1_RFID =RFID_Tag;
                                        global_instance.T6_R1=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/shopping_mall.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }
                            break;
                            case  8538681://else if(RFID_Tag=="8538681")
                           {
                                        global_instance.T6_R1_VALUE= *(global_instance.MQTT_JSON_MAP_LOAD.find(RFID_Tag));
                                        global_instance.T6_R1_RFID =RFID_Tag;
                                        global_instance.T6_R1=1;
                                        emit_model_placed();
                                        std::cout<<"global_instance.T6_R1 in MQTT()=:"<<global_instance.T6_R1<<std::endl;
                                        global_table_instance.TABLE6->timer1->stop();
                                        global_table_instance.TABLE6->T6_R1_MODULE=new T6_R1_Model("img/hospital.jpg",global_table_instance.TABLE6->table_6_scene);
                                        global_table_instance.TABLE6->timer->start(100);
                                    }

                            break;
                            case  0://else if(global_instance.T6_R1==1&&RFID_Tag=="0")
                            if(global_instance.T6_R1==1){
                           {
                                        global_instance.T6_R1_RFID =nullptr;
                                        global_instance.T6_R1=0;
                                        emit_model_placed();

                                        global_table_instance.TABLE6->T6_R1_MODULE->RemoveQitem(global_table_instance.TABLE6->table_6_scene);
                                        //global_table_instance.TABLE1->timer->stop();
                                        //global_table_instance.TABLE1->timer1->start(100);
                                        qDebug("Model Removed");
                                    }
                            }
                            break;
                            default://else{qDebug("No");}
                                  qDebug("No");
                            break;
                       }

                    }
                                     break;
                                     default:

                                     break;
                                    }
                                 }
                    break;
                    //table6 finished==================================================================
            }
        }

        }
        else{qDebug("others");}
     });

//      auto subscription = this->tables_topic_client->subscribe(subtopic);
//      if (!subscription) {
//      QMessageBox::critical(this, QLatin1String("Error"), QLatin1String("Could not subscribe. Is there a valid connection?"));
//      }
//      QString const subtopic1 ="SmartDemoTable/Table1/Outgoing" ;
//      tables_topic_client->subscribe(subtopic1);
//MQTT ENDED=========================================================================================================

//DRAW TABLES STARTED================================================================================================
    const int gridheight = 4;
    const int gridwidth = 3;
    //scene= new QGraphicsScene;
    //scene->update();
    for (int y = 0; y < gridheight; ++y)
    {
        for (int x = 0; x < gridwidth; ++x)
        {
            //if(x==0&y==0||x==0&y==3||x==0&y==2||x==2&y==0||x==2&y==2||x==2&y==3){continue;}
            if(x==0&y==0||x==0&y==3||x==2&y==3){continue;}
            CustomProxy *proxy = new CustomProxy(nullptr, Qt::Window);
            proxy->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);//无边框It should be used in the very top of your main function
            proxy->setCacheMode(QGraphicsItem::DeviceCoordinateCache);
            if((x==0)&&(y==2))
            {
                proxy->setWidget(global_table_instance.slider);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(1.1 * rect.width(), 2.4 * rect.height());
                this->scene->addItem(proxy);
                continue;
            }
            if((x==1)&&(y==1))
            {
                proxy->setWidget(global_table_instance.TABLE2);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(x * rect.width(), y * rect.height());
                this->scene->addItem(proxy);
                continue;
            }
            if(x==1&y==2)
            {
                proxy->setWidget(global_table_instance.TABLE3);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(x * rect.width(), y * rect.height());
                this->scene->addItem(proxy);
                continue;
            }
            if(x==1&y==0)
            {
                proxy->setWidget((global_table_instance.TABLE1));
                QRectF rect = proxy->boundingRect();
                proxy->setPos(x * rect.width(), y * rect.height());
                this->scene->addItem(proxy);
                continue;
            }

            if(x==1&y==3)
            {
                proxy->setWidget(global_table_instance.TABLE4);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(x * rect.width(), y * rect.height());
                this->scene->addItem(proxy);
                continue;
            }

            if(x==0&y==1)
            {
                proxy->setWidget(global_table_instance.TABLE5);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(x * rect.width(), y * rect.height());
                this->scene->addItem(proxy);
                continue;
            }
            if(x==2&y==1)
            {std::cout<<"T666666!!!"<<std::endl;
                proxy->setWidget(global_table_instance.TABLE6);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(x * rect.width(), y * rect.height());
                this->scene->addItem(proxy);
                continue;
            }
            if(x==2&y==2)
            {std::cout<<"chart!!!"<<std::endl;
                proxy->setWidget(global_table_instance.chart);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(1.08 * rect.width(), 1.36 * rect.height());
                this->scene->addItem(proxy);
                continue;
            }

            if(x==2&y==0)
            {std::cout<<"T666666!!!"<<std::endl;
                proxy->setWidget(global_table_instance.commands_box);
                proxy->resize(820, 500);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(1.6 * rect.width(), 0 * rect.height());
                this->scene->addItem(proxy);
                continue;
            }
            //global_table_instance.commands_box
        }
    }

    this->view1 = new QGraphicsView(this->scene);
    this->view1->scale(0.63, 0.63);
    this->view1->setRenderHints(view1->renderHints() | QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    this->view1->setBackgroundBrush(QPixmap(":/No-Ones-Laughing-3.jpg"));
    this->view1->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    this->view1->show();
    this->view1->setWindowTitle("smart grid table");
    this->setCentralWidget(this->view1);
//DRAW TABLES ENDED================================================================================================


//SIGNAL SLOTS CONNECTIONS=========================================================================================
    timer_calendar = new QTimer(this);//new个对象
    connect(timer_calendar,SIGNAL(timeout()),this,SLOT(timer_Update()));//timeout超时事件
    //timer_calendar->start(10);//每隔一秒调用一次槽函数
    //connect(&(global_instance), SIGNAL(valueChanged(global_instance.aa)), this, SLOT(Mainwindow_Data_Change()));
    //connect函数第一个参数必须是指向某个object的指针
    connect(this,&mainwindow::sig_model_placed,this,&mainwindow::Mainwindow_Data_Change);
    connect(this,&mainwindow::chart_update,this,&mainwindow::Dynamic_Chart_Change);
    //connect(this,&mainwindow::p_nom_max,this,&mainwindow::P_Nom_Max_True);
    connect(this,&mainwindow::T3R0_p_nom_max,this,&mainwindow::T3R0_P_Nom_Max_True);
    connect(this,&mainwindow::T3R1_p_nom_max,this,&mainwindow::T3R1_P_Nom_Max_True);
    connect(this,&mainwindow::T3R2_p_nom_max,this,&mainwindow::T3R2_P_Nom_Max_True);
    connect(this,&mainwindow::T3R3_p_nom_max,this,&mainwindow::T3R3_P_Nom_Max_True);
    connect(this,&mainwindow::T3R4_p_nom_max,this,&mainwindow::T3R4_P_Nom_Max_True);
    connect(this,&mainwindow::T3R5_p_nom_max,this,&mainwindow::T3R5_P_Nom_Max_True);
    connect(this,&mainwindow::T3R6_p_nom_max,this,&mainwindow::T3R6_P_Nom_Max_True);
    connect(this,&mainwindow::T3R7_p_nom_max,this,&mainwindow::T3R7_P_Nom_Max_True);
    //connect(this,&mainwindow::p_nom_max,this,&mainwindow::P_Nom_Max_True);
    //connect signals from global_tables Tx_Rx_clicked to Tx_Rx_Pub
    connect(global_table_instance.TABLE1,&table1::T1_R5_clicked,this,&mainwindow::T1_R5_Pub);
    connect(global_table_instance.TABLE1,&table1::T1_R1_clicked,this,&mainwindow::T1_R1_Pub);

    connect(global_table_instance.TABLE6,&table6::T6_R1_clicked,this,&mainwindow::T6_R1_Pub);
    connect(global_table_instance.TABLE6,&table6::T6_R2_clicked,this,&mainwindow::T6_R2_Pub);
    connect(global_table_instance.TABLE6,&table6::T6_R3_clicked,this,&mainwindow::T6_R3_Pub);
    connect(global_table_instance.TABLE6,&table6::T6_R4_clicked,this,&mainwindow::T6_R4_Pub);
    connect(global_table_instance.TABLE6,&table6::T6_R5_clicked,this,&mainwindow::T6_R5_Pub);
    connect(global_table_instance.TABLE6,&table6::T6_R6_clicked,this,&mainwindow::T6_R6_Pub);
    connect(global_table_instance.TABLE6,&table6::T6_R7_clicked,this,&mainwindow::T6_R7_Pub);

    connect(global_table_instance.TABLE4,&table4::T4_R1_clicked,this,&mainwindow::T4_R1_Pub);
    connect(global_table_instance.TABLE4,&table4::T4_R2_clicked,this,&mainwindow::T4_R2_Pub);
    connect(global_table_instance.TABLE4,&table4::T4_R3_clicked,this,&mainwindow::T4_R3_Pub);
    connect(global_table_instance.TABLE4,&table4::T4_R4_clicked,this,&mainwindow::T4_R4_Pub);
    connect(global_table_instance.TABLE4,&table4::T4_R5_clicked,this,&mainwindow::T4_R5_Pub);
    connect(global_table_instance.TABLE4,&table4::T4_R6_clicked,this,&mainwindow::T4_R6_Pub);
    connect(global_table_instance.TABLE4,&table4::T4_R7_clicked,this,&mainwindow::T4_R7_Pub);

    connect(global_table_instance.TABLE3,&table3::T3_R0_clicked,this,&mainwindow::T3_R0_Pub);
    connect(global_table_instance.TABLE3,&table3::T3_R1_clicked,this,&mainwindow::T3_R1_Pub);
    connect(global_table_instance.TABLE3,&table3::T3_R2_clicked,this,&mainwindow::T3_R2_Pub);
    connect(global_table_instance.TABLE3,&table3::T3_R3_clicked,this,&mainwindow::T3_R3_Pub);
    connect(global_table_instance.TABLE3,&table3::T3_R4_clicked,this,&mainwindow::T3_R4_Pub);
    connect(global_table_instance.TABLE3,&table3::T3_R5_clicked,this,&mainwindow::T3_R5_Pub);
    connect(global_table_instance.TABLE3,&table3::T3_R6_clicked,this,&mainwindow::T3_R6_Pub);
    connect(global_table_instance.TABLE3,&table3::T3_R7_clicked,this,&mainwindow::T3_R7_Pub);
    //connect(global_table_instance.TABLE5,&table5::T5_R0_clicked,this,&mainwindow::T5_R0_Pub);
    connect(global_table_instance.TABLE5,&table5::T5_R1_clicked,this,&mainwindow::T5_R1_Pub);
    connect(global_table_instance.TABLE5,&table5::T5_R2_clicked,this,&mainwindow::T5_R2_Pub);
    connect(global_table_instance.TABLE5,&table5::T5_R3_clicked,this,&mainwindow::T5_R3_Pub);
    connect(global_table_instance.TABLE5,&table5::T5_R4_clicked,this,&mainwindow::T5_R4_Pub);
    connect(global_table_instance.TABLE5,&table5::T5_R5_clicked,this,&mainwindow::T5_R5_Pub);
    connect(global_table_instance.TABLE5,&table5::T5_R6_clicked,this,&mainwindow::T5_R6_Pub);
    connect(global_table_instance.TABLE5,&table5::T5_R7_clicked,this,&mainwindow::T5_R7_Pub);
    connect(global_table_instance.TABLE2,&table2::table2_clicked,this,&mainwindow::show_table2);

    connect(global_table_instance.commands_box,&ComboBox_Window::command_pub_sig,this,&mainwindow::command_Pub);
    connect(global_table_instance.slider,&Slider_Window::slider_update,this,&mainwindow::change_size);
    //connectMarkers();
    QGraphicsTextItem* gti = new QGraphicsTextItem("Smart Grid GUI");
    gti->setFont(QFont("Arial", 18));
    gti->setFlag(QGraphicsTextItem::ItemIgnoresTransformations, true);
    scene->addItem(gti);
}



void mainwindow::Change_Json_Gen(QString RFID_Tag_No, double p_nom_max_value){
    /*if (RFID_Tag_No==NULL||p_nom_max_value==NULL){return;}

    QFile file("D:/graduation/graduation/Graduation_internship_code/embedded tables/3/embedded tables/embeddeddialogs/Scenario1_default.json");
    if(!file.open(QIODevice::ReadWrite)) {
        qDebug() << "File open failed!";
    } else {
        qDebug() <<"File open successfully!";
    }
    QString string_value = file.readAll();
    QString readonly_config=string_value;

    // 开始解析 解析成功返回QJsonDocument对象否则返回null
    QJsonParseError err_rpt;
    QJsonDocument root_document = QJsonDocument::fromJson(readonly_config.toUtf8(), &err_rpt);
    //QJsonParseError err_rpt;
    QJsonDocument  jdc = QJsonDocument::fromJson(readonly_config.toUtf8(), &err_rpt);//字符串格式化为JSON
    QJsonObject obj = jdc.object();

    QJsonObject RFID_No = obj[RFID_Tag_No].toObject();
    QJsonArray loads = RFID_No["generators"].toArray();
    QJsonObject lll=loads[0].toObject();
    lll["p_nom_max"]=p_nom_max_value;
    loads.replace(0, lll);
    RFID_No["generators"]=loads;
    obj[RFID_Tag_No]=RFID_No;
    jdc.setObject(obj);
    //file.seek(0);
    file.write(jdc.toJson());*/
}

void mainwindow::Change_Json_Load(QString RFID_Tag_No, double p_set_value){
    /*if (RFID_Tag_No==NULL||p_set_value==NULL){return;}

    QFile file("D:/graduation/graduation/Graduation_internship_code/embedded tables/3/embedded tables/embeddeddialogs/Scenario1_default.json");
    if(!file.open(QIODevice::ReadWrite)) {
        qDebug() << "File open failed!";
    } else {
        qDebug() <<"File open successfully!";
    }
    QString string_value = file.readAll();
    QString readonly_config=string_value;

    // 开始解析 解析成功返回QJsonDocument对象否则返回null
    QJsonParseError err_rpt;
    QJsonDocument root_document = QJsonDocument::fromJson(readonly_config.toUtf8(), &err_rpt);
    //QJsonParseError err_rpt;
    QJsonDocument  jdc = QJsonDocument::fromJson(readonly_config.toUtf8(), &err_rpt);//字符串格式化为JSON
    QJsonObject obj = jdc.object();

    QJsonObject RFID_No = obj[RFID_Tag_No].toObject();
    QJsonArray loads = RFID_No["loads"].toArray();
    QJsonObject lll=loads[0].toObject();
    lll["p_set"]=p_set_value;
    loads.replace(0, lll);
    RFID_No["loads"]=loads;
    obj[RFID_Tag_No]=RFID_No;
    jdc.setObject(obj);
    //file.seek(0);
    file.write(jdc.toJson());*/
}

void mainwindow::change_size(){
    const int gridheight = 4;
    const int gridwidth = 3;
    //scene= new QGraphicsScene;
    //scene->update();
    for (int y = 0; y < gridheight; ++y)
    {
        for (int x = 0; x < gridwidth; ++x)
        {
            //if(x==0&y==0||x==0&y==3||x==0&y==2||x==2&y==0||x==2&y==2||x==2&y==3){continue;}
            if(x==0&y==0||x==0&y==3||x==2&y==3){continue;}
            proxy = new CustomProxy(nullptr, Qt::Window);
            proxy->setWindowFlags(Qt::Window | Qt::FramelessWindowHint);//无边框It should be used in the very top of your main function
            proxy->setCacheMode(QGraphicsItem::DeviceCoordinateCache);
            if((x==0)&&(y==2))
            {
                proxy->setWidget(global_table_instance.slider);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(1.1 * rect.width(), 2.4 * rect.height());
                this->scene->addItem(proxy);
                continue;
            }
            if((x==1)&&(y==1))
            {
                proxy->setWidget(global_table_instance.TABLE2);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(x * rect.width(), y * rect.height());
                this->scene->addItem(proxy);
                continue;
            }
            if(x==1&y==2)
            {
                proxy->setWidget(global_table_instance.TABLE3);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(x * rect.width(), y * rect.height());
                this->scene->addItem(proxy);
                continue;
            }
            if(x==1&y==0)
            {
                proxy->setWidget((global_table_instance.TABLE1));
                QRectF rect = proxy->boundingRect();
                proxy->setPos(x * rect.width(), y * rect.height());
                this->scene->addItem(proxy);
                continue;
            }

            if(x==1&y==3)
            {
                proxy->setWidget(global_table_instance.TABLE4);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(x * rect.width(), y * rect.height());
                this->scene->addItem(proxy);
                continue;
            }

            if(x==0&y==1)
            {
                proxy->setWidget(global_table_instance.TABLE5);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(x * rect.width(), y * rect.height());
                this->scene->addItem(proxy);
                continue;
            }
            if(x==2&y==1)
            {std::cout<<"T666666!!!"<<std::endl;
                proxy->setWidget(global_table_instance.TABLE6);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(x * rect.width(), y * rect.height());
                this->scene->addItem(proxy);
                continue;
            }
            if(x==2&y==2)
            {std::cout<<"chart!!!"<<std::endl;
                proxy->setWidget(global_table_instance.chart);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(1.08 * rect.width(), 1.36 * rect.height());
                this->scene->addItem(proxy);
                continue;
            }

            if(x==2&y==0)
            {std::cout<<"T666666!!!"<<std::endl;
                proxy->setWidget(global_table_instance.commands_box);
                proxy->resize(820, 500);
                QRectF rect = proxy->boundingRect();
                proxy->setPos(1.6 * rect.width(), 0 * rect.height());
                this->scene->addItem(proxy);
                continue;
            }
            //global_table_instance.commands_box
        }
    }

    this->view1 = new QGraphicsView(this->scene);
    this->view1->scale(global_table_instance.slider->num, global_table_instance.slider->num);
    this->view1->setRenderHints(view1->renderHints() | QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    this->view1->setBackgroundBrush(QPixmap(":/No-Ones-Laughing-3.jpg"));
    this->view1->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    this->view1->show();
    this->view1->setWindowTitle("smart grid table");
    this->setCentralWidget(this->view1);
   // if (global_table_instance.slider->num<0.4){global_table_instance.slider->num=0.4;}
};



void mainwindow::T1_R5_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        std::string mid;
        QMqttTopicName uu = pubtopic;
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_nom_max"+" "+global_instance.T1_R5_RFID.toStdString()+" "+std::to_string(global_instance.T1_R5_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_GEN[global_instance.T1_R5_RFID]=global_instance.T1_R5_VALUE;
        Change_Json_Gen(global_instance.T1_R5_RFID,global_instance.T1_R5_VALUE);
};

void mainwindow::T1_R1_Pub(){
    std::cout<<"=========================================="<<std::endl;
    QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
    std::string mid;
    QMqttTopicName uu = pubtopic;
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_nom_max"+" "+global_instance.T1_R1_RFID.toStdString()+" "+std::to_string(global_instance.T1_R1_VALUE)+'"'+"}";
    QString content1 = QString::fromStdString(mid);
    QByteArray content =content1.toUtf8();
    this->tables_topic_client->publish(uu,content);
    global_instance.MQTT_JSON_MAP_GEN[global_instance.T1_R1_RFID]=global_instance.T1_R1_VALUE;
    Change_Json_Gen(global_instance.T1_R1_RFID,global_instance.T1_R1_VALUE);
};

void mainwindow::T6_R1_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T6_R1_RFID.toStdString()+" "+to_string(global_instance.T6_R1_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T6_R1_RFID]=global_instance.T6_R1_VALUE;
        Change_Json_Load(global_instance.T6_R1_RFID,global_instance.T6_R1_VALUE);
};
void mainwindow::T6_R2_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T6_R2_RFID.toStdString()+" "+to_string(global_instance.T6_R2_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T6_R2_RFID]=global_instance.T6_R2_VALUE;
        Change_Json_Load(global_instance.T6_R2_RFID,global_instance.T6_R2_VALUE);
};
void mainwindow::T6_R3_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T6_R3_RFID.toStdString()+" "+to_string(global_instance.T6_R3_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T6_R3_RFID]=global_instance.T6_R3_VALUE;
        Change_Json_Load(global_instance.T6_R3_RFID,global_instance.T6_R3_VALUE);
};
void mainwindow::T6_R4_Pub(){
    std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T6_R4_RFID.toStdString()+" "+to_string(global_instance.T6_R4_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T6_R4_RFID]=global_instance.T6_R4_VALUE;
        Change_Json_Load(global_instance.T6_R4_RFID,global_instance.T6_R4_VALUE);
};
void mainwindow::T6_R5_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T6_R5_RFID.toStdString()+" "+to_string(global_instance.T6_R5_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T6_R5_RFID]=global_instance.T6_R5_VALUE;
        Change_Json_Load(global_instance.T6_R5_RFID,global_instance.T6_R5_VALUE);
};
void mainwindow::T6_R6_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T6_R6_RFID.toStdString()+" "+to_string(global_instance.T6_R6_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T6_R6_RFID]=global_instance.T6_R6_VALUE;
        Change_Json_Load(global_instance.T6_R6_RFID,global_instance.T6_R6_VALUE);
};
void mainwindow::T6_R7_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T6_R7_RFID.toStdString()+" "+to_string(global_instance.T6_R7_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T6_R7_RFID]=global_instance.T6_R7_VALUE;
        Change_Json_Load(global_instance.T6_R7_RFID,global_instance.T6_R7_VALUE);
};


void mainwindow::T4_R1_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T4_R1_RFID.toStdString()+" "+to_string(global_instance.T4_R1_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T4_R1_RFID]=global_instance.T4_R1_VALUE;
        Change_Json_Load(global_instance.T4_R1_RFID,global_instance.T4_R1_VALUE);
};
void mainwindow::T4_R2_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T4_R2_RFID.toStdString()+" "+to_string(global_instance.T4_R2_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T4_R2_RFID]=global_instance.T4_R2_VALUE;
        Change_Json_Load(global_instance.T4_R2_RFID,global_instance.T4_R2_VALUE);
};
void mainwindow::T4_R3_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T4_R3_RFID.toStdString()+" "+to_string(global_instance.T4_R3_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T4_R3_RFID]=global_instance.T4_R3_VALUE;
        Change_Json_Load(global_instance.T4_R3_RFID,global_instance.T4_R3_VALUE);
};
void mainwindow::T4_R4_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T4_R4_RFID.toStdString()+" "+to_string(global_instance.T4_R4_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T4_R4_RFID]=global_instance.T4_R4_VALUE;
        Change_Json_Load(global_instance.T4_R4_RFID,global_instance.T4_R4_VALUE);
};
void mainwindow::T4_R5_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T4_R5_RFID.toStdString()+" "+to_string(global_instance.T4_R5_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T4_R5_RFID]=global_instance.T4_R5_VALUE;
        Change_Json_Load(global_instance.T4_R5_RFID,global_instance.T4_R5_VALUE);
};
void mainwindow::T4_R6_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T4_R6_RFID.toStdString()+" "+to_string(global_instance.T4_R6_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T4_R6_RFID]=global_instance.T4_R6_VALUE;
        Change_Json_Load(global_instance.T4_R6_RFID,global_instance.T4_R6_VALUE);
};
void mainwindow::T4_R7_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T4_R7_RFID.toStdString()+" "+to_string(global_instance.T4_R7_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T4_R7_RFID]=global_instance.T4_R7_VALUE;
        Change_Json_Load(global_instance.T4_R7_RFID,global_instance.T4_R7_VALUE);
};


void mainwindow::T3_R0_Pub(){
    std::cout<<"=========================================="<<std::endl;
    QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
    std::string mid;
    QMqttTopicName uu = pubtopic;
    if(global_instance.T3R0_p_nom_max==true){
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_nom_max"+" "+global_instance.T3_R0_RFID.toStdString()+" "+std::to_string(global_instance.T3_R0_VALUE)+'"'+"}";
//        global_instance.p_nom_max=false;
        global_instance.MQTT_JSON_MAP_GEN[global_instance.T3_R0_RFID]=global_instance.T3_R0_VALUE;
        Change_Json_Gen(global_instance.T3_R0_RFID,global_instance.T3_R0_VALUE);
    }
    else{
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T3_R0_RFID.toStdString()+" "+to_string(global_instance.T3_R0_VALUE)+'"'+"}";
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T3_R0_RFID]=global_instance.T3_R0_VALUE;
        Change_Json_Load(global_instance.T3_R0_RFID,global_instance.T3_R0_VALUE);

    }
    QString content1 = QString::fromStdString(mid);
    QByteArray content =content1.toUtf8();
    this->tables_topic_client->publish(uu,content);
};
void mainwindow::T3_R1_Pub(){

    std::cout<<"=========================================="<<std::endl;
    QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
    std::string mid;
    QMqttTopicName uu = pubtopic;
    if(global_instance.T3R1_p_nom_max==true){
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_nom_max"+" "+global_instance.T3_R1_RFID.toStdString()+" "+std::to_string(global_instance.T3_R1_VALUE)+'"'+"}";
        global_instance.MQTT_JSON_MAP_GEN[global_instance.T3_R1_RFID]=global_instance.T3_R1_VALUE;
        Change_Json_Gen(global_instance.T3_R1_RFID,global_instance.T3_R1_VALUE);
    }
    else{
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T3_R1_RFID.toStdString()+" "+to_string(global_instance.T3_R1_VALUE)+'"'+"}";
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T3_R1_RFID]=global_instance.T3_R1_VALUE;
        Change_Json_Load(global_instance.T3_R1_RFID,global_instance.T3_R1_VALUE);
    }
    QString content1 = QString::fromStdString(mid);
    QByteArray content =content1.toUtf8();
    this->tables_topic_client->publish(uu,content);

};
void mainwindow::T3_R2_Pub(){

    std::cout<<"=========================================="<<std::endl;
    QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
    std::string mid;
    QMqttTopicName uu = pubtopic;
    if(global_instance.T3R2_p_nom_max==true){
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_nom_max"+" "+global_instance.T3_R2_RFID.toStdString()+" "+std::to_string(global_instance.T3_R2_VALUE)+'"'+"}";
        global_instance.MQTT_JSON_MAP_GEN[global_instance.T3_R2_RFID]=global_instance.T3_R2_VALUE;
        Change_Json_Gen(global_instance.T3_R2_RFID,global_instance.T3_R2_VALUE);
    }
    else{
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T3_R2_RFID.toStdString()+" "+to_string(global_instance.T3_R2_VALUE)+'"'+"}";
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T3_R2_RFID]=global_instance.T3_R2_VALUE;
        Change_Json_Load(global_instance.T3_R2_RFID,global_instance.T3_R2_VALUE);
    }
    QString content1 = QString::fromStdString(mid);
    QByteArray content =content1.toUtf8();
    this->tables_topic_client->publish(uu,content);
};
void mainwindow::T3_R3_Pub(){
    std::cout<<"=========================================="<<std::endl;
    QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
    std::string mid;
    QMqttTopicName uu = pubtopic;
    if(global_instance.T3R3_p_nom_max==true){
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_nom_max"+" "+global_instance.T3_R3_RFID.toStdString()+" "+std::to_string(global_instance.T3_R3_VALUE)+'"'+"}";
        global_instance.MQTT_JSON_MAP_GEN[global_instance.T3_R3_RFID]=global_instance.T3_R3_VALUE;
        Change_Json_Gen(global_instance.T3_R3_RFID,global_instance.T3_R3_VALUE);
    }
    else{
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T3_R3_RFID.toStdString()+" "+to_string(global_instance.T3_R3_VALUE)+'"'+"}";
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T3_R3_RFID]=global_instance.T3_R3_VALUE;
        Change_Json_Load(global_instance.T3_R3_RFID,global_instance.T3_R3_VALUE);
    }
    QString content1 = QString::fromStdString(mid);
    QByteArray content =content1.toUtf8();
    this->tables_topic_client->publish(uu,content);
};
void mainwindow::T3_R4_Pub(){

    std::cout<<"=========================================="<<std::endl;
    QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
    std::string mid;
    QMqttTopicName uu = pubtopic;
    if(global_instance.T3R4_p_nom_max==true){
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_nom_max"+" "+global_instance.T3_R4_RFID.toStdString()+" "+std::to_string(global_instance.T3_R4_VALUE)+'"'+"}";
        global_instance.MQTT_JSON_MAP_GEN[global_instance.T3_R4_RFID]=global_instance.T3_R4_VALUE;
        Change_Json_Gen(global_instance.T3_R4_RFID,global_instance.T3_R4_VALUE);
    }
    else{
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T3_R4_RFID.toStdString()+" "+to_string(global_instance.T3_R4_VALUE)+'"'+"}";
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T3_R4_RFID]=global_instance.T3_R4_VALUE;
        Change_Json_Load(global_instance.T3_R4_RFID,global_instance.T3_R4_VALUE);
    }
    QString content1 = QString::fromStdString(mid);
    QByteArray content =content1.toUtf8();
    this->tables_topic_client->publish(uu,content);

};
void mainwindow::T3_R5_Pub(){

    std::cout<<"=========================================="<<std::endl;
    QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
    std::string mid;
    QMqttTopicName uu = pubtopic;
    if(global_instance.T3R5_p_nom_max==true){
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_nom_max"+" "+global_instance.T3_R5_RFID.toStdString()+" "+std::to_string(global_instance.T3_R5_VALUE)+'"'+"}";
        global_instance.MQTT_JSON_MAP_GEN[global_instance.T3_R5_RFID]=global_instance.T3_R5_VALUE;
        Change_Json_Gen(global_instance.T3_R5_RFID,global_instance.T3_R5_VALUE);
    }
    else{
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T3_R5_RFID.toStdString()+" "+to_string(global_instance.T3_R5_VALUE)+'"'+"}";
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T3_R5_RFID]=global_instance.T3_R5_VALUE;
        Change_Json_Load(global_instance.T3_R5_RFID,global_instance.T3_R5_VALUE);
    }
    QString content1 = QString::fromStdString(mid);
    QByteArray content =content1.toUtf8();
    this->tables_topic_client->publish(uu,content);

};
void mainwindow::T3_R6_Pub(){
    std::cout<<"=========================================="<<std::endl;
    QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
    std::string mid;
    QMqttTopicName uu = pubtopic;
    if(global_instance.T3R6_p_nom_max==true){
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_nom_max"+" "+global_instance.T3_R6_RFID.toStdString()+" "+std::to_string(global_instance.T3_R6_VALUE)+'"'+"}";
        global_instance.MQTT_JSON_MAP_GEN[global_instance.T3_R6_RFID]=global_instance.T3_R6_VALUE;
        Change_Json_Gen(global_instance.T3_R6_RFID,global_instance.T3_R6_VALUE);
    }
    else{
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T3_R6_RFID.toStdString()+" "+to_string(global_instance.T3_R6_VALUE)+'"'+"}";
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T3_R6_RFID]=global_instance.T3_R6_VALUE;
        Change_Json_Load(global_instance.T3_R6_RFID,global_instance.T3_R6_VALUE);
    }
    QString content1 = QString::fromStdString(mid);
    QByteArray content =content1.toUtf8();
    this->tables_topic_client->publish(uu,content);

};
void mainwindow::T3_R7_Pub(){

    std::cout<<"=========================================="<<std::endl;
    QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
    std::string mid;
    QMqttTopicName uu = pubtopic;
    if(global_instance.T3R7_p_nom_max==true){
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_nom_max"+" "+global_instance.T3_R7_RFID.toStdString()+" "+std::to_string(global_instance.T3_R7_VALUE)+'"'+"}";
        global_instance.MQTT_JSON_MAP_GEN[global_instance.T3_R7_RFID]=global_instance.T3_R7_VALUE;
        Change_Json_Gen(global_instance.T3_R7_RFID,global_instance.T3_R7_VALUE);
    }
    else{
        mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T3_R7_RFID.toStdString()+" "+to_string(global_instance.T3_R7_VALUE)+'"'+"}";
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T3_R7_RFID]=global_instance.T3_R7_VALUE;
        Change_Json_Load(global_instance.T3_R7_RFID,global_instance.T3_R7_VALUE);
    }
    QString content1 = QString::fromStdString(mid);
    QByteArray content =content1.toUtf8();
    this->tables_topic_client->publish(uu,content);
};


void mainwindow::T5_R1_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T5_R1_RFID.toStdString()+" "+to_string(global_instance.T5_R1_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T5_R1_RFID]=global_instance.T5_R1_VALUE;
        Change_Json_Load(global_instance.T5_R1_RFID,global_instance.T5_R1_VALUE);
};
void mainwindow::T5_R2_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T5_R2_RFID.toStdString()+" "+to_string(global_instance.T5_R2_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T5_R2_RFID]=global_instance.T5_R2_VALUE;
        Change_Json_Load(global_instance.T5_R2_RFID,global_instance.T5_R2_VALUE);
};
void mainwindow::T5_R3_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T5_R3_RFID.toStdString()+" "+to_string(global_instance.T5_R3_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T5_R3_RFID]=global_instance.T5_R3_VALUE;
        Change_Json_Load(global_instance.T5_R3_RFID,global_instance.T5_R3_VALUE);
};
void mainwindow::T5_R4_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T5_R4_RFID.toStdString()+" "+to_string(global_instance.T5_R4_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T5_R4_RFID]=global_instance.T5_R4_VALUE;
        Change_Json_Load(global_instance.T5_R4_RFID,global_instance.T5_R4_VALUE);
};
void mainwindow::T5_R5_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T5_R5_RFID.toStdString()+" "+to_string(global_instance.T5_R5_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T5_R5_RFID]=global_instance.T5_R5_VALUE;
        Change_Json_Load(global_instance.T5_R5_RFID,global_instance.T5_R5_VALUE);
};
void mainwindow::T5_R6_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T5_R6_RFID.toStdString()+" "+to_string(global_instance.T5_R6_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T5_R6_RFID]=global_instance.T5_R6_VALUE;
        Change_Json_Load(global_instance.T5_R6_RFID,global_instance.T5_R6_VALUE);
};
void mainwindow::T5_R7_Pub(){
        std::cout<<"=========================================="<<std::endl;
        QString pubtopic="SmartDemoTable/GUI/Outgoing";//+global_instance.T1_R5_RFID;
        QMqttTopicName uu = pubtopic;
        std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+"p_set"+" "+global_instance.T5_R7_RFID.toStdString()+" "+to_string(global_instance.T5_R7_VALUE)+'"'+"}";
        QString content1 = QString::fromStdString(mid);
        QByteArray content =content1.toUtf8();
        this->tables_topic_client->publish(uu,content);
        global_instance.MQTT_JSON_MAP_LOAD[global_instance.T5_R7_RFID]=global_instance.T5_R7_VALUE;
        Change_Json_Load(global_instance.T5_R7_RFID,global_instance.T5_R7_VALUE);
};
void mainwindow::show_table2(){
    //global_table_instance.TABLE2->close();
qDebug("chen");
}

void mainwindow::command_Pub(){
    QString pubtopic="SmartDemoTable/GUI/Outgoing";
    QMqttTopicName uu = pubtopic;
    std::string mid =std::string("{")+'"'+"command"+'"'+":"+" "+'"'+global_table_instance.commands_box->command.toStdString()+'"'+"}";
    QString content1 = QString::fromStdString(mid);
    QByteArray content =content1.toUtf8();
    this->GUI_topic_client->publish(uu,content);
    qDebug("layyyy");
}


void mainwindow::timer_Update()
{
}

void mainwindow::Mainwindow_Data_Change(){
    std::cout<<"signal has been sent!!!!!!!!!!!!!"<<std::endl;
    std::cout<<"global_instance.Coal_PW_1071771887=:"<<global_instance.Coal_PW_1071771887.toStdString()<<std::endl;
    std::cout<<"global_instance.MQTT_RFID_VALUE=:"<<global_instance.MQTT_RFID_VALUE.toStdString()<<std::endl;
    std::cout<<"global_instance.T1_R5 in Mainwindow_Data_Change()=:"<<global_instance.T3_R3_VALUE<<std::endl;
    //global_instance.T1_R5=1;
    global_table_instance.TABLE1->T1_R5_TextData_change();
    global_table_instance.TABLE1->T1_R1_TextData_change();

    global_table_instance.TABLE3->T3_R5_TextData_change();
    global_table_instance.TABLE3->T3_R6_TextData_change();
    global_table_instance.TABLE3->T3_R7_TextData_change();
    global_table_instance.TABLE3->T3_R4_TextData_change();
    global_table_instance.TABLE3->T3_R3_TextData_change();
    global_table_instance.TABLE3->T3_R2_TextData_change();
    global_table_instance.TABLE3->T3_R1_TextData_change();
    global_table_instance.TABLE3->T3_R0_TextData_change();

    global_table_instance.TABLE4->T4_R1_TextData_change();
    global_table_instance.TABLE4->T4_R2_TextData_change();
    global_table_instance.TABLE4->T4_R3_TextData_change();
    global_table_instance.TABLE4->T4_R4_TextData_change();
    global_table_instance.TABLE4->T4_R5_TextData_change();
    global_table_instance.TABLE4->T4_R6_TextData_change();
    global_table_instance.TABLE4->T4_R7_TextData_change();

    global_table_instance.TABLE6->T6_R1_TextData_change();
    global_table_instance.TABLE6->T6_R2_TextData_change();
    global_table_instance.TABLE6->T6_R3_TextData_change();
    global_table_instance.TABLE6->T6_R4_TextData_change();
    global_table_instance.TABLE6->T6_R5_TextData_change();
    global_table_instance.TABLE6->T6_R6_TextData_change();
    global_table_instance.TABLE6->T6_R7_TextData_change();

    global_table_instance.TABLE5->T5_R5_TextData_change();
    global_table_instance.TABLE5->T5_R6_TextData_change();
    global_table_instance.TABLE5->T5_R7_TextData_change();
    global_table_instance.TABLE5->T5_R4_TextData_change();
    global_table_instance.TABLE5->T5_R3_TextData_change();
    global_table_instance.TABLE5->T5_R2_TextData_change();
    global_table_instance.TABLE5->T5_R1_TextData_change();
    global_table_instance.chart->chart_data_change();
}

void mainwindow::Dynamic_Chart_Change(){

    global_table_instance.chart->chart_data_change();
    }

void mainwindow::P_Nom_Max_True(){
    global_instance.p_nom_max=true;
    }

void mainwindow::T3R0_P_Nom_Max_True(){
    global_instance.T3R0_p_nom_max=true;
    }

void mainwindow::T3R1_P_Nom_Max_True(){
    global_instance.T3R1_p_nom_max=true;
    }

void mainwindow::T3R2_P_Nom_Max_True(){
    global_instance.T3R2_p_nom_max=true;
    }

void mainwindow::T3R3_P_Nom_Max_True(){
    global_instance.T3R3_p_nom_max=true;
    }

void mainwindow::T3R4_P_Nom_Max_True(){
    global_instance.T3R4_p_nom_max=true;
    }

void mainwindow::T3R5_P_Nom_Max_True(){
    global_instance.T3R5_p_nom_max=true;
    }

void mainwindow::T3R6_P_Nom_Max_True(){
    global_instance.T3R6_p_nom_max=true;
    }
void mainwindow::T3R7_P_Nom_Max_True(){
    global_instance.T3R7_p_nom_max=true;
    }

void mainwindow::emit_model_placed()
{
    emit sig_model_placed(10999);
}


void mainwindow::emit_T3R0_p_nom_max()
{
    emit T3R0_p_nom_max(10999);
}


void mainwindow::emit_T3R1_p_nom_max()
{
    emit T3R1_p_nom_max(10999);
}
void mainwindow::emit_T3R2_p_nom_max()
{
    emit T3R2_p_nom_max(10999);
}
void mainwindow::emit_T3R3_p_nom_max()
{
    emit T3R3_p_nom_max(10999);
}
void mainwindow::emit_T3R4_p_nom_max()
{
    emit T3R4_p_nom_max(10999);
}
void mainwindow::emit_T3R5_p_nom_max()
{
    emit T3R5_p_nom_max(10999);
}
void mainwindow::emit_T3R6_p_nom_max()
{
    emit T3R6_p_nom_max(10999);
}
void mainwindow::emit_T3R7_p_nom_max()
{
    emit T3R7_p_nom_max(10999);
}

void mainwindow::emit_chart_update()
{
    emit chart_update(10999);
}

void mainwindow::mousePressEvent(QMouseEvent *event){
    //qDebug("mouse pressed in blank space....table1.a::: %d",global_instance.aa );
}
