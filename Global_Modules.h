/******************************************************************************
 * Project    : Smart Grid Table GUI
 * Author     : Dingzhu Chen
 *
 * Description: header file for all subsystems
******************************************************************************/
#ifndef GLOBAL_TABLES_H
#define GLOBAL_TABLES_H


#include <QDebug>
#include <QList>
#include <QString>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonValue>
#include <iostream>
#include <QFont>
#include "global_items.h"
#include "Global_Models.h"
#include <QtCharts>

#include <QMainWindow>
#include <QPushButton>
#include <QLineEdit>
#include <QLayout>
#include <QLabel>
#include <QComboBox>
#include <QMessageBox>
#include <QDialog>
#include <Slider_Window.h>

class table1: public QGraphicsView//注意别漏了public,否则无法转换哦
{Q_OBJECT
public:
    table1();//构造函数
    QPlainTextEdit *T1_R5_TextEdit;
    QPlainTextEdit *T1_R1_TextEdit;
    QPushButton *T1_R5_Btn;


    //QString readonly_string1(QString file_path);
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    table1 *Table1;
    QTimer *timer;
    QTimer *timer1;
    T1_R5_Model *T1_R5_MODULE;
    T1_R1_Model *T1_R1_MODULE;
    QGraphicsScene *table_1_scene;
signals:
    /*signals don't have return value,they can have parameters,
    signal functions don't need implementations, they only need to be declare*/
    void T1_R5_clicked(int a);
    void T1_R1_clicked(int a);


private:
    //QTimer *timer;
    //table1 *Table1;
    //protected:
    //friend void mouseMoveEvent(QMouseEvent *event);
    public slots:
    void T1_R5_TextData_change();
    void T1_R1_TextData_change();
    void T1_R5_Button_change();
    void T1_R1_Button_change();
    void showrealtimestatus();
};


class table2: public QGraphicsView//注意别漏了public,否则无法转换哦


//class table2: public QMainWindow
{Q_OBJECT
public:
    table2();//构造函数
    QPlainTextEdit *nameLineEdit;
    //void QWidget::mouseMoveEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    //virtual void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    table2 *Table2;

signals:
    /*signals don't have return value,they can have parameters,
    signal functions don't need implementations, they only need to be declare*/
    void table2_clicked(int a);
private:
    QGraphicsScene *table_2_scene;
    QTimer *timer;
};



class table3: public QGraphicsView//注意别漏了public,否则无法转换哦
{Q_OBJECT
public:
    table3();//table3 constructor
    QPlainTextEdit *T3_R7_TextEdit;
    QPlainTextEdit *T3_R6_TextEdit;
    QPlainTextEdit *T3_R5_TextEdit;
    QPlainTextEdit *T3_R4_TextEdit;
    QPlainTextEdit *T3_R3_TextEdit;
    QPlainTextEdit *T3_R2_TextEdit;
    QPlainTextEdit *T3_R1_TextEdit;
    QPlainTextEdit *T3_R0_TextEdit;

    QPushButton *T3_R7_Btn;
    QPushButton *T3_R6_Btn;
    QPushButton *T3_R5_Btn;
    QPushButton *T3_R4_Btn;
    QPushButton *T3_R3_Btn;
    QPushButton *T3_R2_Btn;
    QPushButton *T3_R1_Btn;
    QPushButton *T3_R0_Btn;

    T3_R0_Model*T3_R0_MODULE;
    T3_R7_Model*T3_R7_MODULE;
    T3_R2_Model*T3_R2_MODULE;
    T3_R1_Model*T3_R1_MODULE;
    T3_R3_Model*T3_R3_MODULE;
    T3_R4_Model*T3_R4_MODULE;
    T3_R5_Model*T3_R5_MODULE;
    T3_R6_Model*T3_R6_MODULE;
    QTimer *timer;
        QTimer *timer1;
    //void QWidget::mouseMoveEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    //virtual void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    table3 *Table3;
    QGraphicsScene *table_3_scene;

signals:
    /*signals don't have return value,they can have parameters,
    signal functions don't need implementations, they only need to be declare*/
    void T3_R7_clicked(int a);
    void T3_R6_clicked(int a);
    void T3_R5_clicked(int a);
    void T3_R4_clicked(int a);
    void T3_R3_clicked(int a);
    void T3_R2_clicked(int a);
    void T3_R1_clicked(int a);
    void T3_R0_clicked(int a);
public slots:
   void T3_R7_TextData_change();
   void T3_R6_TextData_change();
   void T3_R5_TextData_change();
   void T3_R4_TextData_change();
   void T3_R3_TextData_change();
   void T3_R2_TextData_change();
   void T3_R1_TextData_change();
   void T3_R0_TextData_change();

   void T3_R7_Button_change();
   void T3_R6_Button_change();
   void T3_R5_Button_change();
   void T3_R4_Button_change();
   void T3_R3_Button_change();
   void T3_R2_Button_change();
   void T3_R1_Button_change();
   void T3_R0_Button_change();
private:
    //T1_R5_Model *T1_R5_Model;
    //QTimer *timer;
    //gun *Gun;
//protected:
    //friend void mouseMoveEvent(QMouseEvent *event);
};

class table4: public QGraphicsView//注意别漏了public,否则无法转换哦
{Q_OBJECT
public:
    table4();//table4 constructor
    //void QWidget::mouseMoveEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    //virtual void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);

    table4 *Table4;
    QGraphicsScene *table_4_scene;
    //battery_storage *Battery_storage;
    QTimer *timer;
    QTimer *timer1;
    QPlainTextEdit *T4_R7_TextEdit;
    QPlainTextEdit *T4_R6_TextEdit;
    QPlainTextEdit *T4_R5_TextEdit;
    QPlainTextEdit *T4_R4_TextEdit;
    QPlainTextEdit *T4_R3_TextEdit;
    QPlainTextEdit *T4_R2_TextEdit;
    QPlainTextEdit *T4_R1_TextEdit;
    QPlainTextEdit *T4_R0_TextEdit;

    QPushButton *T4_R7_Btn;
    QPushButton *T4_R6_Btn;
    QPushButton *T4_R5_Btn;
    QPushButton *T4_R4_Btn;
    QPushButton *T4_R3_Btn;
    QPushButton *T4_R2_Btn;
    QPushButton *T4_R1_Btn;
    QPushButton *T4_R0_Btn;

    T4_R0_Model*T4_R0_MODULE;
    T4_R7_Model*T4_R7_MODULE;
    T4_R2_Model*T4_R2_MODULE;
    T4_R1_Model*T4_R1_MODULE;
    T4_R3_Model*T4_R3_MODULE;
    T4_R4_Model*T4_R4_MODULE;
    T4_R5_Model*T4_R5_MODULE;
    T4_R6_Model*T4_R6_MODULE;
signals:
    void T4_R1_clicked(int a);
    void T4_R2_clicked(int a);
    void T4_R3_clicked(int a);
    void T4_R4_clicked(int a);
    void T4_R5_clicked(int a);
    void T4_R6_clicked(int a);
    void T4_R7_clicked(int a);
public slots:
    void T4_R7_TextData_change();
    void T4_R6_TextData_change();
    void T4_R5_TextData_change();
    void T4_R4_TextData_change();
    void T4_R3_TextData_change();
    void T4_R2_TextData_change();
    void T4_R1_TextData_change();
    //void T4_R0_TextData_change();
    void T4_R1_Button_change();
    void T4_R2_Button_change();
    void T4_R3_Button_change();
    void T4_R4_Button_change();
    void T4_R5_Button_change();
    void T4_R6_Button_change();
    void T4_R7_Button_change();
    //gun *Gun;
//protected:
    //friend void mouseMoveEvent(QMouseEvent *event);
    //table4 *Table4;
};

class table5: public QGraphicsView//注意别漏了public,否则无法转换哦
{Q_OBJECT
public:
    table5();//构造函数
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    table5 *Table5;
    QGraphicsScene *table_5_scene;
    QTimer *timer;
        QTimer *timer1;

    QPlainTextEdit *T5_R7_TextEdit;
    QPlainTextEdit *T5_R6_TextEdit;
    QPlainTextEdit *T5_R5_TextEdit;
    QPlainTextEdit *T5_R4_TextEdit;
    QPlainTextEdit *T5_R3_TextEdit;
    QPlainTextEdit *T5_R2_TextEdit;
    QPlainTextEdit *T5_R1_TextEdit;
    QPlainTextEdit *T5_R0_TextEdit;

    QPushButton *T5_R7_Btn;
    QPushButton *T5_R6_Btn;
    QPushButton *T5_R5_Btn;
    QPushButton *T5_R4_Btn;
    QPushButton *T5_R3_Btn;
    QPushButton *T5_R2_Btn;
    QPushButton *T5_R1_Btn;
    QPushButton *T5_R0_Btn;

    T5_R0_Model*T5_R0_MODULE;
    T5_R7_Model*T5_R7_MODULE;
    T5_R2_Model*T5_R2_MODULE;
    T5_R1_Model*T5_R1_MODULE;
    T5_R3_Model*T5_R3_MODULE;
    T5_R4_Model*T5_R4_MODULE;
    T5_R5_Model*T5_R5_MODULE;
    T5_R6_Model*T5_R6_MODULE;
signals:
    void T5_R1_clicked(int a);
    void T5_R2_clicked(int a);
    void T5_R3_clicked(int a);
    void T5_R4_clicked(int a);
    void T5_R5_clicked(int a);
    void T5_R6_clicked(int a);
    void T5_R7_clicked(int a);
public slots:
    void T5_R7_TextData_change();
    void T5_R6_TextData_change();
    void T5_R5_TextData_change();
    void T5_R4_TextData_change();
    void T5_R3_TextData_change();
    void T5_R2_TextData_change();
    void T5_R1_TextData_change();
    //void T5_R0_TextData_change();
    void T5_R1_Button_change();
    void T5_R2_Button_change();
    void T5_R3_Button_change();
    void T5_R4_Button_change();
    void T5_R5_Button_change();
    void T5_R6_Button_change();
    void T5_R7_Button_change();



private:


//protected:
//    friend void mouseMoveEvent(QMouseEvent *event);
//  table6 *Table6;


};

class table6: public QGraphicsView//注意别漏了public,否则无法转换哦
{Q_OBJECT
public:
    table6();//table6 constructors
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    table6 *Table6;
    QGraphicsScene *table_6_scene;
    QTimer *timer;
        QTimer *timer1;

    QPlainTextEdit *T6_R7_TextEdit;
    QPlainTextEdit *T6_R6_TextEdit;
    QPlainTextEdit *T6_R5_TextEdit;
    QPlainTextEdit *T6_R4_TextEdit;
    QPlainTextEdit *T6_R3_TextEdit;
    QPlainTextEdit *T6_R2_TextEdit;
    QPlainTextEdit *T6_R1_TextEdit;
    QPlainTextEdit *T6_R0_TextEdit;

    QPushButton *T6_R7_Btn;
    QPushButton *T6_R6_Btn;
    QPushButton *T6_R5_Btn;
    QPushButton *T6_R4_Btn;
    QPushButton *T6_R3_Btn;
    QPushButton *T6_R2_Btn;
    QPushButton *T6_R1_Btn;
    QPushButton *T6_R0_Btn;

    T6_R0_Model*T6_R0_MODULE;
    T6_R7_Model*T6_R7_MODULE;
    T6_R2_Model*T6_R2_MODULE;
    T6_R1_Model*T6_R1_MODULE;
    T6_R3_Model*T6_R3_MODULE;
    T6_R4_Model*T6_R4_MODULE;
    T6_R5_Model*T6_R5_MODULE;
    T6_R6_Model*T6_R6_MODULE;


signals:
    /*signals don't have return value,they can have parameters,
    signal functions don't need implementations, they only need to be declare*/
    void T6_R1_clicked(int a);
    void T6_R2_clicked(int a);
    void T6_R3_clicked(int a);
    void T6_R4_clicked(int a);
    void T6_R5_clicked(int a);
    void T6_R6_clicked(int a);
    void T6_R7_clicked(int a);



public slots:

    void T6_R7_TextData_change();
    void T6_R6_TextData_change();
    void T6_R5_TextData_change();
    void T6_R4_TextData_change();
    void T6_R3_TextData_change();
    void T6_R2_TextData_change();
    void T6_R1_TextData_change();
    //void T6_R0_TextData_change();
    void T6_R1_Button_change();
    void T6_R2_Button_change();
    void T6_R3_Button_change();
    void T6_R4_Button_change();
    void T6_R5_Button_change();
    void T6_R6_Button_change();
    void T6_R7_Button_change();
private:
//protected:
    //friend void mouseMoveEvent(QMouseEvent *event);
    //table6 *Table6;
};

class dynamic_chart: public QMainWindow
{Q_OBJECT
public:
    QLegendMarker *marker;
    QGraphicsScene *chart_scene;
    dynamic_chart *Dynamic_Chart;
    dynamic_chart();
public slots:
    void connectMarkers();
    void chart_data_change();
    void handleMarkerClicked();

signals:



};

class ComboBox_Window : public QMainWindow
{
    Q_OBJECT

public:
    QString command;
        ComboBox_Window * commands_box;
    ComboBox_Window(QWidget *parent = NULL):QMainWindow(parent)
    {
        QLabel *lbl_caption = new QLabel(QWidget::tr("COMMAND:"));
        QFont font ( "Microsoft YaHei", 20, 75);
        lbl_caption->setFont(font);
        lbl_caption->move(10,0);
        lbl_caption->setFixedWidth(210);
        lbl_caption->setFixedHeight(80);
        lbl_caption->setParent(this);
        lbl_caption->show();
        cbo_sex_1 = new QComboBox();
        cbo_sex_1->resize(475,60);
        QFont Appfont;
        Appfont.setPointSize(24);
        //QString redPart = QString("<span style='color:#ff0000;'>%1</span>").arg(">>>");

//        mqtt public              -> Switch to public MQTT broker
//        mqtt local               -> Switch to local MQTT broker

        cbo_sex_1->addItem("help--> Print all commands");
        cbo_sex_1->addItem(QWidget::tr("shutdown--> Close the program"));
        cbo_sex_1->addItem(QWidget::tr("table list--> Print the names of all table sections"));
        cbo_sex_1->addItem(QWidget::tr("table reboot all--> Reboot all table sections"));
        cbo_sex_1->addItem(QWidget::tr("scenario reload--> Find and reload all scenarios in Static/"));
        cbo_sex_1->addItem(QWidget::tr("scenario list--> Print all available scenarios"));
        cbo_sex_1->addItem(QWidget::tr("scenario current--> Print the current active scenario"));
        cbo_sex_1->addItem(QWidget::tr("mode set lopf--> Set calculation type to LOPF"));
        cbo_sex_1->addItem(QWidget::tr("mode set lpf--> Set calculation type to LPF"));
        cbo_sex_1->addItem(QWidget::tr("mode set pf--> Set calculation type to PF"));
        cbo_sex_1->addItem(QWidget::tr("modules list-->Print number of modules placed on each table"));
        cbo_sex_1->addItem(QWidget::tr("calculate--> PyPSA simulation refresh"));
        cbo_sex_1->move(225,10);
        cbo_sex_1->setParent(this);
        cbo_sex_1->show();
        QPushButton *btn = new QPushButton(QWidget::tr("Select"));
        btn =new QPushButton;
        btn->resize(100, 50);
        btn->move(710, 10);
        btn->setText("Publish");
        btn->setStyleSheet("background-color: rgb(255, 255, 0);");
        btn->setParent(this);
        btn->show();
        QListView *view = new QListView(cbo_sex_1);
        view->setStyleSheet("QListView::item{height: 30px}");
        cbo_sex_1->setView(view);
//        table reboot [SECTION]   -> Reboot given table section
        cbo_sex_2 = new QComboBox();
        cbo_sex_2->resize(475,60);
        cbo_sex_2->addItem("table reboot Table1");
        cbo_sex_2->addItem(QWidget::tr("table reboot Table2"));
        cbo_sex_2->addItem(QWidget::tr("table reboot Table3"));
        cbo_sex_2->addItem(QWidget::tr("table reboot Table4"));
        cbo_sex_2->addItem(QWidget::tr("table reboot Table5"));
        cbo_sex_2->addItem(QWidget::tr("table reboot Table6"));
        cbo_sex_2->move(225,80);
        cbo_sex_2->setParent(this);
        cbo_sex_2->show();
        QListView *view_1 = new QListView(cbo_sex_2);
        view_1->setStyleSheet("QListView::item{height: 30px}");
        cbo_sex_2->setView(view_1);
        QPushButton *btn_1 = new QPushButton;
        btn_1 =new QPushButton;
        btn_1->resize(100, 50);
        btn_1->move(710, 80);
        btn_1->setText("Publish");
        btn_1->setStyleSheet("background-color: rgb(255, 255, 0);");
        btn_1->setParent(this);
        btn_1->show();
//        table shutdown [SECTION] -> Shutdown the given table section
        cbo_sex_3 = new QComboBox();
        cbo_sex_3->resize(475,60);
        cbo_sex_3->addItem("table shutdown Table1");
        cbo_sex_3->addItem(QWidget::tr("table shutdown Table2"));
        cbo_sex_3->addItem(QWidget::tr("table shutdown Table3"));
        cbo_sex_3->addItem(QWidget::tr("table shutdown Table4"));
        cbo_sex_3->addItem(QWidget::tr("table shutdown Table5"));
        cbo_sex_3->addItem(QWidget::tr("table shutdown Table6"));
        cbo_sex_3->move(225,150);
        cbo_sex_3->setParent(this);
        cbo_sex_3->show();
        QListView *view_2 = new QListView(cbo_sex_3);
        view_2->setStyleSheet("QListView::item{height: 30px}");
        cbo_sex_3->setView(view_2);
        QPushButton *btn_2 = new QPushButton;
        btn_2 =new QPushButton;
        btn_2->resize(100, 50);
        btn_2->move(710, 150);
        btn_2->setText("Publish");
        btn_2->setStyleSheet("background-color: rgb(255, 255, 0);");
        btn_2->setParent(this);
        btn_2->show();

//        table poweron [SECTION]  -> Boot up the given table section
        cbo_sex_4 = new QComboBox();
        cbo_sex_4->resize(475,60);
        cbo_sex_4->addItem("table poweron Table1");
        cbo_sex_4->addItem(QWidget::tr("table poweron Table2"));
        cbo_sex_4->addItem(QWidget::tr("table poweron Table3"));
        cbo_sex_4->addItem(QWidget::tr("table poweron Table4"));
        cbo_sex_4->addItem(QWidget::tr("table poweron Table5"));
        cbo_sex_4->addItem(QWidget::tr("table poweron Table6"));
        cbo_sex_4->move(225,220);
        cbo_sex_4->setParent(this);
        cbo_sex_4->show();
        QListView *view_3 = new QListView(cbo_sex_4);
        view_3->setStyleSheet("QListView::item{height: 30px}");
        cbo_sex_4->setView(view_3);
        QPushButton *btn_3 = new QPushButton;
        btn_3 =new QPushButton;
        btn_3->resize(100, 50);
        btn_3->move(710, 220);
        btn_3->setText("Publish");
        btn_3->setStyleSheet("background-color: rgb(255, 255, 0);");
        btn_3->setParent(this);
        btn_3->show();

//        scenario set -s [NAME]   -> Switch to given static scenario
        cbo_sex_5 = new QComboBox();
        cbo_sex_5->resize(475,60);
        cbo_sex_5->addItem("scenario set -s Demo");
        cbo_sex_5->addItem(QWidget::tr("scenario set -s Default"));
        cbo_sex_5->addItem(QWidget::tr("scenario set -s Winter"));
        cbo_sex_5->addItem(QWidget::tr("scenario set -s Summer"));
        cbo_sex_5->addItem(QWidget::tr("scenario set -s EV"));
        cbo_sex_5->addItem(QWidget::tr("scenario set -s Night"));
        cbo_sex_5->addItem(QWidget::tr("scenario set -s Summerday"));
        cbo_sex_5->move(225,290);
        cbo_sex_5->setParent(this);
        cbo_sex_5->show();
        QListView *view_4 = new QListView(cbo_sex_5);
        view_4->setStyleSheet("QListView::item{height: 30px}");
        cbo_sex_5->setView(view_4);
        QPushButton *btn_4 = new QPushButton;
        btn_4 =new QPushButton;
        btn_4->resize(100, 50);
        btn_4->move(710, 290);
        btn_4->setText("Publish");
        btn_4->setStyleSheet("background-color: rgb(255, 255, 0);");
        btn_4->setParent(this);
        btn_4->show();
//        scenario set -d [NAME]   -> Switch to given dynamic scenario

        cbo_sex_6 = new QComboBox();
        cbo_sex_6->resize(475,60);
        cbo_sex_6->addItem("scenario set -d Demo");
        cbo_sex_6->addItem(QWidget::tr("scenario set -d Default"));
        cbo_sex_6->addItem(QWidget::tr("scenario set -d Winter"));
        cbo_sex_6->addItem(QWidget::tr("scenario set -d Summer"));
        cbo_sex_6->addItem(QWidget::tr("scenario set -d EV"));
        cbo_sex_6->addItem(QWidget::tr("scenario set -d Night"));
        cbo_sex_6->addItem(QWidget::tr("scenario set -d Summerday"));
        cbo_sex_6->move(225,360);
        cbo_sex_6->setParent(this);
        cbo_sex_6->show();
        QListView *view_5 = new QListView(cbo_sex_6);
        view_5->setStyleSheet("QListView::item{height: 30px}");
        cbo_sex_6->setView(view_5);
        QPushButton *btn_5 = new QPushButton;
        btn_5 =new QPushButton;
        btn_5->resize(100, 50);
        btn_5->move(710, 360);
        btn_5->setText("Publish");
        btn_5->setStyleSheet("background-color: rgb(255, 255, 0);");
        btn_5->setParent(this);
        btn_5->show();

        connect(cbo_sex_1, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(on_sel_sex(const QString &)));
        connect(btn, SIGNAL(clicked()), this, SLOT(on_click_sel()));
        connect(btn_1, SIGNAL(clicked()), this, SLOT(table_reboot()));
        connect(btn_2, SIGNAL(clicked()), this, SLOT(table_shutdown()));
        connect(btn_3, SIGNAL(clicked()), this, SLOT(table_poweron()));
        connect(btn_4, SIGNAL(clicked()), this, SLOT(static_synario()));
        connect(btn_5, SIGNAL(clicked()), this, SLOT(dynamic_synario()));
    }


signals:
    /*signals don't have return value,they can have parameters,
    signal functions don't need implementations, they only need to be declare*/
    void command_pub_sig(int a);
public slots:
    void emit_command_sig(){
        emit command_pub_sig(333);
    }

private:
QComboBox *cbo_sex_1;
QComboBox *cbo_sex_2;
QComboBox *cbo_sex_3;
QComboBox *cbo_sex_4;
QComboBox *cbo_sex_5;
QComboBox *cbo_sex_6;
private slots:
    void on_sel_sex(const QString &text)
    {
//        QString str;
//        str = "You select " + text;
//        QMessageBox::information(this, tr("Info"), str);
    }

    void on_click_sel()
    {

        QString str;
        command =cbo_sex_1->currentText().split("--")[0];
        emit_command_sig();
        //str = "You select " + cbo_sex_1->currentText();
        std::cout<<command.split("--")[0].toStdString()<<std::endl;
        if(command.split("--")[0]=="table list"){std::cout<<"lollll"<<std::endl;
        }
        //QMessageBox::information(this, tr("Info"), str);

    }
    void table_reboot()
    {

        command =cbo_sex_2->currentText();
        emit_command_sig();
        QString str;
        //str = "You select " + cbo_sex_2->currentText();
        //QMessageBox::information(this, tr("Info"), str);

    }
    void table_shutdown()
    {        command =cbo_sex_3->currentText();
             emit_command_sig();
        QString str;
        //str = "You select " + cbo_sex_3->currentText();
        //QMessageBox::information(this, tr("Info"), str);
    }
    void table_poweron()
    {        command =cbo_sex_4->currentText();
             emit_command_sig();
        QString str;
        //str = "You select " + cbo_sex_4->currentText();
        //QMessageBox::information(this, tr("Info"), str);
    }
    void static_synario(){
        command =cbo_sex_5->currentText();
        emit_command_sig();
        QString str;
        //str = "You select " + cbo_sex_5->currentText();
        //QMessageBox::information(this, tr("Info"), str);
    }
    void dynamic_synario(){
        command =cbo_sex_6->currentText();
        emit_command_sig();
        QString str;
        //str = "You select " + cbo_sex_6->currentText();
        //QMessageBox::information(this, tr("Info"), str);
    }
};

class global_tables
{
public:

    global_tables();
    Slider_Window *slider;
    ComboBox_Window * commands_box;
    dynamic_chart * chart;
    table1 *TABLE1;
    table2 *TABLE2;
    table3 *TABLE3;
    table4 *TABLE4;
    table5 *TABLE5;
    table6 *TABLE6;
    static global_tables& Instance()
    {
        static global_tables sta_table_obj;
        return sta_table_obj;
    }
};

namespace {
    global_tables& global_table_instance = global_tables::Instance();
}

#endif // GLOBAL_TABLES_H
